<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Drive extends CI_Controller {

    private $data = array(),
            $model = array("pasta_model","Evento_model","Arquivo_model"),
            $titulo = "Drive", 
            $view = "Drive_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();
        
        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;

    }
    
    public function index() { 
        $this->drive();
    }

    public function drive() { 

        $cod_cliente = $this->uri->segment(2);
        $cod_pasta = $this->uri->segment(3);

        $this->data['dados_cod_pasta']=$this->pasta_model->listar_select();
        $this->data['dados_evento']=$this->Evento_model->listar_select();

        if ($this->session->userdata('tipo')=="C") {
            $cod_cliente = $this->session->userdata('cliente');
            // redirect(site_url("drive/".$this->session->userdata('tipo')."/{$cod_pasta}"));
        }

        if (!$cod_cliente or !$cod_pasta){
            $this->data['operacao']="listar_cliente";
            $this->data['dados']=$this->Cliente_model->listar_drive();
        }
        if ($cod_cliente and !$cod_pasta){
            $this->data['cliente']=$this->Cliente_model->editar($cod_cliente);
            $this->data['operacao']="listar_pasta";
            $this->data['dados']=$this->pasta_model->listar_pastacliente($cod_cliente);
        }
        if ($cod_cliente and $cod_pasta){
            $this->data['cliente']=$this->Cliente_model->editar($cod_cliente);
            $this->data['cod_pasta']=$this->pasta_model->editar($cod_pasta);
            $this->data['operacao']="listar_arquivo";
            $this->data['dados_arquivos']=$this->Arquivo_model->listar(array("cod_cliente" => $cod_cliente,"cod_pasta" => $cod_pasta));
        }

        //echo "cliente: $cod_cliente; cod_pasta: $cod_pasta; operacao:".$this->data['operacao'];
        
        $this->load->view('Pagina',$this->data);

    }

    public function buscar() { 
            
    }


}