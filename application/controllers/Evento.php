<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Evento extends CI_Controller {

    private $data = array(),
            $model = array("pasta_model","Evento_model"),
            $titulo = "Evento", 
            $view = "Evento_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function listar() {
        
        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."evento/listar";
        $config['total_rows'] = count($this->Evento_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->Evento_model->listar($inicio);
        $this->data['operacao']="listar";
        $this->load->view('Pagina',$this->data);
    }

    public function novo() {
        $this->data['dados_cod_pasta']=$this->pasta_model->listar_select();
        $this->data['operacao']="novo";
        $this->load->view('Pagina',$this->data);
    }

    public function validar() {
        echo json_encode($this->Evento_model->validar()); 
    }

    public function inserir() {
        $this->Evento_model->inserir();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('evento/listar'));
    }

    public function salvar($cod_evento) {
        $this->Evento_model->salvar($cod_evento);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('evento/listar'));
    }

    public function editar($cod_evento) {
        $this->data['dados']=$this->Evento_model->editar($cod_evento);
        $this->data['dados_cod_pasta']=$this->pasta_model->listar_select();
        $this->data['operacao']="editar";
        $this->load->view('Pagina',$this->data);
    }

    public function excluir($cod_evento) {
        $this->Evento_model->excluir($cod_evento);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        
        redirect(site_url('evento/listar'));
    }

    public function relatorio_evento($formato="pdf") {

        $this->data['dados']=$this->Evento_model->listar("","S");

        $this->functions->exportar($this->data,$formato,"Relatorio_evento");

    }
}
