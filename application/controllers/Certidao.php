<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Certidao extends CI_Controller {

    private $data = array(),
            $model = array("Link_model"),
            $titulo = "Certidão", 
            $view = "Certidao_view";
            
    function __construct() {
        parent::__construct();

        //Verifica se esta logado
         $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function listar() {
        $this->data['dados']=$this->Cliente_model->editar($this->session->userdata('cliente'));
        $this->data['documento']=preg_replace('/[^0-9]/', '',$this->Cliente_model->editar($this->session->userdata('cliente'))["documento"]);
        $this->data['dados_links']=$this->Link_model->listar_localizacao();
        $this->load->view('Pagina',$this->data);
    }
}
