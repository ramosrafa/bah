<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Notificacao extends CI_Controller {

    private $data = array(),
            $model = array("Notificacao_model","Usuario_model","Template_model"),
            $titulo = "Notificação", 
            $view = "Notificacao_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
    }

    public function listar() {
        //echo "istar";exit;
        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."notificacao/listar";
        $config['total_rows'] = count($this->Notificacao_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->Notificacao_model->listar();
        $this->data['operacao']="listar";
        $this->load->view('Pagina',$this->data); 
    }

    public function cliente() {
        //echo "cliente";exit;
        $this->data['dados']=$this->Notificacao_model->cliente(); 
        $this->data['operacao']="cliente";
        $this->load->view('Pagina',$this->data);
    }

    public function novo() {
        $this->data['dados_cliente']=$this->Cliente_model->listar_select();
        $this->data['operacao']="novo";
        $this->load->view('Pagina',$this->data);
    }

    public function inserir() {
        $this->Notificacao_model->inserir();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('notificacao/listar'));
    }

    public function salvar($cod_notificacao) {
        $this->Notificacao_model->salvar($cod_notificacao);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('notificacao/listar'));
    }

    public function editar($cod_notificacao) {
        $this->data['dados']=$this->Notificacao_model->editar($cod_notificacao);
        $this->data['dados_usuario']=$this->Notificacao_model->listar_quemleu($cod_notificacao);
        $this->data['dados_cliente']=$this->Cliente_model->listar_select();
        $this->data['operacao']="editar";
        $this->load->view('Pagina',$this->data);
    }

    public function excluir($cod_notificacao) {
        $this->Notificacao_model->excluir($cod_notificacao);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        
        redirect(site_url('notificacao/listar'));
    }

}
