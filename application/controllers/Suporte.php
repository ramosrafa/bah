<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Suporte extends CI_Controller {

    private $data = array(),
            $model = array("pasta_model","Notificacao_model"),
            $titulo = "Suporte", 
            $view = "Suporte_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function index() {
        $this->dados();
    }

    public function dados() {
        $this->data['dados_cod_pasta']=$this->pasta_model->listar_select();
        $this->load->view('Pagina',$this->data);
    }

    public function enviar() {
		$nome = $this->input->get_post('nome');
		$email = $this->input->get_post('email');
		$cod_pasta = $this->input->get_post('cod_pasta');
		$mensagem = $this->input->get_post('mensagem');
        
		$mail_texto =
				"
				<br>Nome: $nome
				<br>Email: $email
				<br>Departamento: $cod_pasta
				<br>Mensagem: $mensagem
				";

        $myemail = new Myemail;
        $myemail->cod_cliente($this->session->userdata('cliente'));
        $myemail->from(TITULO, EMAIL_SUPORTE);
        $myemail->to("BAH",EMAIL_SUPORTE);
        $myemail->subject("BAH Suporte");
        $myemail->body($mail_texto);
        $myemail->enviar();

        $this->Notificacao_model->json_inserir($this->session->userdata('cod_cliente'));
        
        echo "1";
    }
    
}
