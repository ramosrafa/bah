<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Painel extends CI_Controller {

    private $data = array(),
            $model = array("Notificacao_model","Servico_model","Alvara_model","Coleta_model","Usuario_model","Evento_model","Entrega_model"),
            $titulo = "Painel", 
            $view = "Painel_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
    }
    
    public function resumo($cliente="") { 
        
        $this->data['dados_notificacoes']=$this->Notificacao_model->listar_ultimas($this->session->userdata('cliente'));
        
        if( $this->session->userdata('tipo') =="C" ){

            $this->data['view']="PainelC_view";
            
            $this->data['dados_situacaocertificado']=$this->Cliente_model->situacao_certificado($this->session->userdata('cliente'));
            $this->data['dados_servico']=$this->Servico_model->listar_servicovencido($this->session->userdata('cliente'));
        }
        if( $this->session->userdata('tipo') =="T" ){

            $this->data['view']="PainelT_view";

            $this->data['dados_procuracoes']=$this->Cliente_model->listar_procuracao();
            $this->data['dados_certificados']=$this->Cliente_model->listar_certificadovencido();
            $this->data['dados_alvaras']=$this->Alvara_model->listar_alvarasvencidos();
            $this->data['dados_clientessemusuario']=$this->Cliente_model->listar_semusuario();
            $this->data['dados_servicos']=$this->Servico_model->listar_servicovencido();
        }
        $this->load->view('Pagina',$this->data);
    }

    public function painel($calendario_dia=0,$calendario_mes=0,$calendario_ano=0) { 

        $dia = ($calendario_dia>0)?$calendario_dia:date('d');
        $mes = ($calendario_mes>0)?$calendario_mes:date('m');
        $ano = ($calendario_ano>0)?$calendario_ano:date('Y');

        $dados=$this->Cliente_model->evento("{$dia}/{$mes}/{$ano}",$this->session->userdata('cliente'));

        $retorno = array(
            "calendario" => $this->painel_calendario($dados,$dia,$mes,$ano),
            "coleta" => $this->painel_coleta($dados,$dia,$mes,$ano),
            "entrega" => $this->painel_entrega($dados,$dia,$mes,$ano)
        );

        echo json_encode($retorno);
    }

    public function painel_calendario($dados,$dia=0,$mes=0,$ano=0) { 
        
        //Trata mês para navegação
        $mes_anterior="";
        $ano_anterior="";
        $mes_posterior="";
        $ano_posterior="";
        if ($mes==1) {
            $mes_anterior = 12;
            $ano_anterior = $ano-1;
        } else {
            $mes_anterior = $mes-1;
            $ano_anterior = $ano;
        }
        if ($mes==12) {
            $mes_posterior = 1;
            $ano_posterior = $ano +1;
        } else {
            $mes_posterior = $mes+1;
            $ano_posterior = $ano;
        }
        $mes_anterior = ($mes_anterior<10)?"0{$mes_anterior}":$mes_anterior;
        $mes_posterior = ($mes_posterior<10)?"0{$mes_posterior}":$mes_posterior;

        $html_dias="";
        for ($i=1;$i<=32;$i++){
            $diadasemana = date("w",mktime(0,0,0,$mes,$i,$ano));

            $mes_ = $mes;
            $i_ = ($i<10)?"0{$i}":$i;
            $data="{$ano}-{$mes_}-{$i_}";
            
            if($i == 1) {
                for ($b=0;$b<$diadasemana;$b++){
                    $html_dias.="<div class=\"\">&nbsp;</div>";
                }
            }
            
            //Dia Atual
            $class_dia_atual = "";
            if (($ano == date('Y'))and($mes == date('m'))and($i == date('d'))) $class_dia_atual = "span-calendario-diaatual";

            //Verifica se existe atividade no dia
            $class_dia_obrigacao = "";
            $title="";
            if (!empty($dados["evento_cliente"])){
                foreach($dados["evento_cliente"][$this->session->userdata('cliente')] as $value){
                    if ($data == $value["data_vencimento"]){
                        $title.=$value["nome"];
                        $class_dia_obrigacao = "span-calendario-diaobrigacao div-calendario-seleciona";
                    }
                }
            }
            
            foreach($dados["coleta"] as $value){
                if ($data == $value["data_vencimento"]){
                    $title.=$value["nome"];
                    $class_dia_obrigacao = "span-calendario-diaobrigacao div-calendario-seleciona";
                }
            }
            
            foreach($dados["feriado"] as $value){
                if ($data == $value["data"]){
                    $title.=$value["nome"];
                    $class_dia_obrigacao = "span-calendario-diaferiado div-calendario-seleciona";
                }
            }
            
            if (checkdate($mes,$i,$ano)) {
                $html_dias.="<div class=\"\"><span class=\"{$class_dia_atual} {$class_dia_obrigacao}\" title=\"{$title}\" data-dia=\"{$i}\" data-mes=\"{$mes}\" data-ano=\"{$ano}\">{$i}</span></div>";
            }
        }
        
        $html = "
                <div class=\"div-calendario-nav\">
                    <div class=\"div-calendario-nav-voltar div-calendario-seleciona\" data-dia=\"0\" data-mes=\"{$mes_anterior}\" data-ano=\"{$ano_anterior}\">
                        <span class=\"glyphicon glyphicon-menu-left\" aria-hidden=\"true\"></span>
                    </div>
                    <div class=\"div-calendario-nav-data\">
                        <strong>".MES[$mes]."</strong> {$ano}
                    </div>
                    <div class=\"div-calendario-nav-avancar div-calendario-seleciona\" data-dia=\"0\" data-mes=\"{$mes_posterior}\" data-ano=\"{$ano_posterior}\">
                        <span class=\"glyphicon glyphicon-menu-right\" aria-hidden=\"true\"></span>
                    </div>
                </div>
                <div class=\"div-calendario-diasemana\">
                    <div>D</div>
                    <div>S</div>
                    <div>T</div>
                    <div>Q</div>
                    <div>Q</div>
                    <div>S</div>
                    <div>S</div>
                </div>
                <div class=\"div-calendario-dia\">
                {$html_dias}
                </div>
            ";


        return $html;
    }

    public function painel_coleta($dados,$dia,$mes,$ano) { 

        $html = "";

        foreach($dados["coleta"] as $value){
            if ($value["tipo"]=="C") {
                if ($mes==date("m",strtotime($value["data_vencimento"])) and $ano==date("Y",strtotime($value["data_vencimento"])) ){
                    $html .= "
                            <tr class=\"tr-linha\" data-objeto=\"".LOCAL."coleta\" data-acao=\"ver\" data-cod=\"".$value["cod"]."\" data-aux=\"".$value["data_vencimento"]."\" title=\"Coleta\" >
                                <td><span class=\"glyphicon glyphicon-certificate\" aria-hidden=\"true\"></span></td>
                                <td><span class=\"span-data\">".$value["data_vencimento_"]." ".$value["pasta"]."</span><br>".$value["texto"]. "</td>
                            </tr>
                            ";
                }
            }
        }
        
        if (!empty($dados["evento_cliente"])){
            foreach($dados["evento_cliente"][$this->session->userdata('cliente')] as $value){
                if ($value["tipo"]=="C") {
                    if ($mes==date("m",strtotime($value["data_vencimento"])) and $ano==date("Y",strtotime($value["data_vencimento"])) ){
                        $html .= "
                                <tr class=\"tr-linha\" data-objeto=\"".LOCAL."coleta\" data-acao=\"evento\" data-cod=\"".$value["cod"]."\" data-aux=\"".$value["data_vencimento"]."\" title=\"Evento\" >
                                    <td><span class=\"glyphicon glyphicon-certificate\" aria-hidden=\"true\"></span></td>
                                    <td><span class=\"span-data\">".$value["data_vencimento_"]." ".$value["pasta"]."</span><br>"." ".$value["texto"]."</td>
                                </tr>
                                ";
                    }
                }
            }
        }
        
        $html .= "
                <tr>
                    <td>&nbsp;</td>
                    <td align=\"right\"><a href=\"".LOCAL."coleta\listar\">Ver todos</a></td>
                </tr>
        ";
        return $html;
    }

    public function painel_entrega($dados,$dia,$mes,$ano) { 
        $html = "";
        if (!empty($dados["evento_cliente"])){
            foreach($dados["evento_cliente"][$this->session->userdata('cliente')] as $value){
                if ($value["tipo"]=="T") {
                    if ($mes==date("m",strtotime($value["data_vencimento"])) and $ano==date("Y",strtotime($value["data_vencimento"])) ){
                        $arquivo ="";
                        if ($value["arquivo"]<>""){
                            $arquivo ="<a class=\"a-acao-d\" data-objeto=\"".LOCAL."arquivo"."\" data-acao=\"a\" data-a=\"".$value["arquivo"]."\">&nbsp;&nbsp;<span class=\"glyphicon glyphicon-cloud-download\" aria-hidden=\"true\"></span>&nbsp;&nbsp;</a>";
                        }
                        $html .= "
                                <tr>
                                    <td><span class=\"glyphicon glyphicon-certificate\" aria-hidden=\"true\"></span></td>
                                    <td>
                                        <span class=\"span-data\">".$value["data_vencimento_"]." ".$value["pasta"]."</span><br>".$value["nome"]."
                                        {$arquivo}
                                    </td>
                                </tr>
                                ";
                    }
                }
            }
        }   
        
        $html .= "
                <tr>
                    <td>&nbsp;</td>
                    <td align=\"right\"><a href=\"".LOCAL."drive\">Ver todos</a></td>
                </tr>
        ";
        return $html;
    }

    //Marcar notificação como lida
    public function lida($cod_notificacao) {

        return $this->Notificacao_model->lida($cod_notificacao);
        
    }

    public function grafico_quantidademes() {
        echo $this->Entrega_model->grafico_quantidademes();
    }

}
