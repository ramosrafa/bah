<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Manual extends CI_Controller {

    private $data = array(),
            $model = "",
            $titulo = "Manual", 
            $view = "Manual_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);
    }

    public function topicos() {
        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        $this->load->view('Pagina',$this->data);
    }

    
}
