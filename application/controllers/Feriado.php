<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Feriado extends CI_Controller {

    private $data = array(),
            $model = array("Feriado_model", "Cliente_model"),
            $titulo = "Feriado", 
            $view = "Feriado_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
         $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function listar() {       
        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."feriado/listar";
        $config['total_rows'] = count($this->Feriado_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->Feriado_model->listar($inicio); 
        $this->data['operacao']="listar";

        $this->load->view('Pagina',$this->data);
    }

    public function novo() {
        $this->data['operacao']="novo";
        $this->load->view('Pagina',$this->data); 
    }

    public function inserir() {
        $this->Feriado_model->inserir();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        redirect(site_url('feriado/listar'));
    }

    public function salvar($cod_feriado) {
        $this->Feriado_model->salvar($cod_feriado);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('feriado/listar'));
    }

    public function editar($cod_feriado) {
        $this->data['dados']=$this->Feriado_model->editar($cod_feriado);
        $this->data['operacao']="editar";
        $this->load->view('Pagina',$this->data);
    }

    public function cliente() {        
        $uf = $this->session->userdata('cliente_uf');
        $cidade = $this->session->userdata('cliente_cidade');

        $this->data['dados']=$this->Feriado_model->listar_cliente($uf,$cidade); 

        $this->data['operacao']="cliente";
        $this->load->view('Pagina',$this->data);
    }


    public function excluir($cod_feriado) {
        $this->Feriado_model->excluir($cod_feriado);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        
        redirect(site_url('feriado/listar'));
    }

    public function atualizar_feriados() {

        $keys[] = "Z2FicmllbGNhbGRlaXJhMjAwOUBob3RtYWlsLmNvbSZoYXNoPTIzNjYxNjU4OQ";
        $keys[] = "Z2FicmllbGx1aXpjbUBob3RtYWlsLmNvbSZoYXNoPTEwNTA5NDg2NQ";
        $keys[] = "c3VyaWVsamFjb2JzZW5AZ21haWwuY29tJmhhc2g9MjQyNzIyNTM0";
        $keys[] = "bGVvbmFyZG9AdHJpcGxlYWllLmNvbS5iciZoYXNoPTIxMjM1MTM3Ng";
        $keys[] = "bGVvbmFyZG9AY29udGFhZ2lsLmNvbSZoYXNoPTI1NDI5NTI3OQ";
        $keys[] = "bGVva3J1Z2VyMjAxMkBnbWFpbC5jb20maGFzaD0yNjU3OTQ2Njg";
        $keys[] = "am9ueWRvcm5lbGVzQGdtYWlsLmNvbSZoYXNoPTIxOTA1MTkyNQ";

        $dados=$this->Cliente_model->listar_cidades();
        
        $ano = date("Y");

        //Limpa tabela de feriados
        $this->Feriado_model->deletar_todos();

        //Atualiza os feriados
        $count = 0;
        foreach($dados as $value){
            $ano = date("Y");
            $uf = $value["uf"];
            $cidade =urlencode($value["cidade"]);
            if ($uf<>"" and $cidade<>""){
                $url = "https://api.calendario.com.br/?ano={$ano}&estado={$uf}&cidade={$cidade}&token=".$keys[$count];
                echo "<br>URL $url";
                $feriados = file_get_contents($url);
                $feriados = simplexml_load_string($feriados);
                var_dump($feriados);
                foreach($feriados->event as $value_){
                    $this->Feriado_model->atualizar_feriados($value["uf"],$value["cidade"],$value_);
                }
                $count++;
                if ($count==7) $count=0;
            }
        }
        //var_dump($feriados);
    }
    
}