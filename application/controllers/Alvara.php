<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Alvara extends CI_Controller {

    private $data = array(),
            $model = array("Alvara_model","Evento_model"),
            $titulo = "Alvará", 
            $view = "Alvara_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
         $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function listar() {
        
        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."alvara/listar";
        $config['total_rows'] = count($this->Alvara_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->Alvara_model->listar($inicio); 
        $this->data['operacao']="listar";

        $this->load->view('Pagina',$this->data);
    }

    public function novo() {
        $this->data['operacao']="novo";
        $this->load->view('Pagina',$this->data);
    }

    public function inserir() {
        $this->Alvara_model->inserir();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        redirect(site_url('alvara/listar'));
    }

    public function salvar($cod_alvara) {
        $this->Alvara_model->salvar($cod_alvara);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('alvara/listar'));
    }

    public function editar($cod_alvara) {
        $this->data['dados']=$this->Alvara_model->editar($cod_alvara);
        $this->data['dados_evento']=$this->Evento_model->listar_select($cod_alvara);
        $this->data['operacao']="editar";
        $this->load->view('Pagina',$this->data);
    }

    public function excluir($cod_alvara) {
        $this->Alvara_model->excluir($cod_alvara);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        
        redirect(site_url('alvara/listar'));
    }

    public function json_alvaralistar() {
        echo $this->Alvara_model->json_alvaralistar();
    }

}
