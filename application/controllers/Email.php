<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Email extends CI_Controller {

    private $data = array(),
            $model = array("Template_model"),
            $titulo = "E-mail", 
            $view = "Email_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
         $this->functions->usuario_logado();

         //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function index() {

        $template = $this->Template_model->editar(1);

        $myemail = new Myemail;
        $myemail->cod_cliente(0);
        $myemail->from(TITULO, EMAIL_NR);
        $myemail->to("Destinatário","ramosrafa18@gmail.com");
        $myemail->subject($template["titulo"]."- 1");
        $myemail->body($template["texto"]);
        $myemail->custom(array("%TESTE%"=>"TESTE","%DATA%"=>date('Y-m-d H:i:s')));
        $myemail->attachment(LOCAL.LOGO_SISTEMA,"Logo.png");
        echo $myemail->enviar();

    }

    public function atualizar_status() {

        $data = json_decode(file_get_contents("php://input"), true);
        //$data = json_decode('{"event":"unique_opened","email":"ramosrafa18@gmail.com","id":229259,"date":"2020-06-05 01:23:15","ts":1591312995,"message-id":"<202006050122.25614367431@smtp-relay.mailin.fr>","ts_event":1591312995,"subject":"BAH Teste de layout","sending_ip":"185.41.28.128","ts_epoch":1591312995527}', true);
        
        $this->Email_model->atualizar_status($data["message-id"],$data["event"]." ".$data["date"]);

    }

    public function listar() {
        
        //Verifica se esta logado
        if( $this->session->userdata('logado') !== true ) redirect(site_url('login'));

        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."email/listar";
        $config['total_rows'] = count($this->Email_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->Email_model->listar($inicio); 
        $this->data['operacao']="listar";
        $this->load->view('Pagina',$this->data);
    }

    public function ver($cod_email) {

        //Verifica se esta logado
        if( $this->session->userdata('logado') !== true ) redirect(site_url('login'));

        $this->data['dados']=$this->Email_model->editar($cod_email);
        $this->data['operacao']="ver";
        $this->load->view('Pagina',$this->data);
    }

    public function relatorio_email($formato="pdf") {

        $this->data['dados']=$this->Email_model->relatorio_email();

        $this->functions->exportar($this->data,$formato,"Relatorio_email");

    }
    
}
