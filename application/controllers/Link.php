<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Link extends CI_Controller {

    private $data = array(),
            $model = array("Link_model"),
            $titulo = "Links", 
            $view = "Link_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function listar() {
        
        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."link/listar";
        $config['total_rows'] = count($this->Link_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->Link_model->listar($inicio); 
        $this->data['operacao']="listar";

        $this->load->view('Pagina',$this->data);
    }

    public function novo() {
        $this->data['operacao']="novo";
        $this->load->view('Pagina',$this->data); 
    }

    public function inserir() {
        $this->Link_model->inserir();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        redirect(site_url('link/listar'));
    }

    public function salvar($cod_link) {
        $this->Link_model->salvar($cod_link);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('link/listar'));
    }

    public function editar($cod_link) {
        $this->data['dados']=$this->Link_model->editar($cod_link);
        $this->data['operacao']="editar";
        $this->load->view('Pagina',$this->data);
    }

    public function excluir($cod_link) {
        $this->Link_model->excluir($cod_link);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        
        redirect(site_url('link/listar'));
    }

    public function json_linklistar() {
        echo $this->Link_model->json_linklistar();
    }

}
