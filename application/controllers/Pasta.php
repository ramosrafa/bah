<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Pasta extends CI_Controller {

    private $data = array(),
            $model = array("Pasta_model","Evento_model"),
            $titulo = "Pasta", 
            $view = "Pasta_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function listar() {
        
        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."pasta/listar";
        $config['total_rows'] = count($this->Pasta_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->Pasta_model->listar($inicio); 
        $this->data['operacao']="listar";
        $this->load->view('Pagina',$this->data);
    }

    public function listar_pastacliente() {
        $this->data['dados']=$this->Pasta_model->listar_pastacliente(); 
        $this->load->view('Pagina',$this->data);
    }

    public function novo() {
        $this->data['operacao']="novo";
        $this->load->view('Pagina',$this->data);
    }

    public function inserir() {
        $this->Pasta_model->inserir();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('pasta/listar'));
    }

    public function salvar($cod_pasta) {
        $this->Pasta_model->salvar($cod_pasta);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('pasta/listar'));
    }

    public function editar($cod_pasta) {
        $this->data['dados']=$this->Pasta_model->editar($cod_pasta);
        $this->data['dados_evento']=$this->Evento_model->listar_select($cod_pasta);
        $this->data['operacao']="editar";
        $this->load->view('Pagina',$this->data);
    }

    public function excluir($cod_pasta) {
        $this->Pasta_model->excluir($cod_pasta);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        
        redirect(site_url('pasta/listar'));
    }

}
