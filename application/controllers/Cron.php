<?php

class Cron extends CI_Controller {

    private $data = array(),
            $model = array("Usuario_model","Coleta_model");
            
    function __construct() {
        parent::__construct();

        //Carrega Model
        $this->load->model($this->model);
    }

    public function hora() {
        $this->functions->escrever_log ('cron-log-' . date('Y-m-d') . '.php',"a+", 'CRON hora: '.ENVIRONMENT.date("jnY-Hi"));
    }

    public function dia() {
        $this->functions->escrever_log ('cron-log-' . date('Y-m-d') . '.php',"a+", 'CRON dia: '.ENVIRONMENT.date("jnY-Hi"));
        
        //TESTE
        $template = $this->Template_model->editar(9);
        $myemail = new Myemail;
        $myemail->cod_cliente(0);
        $myemail->from("TESTE", EMAIL_NR);
        $myemail->to("BAH","ramosrafa18@gmail.com");
        $myemail->subject($template["titulo"]);
        $myemail->body($template["texto"]);
        $myemail->custom(array("%EMPRESA%"=>"EMPRESA","%VENCIMENTO%"=>$value["certificado_vencimento_"]));
        $myemail->enviar();

        $this->eventos_dia();

        //if (30==date("t")){
        if (date("d")==date("t")){
            $this->certificados_vencidos();
            $this->eventos_mes();
        }
    }
    
    public function mes() {
        $this->functions->escrever_log ('cron-log-' . date('Y-m-d') . '.php',"a+", 'CRON mes: '.ENVIRONMENT.date("jnY-Hi"));
    }

    public function ano() {
        $this->functions->escrever_log ('cron-log-' . date('Y-m-d') . '.php',"a+", 'CRON ano: '.ENVIRONMENT.date("jnY-Hi"));
    }

    public function certificados_vencidos() {

        $template = $this->Template_model->editar(9);

        $certificados = $this->Cliente_model->listar_certificadovencido(true);

        foreach ($certificados as $key => $value){
            if ($value ["situacao"]<>"ok") {
                $usuarios = $this->Usuario_model->listar_usuarioscliente($value["cod_cliente"]);
                foreach($usuarios as $value_){
                    if ($value_["config_receberemail"]=="S"){

                        $myemail = new Myemail;
                        $myemail->cod_cliente(0);
                        $myemail->from(TITULO, EMAIL_NR);
                        $myemail->to("BAH","ramosrafa18@gmail.com");
                        $myemail->subject($template["titulo"]);
                        $myemail->body($template["texto"]);
                        $myemail->custom(array("%EMPRESA%"=>$value["nome"],"%VENCIMENTO%"=>$value["certificado_vencimento_"]));
                        //$myemail->enviar();
                        
                        $this->functions->escrever_log ('cron-log-' . date('Y-m-d') . '.php',"a+","CRON dia Certificados: ".$value["nome"]."; Vencimento:".$value["certificado_vencimento_"]."; Nome:".$value_["nome"]."; Email:".$value_["email"]);
                    }
                }
            }
        }
    }

    public function eventos_dia() {
        $template = $this->Template_model->editar(12);
        
    }

    public function eventos_mes() {
        
        $template = $this->Template_model->editar(11);
        
        $data = new DateTime();
        $data->setDate(date("Y"), date("m"), date("d"));

        $clientes = $this->Cliente_model->evento($data);
        
        /*

        foreach($clientes as $value){
            echo "<br>Cliente: {$value["cod_cliente"]} {$value["cliente"]}";
            $eventos = $this->Coleta_model->listar_calendario($value["cod_cliente"],$data->format('m'),$data->format('Y'));

            $html_evento = "<ul>";
            $count = 0;
            foreach($eventos as $value_){
                echo "<br>-Evento: {$value_["data_vencimento"]} {$value_["nome"]}";
                $html_evento.="<li><strong>".$value_["data_vencimento_"]."</strong> ".$value_["nome"]."</li>";
                $count++;
            }
            $html_evento .= "</ul>";

            if ($count>0){
                $usuarios = $this->Usuario_model->listar_usuarioscliente($value["cod_cliente"],"C");
                foreach($usuarios as $value_){
                    echo "<br>--ENVIO Usuário: {$value_["nome"]} {$value_["email"]}";
                    $myemail = new Myemail;
                    $myemail->cod_cliente(0);
                    $myemail->from(TITULO, EMAIL_NR);
                    $myemail->to("BAH",$value_["email"]);
                    $myemail->subject($template["titulo"]);
                    $myemail->body($template["texto"]);
                    $myemail->custom(array("%CLIENTE%"=>$value["cliente"],"%RAZAOSOCIAL%"=>$value["razao_social"],"%USUARIO%"=>$value_["nome"],"%EVENTO%"=>$html_evento));
                    //$myemail->enviar();
                }
            }
        }
        */
    }
}