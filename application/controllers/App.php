<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class App extends CI_Controller {

    function __construct() {
        parent::__construct();
        
        $chave = $this->uri->segment(3); 
        if ($chave<>"tri2007" and $chave <> "41e0055176bdec5cb8d6bf1430ece61d") die ("Directory access is forbidden.");
        
        //echo ($this->uri->segment(4)); exit;
        
        header("Access-Control-Allow-Origin: *");
    }

    public function enviar_email(){ 

		$cod_pasta = $this->input->get_post('cod_pasta');
		$nome = $this->input->get_post('nome');
		$email = $this->input->get_post('email');
		$mensagem = $this->input->get_post('mensagem');		

		$mail_subject = "BAH APP . Suporte";
		$mail_texto =
				"
				<br>Pasta / Categoria: $cod_pasta
				<br>-
				<br>Nome: $nome
				<br>Email: $email
				<br>Mensagem: $mensagem
				";
		$mail_html = $this->functions->formatar_email(EMAIL_SUPORTE,$mail_subject,$mail_texto);

		$this->email->from(EMAIL_ADM, 'BAH');
		$this->email->to(EMAIL_SUPORTE);
		$this->email->subject($mail_subject);
		$this->email->set_mailtype("html");
		$this->email->message($mail_html);

		$op = $this->email->send(); 
        
        $cod_cliente = $this->uri->segment(4);
        $this->Notificacao_model->json_inserir($cod_cliente);
        
        echo $op; 
        
    }

    public function painel() { 

        $mes = $this->uri->segment(4);
        $ano = $this->uri->segment(5);
        $cod_usuario = $this->uri->segment(6);
        $cod_cliente = $this->uri->segment(7);
       
        if (!$mes) $mes = date("m");
        if (!$ano) $ano = date("Y");
        if (!$cod_usuario) $cod_usuario = 1;
        if (!$cod_cliente) $cod_cliente = 1;
        
        echo $this->Cliente_model->ajax_painel($mes,$ano,$cod_usuario,$cod_cliente);
       
    }

    public function download(){ 

        echo $this->Arquivo_model->download("bebc67f16b8047f03c1712011415dcf6");   
        
    }
    
    public function notificacao_ultima(){ 

        $cod_usuario = $this->uri->segment(4);
        echo $this->Notificacao_model->ajax_ultima($cod_usuario); 
        
    }

    public function notificacao_listar(){  

        $cod_usuario = $this->uri->segment(4);
        echo $this->Notificacao_model->ajax_notificacaolistar($cod_usuario);  
        
    }

    public function notificacao_lida(){ 

        $cod_notificacao = $this->uri->segment(4);
        $cod_usuario = $this->uri->segment(5);
        echo $this->Notificacao_model->json_lida($cod_notificacao,$cod_usuario);
        
    }

    public function cliente_listar(){   

        $cod_usuario = $this->uri->segment(4);
        echo $this->Cliente_model->json_clientelistar($cod_usuario);   
        
    }
    
    public function cod_pasta_listar(){   

        $cod_usuario = $this->uri->segment(4);
        echo $this->pasta_model->json_cod_pastalistar($cod_usuario);   
        
    }
    
    public function upload_listar(){   

        $cod_usuario = $this->uri->segment(4);
        $cod_pasta = $this->uri->segment(5);
        echo $this->Cliente_model->json_uploadlistar($cod_usuario,$cod_pasta);       
        
    }
    
    public function busca_listar(){     

        $cod_usuario = $this->uri->segment(4);
        echo $this->Cliente_model->json_buscalistar($cod_usuario);    
        
    }
    
    public function usuario_login(){

        echo $this->Usuario_model->json_usuariologin();  
        
    }

    public function usuario_listar(){

        $cod_usuario = $this->uri->segment(4);
        $tipo = $this->uri->segment(5);
        echo $this->Usuario_model->json_usuariolistar($cod_usuario,$tipo);     
        
    }

    public function usuario_pasta(){ 
        
        $cod_usuario = $this->uri->segment(4);
        echo $this->Usuario_model->json_usuariopasta($cod_usuario);
        
    }
    
    public function usuario_cliente(){ 
        
        $cod_usuariologado = $this->uri->segment(4);
        $cod_usuario = $this->uri->segment(5);
        $tipo = $this->uri->segment(6);
        echo $this->Usuario_model->json_usuariocliente($cod_usuariologado,$cod_usuario,$tipo);
        
    }
    
    public function usuario_inserir(){

        echo $this->Usuario_model->inserir();
        
    }

    public function usuario_editar(){

        $cod_usuario = $this->uri->segment(4);
        echo $this->Usuario_model->json_usuarioeditar($cod_usuario);
        
    }

    public function usuario_salvar(){
        
        echo $this->Usuario_model->json_usuariosalvar();
        
    }

    public function usuario_excluir(){ 
        
        echo $this->Usuario_model->json_usuarioexcluir(); 
        
    }

    public function evento_listar(){
        
        echo $this->Evento_model->json_eventolistar();
        
    }

    public function push_registrar(){ 
        $cod_cliente = $this->uri->segment(4);
        $id = $this->uri->segment(5);
        $platform = $this->uri->segment(6);
        echo $this->Cliente_model->json_clientesalvardevice($cod_cliente,$id,$platform);
    }
    
    public function app_push($chave="",$device="",$platform="Android",$titulo="",$mensagem="") {
        //echo "bah.tripleaie.com.br/app/app_push/41e0055176bdec5cb8d6bf1430ece61d/d6m4HljFExw:APA91bGdcICik5wxDdBfnbJChkVsNQXL2PTq5P4c1V3ghVO1F0VfaBvjUd97XjpuMnjQ6UL6a_Jn81Oq4tCegkEPKQvqct1YnJyxbXGxR4HbwY9MWtD2TWWqqk4h-on6keMIN01XAr4G/Android";exit;
        
        $push = $this->functions->app_push($device,$platform,$titulo,$mensagem);
        
    }
    
    
}
