<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Usuario extends CI_Controller {

    private $data = array(),
            $model = array("Usuario_model","Setor_model"),
            $titulo = "Usuário", 
            $view = "Usuario_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
         $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }
    
    public function listar() {

        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."usuario/listar";
        $config['total_rows'] = count($this->Usuario_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->Usuario_model->listar($inicio);
        $this->data['operacao']="listar";
        $this->load->view('Pagina',$this->data);
        
    }

    public function listar_json() {

        echo $this->Usuario_model->listar_json();
        
    }

    public function novo() {
        $this->data['dados_usuariopasta']=$this->Usuario_model->usuario_pasta();
        $this->data['dados_setor']=$this->Setor_model->listar_select();
        $this->data['dados_usuariocliente']=$this->Usuario_model->usuario_cliente();
        $this->data['operacao']="novo";
        $this->load->view('Pagina',$this->data);
    }

    public function validar() {
        echo json_encode($this->Usuario_model->validar()); 
    }

    public function inserir() {
        $this->Usuario_model->inserir();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('usuario/listar'));

    }

    public function salvar($cod_usuario) {
        $this->Usuario_model->salvar($cod_usuario);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('usuario/listar'));
    }

    public function salvar_colunaclientes() {
        echo "<br>salvar_colunaclientes: INI";
        $this->Usuario_model->salvar_colunaclientes();
        echo "<br>salvar_colunaclientes: FIM";
    }

    public function editar($cod_usuario) {
        $this->data['dados']=$this->Usuario_model->editar($cod_usuario);
        $this->data['dados_usuariopasta']=$this->Usuario_model->usuario_pasta($cod_usuario);
        $this->data['dados_setor']=$this->Setor_model->listar_select();
        $this->data['dados_usuariocliente']=$this->Usuario_model->usuario_cliente($cod_usuario);
        $this->data['operacao']="editar";
        $this->load->view('Pagina',$this->data);
    }

    public function excluir($cod_usuario) {
        $this->Usuario_model->excluir($cod_usuario);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        
        redirect(site_url('usuario/listar'));
    }

    public function aceitar_termodeuso() {
        $this->Usuario_model->aceitar_termodeuso();
        echo json_encode(array('op'=>'1', 'msg' => "Obrigado por aceitar os termos de uso.(".$this->session->userdata('cod_usuario').")"));        
    }
    
    public function relatorio_usuario($formato="pdf") {
        $this->data['dados']=$this->Usuario_model->listar("");
        $this->functions->exportar($this->data,$formato,"Relatorio_usuario");
        
    }
    
}

