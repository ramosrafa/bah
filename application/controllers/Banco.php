<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Banco extends CI_Controller {

    private $data = array(),
            $model = array("Banco_model","Evento_model"),
            $titulo = "Banco", 
            $view = "Banco_view";
            
    function __construct() {
        parent::__construct();

        //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function listar() {
        
        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."banco/listar";
        $config['total_rows'] = count($this->Banco_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->Banco_model->listar($inicio); 
        $this->data['operacao']="listar";
        $this->load->view('Pagina',$this->data);
    }

    public function listar_bancocliente() {
        $this->data['dados']=$this->Banco_model->listar_bancocliente(); 
        $this->load->view('Pagina',$this->data);
    }

    public function novo() {
        $this->data['operacao']="novo";
        $this->load->view('Pagina',$this->data);
    }

    public function inserir() {
        $this->Banco_model->inserir();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('banco/listar'));
    }

    public function salvar($cod_banco) {
        $this->Banco_model->salvar($cod_banco);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('banco/listar'));
    }

    public function editar($cod_banco) {
        $this->data['dados']=$this->Banco_model->editar($cod_banco);
        $this->data['dados_evento']=$this->Evento_model->listar_select($cod_banco);
        $this->data['operacao']="editar";
        $this->load->view('Pagina',$this->data);
    }

    public function excluir($cod_banco) {
        $this->Banco_model->excluir($cod_banco);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        
        redirect(site_url('banco/listar'));
    }

}
