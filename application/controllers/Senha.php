<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Senha extends CI_Controller {

    private $data = array(),
            $model = "Usuario_model",
            $titulo = "Senha", 
            $view = "Senha_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);
    }

    public function nova() {
        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        $this->load->view('Pagina',$this->data);
    }

    public function salvar() {
        $this->Usuario_model->salvar_senha();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
    }
    
}
