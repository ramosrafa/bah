<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Template extends CI_Controller {

    private $data = array(),
            $model = "Template_model",
            $titulo = "Template", 
            $view = "Template_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);
        
        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;

    }


    public function listar() {
        
        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."template/listar";
        $config['total_rows'] = count($this->Template_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->Template_model->listar($inicio); 
        $this->data['operacao']="listar";
        $this->load->view('Pagina',$this->data);
    }

    public function novo() {
        $this->data['operacao']="novo";
        $this->load->view('Pagina',$this->data);
    }

    public function inserir() {
        $this->Template_model->inserir();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('template/listar'));
    }

    public function salvar($cod_template) {
        $this->Template_model->salvar($cod_template);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('template/listar'));
    }

    public function editar($cod_template) {
        $this->data['dados']=$this->Template_model->editar($cod_template);
        $this->data['operacao']="editar";
        $this->load->view('Pagina',$this->data);
    }

    public function excluir($cod_template) {
        $this->Template_model->excluir($cod_template);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        
        redirect(site_url('template/listar'));
    }

    public function testar($cod_template) {

        $template = $this->Template_model->editar($cod_template); 

        $myemail = new Myemail;
        $myemail->cod_cliente(0);
        $myemail->from(TITULO, EMAIL_NR);
        $myemail->to("BAH","ramosrafa18@gmail.com");
        $myemail->subject($template["titulo"]);
        $myemail->body($template["texto"]);
        $myemail->custom(array());
        $myemail->enviar();

        echo 1;
    }

}
