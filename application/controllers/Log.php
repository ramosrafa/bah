<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Log extends CI_Controller {

    private $data = array(),
            $model = array("pasta_model","Log_model"),
            $titulo = "Log", 
            $view = "Log_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function listar() {
        
        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."log/listar";
        $config['total_rows'] = count($this->Log_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->Log_model->listar($inicio);
        $this->data['operacao']="listar";
        $this->load->view('Pagina',$this->data);
    }

    public function ver($cod_log) {
        $this->data['dados']=$this->Log_model->ver($cod_log);
        $this->data['operacao']="ver";
        $this->load->view('Pagina',$this->data);
    }
}
