<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Servico extends CI_Controller {

    private $data = array(),
            $model = array("Servico_model","Tarefa_model","pasta_model"),
            $titulo = "Serviço", 
            $view = "Servico_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();
        
        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function contar() {
        echo $this->Servico_model->contar();
    }
    
    public function listar() {
        
        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."Servico/listar";
        $config['total_rows'] = count($this->Servico_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->Servico_model->listar($inicio);
        $this->data['operacao']="listar";
        $this->load->view('Pagina',$this->data);
    }

    public function grafico_quantidademes() {
        echo $this->Servico_model->grafico_quantidademes();
    }

    public function novo() {
        $this->data['dados_tarefa']=$this->Tarefa_model->listar_select();
        $this->data['dados_cliente']=$this->Cliente_model->listar_select();
        $this->data['dados_cod_pasta']=$this->pasta_model->listar_select();
        $this->data['operacao']="novo";
        $this->load->view('Pagina',$this->data);
    }

    public function inserir() {
        $this->Servico_model->inserir();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('Servico/listar'));
    }

    public function salvar($cod_servico) {
        $this->Servico_model->salvar($cod_servico);
        //$this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('Servico/listar'));
    }

    public function editar($cod_servico) {
        $this->data['dados']=$this->Servico_model->editar($cod_servico);
        $this->data['dados_tarefa']=$this->Tarefa_model->listar_select();
        $this->data['dados_cliente']=$this->Cliente_model->listar_select();
        $this->data['dados_cod_pasta']=$this->pasta_model->listar_select();
        $this->data['dados_etapas']=$this->Servico_model->listar_etapas($cod_servico);
        $this->data['operacao']="editar";
        //Evita que usuário tipo cliente edite o registro
        if ($this->session->userdata('tipo')=="C") $this->data['operacao']="ver";
        $this->load->view('Pagina',$this->data);
    }

    public function excluir($cod_servico) {
        $this->Servico_model->excluir($cod_servico);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        
        redirect(site_url('servico/listar'));
    }

    public function excluir_arquivo($pasta,$cod_servico,$cod_servicoetapa) {
        $arquivo ="./data/{$pasta}/servico{$cod_servico}-{$cod_servicoetapa}.pdf";
        if (unlink ($arquivo)){
            echo json_encode(array('op'=>'1', 'msg' => "Arquivo excluído."));
        }else{
            echo json_encode(array('op'=>'0', 'msg' => "Houve um erro na exclusão."));  
        }
        log_message('debug', "excluir_arquivo arquivo:$arquivo");
    }

    public function ver($cod_servico) {
        $this->data['dados']=$this->Servico_model->editar($cod_servico);
        $this->data['dados_etapas']=$this->Servico_model->listar_etapas($cod_servico);
        $this->data['operacao']="ver";
        $this->load->view('Pagina',$this->data);
    }

    public function selecionar() { 
        $servico = $this->uri->segment(3);
        $this->session->set_userdata( 'Servico', $servico); 
        echo $servico;
    }
    public function calcular_previsao($data="",$dias="") { 
        $data_etapa = date('d/m/Y', strtotime("+{$dias} day",strtotime($data)));
        //$data_previsao = $this->Servico_model->servico_data_previsao($cod_servico);
        //$data_previsao = $data_previsao["data_previsao"];
        $data_previsao = $data_etapa;
        
        echo json_encode(array('data_etapa'=>$data_etapa, 'data_previsao' => date('d/m/Y', strtotime($data_previsao))));
        
    }
    
    public function relatorio_servicovencido($formato="pdf") {

        $this->data['dados']=$this->Servico_model->listar_servicovencido();

        $this->functions->exportar($this->data,$formato,"Relatorio_servicovencido");

    }

    
}
