<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Entrega extends CI_Controller {

    private $data = array(),
            $model = array("Entrega_model","Evento_model"),
            $titulo = "Entrega", 
            $view = "Entrega_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
    }
    
    public function entrega() {
        
        $this->data['view']="Entrega_view";
        $this->data['operacao']="entrega";
        $this->load->view('Pagina',$this->data);
    }

    public function manual() {
        //Tela para testar entrega sem ajax
        $this->data['view']="Entrega_view";
        $this->data['operacao']="manual";
        $this->load->view('Pagina',$this->data);
    }

    public function enviar() {

        echo $this->Entrega_model->enviar();
        
    }

}
