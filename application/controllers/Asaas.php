<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Asaas extends CI_Controller {

    private $data = array(),
            $model = array(),
            $titulo = "Asaas", 
            $view = "Asaas_view";
            
    function __construct() {
        parent::__construct();

        //Verifica se esta logado
         $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);
         
        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function executar($funcao="") {
        
        $this->asaas->executar($funcao);
    }
    
}
