<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Relatorio extends CI_Controller {

    private $data = array(),
            $model = "",
            $titulo = "Relatório", 
            $view = "Relatorio_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function diverso() {
        $this->data['operacao']="diverso";
        $this->load->view('Pagina',$this->data);
    }

    public function cliente_evento() {
        $this->data['operacao']="cliente_evento";
        $this->load->view('Pagina',$this->data);
    }

}
