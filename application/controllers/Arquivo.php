<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Arquivo extends CI_Controller {

    private $data = array(),
            $model = array("Arquivo_model"),
            $titulo = "Arquivo", 
            $view = "Drive_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
    }

    //Download do arquivo
    public function a($arquivo="") {
        /*
        echo "<br>arquivo<br>";
        $arquivo = $this->input->get_post("arquivo");
        var_dump($arquivo);
        echo "<br>arquivo_download<br>";
        $arquivo_download = $this->input->get_post("arquivo_download");
        var_dump($arquivo_download);
        exit;
        */
        if ($arquivo<>"" and $arquivo<>"undefined"){
            $arr[] = $arquivo;
            return $this->Arquivo_model->download($arr,false);
        } else {
            $arquivo = $this->input->get_post("arquivo");
            foreach( $arquivo as $value ) {

                $arr[]=$value;
            }
            return $this->Arquivo_model->download($arr,true);
        }
        
    }

    //Remover arquivo
    public function e($arquivo) {

        return $this->Arquivo_model->excluir($arquivo);
        
    }

}
