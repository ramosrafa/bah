<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Formulario extends CI_Controller {

    private $data = array(),
            $model = array("Formulario_model","pasta_model"),
            $titulo = "Formulário", 
            $view = "Formulario_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function listar() {
        
        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."formulario/listar";
        $config['total_rows'] = count($this->Formulario_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->Formulario_model->listar($inicio); 
        $this->data['operacao']="listar";
        $this->load->view('Pagina',$this->data);
    }

    public function listar_etapas($cod_formulario) {
        echo json_encode($this->Formulario_model->listar_etapas($cod_formulario)); 
    }

    public function novo() {
        $this->data['dados_cod_pasta']=$this->pasta_model->listar_select();
        $this->data['operacao']="novo";
        $this->load->view('Pagina',$this->data);
    }

    public function inserir() {
        $this->Formulario_model->inserir();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('formulario/listar'));
    }

    public function salvar($cod_formulario) {
        $this->Formulario_model->salvar($cod_formulario);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('formulario/listar'));
    }

    public function editar($cod_formulario) {
        $this->data['dados']=$this->Formulario_model->editar($cod_formulario);
        $this->data['dados_cod_pasta']=$this->pasta_model->listar_select();
        $this->data['dados_campos']=$this->Formulario_model->listar_campos($cod_formulario);
        $this->data['operacao']="editar";
        $this->load->view('Pagina',$this->data);
    }

    public function excluir($cod_formulario) {
        $this->Formulario_model->excluir($cod_formulario);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        
        redirect(site_url('formulario/listar'));
    }

    
}
