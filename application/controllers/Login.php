<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Login extends CI_Controller {

    private $data = array(),
            $model = array("Usuario_model"),
            $titulo = "Login", 
            $view = "Login_view";

    function __construct() {
        parent::__construct();

        //Carrega Model
        $this->load->model($this->model);
        
        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
        header("Access-Control-Allow-Origin: *");
    }
    
	//Carrega view de login
    public function index() {
		if( $this->session->userdata('logado') !== true ) {
	        $this->load->view('Pagina',$this->data);
		} else {
            redirect(site_url('painel/resumo'));
		}
    }

	//Valida cadastro e efetua login
    public function logar(){
		$email = $this->input->get_post('email');
		$senha = $this->input->get_post('senha');		
		$from = $this->input->get_post('from');
        
		if( $email && $senha) {
			$operacao_ajax = $this->Usuario_model->logar($email, $senha);
			if( $operacao_ajax === true or $operacao_ajax == 1 ) {
                if ($from == "BAH") {
                    //Se for TripleAie
                    if ($this->session->userdata('tipo')=="T") echo json_encode(array("op" => "1","erro" => "","clientes"=>"","login_controller"=>$this->session->userdata('login_controller'),"login_event"=>$this->session->userdata('login_event')));
                    //Se for Cliente
                    if ($this->session->userdata('tipo')=="C") echo json_encode(array("op" => "2","erro" => "","clientes"=>json_encode($this->session->userdata('clientes')),"login_controller"=>$this->session->userdata('login_controller'),"login_event"=>$this->session->userdata('login_event')));
                } else {
                    redirect(site_url());
                }
			} else {
                if ($from == "BAH") {
                    echo json_encode(array("op" => "0","erro" => "Usuário ou senha inválidos!"));
                } else {
                    redirect("https://www.tripleaie.com.br/bahLogin");
                }
			}
		} else {
			echo json_encode(array("op" => "0","erro" => "Informe todos os campos!"));
        }
    }

	//Fecha a sessão e sai do sistema
    public function sair() {
        $this->session->sess_destroy();
        redirect(site_url(''));
    }

	//Gera nova senha e encaminha para o usuário
    public function recuperar_senha() {
        $recuperar_email = $this->input->get_post('recuperar_email');

        $er = "/^(([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}){0,1}$/";
        if (!preg_match($er, $recuperar_email)){
            $operacao_ajax = 'Informe um e-mail válido';
            echo $operacao_ajax;
            return false;
        }
        
        $novasenha = $this->Usuario_model->recuperar_senha($recuperar_email);

        if ($novasenha["existe"]==0){
            $operacao_ajax = 'E-mail não localizado. Entre em contato com o suporte.';
            echo $operacao_ajax;
            return false; 
            
        }else{
            
            $template = $this->Template_model->editar(4);

            $myemail = new Myemail;
            $myemail->cod_cliente(0);
            $myemail->from(TITULO, EMAIL_NR);
            $myemail->to("BAH",$recuperar_email);
            $myemail->subject($template["titulo"]);
            $myemail->body($template["texto"]);
            $myemail->custom(array("%USUARIO%"=>$novasenha["usuario"],"%NOVASENHA%"=>$novasenha["novasenha"]));
            $myemail->enviar();

            echo "Email enviado para ".$recuperar_email.". Por favor confira sua caixa de entrada e caixa de lixo eletrônico.";
        }

    }

}
