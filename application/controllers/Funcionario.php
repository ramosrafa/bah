<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Funcionario extends CI_Controller {

    private $data = array(),
            $model = array("Funcionario_model"),
            $titulo = "Funcionário", 
            $view = "Funcionario_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function listar() {
        
        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."funcionario/listar";
        $config['total_rows'] = count($this->Funcionario_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->Funcionario_model->listar($inicio); 
        $this->data['operacao']="listar";
        $this->load->view('Pagina',$this->data);
    }

    public function listar_funcionariocliente() {
        $this->data['dados']=$this->Funcionario_model->listar_funcionariocliente(); 
        $this->load->view('Pagina',$this->data);
    }

    public function novo() {
        $this->data['operacao']="novo";
        $this->data['dados_cliente']=$this->Cliente_model->listar_select();
        $this->load->view('Pagina',$this->data);
    }

    public function inserir() {
        $this->Funcionario_model->inserir();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('funcionario/listar'));
    }

    public function salvar($cod_funcionario) {
        $this->Funcionario_model->salvar($cod_funcionario);

        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('funcionario/listar'));
    }

    public function editar($cod_funcionario) {
        $this->data['dados']=$this->Funcionario_model->editar($cod_funcionario);
        $this->data['dados_cliente']=$this->Cliente_model->listar_select();
        $this->data['operacao']="editar";
        $this->load->view('Pagina',$this->data);
    }

    public function excluir($cod_funcionario) {
        $this->Funcionario_model->excluir($cod_funcionario);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        
        redirect(site_url('funcionario/listar'));
    }

    public function relatorio_funcionario($formato="pdf") {

        $this->data['dados']=$this->Funcionario_model->listar();

        $this->functions->exportar($this->data,$formato,"Relatorio_funcionario");

    }

}
