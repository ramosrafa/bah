<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Equipe extends CI_Controller {

    private $data = array(),
            $model = array("Usuario_model"),
            $titulo = "Equipe", 
            $view = "Equipe_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
         $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;

    }
    
    public function ver() {
        $this->data['view']="Equipe_view";
        $this->data['dados_equipe']=$this->Usuario_model->listar_usuarioscliente($this->session->userdata('cliente'));

        $this->load->view('Pagina',$this->data);
        
    }
}
