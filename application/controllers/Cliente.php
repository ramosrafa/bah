<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Cliente extends CI_Controller {

    private $data = array(),
            $model = array("Certificadotipo_model","Alvara_model","Regime_model","Notificacao_model","Usuario_model","Funcionario_model","Arquivo_model"),
            $titulo = "Cliente", 
            $view = "Cliente_view";
            
    function __construct() {
        parent::__construct();

        //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);
         
        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function index() {
        $this->listar();
    }
    
    public function contar() {
        echo $this->Cliente_model->contar();
    }
    
    public function asaas($funcao="") {
        
        $this->asaas->executar($funcao);
    }
    
    public function listar() {
        
        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."cliente/listar";
        $config['total_rows'] = count($this->Cliente_model->listar("","N"));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['dados']=$this->Cliente_model->listar($inicio,"N");
        $this->data['operacao']="listar";
        $this->load->view('Pagina',$this->data);
    }

    public function novo() {
        $this->data['dados_clientematriz']=$this->Cliente_model->listar_select();
        $this->data['dados_clienteevento']=$this->Cliente_model->cliente_evento();
        $this->data['dados_certificadotipo']=$this->Certificadotipo_model->listar_select();
        $this->data['dados_tipoalvara']=$this->Alvara_model->listar_select();
        $this->data['dados_regime']=$this->Regime_model->listar_select();
        $this->data['operacao']="novo";
        $this->load->view('Pagina',$this->data);
    }

    public function inserir() {
        $this->Cliente_model->inserir();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('cliente/listar'));
    }

    public function salvar($cod_cliente) {
        $this->Cliente_model->salvar($cod_cliente);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('cliente/listar'));
    }

    public function editar($cod_cliente) {
        $this->data['dados']=$this->Cliente_model->editar($cod_cliente);
        $this->data['dados_clientematriz']=$this->Cliente_model->listar_select();
        $this->data['dados_clienteevento']=$this->Cliente_model->cliente_evento($cod_cliente);
        $this->data['dados_certificadotipo']=$this->Certificadotipo_model->listar_select();
        $this->data['dados_tipoalvara']=$this->Alvara_model->listar_select();
        $this->data['dados_regime']=$this->Regime_model->listar_select();
        $this->data['dados_arquivos']=$this->Arquivo_model->listar(array("cod_cliente" => $cod_cliente));
        $this->data['dados_filiais']=$this->Cliente_model->listar_filiais($cod_cliente);        
        $this->data['dados_quadro']=$this->Cliente_model->listar_quadro($cod_cliente);        
        $this->data['dados_alvara']=$this->Cliente_model->listar_alvaras($cod_cliente);        
        $this->data['dados_usuarios']=$this->Usuario_model->listar_usuarioscliente($cod_cliente,"");        
        $this->data['dados_servicos']=$this->Cliente_model->listar_servicosdocliente($cod_cliente);        
        $this->data['dados_coleta']=$this->Cliente_model->listar_coleta($cod_cliente);        
        $this->data['dados_notificacao']=$this->Notificacao_model->listar_notificacaodocliente($cod_cliente);        
        $this->data['dados_funcionarios']=$this->Funcionario_model->listar_funcionariodocliente($cod_cliente);        
        $this->data['operacao']="editar";
        $this->load->view('Pagina',$this->data);
    }

    public function codigo($cod_cliente) {
        $this->data['dados']=$this->Cliente_model->codigo($cod_cliente);
        echo json_encode(array('op'=>'1', 'cod_interno' => $this->data['dados']["cod_interno"])); 
    }

    public function excluir($cod_cliente) {
        $this->Cliente_model->excluir($cod_cliente);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        
        redirect(site_url('cliente/listar'));
    }

    public function selecionar() { 
        $cliente = explode("~",$this->uri->segment(3));
        $this->session->set_userdata( 'cliente', $cliente[0]); 
        $this->session->set_userdata( 'cliente_uf', $cliente[1]); 
        $this->session->set_userdata( 'cliente_cidade', str_replace("%20"," ",$cliente[2])); 
    }
    
    public function associar_evento($associar,$cod_cliente,$cod_evento) { 
        echo ($this->Cliente_model->associar_evento($associar,$cod_cliente,$cod_evento));
    }
    
    public function evento() {

        if ($this->input->get_post('busca_data') and @$this->input->get_post('busca_data')<>""){
            $data = "01/".$this->input->get_post('busca_data');
        } else {
            $data = "01/".date("m")."/".date("Y");
        }

        $cliente = $this->input->get_post('busca_cliente');
        $evento = $this->input->get_post('busca_evento');

        $this->data['dados']=$this->Cliente_model->evento($data,"",$cliente,$evento);
        $this->data['titulo']="Cliente Evento";
        $this->data['view']="ClienteEvento_view";
        $this->load->view('Pagina',$this->data);
    }

    public function estrategia() {
        $this->data['dados']=$this->Cliente_model->estrategia($this->session->userdata('cliente'));
        $this->data['view']="ClienteEstrategia_view";
        $this->load->view('Pagina',$this->data);
    }

    public function favoritar($cod_cliente) { 
        $this->Cliente_model->favoritar($cod_cliente);
    }

    public function desfavoritar($cod_cliente) {
        $this->Cliente_model->desfavoritar($cod_cliente);
    }

    public function excluir_certificado($cliente,$ext) {
        $arquivo ="./data/".md5($cliente)."/certificado.{$ext}";
        if (unlink ($arquivo)){
            echo json_encode(array('op'=>'1', 'msg' => "Arquivo excluído."));
        }else{
            echo json_encode(array('op'=>'0', 'msg' => "Houve um erro na exclusão."));  
        }
        log_message('debug', "excluir_certificado arquivo:$arquivo");
    }

    public function certidao_federal() {
        $this->load->view('Certidao_federal_view');
    }

    public function comunicar_estrategia($cod_cliente) {
        $this->Cliente_model->comunicar_estrategia($cod_cliente);
        echo json_encode(array('op'=>'1', 'msg' => "Comunicado enviado ({$cod_cliente})"));        
    }
    
    public function relatorio_clienteficha($cod_cliente,$formato="pdf") {

        $this->data['dados']=$this->Cliente_model->editar($cod_cliente);
        $this->data['dados_quadro']=$this->Cliente_model->listar_quadro($cod_cliente);
        $this->data['dados_alvara']=$this->Cliente_model->listar_alvaras($cod_cliente); 

        $this->functions->exportar($this->data,$formato,"Relatorio_clienteficha");
    }
    
    public function relatorio_evento($formato="pdf") {

        $this->data['dados']=$this->Cliente_model->listar("","S");

        $this->functions->exportar($this->data,$formato,"Relatorio_clienteevento");

    }
    
    public function relatorio_cliente($formato="pdf") {

        $this->data['dados']=$this->Cliente_model->listar("","S");

        $this->functions->exportar($this->data,$formato,"Relatorio_cliente");

    }
    
    public function relatorio_entregas($formato="pdf",$cod_cliente="") {

        $this->data['dados']=$this->Arquivo_model->listar(array("cod_cliente" => $cod_cliente));

        $this->functions->exportar($this->data,$formato,"Relatorio_clienteentregas");

    }
    
    public function relatorio_alvaras($formato="pdf") {

        $this->data['dados']=$this->Cliente_model->relatorio_alvaras();

        $this->functions->exportar($this->data,$formato,"Relatorio_clientealvaras");

    }
    
    public function relatorio_quadro($formato="pdf",$cod_cliente="") {

        $this->data['dados']=$this->Cliente_model->relatorio_quadro($cod_cliente);

        $this->functions->exportar($this->data,$formato,"Relatorio_clientequadro");

    }
    
    public function relatorio_regimetributario($formato="pdf") {

        $this->data['dados']=$this->Cliente_model->relatorio_regimetributario();

        $this->functions->exportar($this->data,$formato,"Relatorio_regimetributario");
        
    }
    
    public function relatorio_estrategia($formato="pdf",$cod_cliente) {

        $this->data['dados']=$this->Cliente_model->estrategia($cod_cliente);

        $this->functions->exportar($this->data,$formato,"Relatorio_clienteestrategia");

    }
    
    public function relatorio_certificadosvencimento($formato="pdf") {

        $this->data['dados']=$this->Cliente_model->listar_certificadovencido();

        $this->functions->exportar($this->data,$formato,"Relatorio_certificadosvencimento");

    }
    
    public function relatorio_procuracaovencimento($formato="pdf") {
        
        $this->data['dados']=$this->Cliente_model->listar_procuracao();

        $this->functions->exportar($this->data,$formato,"Relatorio_procuracaovencimento");
    }
    
    
}
