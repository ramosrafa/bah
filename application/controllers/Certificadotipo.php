<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Certificadotipo extends CI_Controller {

    private $data = array(),
            $model = "Certificadotipo_model",
            $titulo = "Tipo Certificado", 
            $view = "certificadotipo_view";
            
    function __construct() {
        parent::__construct();

         //Verifica se esta logado
         $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function listar() {
        
        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."certificadotipo/listar";
        $config['total_rows'] = count($this->certificadotipo_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->certificadotipo_model->listar($inicio); 
        $this->data['operacao']="listar";
        $this->load->view('Pagina',$this->data);
    }

    public function novo() {
        $this->data['operacao']="novo";
        $this->load->view('Pagina',$this->data);
    }

    public function inserir() {
        $this->certificadotipo_model->inserir();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('certificadotipo/listar'));
    }

    public function salvar($cod_certificadotipo) {
        $this->certificadotipo_model->salvar($cod_certificadotipo);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('certificadotipo/listar'));
    }

    public function editar($cod_certificadotipo) {
        $this->data['dados']=$this->certificadotipo_model->editar($cod_certificadotipo);
        $this->data['operacao']="editar";
        $this->load->view('Pagina',$this->data);
    }

    public function excluir($cod_certificadotipo) {
        $this->certificadotipo_model->excluir($cod_certificadotipo);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        
        redirect(site_url('certificadotipo/listar'));
    }

}
