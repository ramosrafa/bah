<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Coleta extends CI_Controller {

    private $data = array(),
            $model = array("Coleta_model","pasta_model","Formulario_model","Arquivo_model","Evento_model"),
            $titulo = "Coleta", 
            $view = "Coleta_view";
            
    function __construct() {
        parent::__construct();

        //Verifica se esta logado
        $this->functions->usuario_logado();

        //Carrega Model
        $this->load->model($this->model);

        //Dados que serão carregados na view
        $this->data['titulo']=$this->titulo;
        $this->data['view']=$this->view;
        
    }

    public function listar() {
        //Paginação        
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url'] = LOCAL."coleta/listar";
        $config['total_rows'] = count($this->Coleta_model->listar(""));
        $config['per_page'] = LIMIT;
        $config['first_link'] = 'Primeiro';
        $config['last_link'] = 'Último';
        $this->mypagination->initialize($config);  
        $this->data['paginacao'] =  $this->mypagination->create_links();
        
        $this->data['busca']=$this->input->get_post('busca');
        $this->data['dados']=$this->Coleta_model->listar($inicio); 
        $this->data['operacao']="listar";
        $this->load->view('Pagina',$this->data);
    }

    public function novo() {
        $this->data['operacao']="novo";
        $this->data['dados_coletacliente']=$this->Coleta_model->coleta_cliente();//Camo clientes
        $this->data['dados_cod_pasta']=$this->pasta_model->listar_select();
        $this->data['dados_formulario']=$this->Formulario_model->listar_select();
        $this->load->view('Pagina',$this->data);
    }

    public function inserir() {
        $this->Coleta_model->inserir();
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('coleta/listar'));
    }

    public function salvar($cod_coleta) {
        $this->Coleta_model->salvar($cod_coleta);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));

        redirect(site_url('coleta/listar'));
    }

    public function enviar($cod_coleta) {
        $this->Coleta_model->enviar($cod_coleta);

        redirect(site_url('coleta/listar'));
    }

    public function editar($cod_coleta) {
        $this->data['dados']=$this->Coleta_model->editar($cod_coleta);
        $this->data['dados_coletacliente']=$this->Coleta_model->coleta_cliente($cod_coleta);//Campo cliente
        $this->data['dados_cod_pasta']=$this->pasta_model->listar_select();
        $this->data['dados_listarcliente']=$this->Coleta_model->listar_cliente($cod_coleta);
        $this->data['dados_arquivos']=$this->Arquivo_model->listar(array("cod_coleta" => $cod_coleta));
        $this->data['operacao']="editar";
        
        //Evita que usuário tipo cliente edite o registro
        if ($this->session->userdata('tipo')=="C") $this->data['operacao']="ver";
        $this->load->view('Pagina',$this->data);
    }

    public function ver($cod_coleta) {
        $this->data['dados']=$this->Coleta_model->ver($cod_coleta);
        $this->data['dados_arquivos']=$this->Arquivo_model->listar(array("cod_coleta" => $cod_coleta,"cod_cliente" => $this->session->userdata('cliente')));
        $this->data['dados_campos']=$this->Formulario_model->listar_campos($this->data['dados']["cod_formulario"]);
        $this->data['operacao']="ver";
        $this->load->view('Pagina',$this->data);
    }

    public function evento($cod_evento,$data_vencimento) {
        $existe = $this->Coleta_model->existe($cod_evento,$data_vencimento);
        
        if ($existe){
            $this->data['dados']=$this->Coleta_model->ver($existe["cod_coleta"]);
            $this->data['dados_arquivos']=$this->Arquivo_model->listar(array("cod_coleta" => $existe["cod_coleta"],"cod_cliente" => $this->session->userdata('cliente')));
            $this->data['dados_campos']=$this->Formulario_model->listar_campos($this->data['dados']["cod_formulario"]);
        } else {
            $cod_coleta = $this->Coleta_model->inserir_evento($cod_evento,$data_vencimento);
            $this->data['dados']=$this->Coleta_model->ver($cod_coleta);
        }
        $this->data['operacao']="ver";
        $this->load->view('Pagina',$this->data);
    }

    public function excluir($cod_coleta) {
        $this->Coleta_model->excluir($cod_coleta);
        $this->functions->registra_log($this->titulo,__FUNCTION__,serialize ($this->input->post()));
        
        redirect(site_url('coleta/listar'));
    }

}
