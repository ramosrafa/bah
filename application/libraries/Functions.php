<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Functions {
 
	private $data = array();
 
    function __construct() {
		$this->CI =& get_instance();
        $this->CI->load->driver('session'); 
	}
 
    function usuario_online() {
		$this->CI->Online_model->registrar();
		return $this->CI->Online_model->contar();
    }

    function registra_ultimologin($cod_usuario){
		$this->CI->Usuario_model->registra_ultimologin($cod_usuario);
	}

    function registra_log($titulo,$function,$post){
		$this->CI->Log_model->inserir($titulo,$function,$post);
	}

	//Verifica se esta logado
    function usuario_logado(){
		if ($this->CI->uri->segment(1)<>"") $this->CI->session->set_userdata( 'login_controller', $this->CI->uri->segment(1));
		if ($this->CI->uri->segment(2)<>"") $this->CI->session->set_userdata( 'login_event', $this->CI->uri->segment(2));
		if (( $this->CI->session->userdata('tipo') !== "C" and $this->CI->session->userdata('tipo') !== "T" ) or ($this->CI->session->userdata('logado') !== true)) {
			session_destroy();
			redirect(site_url('login/'));
		}
	}

    function sql_auditoria($table){
		$sql = ",
				(select cusu_.nome from cad_usuario cusu_ where cusu_.cod_usuario = {$table}.cod_usuario_c) as 'usuario_c',
				date_format({$table}.data_c, '%d/%m/%Y %H:%i') as 'data_c_',
				(select cusu_.nome from cad_usuario cusu_ where cusu_.cod_usuario = {$table}.cod_usuario_a) as 'usuario_a',
				date_format({$table}.data_a, '%d/%m/%Y %H:%i') as 'data_a_'";
		return $sql;
	}

	function table_column($defaut="",$column="",$title="",$width="*",$class="a-acao"){
		if (substr($column,-1,1)=="_") $column = substr($column,0,-1);
		$objeto = $this->CI->uri->segment("1");
		$orderby_column = $this->CI->input->post('orderby_column');
		$orderby_order = $this->CI->input->post('orderby_order')?$this->CI->input->post('orderby_order'):"ASC";

		$order = "";
		if ($orderby_column==$column or ($orderby_column=="" and $defaut=="1")){
			if ($orderby_order=="ASC") $order = "<span class=\"caret\" style=\"border-top: 0;border-bottom: 6px dashed;\"></span>";
			if ($orderby_order=="DESC") $order = "<span class=\"caret\"></span>";
		}
		
		$html = "<th width=\"{$width}\" class=\"{$class}\" data-objeto=\"".LOCAL."{$objeto}\" data-acao=\"listar\" data-orderbycolumn=\"{$column}\">{$title}&nbsp;{$order}</th>";
		return $html;
	}

    function cliente_folder($codigo="") {

		if ($codigo=="") return "";
		
		$folder = './data/'.md5($codigo);

		if (!file_exists ($folder)) mkdir($folder);
		$folder = $folder."/";
        
        $arquivo=$folder."index.html";
        if (!file_exists($arquivo)){
            //echo $arquivo;exit;
            $fp = fopen($arquivo , "w");
            $fw = fwrite($fp, "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body></html>");				
        }
		
		return $folder; 
    }

    function cliente_folderlocal($codigo="") {

		$folder = LOCAL.'data/'.md5($codigo);
		if (!file_exists ($folder)) mkdir($folder);
		$folder = $folder."/";
        
		return $folder; 
    }

    function arquivo_marca($file,$type) {
		// Load the stamp and the photo to apply the watermark to
		$stamp = imagecreatefrompng(LOCAL.LOGO_SISTEMA);

		if($type == 'jpeg') $type = 'jpg';
		switch($type){
			case 'gif':$im = imagecreatefromgif($file); break;
			case 'jpg': $im = imagecreatefromjpeg($file); break;
			case 'png': $im = imagecreatefrompng($file); break;
			default : $im = imagecreatefromjpeg($file);
		}

		// Set the margins for the stamp and get the height/width of the stamp image
		$marge_right = 10;
		$marge_bottom = 10;
		$sx = imagesx($stamp);
		$sy = imagesy($stamp);
		
		// Copy the stamp image onto our photo using the margin offsets and the photo 
		// width to calculate positioning of the stamp. 
		imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));

		if($type == 'jpeg') $type = 'jpg';
		switch($type){
			case 'gif':$im = imagegif($im, $file, 100); break;
			case 'jpg': $im = imagejpeg($im, $file, 100); break;
			case 'png': $im = imagepng($im, $file, 5); break;
			default : $im = imagejpeg($im, $file, 100);
		}
		
	}

	function arquivo_resizemax($file, $max,$type) {
		list($w, $h) = getimagesize($file);
		
		if ($w>$h) {
			$dif = $w-$max;
			$dif = ($dif/$w);
		} else {
			$dif = $h-$max;
			$dif = ($dif/$h);
		}
		$width = $w - ($w*$dif);
		$height = $h - ($h*$dif);
		//echo "$width,$height";exit;
		
		/* calculate new image size with ratio */
		$ratio = max($width/$w, $height/$h);
		$h = ceil($height / $ratio);
		$x = ($w - $width / $ratio) / 2;
		$w = ceil($width / $ratio);
		
		/* read binary data from image file */
		$imgString = file_get_contents($file);
		
		/* create image from string */
		$image = imagecreatefromstring($imgString);
		$tmp = imagecreatetruecolor($width, $height);
		imagecopyresampled($tmp, $image,0, 0,$x, 0,$width, $height,$w, $h);
		
		if($type == 'jpeg') $type = 'jpg';
		switch($type){
			case 'gif': imagegif($tmp, $file, 100); break;
			case 'jpg': imagejpeg($tmp, $file, 100); break;
			case 'png': imagepng($tmp, $file, 5); break;
			default : imagejpeg($tmp, $file, 100);
		}
		
		/* cleanup memory */
		imagedestroy($image);
		imagedestroy($tmp);

		return $file;
	}

	function verifica_feriado($feriados,$data_vencimento,$uf="",$cidade=""){
		$existe=0;
		foreach($feriados as $value){
			if ($value["data"]==date('Y-m-d', $data_vencimento) 
				and (@$value["uf"]==@$uf or @$value["uf"]=="")
				and (@$value["cidade"]==$cidade or @$value["cidade"]=="")
				) {
				$existe=1;
			}
		}
		return $existe;
	}

	function evento_calculavencimento($var,$feriados,$uf="",$cidade="") {

		$data_vencimento=strtotime($var["data_vencimento"]);
		//log_message('debug', "Functions evento_calculavencimento");
		//log_message('debug', "Functions data_vencimento inicial: {$data_vencimento} ".date('Y-m-d', $data_vencimento). " UF:{$uf} Cidade:{$cidade}");

		if ($var["vencimento_meses"]<>"" and $var["vencimento_meses"]<>"0"){
			$meses = $var["vencimento_meses"];
			$data_vencimento=strtotime("+{$meses} month",$data_vencimento);
		} 
		//log_message('debug', "Functions data_vencimento após calculo mês: {$data_vencimento} ".date('Y-m-d', $data_vencimento));

		$dias = $var["vencimento_diastipo"]=="T"?date("t",$data_vencimento):$var["vencimento_dias"];
		//log_message('debug', "Functions data_vencimento DIAS: {$dias} ");

		//echo $dias; exit;
		for ($i=1;$i<=intval($dias)-1;$i++){
			$vferiado = $this->verifica_feriado($feriados,$data_vencimento,$uf,$cidade);
			//echo "<br>vct:".$var["vencimento_diastipo"]."; data:".date('Y-m-d', $data_vencimento)."; W:".date("w", $data_vencimento)."; Feriado:$vferiado";
			if ($var["vencimento_diastipo"]=="T") $data_vencimento=strtotime("+1 day",$data_vencimento);
			if ($var["vencimento_diastipo"]=="C") $data_vencimento=strtotime("+1 day",$data_vencimento);
			if ($var["vencimento_diastipo"]=="U" and date("w", $data_vencimento)<>0 and date("w", $data_vencimento)<>6 and $vferiado==0) $data_vencimento=strtotime("+1 day",$data_vencimento);
			if ($var["vencimento_diastipo"]=="U" and date("w", $data_vencimento)<>0 and date("w", $data_vencimento)<>6 and $vferiado==1) $data_vencimento=strtotime("+2 day",$data_vencimento);
			if ($var["vencimento_diastipo"]=="U" and date("w", $data_vencimento)==0) $data_vencimento=strtotime("+2 day",$data_vencimento);
			if ($var["vencimento_diastipo"]=="U" and date("w", $data_vencimento)==6) $data_vencimento=strtotime("+2 day",$data_vencimento);
		}
		//log_message('debug', "Functions data_vencimento após calculo dias: {$data_vencimento} ".date('Y-m-d', $data_vencimento));

		$vferiado = $this->verifica_feriado($feriados,$data_vencimento,$uf,$cidade);

		if ($vferiado==1 and $var["vencimento_ajuste"]=="A") $data_vencimento = strtotime("-1 day",$data_vencimento);
		if ($vferiado==1 and $var["vencimento_ajuste"]=="P") $data_vencimento = strtotime("+1 day",$data_vencimento);
		if (date("w", $data_vencimento)==0 and $var["vencimento_ajuste"]=="A") $data_vencimento = strtotime("-2 day",$data_vencimento);
		if (date("w", $data_vencimento)==6 and $var["vencimento_ajuste"]=="A") $data_vencimento = strtotime("-1 day",$data_vencimento);
		if (date("w", $data_vencimento)==0 and $var["vencimento_ajuste"]=="P") $data_vencimento = strtotime("+1 day",$data_vencimento);
		if (date("w", $data_vencimento)==6 and $var["vencimento_ajuste"]=="P") $data_vencimento = strtotime("+2 day",$data_vencimento);
		//log_message('debug', "Functions data_vencimento após ajuste último dia útil: {$data_vencimento} ".date('Y-m-d', $data_vencimento));

		return $data_vencimento;
	}

    function arquivo_upload($config="") {
		//print_r($config);echo "<br>TMP_NAME ".$_FILES[$config["input"]]["tmp_name"];echo "<br>NAME ".$_FILES[$config["input"]]["name"];
		//$config['input'] = $input;
		//$config['upload_path'] = $this->session->userdata('folder');
		//$config['file_name'] = $novo_nome;
		//$config['allowed_types'] = 'gif|jpg|jpeg|png';
		//$config['file_ext_tolower'] = true;
		//$config['max_size'] = 5000;
		$retorno = "";

        ////log_message('debug', "arquivo_upload file_name ".$config["file_name"]);
        ////log_message('debug', "arquivo_upload upload_path ".$config["upload_path"]);
        if (!file_exists ($config["upload_path"])) mkdir($config["upload_path"]);
		if( $_FILES ) { // Verificando se existe o envio de arquivos.
			if($_FILES[$config["input"]]['tmp_name']) { // Verifica se o input não está vazio.
				//Deleta se já existir o arquivo com outras extensões
				if (file_exists($config["upload_path"] . $config["file_name"].".jpg")) unlink($config["upload_path"] . $config["file_name"].".jpg");
				if (file_exists($config["upload_path"] . $config["file_name"].".jpeg")) unlink($config["upload_path"] . $config["file_name"].".jpeg");
				if (file_exists($config["upload_path"] . $config["file_name"].".gif")) unlink($config["upload_path"] . $config["file_name"].".gif");
				if (file_exists($config["upload_path"] . $config["file_name"].".png")) unlink($config["upload_path"] . $config["file_name"].".png");
				
				//Verifica o tipo
				$type = strtolower(substr(strrchr($_FILES[$config["input"]]['name'],"."),1));

                ////log_message('debug', "arquivo_upload type: $type");
                
				if($type <> 'gif'){
					//Redimensiona
					$this->arquivo_resizemax( $_FILES[$config["input"]]['tmp_name'], 400,$type);
					
					//Adiciona marca d'água
                    if ($config["imagejpeg"]==true) $this->arquivo_marca($_FILES[$config["input"]]['tmp_name'],$type);
				}

                ////log_message('debug', "arquivo_upload move_uploaded_file 2" );
                
				$data["extension"] = pathinfo($_FILES[$config["input"]]['name'], PATHINFO_EXTENSION);
				$data["tmp_name"] = $_FILES[$config["input"]]['tmp_name'];
				$data["upload_path"] = $config["upload_path"];
				$data["file_name"] = $config["file_name"] .".".$data["extension"] ;
                
                $destination = strtolower ($config["upload_path"] . $config["file_name"] .".".$data["extension"]);
				if( move_uploaded_file( $data["tmp_name"], $destination )) {
                    ////log_message('debug', "arquivo_upload move_uploaded_file destination: $destination");
                    
                    //Converter para jpg
                    if ($config["imagejpeg"]==true){
                        $img_info = getimagesize($destination);
                        $width = $img_info[0];
                        $height = $img_info[1];
                        ////log_message('debug', "arquivo_upload img_info ".$img_info[2]);
                        switch ($img_info[2]) {
                            case IMAGETYPE_GIF  : $src = imagecreatefromgif($destination);  break;
                            case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($destination); break;
                            case IMAGETYPE_PNG  : $src = imagecreatefrompng($destination);  break;
                        }

                        $tmp = imagecreatetruecolor($width, $height);
                        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
                        imagejpeg($tmp, strtolower ($config["upload_path"] . $config["file_name"] .".jpg"));
                    }
					$retorno  = $data;
				} else {			
					$retorno  = false;
				}
				/* cleanup memory */
				//imagedestroy($_FILES[$config["input"]]['tmp_name']);
			}
		}
		return $retorno;
    }        

    function escrever_log($filepath="",$mode="a+",$texto="") {
		$handle = fopen(APPPATH . "logs/{$filepath}", $mode);
		fwrite($handle, $texto."\n");
        fclose($handle);
    }

    function app_push($device="",$platform="Android",$titulo="",$mensagem="") {
        
        if ($titulo=="") $titulo = TITULO." ".date('Y-m-d h:i:s');
        if ($mensagem=="") $mensagem = "Mensagem enviada em ".date('Y-m-d h:i:s');
        
        $url = 'https://gcm-http.googleapis.com/gcm/send';

        $fields = array(
            "registration_ids" => array($device),  
            "data" => array(
				"title" => $titulo,
				"message" => $mensagem,
				'msgcnt' => count($mensagem),
				'timestamp' => date('Y-m-d h:i:s'),
			  ),
        );
        
		$headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,   
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);    
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        ////log_message('debug', "app_push: {$result}");
    }
	
	function after ($ini, $var) {
        if (!is_bool(strpos($var, $ini))) return substr($var, strpos($var,$ini)+strlen($ini));
    }

    function after_last ($ini, $var) {
        if (!is_bool(strrevpos($var, $ini)))
        return substr($var, strrevpos($var, $ini)+strlen($ini));
    }

    function before ($ini, $var) {
        return substr($var, 0, strpos($var, $ini));
    }

    function before_last ($ini, $var) {
        return substr($var, 0, strrevpos($var, $ini));
    }

    function between ($ini, $fim, $var){
        return $this->before ($fim, $this->after($ini, $var));
    }

    function between_last ($ini, $fim, $var){
		return $this->after_last($ini, $this->before_last($fim, $var));
    }
	
    function exportar ($data,$formato,$arquivo){

		////log_message('debug', "Functions.exportar arquivo: $arquivo");

		try {
			if ($formato=="pdf"){

				$mpdf = new mPDF();
				$html = $this->CI->load->view("rels/{$arquivo}_view",$data,TRUE);
				$mpdf->writeHTML($html);
				$mpdf->Output(); // Cria PDF usando 'D' para forçar o download;
				ob_clean(); // Descarta o buffer;            

			} elseif ($formato=="csv"){
				
				header("Content-Description: File Transfer"); 
				header("Content-Disposition: attachment; filename={$arquivo}.csv"); 
				header("Content-Type: application/csv; ");

				$file = fopen('php://output', 'w');

				fputcsv($file, array_keys($data['dados'][0]));
				foreach ($data['dados'] as $line) {
					fputcsv($file,$line); 
				}
				fclose($file); 

			} else{

				$this->CI->load->view("rels/{$arquivo}_view",$data,TRUE);
				
			}
		} catch (Exception $e) {
			echo json_encode(array('op'=>'0', 'msg' => "Exceção capturada: ".$e->getMessage()));
		}

    }
	
    function formatar_email($to="",$subject="",$texto="",$array="") {

		$mail_html = "
						<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" height=\"100%\" width=\"100%\" bgcolor=\"#f6f8f8\" style=\"border-collapse:collapse\">
							<tbody>
								<tr>
									<td>
										<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"100%\" bgcolor=\"#f6f8f8\" style=\"max-width:700px; border:10px color:rgba(223, 17, 105, 1) solid;border-radius:6px !important\">
											<tbody>
												<tr style=\"background-color: #5db230;\">
													<td align=\"center\" style=\"padding:20px;text-align:center\">
														<img src=\"https://www.bah.tripleaie.com.br/web/email.png\" alt=\"TripleA\" border=\"0\" width=\"200px\">
													</td>
												</tr>
											</tbody>
										</table>
										<table cellspacing=\"0\" cellpadding=\"0\" align=\"center\" width=\"100%\" bgcolor=\"#fff\" style=\"max-width:700px; border-radius:6px !important;\">
											<tbody>
												<tr>
													<td align=\"left\" style=\"padding:0px 0 0 15px; background-color:#052055\">
														<h1 style=\"color:rgba(255, 255, 255, 1);text-align:left;font-family:sans-serif;font-size:18px!important;line-height:1.5;font-weight:normal\">{$subject}</h1>
													</td>
												</tr>
												<tr>
													<td align=\"left\" style=\"padding:20px;\">
														<br>
														<p style=\"text-align:left;font-family:sans-serif;font-size:14px!important;line-height:1.5;color:#666666;\">
														{$texto}
														<br>
														<br>Acesse nosso sistema: https://www.bah.tripleaie.com.br
														</p>
														<hr style=\"border-width:1px;border-style:solid;color:#eeeeee;margin:0 10%\">
														<p style=\"font-family:sans-serif;font-size:14px!important;line-height:1.5;color:#666666;\">
														<strong>Atenciosamente</strong>
														<br><strong>TripleA</strong>
														<br>
														<a href=\"http://www.tripleaie.com.br\" title=\"TripleA\" style=\"text-decoration:none\" target=\"_blank\"><strong>http://www.tripleaie.com.br</strong></a>
														</p>
													</td>
												</tr>
											</tbody>
										</table>
						
										<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"100%\" bgcolor=\"#f6f8f8\" style=\"max-width:700px\">
											<tbody>
												<tr>
													<td align=\"center\" style=\"padding:10px;text-align:center\">
														<p style=\"text-align:left;padding:15px;font-family:sans-serif;font-size:11px!important;line-height:1.5;color:#666666;font-weight:lighter\">
														<br />Atenção, não responda este e-mail. Esta mensagem foi enviada através de um endereço que não pode receber mensagens.
														<br />
														<br />TripleA
														<br />http://www.tripleaie.com.br
														<br />".$_SERVER['SERVER_NAME']."
														<br />".ENVIRONMENT.date("jnY-Hi")."
														</p>
													</td>
												</tr>
											</tbody>
										</table>
										
									</td>
								</tr>
							</tbody>
						</table>
		";

		foreach ($array as $key => $value){
			$mail_html = str_replace( $key, $value, $mail_html );
		}
		//echo $mail_html; exit;

		//Log do Email
        $this->escrever_log ('email-log-' . date('Y-m-d') . '.php',"a+","{$to}\n{$subject}\n{$mail_html}");
		
		return $mail_html; 
    }

}

