<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Myemail {

    public $cod_cliente;
    public $from_name;
    public $from_email;
    public $to_name;
    public $to_email;
    public $subject;
    public $body;
    public $attachment=[];
    public $custom;

    function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->driver('session'); 
	}

    public function cod_cliente( $cod_cliente ) {
        $this->cod_cliente = $cod_cliente;
    }

    public function from( $name, $email ) {
        $this->from_name = $name;
        $this->from_email = $email;
    }

    public function to( $name, $email ) {
        $this->to_name = $name;
        
        $this->to_email = $email;
        $this->to_email = ENVIRONMENT<>'production'?$this->CI->session->userdata('email'):$email;
        if($this->to_email=="admin") $this->to_email = EMAIL_NR;
        if($this->to_email=="") $this->to_email = EMAIL_NR;
        $this->to_email = trim($this->to_email);
    }

    public function subject( $subject ) {
        $this->subject = $subject;
    }

    public function body( $val ) {
        $this->body = $val;
    }

    public function attachment( $content=NULL, $name=NULL ) {
        $this->attachment[] = array("content" => base64_encode(file_get_contents($content)),"name" => $name);
    }

    public function custom( $array ) {

        //Altera  o assunto
        foreach ($array as $key => $value){
            $this->subject = str_replace( $key, $value, $this->subject );
        }

        //Altera  o corpo
        foreach ($array as $key => $value){
			$this->body = str_replace( $key, $value, $this->body );
        }

        $this->body =   "
						<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" height=\"100%\" width=\"100%\" style=\"border-collapse:collapse; background-color:#F4F5F5\">
							<tbody>
								<tr>
                                    <td>

										<table cellspacing=\"0\" cellpadding=\"0\" align=\"center\" width=\"100%\" style=\"max-width:700px; border-radius:6px !important;\">
											<tbody>
												<tr>
													<td align=\"left\" style=\"padding:20px;\">
                                                        <img src=\"https://www.bah.tripleaie.com.br/web/email.png\" alt=\"TripleA\" border=\"0\">
													</td>
												</tr>
											</tbody>
										</table>
						
										<table cellspacing=\"0\" cellpadding=\"0\" align=\"center\" width=\"100%\" bgcolor=\"#fff\" style=\"max-width:700px; border-radius:6px !important;\">
											<tbody>
												<tr>
													<td align=\"left\" style=\"padding:20px;\">
														<p style=\"text-align:left;font-family:sans-serif;font-size:14px!important;line-height:1.5;color:#666666;\">
                                                        ".$this->body."
                                                        <br>
                                                        <br>Para maiores dúvidas, entre em contato direto com seu canal de atendimento Triple A.
														</p>
													</td>
												</tr>
											</tbody>
										</table>
						
										<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"100%\" style=\"max-width:700px\">
											<tbody>
												<tr>
													<td align=\"center\" style=\"padding:10px;text-align:center\">
														<p style=\"text-align:left;padding:15px;font-family:sans-serif;font-size:11px!important;line-height:1.5;color:#666666;font-weight:lighter\">
														O BAH Business Anywhere é um produto exclusivo TRIPLE A criado para que nossa comunicação com você seja muito mais segura.
														<br />
														<br />TripleA
														<br />http://www.tripleaie.com.br
														<br />".$_SERVER['SERVER_NAME']."
														<br />".ENVIRONMENT.date("jnY-Hi")."
														</p>
													</td>
												</tr>
											</tbody>
										</table>
										
									</td>
								</tr>
							</tbody>
						</table>
		                ";
    }

    public function enviar(){

        $curl = curl_init();

        $postfields = [
            "sender" => [
                        "name" => $this->from_name,
                        "email" => $this->from_email,
                        ],
            "to" => [
                        [
                        "name" => $this->to_name,
                        "email" => $this->to_email,
                        ]
                    ],
            "subject" => $this->subject,
            "attachment" => ($this->attachment)?$this->attachment:NULL,
            "htmlContent" => $this->body,
        ];

        //Chave BAH: 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
        //Chave CA: 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
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.sendinblue.com/v3/smtp/email",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($postfields),
        CURLOPT_HTTPHEADER => array(
            "accept: application/json",
            "api-key: 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",
            "content-type: application/json"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        
        //Grava registro do envio
        $messageId = json_decode($response)->messageId;
        $this->CI->Email_model->inserir($this->cod_cliente,$messageId,NULL,$this->from_name,$this->from_email,$this->to_name,$this->to_email,$this->subject);

        //log_message('debug', "Myemail enviar from: ".$this->from_email." to: ".$this->to_email." RESPONSE:{$response} ERR {$err} POST: ".json_encode($postfields));

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
    
    /*
    public function log($id){

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.sendinblue.com/v3/smtp/emails?messageId=$id",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "accept: application/json",
            "api-key: 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",
            "content-type: application/json"
        ),
        ));
 
        $response = curl_exec($curl);
        $err = curl_error($curl);
        //var_dump(json_decode($response));exit;
        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return json_decode($response)->transactionalEmails[0] ?? null;
        }
    }
    
    public function status($id=""){

        if ($id=="") return "Não localizado";

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.sendinblue.com/v3/smtp/emails/$id",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "accept: application/json",
            "api-key: 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",
            "content-type: application/json"
        ),
        ));
 
        $response = curl_exec($curl);
        $err = curl_error($curl);
        //var_dump(json_decode($response));exit;
        curl_close($curl);

        if ($err) return "cURL Error #:" . $err;

        $email = json_decode($response);

        if (!$email->email) return "Não localizado";

        foreach ($email->events as $evento) {
            if ($evento->name == 'opened')
                return 'Aberto '.date('d/m/Y H:i', strtotime($evento->time));
        }
        foreach ($email->events as $evento) {
            if ($evento->name == 'delivered')
                return 'Entregue '.date('d/m/Y H:i', strtotime($evento->time));
        }
        foreach ($email->events as $evento) {
            if ($evento->name == 'sent')
                return 'Enviado';
        }

        // Se não parar em nenhum
        return "Não localizado";
    }

    public function atualiza_status($id=""){
        $this->CI->Email_model->atualiza($id,$status);       
    }
    */
}