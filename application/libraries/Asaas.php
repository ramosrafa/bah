<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Asaas {
 
	private $data = array();
 
    function __construct() {
        $this->CI =& get_instance();
	}
 
    function curl($url,$post="") {

        $this->CI->functions->escrever_log ('curl-log-' . date('Y-m-d') . '.php',"a+","url:".ASAAS_URL.$url." ; key:".ASAAS_KEY);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, ASAAS_URL.$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","access_token:".ASAAS_KEY));

        if ($post<>"") {
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
        }

        $response = curl_exec($ch);

        $this->CI->functions->escrever_log ('curl-log-' . date('Y-m-d') . '.php',"a+","url:".ASAAS_URL.$url." ; key:".ASAAS_KEY." ; CURLINFO_HTTP_CODE:".curl_getinfo($ch, CURLINFO_HTTP_CODE)." ; Erro:".curl_error($ch));

        curl_close($ch);

        return($response);

    }
    
    function executar($funcao) {
        //$this->$funcao();
        $this->contas_listar();
    }

    function contas_listar() {

        //Criar conta
        echo "<br>Criar contas:<br>";
        $post = "{
            \"name\": \"ContaÁgil\",
            \"email\": \"contato@contaagil.com\",
            \"cpfCnpj\": \"28474006000110\",
            \"companyType\": \"MEI\",
            \"phone\": \"\",
            \"mobilePhone\": \"\",
            \"address\": \"\",
            \"addressNumber\": \"\",
            \"complement\": \"\",
            \"province\": \"Canoas\",
            \"postalCode\": \"\",
        }";
        $this->curl("accounts",$post );
        
        $post = "{
            \"name\": \"Gane\",
            \"email\": \"ramos@gane.com.br\",
            \"cpfCnpj\": \"08213935000168\",
            \"companyType\": \"MEI\",
            \"phone\": \"\",
            \"mobilePhone\": \"\",
            \"address\": \"\",
            \"addressNumber\": \"\",
            \"complement\": \"\",
            \"province\": \"Canoas\",
            \"postalCode\": \"\",
        }";
        $this->curl("accounts",$post );
        
        //Listar contas
        echo "<br>Listar contas:<br>";
        $curl = $this->curl("accounts","");

        var_dump(json_decode($curl));

    }

    function assinatura_criar() {

        //Criar conta
        echo "<br>assinatura_criar:<br>";
        $post = "{
            \"customer\": \"{CUSTOMER_ID}\",
            \"billingType\": \"BOLETO\",
            \"nextDueDate\": \"2017-05-15\",
            \"value\": 19.9,
            \"cycle\": \"MONTHLY\",
            \"description\": \"Assinatura Plano Pró\",
            \"discount\": {
              \"value\": 10,
              \"dueDateLimitDays\": 0
            },
            \"fine\": {
              \"value\": 1
            },
            \"interest\": {
              \"value\": 2
            }
          }";
        $curl = $this->curl("subscriptions",$post );

        var_dump(json_decode($curl));

    }

    function boleto_gerar() {

        //Gerar boleto
        echo "<br>Boleto:<br>";
        $post = "{
            \"customer\": \"cus_000002212637\",
            \"name\": \"rafael ramos\",
            \"billingType\": \"BOLETO\",
            \"dueDate\": \"2020-06-10\",
            \"value\": 101,
            \"description\": \"Pedido 056984\",
            \"externalReference\": \"056984\",
            \"discount\": {
            \"value\": 10,
            \"dueDateLimitDays\": 0
            },
            \"fine\": {
            \"value\": 1
            },
            \"interest\": {
            \"value\": 2
            },
            \"postalService\": false
            }";

        var_dump($this->curl("payments",$post));

        //Gerar cobrança cartão
        $post = "{
            \"customer\": \"cus_000002212637\",
            \"billingType\": \"CREDIT_CARD\",
            \"dueDate\": \"2020-03-15\",
            \"value\": 20,
            \"description\": \"Pedido 056984\",
            \"externalReference\": \"056984\",
            \"creditCard\": {
            \"holderName\": \"marcelo h almeida\",
            \"number\": \"5162306219378829\",
            \"expiryMonth\": \"05\",
            \"expiryYear\": \"2021\",
            \"ccv\": \"318\"
            },
            \"creditCardHolderInfo\": {
            \"name\": \"Marcelo Henrique Almeida\",
            \"email\": \"marcelo.almeida@gmail.com\",
            \"cpfCnpj\": \"24971563792\",
            \"postalCode\": \"89223-005\",
            \"addressNumber\": \"277\",
            \"addressComplement\": null,
            \"phone\": \"4738010919\",
            \"mobilePhone\": \"47998781877\"
            \"split\": [
                {
                \"walletId\": \"48548710-9baa-4ec1-a11f-9010193527c6\",
                \"fixedValue\": 20.00
                },
                {
                \"walletId\": \"0b763922-aa88-4cbe-a567-e3fe8511fa06\",
                \"percentualValue\": 10.00
                }
            }
            }";

        echo "<br>Cartão:<br>";
        var_dump($this->curl("payments",$post));

    }

    function cliente_listar() {

        //Lista cliente
        echo "<br>cliente_listar:<br>";
        var_dump($this->curl("customers",""));
    }

}

