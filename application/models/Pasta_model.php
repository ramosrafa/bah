<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Pasta_model extends CI_Model {
    
    private $data = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	//Retorna a quantidade total de registros da tabela
    function contar(){
		return $this->db->count_all('cad_pasta');
	}

    public function editar($cod_pasta) {
		$this->db->select(" 'pasta.editar',
						  	cpas.cod_pasta,
							cpas.nome,
                            cpas.texto,
                            cpas.icone".$this->functions->sql_auditoria("cpas")
						);
        
        $this->db->from('cad_pasta cpas');
        
		$this->db->where("cod_pasta = '{$cod_pasta}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'pasta.listar',
						  	cpas.cod_pasta,
							cpas.nome,
                            cpas.icone"
							,FALSE
						);
        
        $this->db->from('cad_pasta cpas');

        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("cpas.nome like '%{$busca}%'");
        
        $orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"nome";
        $orderby_order = $this->input->get_post('orderby_order');
        $this->db->order_by($orderby_column, $orderby_order);

        if (LIMIT<> 0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_select() {
		$this->db->select(" 'pasta.listar',
						  	cpas.cod_pasta,
							cpas.nome,
                            cpas.icone"
							,FALSE
						);
        
        $this->db->from('cad_pasta cpas');

        $this->db->order_by("cpas.nome", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_pastacliente($cod_cliente) {
		$this->db->select(" 'pasta.listar_pastacliente',
						  	cpas.cod_pasta,
							cpas.nome,
                            cpas.icone,
                            (select count(*) from cad_arquivo car_ where car_.cod_pasta = cpas.cod_pasta and car_.cod_cliente = '{$cod_cliente}') as 'qtd'"
							,FALSE
						);
        
        $this->db->from('cad_pasta cpas');
        $this->db->join('cad_usuariopasta cupas', "(cupas.cod_pasta = cpas.cod_pasta and cupas.cod_usuario = '".$this->session->userdata('cod_usuario')."')",'inner');

		$query = $this->db->get(); 
        return $query->result_array();    
    }

    public function listar_pastausuario($cod_usuario) {
		$this->db->select(" 'pasta.listar_pastausuario',
						  	cpas.cod_pasta,
							cpas.nome,
                            cpas.icone"
							,FALSE
						);
        
        $this->db->from('cad_pasta cpas');
        
        if ($cod_usuario<>"") {
            $this->db->join('cad_usuariopasta cupas', "(cupas.cod_pasta = cpas.cod_pasta and cupas.cod_usuario = '{$cod_usuario}')",'inner');
        }
           
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function inserir() {
        
        $nome = $this->input->get_post('nome');
        $texto = $this->input->get_post('texto');
        $icone = $this->input->get_post('icone');

        $this->data["nome"]=$nome;
        $this->data["texto"]=$texto;
        $this->data["icone"]=$icone;
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_pasta', $this->data);

        
    }

    public function salvar($cod_pasta) {
        
        $nome = $this->input->get_post('nome');
        $texto = $this->input->get_post('texto');
        $icone = $this->input->get_post('icone');

        $this->data["nome"]=$nome;
        $this->data["texto"]=$texto;
        $this->data["icone"]=$icone;
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->where('cod_pasta = ',$cod_pasta);
		$this->db->update('cad_pasta', $this->data);
        
    }

    public function excluir($cod_pasta) {

        $this->db->where("cod_pasta", $cod_pasta);
		$this->db->delete('cad_pasta');
        
    }

    //JSON

    public function json_pastalistar($cod_usuario="") {
        
		$this->db->select(" 'json_pastalistar',
						  	cpas.cod_pasta,
							cpas.nome,
                            cpas.icone"
							,FALSE
						);
        
        $this->db->from('cad_pasta cpas');
        
        if ($cod_usuario<>"") {
            $this->db->join('cad_usuariopasta cupas', "(cupas.cod_pasta = cpas.cod_pasta and cupas.cod_usuario = '{$cod_usuario}')",'inner');
        }
           
		$query = $this->db->get(); 
		return json_encode($query->result_array());    
    }    
}

