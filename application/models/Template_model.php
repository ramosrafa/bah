<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Template_model extends CI_Model {
    
    private $data = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	//Retorna a quantidade total de registros da tabela
    function contar(){
		return $this->db->count_all('cad_template');
	}

    public function editar($cod_template) {
		$this->db->select(" 'template.editar',
						  	ctem.cod_template,
							ctem.nome,
							ctem.titulo,
                            ctem.texto,
                            ctem.obs".$this->functions->sql_auditoria("ctem")
						);
        
        $this->db->from('cad_template ctem');
        
		$this->db->where("cod_template = '{$cod_template}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'template.listar',
						  	ctem.cod_template,
							ctem.nome,
                            ctem.titulo,
                            ctem.obs"
							,FALSE
						);
        
        $this->db->from('cad_template ctem');

        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("ctem.nome like '%{$busca}%'");
        
        $orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"cod_template";
        $orderby_order = $this->input->get_post('orderby_order');
        $this->db->order_by($orderby_column, $orderby_order);

        if (LIMIT<>0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_select() {
		$this->db->select(" 'template.listar',
						  	ctem.cod_template,
							ctem.nome"
							,FALSE
						);
        
        $this->db->from('cad_template ctem');

        $this->db->order_by("ctem.nome", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function inserir() {
        
        $nome = $this->input->get_post('nome');
        $titulo = $this->input->get_post('titulo');
        $texto = $this->input->get_post('texto');
        $obs = $this->input->get_post('obs');

        $this->data["nome"]=$nome;
        $this->data["titulo"]=$titulo;
        $this->data["texto"]=$texto;
        $this->data["obs"]=$obs;
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_template', $this->data);
       
    }

    public function salvar($cod_template) {
        
        $nome = $this->input->get_post('nome');
        $titulo = $this->input->get_post('titulo');
        $texto = $this->input->get_post('texto');
        $obs = $this->input->get_post('obs');

        //$this->data["nome"]=$nome;
        $this->data["titulo"]=$titulo;
        $this->data["texto"]=$texto;
        $this->data["obs"]=$obs;
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->where('cod_template = ',$cod_template);
		$this->db->update('cad_template', $this->data);
        
    }

    public function excluir($cod_template) {

        //$this->db->where("cod_template", $cod_template);
		//$this->db->delete('cad_template');
        
    }
}

