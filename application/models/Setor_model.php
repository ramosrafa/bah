<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Setor_model extends CI_Model {
    
    private $data = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	//Retorna a quantidade total de registros da tabela
    function contar(){
		return $this->db->count_all('cad_setor');
	}

    public function editar($cod_setor) {
		$this->db->select(" 'setor.editar',
						  	cset.cod_setor,
							cset.nome".$this->functions->sql_auditoria("cset")
						);
        
        $this->db->from('cad_setor cset');
        
		$this->db->where("cod_setor = '{$cod_setor}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'setor.listar',
						  	cset.cod_setor,
							cset.nome"
							,FALSE
						);
        
        $this->db->from('cad_setor cset');

        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("cset.nome like '%{$busca}%'");
        
        $orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"nome";
        $orderby_order = $this->input->get_post('orderby_order');
        $this->db->order_by($orderby_column, $orderby_order);

        if (LIMIT<> 0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_select() {
		$this->db->select(" 'setor.listar_select',
						  	cset.cod_setor,
							cset.nome"
							,FALSE
						);
        
        $this->db->from('cad_setor cset');

        $this->db->order_by("cset.nome", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function inserir() {
        
        $nome = $this->input->get_post('nome');

        $this->data["nome"]=$nome;
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_setor', $this->data);

        
    }

    public function salvar($cod_setor) {
        
        $nome = $this->input->get_post('nome');

        $this->data["nome"]=$nome;
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->where('cod_setor = ',$cod_setor);
		$this->db->update('cad_setor', $this->data);
        
    }

    public function excluir($cod_setor) {

        $this->db->where("cod_setor", $cod_setor);
		$this->db->delete('cad_setor');
        
    }
 
}
