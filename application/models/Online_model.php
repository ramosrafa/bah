<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Online_model extends CI_Model {
    
    private $data = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	//Retorna a quantidade total de registros da tabela
    function contar(){
		return $this->db->count_all('cad_online');
	}

	//Inserir perfil logado
    public function registrar() {
		if( $this->session->userdata('logado') == true ) {
			$timestamp=time(); 
			$timeout=time()-60;

			$this->db->query("insert into cad_online (cod_usuario, timestamp) values('".$this->session->userdata('cod_usuario')."', '{$timestamp}') on duplicate key update timestamp='{$timestamp}'");
			$this->db->query("delete from cad_online where timestamp<{$timeout}");

		}
    }
}

