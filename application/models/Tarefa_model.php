<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Tarefa_model extends CI_Model {
    
    private $data = array(),
            $data_etapas = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	//Retorna a quantidade total de registros da tabela
    function contar(){
		return $this->db->count_all('cad_tarefa');
	}

    public function editar($cod_tarefa) {
		$this->db->select(" 'tarefa.editar',
						  	ctar.cod_tarefa,
							ctar.cod_pasta,
							ctar.nome,
                            ctar.previsao,
                            ctar.texto,
                            ctar.valor".$this->functions->sql_auditoria("ctar")
						);
        
        $this->db->from('cad_tarefa ctar');
        
		$this->db->where("cod_tarefa = '{$cod_tarefa}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'tarefa.listar',
                            ctar.cod_tarefa,
                            ctar.cod_pasta,
							ctar.nome,
							ctar.previsao,
                            ctar.valor,
                            cpas.nome as 'cod_pasta'"
							,FALSE
						);
        
        $this->db->from('cad_tarefa ctar');
        $this->db->join('cad_pasta cpas', "ctar.cod_pasta = cpas.cod_pasta",'left');

        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("ctar.nome like '%{$busca}%'");
        
        $orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"nome";
        $orderby_order = $this->input->get_post('orderby_order');
        $this->db->order_by($orderby_column, $orderby_order);

        if (LIMIT<>0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    
    public function listar_select() {
		$this->db->select(" 'tarefa.listar_select',
						  	ctar.cod_tarefa,
							ctar.nome"
							,FALSE
						);
        
        $this->db->from('cad_tarefa ctar');

        $this->db->order_by("ctar.nome", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_etapas($cod_tarefa) {
		$this->db->select(" 'tarefa.listar_etapasservico',
						  	cte.cod_tarefaetapa,
						  	cte.cod_tarefa,
						  	cte.tipo,
							cte.ordem,
							cte.nome,
                            cte.arquivo,
                            cte.arquivo_original,
                            cte.previsao,
                            ctar.texto"
							,FALSE
						);
        
        $this->db->from('cad_tarefaetapa cte');
        $this->db->join('cad_tarefa ctar', "ctar.cod_tarefa = cte.cod_tarefa",'left');
        $this->db->where("cte.cod_tarefa = '{$cod_tarefa}'");
        $this->db->order_by("cte.ordem*1", "asc",FALSE);
        
		$query = $this->db->get(); 
        $dados = $query->result_array();

        $retorno = array();        
        foreach($dados as $value){
            $arquivo = "./data/tarefa/".$value["arquivo"];
            
            if (!file_exists($arquivo)){
                $arquivo = "";
            }
            
            array_push($retorno, array('cod_tarefaetapa'=>$value["cod_tarefaetapa"],
            'cod_tarefa'=>$value["cod_tarefa"],
            'tipo'=>$value["tipo"],
            'ordem'=>$value["ordem"],
            'nome'=>$value["nome"],
            'previsao'=>$value["previsao"],
            'arquivo'=>$value["arquivo"],
            'arquivo_original'=>$value["arquivo_original"],
            'texto'=>$value["texto"]
            ));
        }
        return $retorno;

    }

    public function inserir() {
        
        $this->data["cod_pasta"]=$this->input->get_post('cod_pasta');
        $this->data["nome"]=$this->input->get_post('nome');
        $this->data["previsao"]=$this->input->get_post('previsao');
        $this->data["texto"]=$this->input->get_post('texto');
        $this->data["valor"]=$this->input->get_post('valor');
        
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_tarefa', $this->data);

        $insert_id = $this->db->insert_id();
        
        $this->salvar_etapas($insert_id);
        
    }

    public function salvar($cod_tarefa) {
        
        $this->data["cod_pasta"]=$this->input->get_post('cod_pasta');
        $this->data["nome"]=$this->input->get_post('nome');
        $this->data["previsao"]=$this->input->get_post('previsao');
        $this->data["texto"]=$this->input->get_post('texto');
        $this->data["valor"]=$this->input->get_post('valor');

        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->where('cod_tarefa = ',$cod_tarefa);
		$this->db->update('cad_tarefa', $this->data);
        
        $this->salvar_etapas($cod_tarefa);
    }

    public function salvar_etapas($cod_tarefa) {
        $this->db->where("cod_tarefa", $cod_tarefa);
		$this->db->delete('cad_tarefaetapa');
        
        $etapa_tipo = $this->input->get_post("etapa_tipo");
        $etapa_ordem = $this->input->get_post("etapa_ordem");
        $etapa_nome = $this->input->get_post("etapa_nome");
        $etapa_previsao = $this->input->get_post("etapa_previsao");
        $etapa_arquivo = $this->input->get_post("etapa_arquivo"); 
        $etapa_arquivoanexado = $this->input->get_post("etapa_arquivoanexado"); 
        $etapa_arquivoanexadooriginal = $this->input->get_post("etapa_arquivoanexadooriginal");
        foreach( $etapa_nome as $key => $n ) {
            if ($etapa_nome[$key]<>""){
                unset($data_etapas);
                $data_etapas = array();

                $this->data_etapas["cod_tarefa"]=$cod_tarefa;
                $this->data_etapas["tipo"]=$etapa_tipo[$key];
                $this->data_etapas["ordem"]=$etapa_ordem[$key];
                $this->data_etapas["nome"]=$etapa_nome[$key];
                $this->data_etapas["previsao"]=$etapa_previsao[$key];

                $this->db->insert('cad_tarefaetapa', $this->data_etapas);

                $insert_id = $this->db->insert_id();

                //Upload do arquivo
                if ($_FILES['etapa_arquivo']['name'][$key]){
                    $name = $_FILES['etapa_arquivo']['name'][$key];
                    //$size = $_FILES['etapa_arquivo']['size'][$key];    
                    $tmp_name = $_FILES['etapa_arquivo']['tmp_name'][$key];
                    $ext = pathinfo($name, PATHINFO_EXTENSION);        

                    $folder = './data/tarefa/';
                    $nome_arquivo = "tarefaetapa{$cod_tarefa}-{$insert_id}.{$ext}";
                    $arquivo = strtolower("{$folder}tarefaetapa{$cod_tarefa}-{$insert_id}.{$ext}");

                    move_uploaded_file($tmp_name, $arquivo);
                    
                    $this->db->query("update cad_tarefaetapa set arquivo = '{$nome_arquivo}', arquivo_original = '{$name}' WHERE `cod_tarefaetapa` = '{$insert_id}'");
                    log_message('debug', "tarefa salvar_etapas:$arquivo");
                }elseif($etapa_arquivoanexado[$key]<>""){
                    
                    $this->db->query("update cad_tarefaetapa set arquivo = '{$etapa_arquivoanexado[$key]}', arquivo_original = '{$etapa_arquivoanexadooriginal[$key]}' WHERE `cod_tarefaetapa` = '{$insert_id}'");
                    log_message('debug', "tarefa salvar_etapas existente:$etapa_arquivoanexado[$key]");
                }

            }     
        }     
        //print_r($this->data_etapas);exit;
        //if (!empty($this->data_etapas)) $this->db->insert_batch('cad_tarefaetapa', $this->data_etapas);
    }

    public function excluir($cod_tarefa) {

        $this->db->where("cod_tarefa", $cod_tarefa);
		$this->db->delete('cad_tarefaetapa');
        
        $this->db->where("cod_tarefa", $cod_tarefa);
		$this->db->delete('cad_tarefa');
        
    }

       
}

