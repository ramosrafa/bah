<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Entrega_model extends CI_Model {
    
    private $data = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

    //Insere novo registro
    public function enviar() {
        $data_atual = date("Y-m-d");
        $cod_cliente = "";
        $obs = "";
        $agrupador = date("YmdGi");
        $arquivos = [];

        //Feriados
        $this->db->select(" 'entrega.enviar',
                            cfer.cod_feriado as 'cod',
                            cfer.nome,
                            cfer.uf,
                            cfer.cidade,
                            cfer.data,  
                            date_format(cfer.data, '%d/%m/%Y') as 'data_'
                            "
        );

        $this->db->from('cad_feriado cfer');

        $query = $this->db->get(); 
        $feriados = $query->result_array();        

        foreach($_FILES['files']['name'] as $key => $array_value){     

            $erro = "";
            $name = $_FILES['files']['name'][$key];
            $size = $_FILES['files']['size'][$key];    
            $tmp_name = $_FILES['files']['tmp_name'][$key];    

            $file = explode(".", $name);
            $arr = explode("-", $file[0]);
            
            //Valida nome do arquivo
            if (count($arr)<3 or count($arr)>4) {
                $retorno[] = array("op" => "0","nome" => $name,"cliente" => "","pasta" => "","data_vencimento" => "","erro" => "Nome inválido");
                continue;
            }
            
            $arquivo_codigo = intval($arr[0]);
            $arquivo_evento = $arr[1];
            $arquivo_data = $arr[2];
            
            //Trata data 
            if (strlen($arquivo_data)==6) {
                $arquivo_data = "01{$arquivo_data}";
            }
            $arquivo_datamanual = @$arr[3];

            if (strlen($arquivo_data)<>8) {
                $retorno[] = array("op" => "0","nome" => $name,"cliente" => "","pasta" => "","data_vencimento" => "","erro" => "Data inválida");
                continue;
            }
            
            //Trata cliente
            $cliente = $this->Cliente_model->editar("",$arquivo_codigo);
            if (!$cliente) {
                $retorno[] = array("op" => "0","nome" => $name,"cliente" => "","pasta" => "","data_vencimento" => "","erro" => "Cliente inválido");
                continue;
            }
            $cod_cliente .= $cliente["cod_cliente"].",";
            
            //Trata evento
            $evento = $this->Evento_model->editar("",$arquivo_evento);
            if (!$evento) {
                $retorno[] = array("op" => "0","nome" => $name,"cliente" => "","pasta" => "","data_vencimento" => "","erro" => "Evento inválido"); 
                continue;
            } 

            //Trata se o cliente possui esse evento
            $clienteevento = $this->Cliente_model->cliente_possuievento($cliente["cod_cliente"],$evento["cod_evento"]);
            if (!$clienteevento) {
                $retorno[] = array("op" => "0","nome" => $name,"cliente" => "","pasta" => "","data_vencimento" => "","erro" => "Evento não vinculado"); 
                continue;
            } 

            //Trata competência
            $competencia = substr($arquivo_data,4,4) . "-" . substr($arquivo_data,2,2) . "-" . substr($arquivo_data,0,2);
            $data_vencimento = $competencia;

            //Trata vencimento manual
            if (count($arr)==4){
                $data_vencimento = substr($arquivo_datamanual,4,4) . "-" . substr($arquivo_datamanual,2,2) . "-" . substr($arquivo_datamanual,0,2);
            } 

            //Calcula vencimento
            if ((count($arr)<4) and ($evento["vencimento"]=="S")){
                $var['data_vencimento'] = $data_vencimento;
                $var['vencimento_dias'] = $evento['vencimento_dias'];
                $var['vencimento_diastipo'] = $evento['vencimento_diastipo'];
                $var['vencimento_meses'] = $evento['vencimento_meses'];
                $var['vencimento_ajuste'] = $evento['vencimento_ajuste'];

                $data_vencimento = $this->functions->evento_calculavencimento($var,$feriados,$cliente["uf"],$cliente["cidade"]);
                $data_vencimento = date('Y-m-d',$data_vencimento);
            }

            //Trata situação vencimento
            $situacao = "S";
            if (strtotime($data_vencimento)<strtotime($data_atual)) $situacao = "danger";
            if (strtotime($data_vencimento)==strtotime($data_atual)) $situacao = "warning";
            if (strtotime($data_vencimento)>strtotime($data_atual)) $situacao = "success";
            if ($data_vencimento == "0000-00-00") $situacao = "success";
                            
            if ((strtotime($data_vencimento)<strtotime($data_atual))and($data_vencimento <> "0000-00-00")) {
                $erro = "Data menor que atual";
            }

            $retorno[] = array("op" => ($erro=="")?"1":"2","nome" => $name,"cliente" => $cliente["nome"],"pasta" => $evento["pasta"],"data_vencimento" => $data_vencimento,"erro" => $erro);

            //Sobe o arquivo
            $folder = $this->functions->cliente_folder($arquivo_codigo);

            $arquivo = md5($name);
            $arquivo = "{$folder}$arquivo";
            
            $attachment = LOCAL."data/".md5($arquivo_codigo)."/".md5($name);

            move_uploaded_file($tmp_name, $arquivo);

            $arquivos[] = array("cod_cliente" => $cliente["cod_cliente"],"attachment" => $attachment,"arquivo" => $arquivo,"nome_original" => $name,"nome_evento" => $evento["nome"],"vencimento" => $data_vencimento);

            $this->db->query("insert into cad_arquivo (
                                                    cod_cliente, 
                                                    cod_evento,
                                                    cod_pasta,
                                                    origem,
                                                    nome_original,
                                                    arquivo,
                                                    data_competencia,
                                                    data_vencimento,
                                                    situacao,
                                                    obs,
                                                    agrupador,
                                                    cod_usuario_c,
                                                    data_c,
                                                    cod_usuario_a,
                                                    data_a)
                                                    values(
                                                    '".$cliente["cod_cliente"]."',
                                                    '".$evento["cod_evento"]."',
                                                    '".$evento["cod_pasta"]."',
                                                    'E',
                                                    '".strtoupper($name)."',
                                                    '".md5($name)."',
                                                    '".$competencia."',
                                                    '".$data_vencimento."',
                                                    '".$situacao."',
                                                    '".$obs."',
                                                    '".$agrupador."',
                                                    '".$this->session->userdata('cod_usuario')."',
                                                    '".date('Y-m-d H:i:s')."',
                                                    '".$this->session->userdata('cod_usuario')."',
                                                    '".date('Y-m-d H:i:s')."'

                                                    )
                                                    on duplicate key update 
                                                    cod_cliente = '".$cliente["cod_cliente"]."',
                                                    cod_evento = '".$evento["cod_evento"]."',
                                                    cod_pasta = '".$evento["cod_pasta"]."',
                                                    origem = 'E',
                                                    nome_original = '".strtoupper($name)."',
                                                    arquivo = '".md5($name)."',
                                                    data_competencia = '".$competencia."',
                                                    data_vencimento = '".$data_vencimento."',
                                                    obs = '".$obs."',
                                                    agrupador = '".$agrupador."',
                                                    cod_usuario_a = '".$this->session->userdata('cod_usuario')."',
                                                    data_a = '".date('Y-m-d H:i:s')."'

                                                    ");

            $cod_arquivo = $this->db->insert_id();                    
        }        
        
        //Envia email e push
        if ($arquivos){


            $template = $this->Template_model->editar(7);
            
            $this->db->select(" 'entrega.enviar',
                                cucli.cod_cliente,
                                cusu.cod_usuario,
                                cusu.email,
                                cusu.nome,
                                cusu.device_id,
                                cusu.device_platform,
                                ccli.nome as 'cliente',
                                ccli.razao_social"
                            );
            
            $this->db->from('cad_usuario cusu');
            $this->db->join('cad_usuariocliente cucli', "cucli.cod_usuario = cusu.cod_usuario",'inner');
            $this->db->join('cad_cliente ccli', "ccli.cod_cliente = cucli.cod_cliente",'inner');
            $this->db->where("cusu.tipo = 'C' and cusu.config_receberemail = 'S'");
            $this->db->where("cucli.cod_cliente in ({$cod_cliente}0)");
            //$this->db->group_by('cusu.cod_usuario');

            $query = $this->db->get();
            $dados = $query->result_array();
                    
            foreach($dados as $value){
                //Envia push
                if ($value["device_id"]<>NULL and $value["device_platform"]<>NULL){
                    //$this->functions->app_push($value["device_id"],$value["device_platform"],TITULO,"Você recebeu novos arquivos");
                }
                
                //Envia email para o usuário
                if ($value["email"]<>NULL){
                    
                    $myemail = new Myemail;

                    $html_anexo="<ul>";
                    foreach($arquivos as $value_anexo){
                        //log_message('debug', "entrega arquivo caminho 2: ".$value_anexo["attachment"]."-".$value_anexo["nome_original"]);
                        if ($value_anexo["cod_cliente"]==$value["cod_cliente"]){
                            $myemail->attachment($value_anexo["attachment"],$value_anexo["nome_original"]);
                            $data_anexo = date_create($value_anexo["vencimento"]);
                            $data_anexo = date_format($data_anexo, 'd/m/Y');
                            if ($value_anexo["vencimento"]=="0000-00-01")$data_anexo="00/00/0000";
                            $html_anexo.="<li>{$data_anexo} ".$value_anexo["nome_evento"]."</li>";
                        }
                    }
                    $html_anexo.="</ul>";

                    $myemail->cod_cliente($value["cod_cliente"]);
                    $myemail->from(TITULO, EMAIL_NR);
                    $myemail->to("BAH",$value["email"]);
                    $myemail->subject($template["titulo"]);
                    $myemail->body($template["texto"]);
                    $myemail->custom(array("%USUARIO%"=>$value["nome"],"%CLIENTE%"=>$value["cliente"],"%RAZAOSOCIAL%"=>$value["razao_social"],"%ARQUIVOS%"=>$html_anexo));
                    $myemail->enviar();
                }
            }
        }
        //var_dump($retorno);
        return json_encode($retorno);
    }

    public function grafico_quantidademes($inicio=0) {  
        $this->db->select(" 'grafico_quantidademes',
                            date_format(carq.data_c,'%m-%Y') as 'mes', 
                            date_format(carq.data_c,'%m') as 'mes_', 
                            count(*) as 'qtd'
                            "
							,FALSE
						);
        
        $this->db->from("cad_arquivo carq");
        //$this->db->where("month(carq.data_c)=month(now())");
        $this->db->group_by("date_format(carq.data_c,'%m-%Y')"); 
        $this->db->order_by("carq.data_c", "asc");
        $this->db->limit(10, 0);
        
		$query = $this->db->get(); 
		return json_encode($query->result_array());    
    }
    
}

