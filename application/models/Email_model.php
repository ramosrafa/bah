<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Email_model extends CI_Model {
    
    private $data = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	//Retorna a quantidade total de registros da tabela
    function contar(){
		return $this->db->count_all('cad_email');
	}

    public function editar($cod_email) {
		$this->db->select(" 'email.editar',
						  	cema.cod_email,
							cema.cod_cliente,
							cema.id_externo,
							cema.uuid_externo,
							cema.from,
                            cema.to,
                            cema.subject,
                            cema.status,
                            cema.data_c,
                            date_format(cema.data_c,'%d/%m/%Y') as 'data_c_',"
						);
        
        $this->db->from('cad_email cema');
        
		$this->db->where("cod_email = '{$cod_email}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'email.listar',
                            cema.cod_email,
                            cema.cod_cliente,
                            cema.id_externo,
                            cema.uuid_externo,
							cema.from,
							cema.to,
                            cema.subject,
                            cema.status,
                            date_format(cema.data_c,'%d/%m/%Y') as 'data_c_',
                            ccli.nome as 'cliente'"
							,FALSE
						);
        
        $this->db->from('cad_email cema');
        $this->db->join('cad_cliente ccli', "cema.cod_cliente = ccli.cod_cliente",'left');

        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("cema.subject like '%{$busca}%' or cema.to like '%{$busca}%' or cema.to like '%{$busca}%'");
        
        $orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"from";
        $orderby_order = $this->input->get_post('orderby_order');
        $this->db->order_by($orderby_column, $orderby_order);

        if (LIMIT<>0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }
    
    public function inserir($cod_cliente=0,$id_externo="",$uuid_externo="",$from_name,$from_email,$to_name,$to_email,$subject) {

        $this->data["cod_cliente"]=$cod_cliente;
        $this->data["id_externo"]=$id_externo;
        $this->data["uuid_externo"]=$uuid_externo;
        $this->data["from"]="$from_name ($from_email)";
        $this->data["to"]="$to_name ($to_email)";
        $this->data["subject"]="$subject";
        $this->data["status"]="Enviado ".date('d-m-Y H:i');
        
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_email', $this->data);
        
    }

    public function atualizar_status($id_externo,$status) {
        
        $this->data["status"]=$status;

		$this->db->where('id_externo = ',$id_externo);
		$this->db->update('cad_email', $this->data);
        
    }

    public function excluir($cod_email) {

        $this->db->where("cod_email", $cod_email);
		$this->db->delete('cad_email');
        
    }

    public function relatorio_email() {
		$this->db->select(" cema.from,
							cema.to,
                            cema.subject,
                            date_format(cema.data_c,'%d/%m/%Y') as 'data_c_',
                            ccli.nome as 'cliente'"
							,FALSE
						);
        
        $this->db->from('cad_email cema');
        $this->db->join('cad_cliente ccli', "cema.cod_cliente = ccli.cod_cliente",'left');
        $this->db->order_by("cema.from", "asc");   
        $this->db->order_by("cema.to", "asc");   
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }
       
}

