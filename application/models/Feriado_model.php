<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Feriado_model extends CI_Model {
		
	private $data = array();  
	
	function __construct() {
			parent::__construct(); 
			$this->load->dbutil();
    }
    function contar(){
		return $this->db->count_all('cad_feriado');
	}

	public function editar($cod_feriado) {
        $this->db->select(" 'feriado.editar',
							cfer.cod_feriado,
							cfer.nome,
							cfer.feriado,
                            cfer.data,
							cfer.uf,
							cfer.cidade ".$this->functions->sql_auditoria("cfer")
					);
			
		$this->db->from('cad_feriado cfer');
			
		$this->db->where("cod_feriado = '{$cod_feriado}'");
		
		$query = $this->db->get(); 
		return $query->row_array();		
	}

	public function listar($inicio=0) {
		$this->db->select(" 'feriado.listar',
							cfer.cod_feriado,
							cfer.nome,
							cfer.feriado,
                            cfer.data,
                            date_format(cfer.data, '%d/%m/%Y') as 'data',
							cfer.uf,
							cfer.cidade "
							,FALSE
						);
			
		$this->db->from('cad_feriado cfer');

		$busca = $this->input->get_post('busca');
		if ($busca) $this->db->where("cfer.nome like '%{$busca}%'");
		
		$orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"nome";
		$orderby_order = $this->input->get_post('orderby_order');
		$this->db->order_by($orderby_column, $orderby_order);

		if (LIMIT<> 0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
			
		$query = $this->db->get(); 
		return $query->result_array();    
	}

	public function listar_cliente($uf="",$cidade="") {
		$this->db->select(" 'feriado.listar_cliente',
							cfer.cod_feriado,
							cfer.nome,
							cfer.feriado,
                            cfer.data,
                            date_format(cfer.data, '%d/%m/%Y') as 'data_',
							cfer.uf,
							cfer.cidade "
							,FALSE
						);
			
		$this->db->from('cad_feriado cfer');

		$this->db->where("(cfer.uf = '{$uf}' or cfer.uf = '')");
		$this->db->where("(cfer.cidade = '{$cidade}' or cfer.cidade = '')");
		$this->db->order_by("cfer.data");
			
		$query = $this->db->get(); 
		return $query->result_array();    
	}

	public function listar_select() {
		$this->db->select(" 'feriado.listar_select',
							cfer.nome,
							cfer.feriado,
							cfer.data,
                            date_format(cfer.data, '%d/%m/%Y') as 'data',
							cfer.uf,
							cfer.cidade "
							,FALSE
						);
				
		$this->db->from('cad_feriado cfer');

		$this->db->order_by("cfer.nome", "asc");
				
		$query = $this->db->get(); 
		return $query->result_array();    
	}
		
	public function inserir() {
			
		$nome = $this->input->get_post('nome');
		$feriado = $this->input->get_post('feriado');
        $data = substr($this->input->get_post('data'),6,4)."-".substr($this->input->get_post('data'),3,2)."-".substr($this->input->get_post('data'),0,2);
		$uf = $this->input->get_post('uf');
		$cidade = $this->input->get_post('cidade');

		$this->data["nome"]=$nome;
		$this->data["feriado"]=$feriado;  
        $this->data["data"]=$data;    
		$this->data["uf"]=$uf;    
		$this->data["cidade"]=$cidade;
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');
		$this->data['data_c']=date('Y-m-d H:i:s');       
		$this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
		$this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->insert('cad_feriado', $this->data);                   
	}       

	public function deletar_todos() {
		$this->db->query("truncate table cad_feriado");
	}

	public function atualizar_feriados($uf,$cidade,$feriado) {
		$nome = $feriado->name;
		$tipo = $feriado->type;
		$data = $feriado->date;
		$data = substr($data,6,4)."-".substr($data,3,2)."-".substr($data,0,2);
		$uf = $uf;
		$cidade = $cidade;

		if ($tipo=="Feriado Estadual"){
			$cidade = "";
		}

		if ($tipo<>"Feriado Estadual" and $tipo<>"Feriado Municipal"){
			$uf = "";
			$cidade = "";
		}

		//Verifica se já existe
		$this->db->select(" 'feriado.atualizar_feriados',
							cfer.cod_feriado"
					);
			
		$this->db->from('cad_feriado cfer');
		$this->db->where("data = '{$data}'");

		$query = $this->db->get(); 
		$row = $query->row_array();	

		if (!$row) {
			$sql = "insert ignore cad_feriado (
											cod_feriado,
											nome, 
											feriado,
											data,
											uf,
											cidade,
											cod_usuario_c,
											data_c,
											cod_usuario_a,
											data_a)
											values(
											'".$nome."',
											'".$nome."',
											'".$tipo."',
											'".$data."',
											'".$uf."',
											'".$cidade."',
											'".$this->session->userdata('cod_usuario')."',
											'".date('Y-m-d H:i:s')."',
											'".$this->session->userdata('cod_usuario')."',
											'".date('Y-m-d H:i:s')."'
											)
											";
			$this->db->query($sql);
		}

	}        

	public function salvar($cod_feriado) {
			
		$nome = $this->input->get_post('nome');
        $data = substr($this->input->get_post('data'),6,4)."-".substr($this->input->get_post('data'),3,2)."-".substr($this->input->get_post('data'),0,2);
		$feriado = $this->input->get_post('feriado');
		$uf = $this->input->get_post('uf');
		$cidade = $this->input->get_post('cidade');

		$this->data["nome"]=$nome;
        $this->data["data"]=$data;
		$this->data["feriado"]=$feriado; 
		$this->data["uf"]=$uf;
		$this->data["cidade"]=$cidade; 
		$this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
		$this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->where('cod_feriado = ',$cod_feriado);
		$this->db->update('cad_feriado', $this->data);		
	}
	
	public function excluir($cod_feriado) {

		$this->db->where("cod_feriado", $cod_feriado);
		$this->db->delete('cad_feriado');
			
	}
}