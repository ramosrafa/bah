<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Regime_model extends CI_Model {
    
    private $data = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	//Retorna a quantidade total de registros da tabela
    function contar(){
		return $this->db->count_all('cad_regime');
	}

    public function editar($cod_regime) {
		$this->db->select(" 'regime.editar',
						  	creg.cod_regime,
							creg.nome,
                            creg.texto".$this->functions->sql_auditoria("creg")
						);
        
        $this->db->from('cad_regime creg');
        
		$this->db->where("cod_regime = '{$cod_regime}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'regime.listar',
						  	creg.cod_regime,
							creg.nome"
							,FALSE
						);
        
        $this->db->from('cad_regime creg');

        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("creg.nome like '%{$busca}%'");
        
        $this->db->order_by("creg.nome", "asc");

        if (LIMIT<>0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_select() {
		$this->db->select(" 'regime.listar',
						  	creg.cod_regime,
							creg.nome"
							,FALSE
						);
        
        $this->db->from('cad_regime creg');

        $this->db->order_by("creg.nome", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function inserir() {
        
        $nome = $this->input->get_post('nome');
        $texto = $this->input->get_post('texto');

        $this->data["nome"]=$nome;
        $this->data["texto"]=$texto;
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_regime', $this->data);

        
    }

    public function salvar($cod_regime) {
        
        $nome = $this->input->get_post('nome');
        $texto = $this->input->get_post('texto');

        $this->data["nome"]=$nome;
        $this->data["texto"]=$texto;
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->where('cod_regime = ',$cod_regime);
		$this->db->update('cad_regime', $this->data);
        
    }

    public function excluir($cod_regime) {

        $this->db->where("cod_regime", $cod_regime);
		$this->db->delete('cad_regime');
        
    }

    //JSON

    public function json_regimelistar($cod_usuario="") {
        
		$this->db->select(" 'json_regimelistar',
						  	creg.cod_regime,
							creg.nome"
							,FALSE
						);
        
        $this->db->from('cad_regime creg');
        
        if ($cod_usuario<>"") {
            $this->db->join('cad_usuarioregime cureg', "(cureg.cod_regime = creg.cod_regime and cureg.cod_usuario = '{$cod_usuario}')",'inner');
        }
           
		$query = $this->db->get(); 
		return json_encode($query->result_array());    
    }    
}

