<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Alvara_model extends CI_Model {
		
	private $data = array();  
	
	function __construct() {
			parent::__construct();
			$this->load->dbutil();
	}

	//Retorna a quantidade total de registros da tabela
	function contar(){
		return $this->db->count_all('cad_alvara');
	}

	public function editar($cod_alvara) {
		$this->db->select(" 'alvara.editar',
							calv.cod_alvara,
							calv.nome,
							calv.texto".$this->functions->sql_auditoria("calv")
					);
			
		$this->db->from('cad_alvara calv');
			
		$this->db->where("cod_alvara = '{$cod_alvara}'");
		
		$query = $this->db->get(); 
		return $query->row_array();		
	}

	public function listar($inicio=0) {
		$this->db->select(" 'alvara.listar',
							calv.cod_alvara,
							calv.nome"
							,FALSE
						);
			
		$this->db->from('cad_alvara calv');

		$busca = $this->input->get_post('busca');
		if ($busca) $this->db->where("calv.nome like '%{$busca}%'");
		
		$orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"nome";
		$orderby_order = $this->input->get_post('orderby_order');
		$this->db->order_by($orderby_column, $orderby_order);

		if (LIMIT<> 0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
			
		$query = $this->db->get(); 
		return $query->result_array();    
	}

	public function listar_select() {
		$this->db->select(" 'alvara.listar',
							calv.cod_alvara,
							calv.nome"
							,FALSE
						);
				
		$this->db->from('cad_alvara calv');

		$this->db->order_by("calv.nome", "asc");
				
		$query = $this->db->get(); 
		return $query->result_array();    
	}

	public function listar_alvarausuario($cod_usuario) {
		$this->db->select(" 'listar_alvaracliente',
							calv.cod_alvara,
						calv.nome"
						,FALSE
					);
			
		$this->db->from('cad_alvara calv');
		
		if ($cod_usuario<>"") {
				$this->db->join('cad_usuarioalvara cualv', "(cualv.cod_alvara = calv.cod_alvara and cualv.cod_usuario = '{$cod_usuario}')",'inner');
		}
					
		$query = $this->db->get(); 
		return $query->result_array();    
	}

	public function listar_alvarasvencidos() {
			
		$this->db->select(" 'alvara.listar_alvarasvencidos',
							ccli.cod_cliente,
							cca.cod_clientealvara,
							cca.nome,
							cca.numero,
							cca.data_vencimento,
							date_format(cca.data_vencimento,'%d/%m/%Y') as 'data_vencimento_',
							case
								when cca.data_vencimento is not null and cca.data_vencimento <> '0000-00-00' and datediff(cca.data_vencimento, now()) >= 0 and datediff(cca.data_vencimento, now()) <= 10 then 'Próximo ao vencimento' 
								when cca.data_vencimento is not null and cca.data_vencimento <> '0000-00-00' and datediff(cca.data_vencimento, now()) > 10 then 'ok' 
								when cca.data_vencimento is not null and cca.data_vencimento <> '0000-00-00' and datediff(cca.data_vencimento, now()) < 0 then 'Vencido' 
								when cca.data_vencimento is null or cca.data_vencimento = '0000-00-00' then 'Sem vencimento' 
							end as 'vencimento_situacao', 
							case 
								when cca.data_vencimento is not null and cca.data_vencimento <> '0000-00-00' and datediff(cca.data_vencimento, now()) >= 0 and datediff(cca.data_vencimento, now()) <= 10 then 'warning' 
								when cca.data_vencimento is not null and cca.data_vencimento <> '0000-00-00' and datediff(cca.data_vencimento, now()) > 10 then 'success' 
								when cca.data_vencimento is not null and cca.data_vencimento <> '0000-00-00' and datediff(cca.data_vencimento, now()) < 0 then 'danger' 
								when cca.data_vencimento is null or cca.data_vencimento = '0000-00-00' then 'warning'  
							end as 'vencimento_classe',
							cca.data_pagamento,
							date_format(cca.data_pagamento,'%d/%m/%Y') as 'data_pagamento_',
							case
								when cca.data_pagamento is not null and cca.data_pagamento <> '0000-00-01' and datediff(cca.data_pagamento, now()) >= 0 and datediff(cca.data_pagamento, now()) <= 10 then 'Próximo ao vencimento do Pgto' 
								when cca.data_pagamento is not null and cca.data_pagamento <> '0000-00-01' and datediff(cca.data_pagamento, now()) > 10 then 'Pgto ok' 
								when cca.data_pagamento is not null and cca.data_pagamento <> '0000-00-01' and datediff(cca.data_pagamento, now()) < 0 then 'Pgto Vencido' 
								when cca.data_pagamento is null or cca.data_pagamento = '0000-00-01' then 'Pgto sem vencimento' 
							end as 'pagamento_situacao', 
							case 
								when cca.data_pagamento is not null and cca.data_pagamento <> '0000-00-01' and datediff(cca.data_pagamento, now()) >= 0 and datediff(cca.data_pagamento, now()) <= 10 then 'warning' 
								when cca.data_pagamento is not null and cca.data_pagamento <> '0000-00-01' and datediff(cca.data_pagamento, now()) > 10 then 'success' 
								when cca.data_pagamento is not null and cca.data_pagamento <> '0000-00-01' and datediff(cca.data_pagamento, now()) < 0 then 'danger' 
								when cca.data_pagamento is null or cca.data_pagamento = '0000-00-01' then 'warning'  
							end as 'pagamento_classe',
							cca.valor,
							ccli.nome as 'cliente',
							calv.nome as 'alvara'"
						,FALSE
					);
			
			$this->db->from('cad_clientealvara cca');
			$this->db->join('cad_cliente ccli', "ccli.cod_cliente = cca.cod_cliente",'left');
			$this->db->join('cad_alvara calv', "cca.cod_alvara = calv.cod_alvara",'left');
			$this->db->where("(ccli.cod_cliente in (".$this->session->userdata('clientes_codigos')."0) or '".$this->session->userdata('admin')."'='S')");
			$this->db->where("cca.vencimento='C' and (datediff(cca.data_vencimento, now()) < 11 or datediff(cca.data_vencimento, now())is NULL) and (cca.data_pagamento is null or cca.data_pagamento= '0000-00-00' or cca.data_pagamento= '0000-00-01')");
			$this->db->order_by("cca.data_vencimento", "asc");
			
		$query = $this->db->get(); 
		return $query->result_array();
					
	}

	public function inserir() {
			
		$nome = $this->input->get_post('nome');
		$texto = $this->input->get_post('texto');

		$this->data["nome"]=$nome;
		$this->data["texto"]=$texto;
		$this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
		$this->data['data_c']=date('Y-m-d H:i:s');       
		$this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
		$this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->insert('cad_alvara', $this->data);

	}

	public function salvar($cod_alvara) {
			
		$nome = $this->input->get_post('nome');
		$texto = $this->input->get_post('texto');

		$this->data["nome"]=$nome;
		$this->data["texto"]=$texto;
		$this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
		$this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->where('cod_alvara = ',$cod_alvara);
		$this->db->update('cad_alvara', $this->data);
			
	}

	public function excluir($cod_alvara) {

		$this->db->where("cod_alvara", $cod_alvara);
		$this->db->delete('cad_alvara');
			
	}

	//JSON

	public function json_alvaralistar() {
			
		$this->db->select(" 'json_alvaralistar',
								calv.cod_alvara,
							calv.nome"
							,FALSE
						);
				
				$this->db->from('cad_alvara calv');
						
		$query = $this->db->get(); 
		return json_encode($query->result_array());    
	}    
}

