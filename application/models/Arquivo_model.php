<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Arquivo_model extends CI_Model {
    
    private $data = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

    //public function listar($cod_cliente="",$origem="",$cod_evento="",$cod_coleta="",$cod_servico="",$cod_pasta="") {
    public function listar($where) {
        
		$this->db->select(" 'arquivo.listar_arquivos',
                            carq.cod_cliente, 
                            carq.origem, 
                            case carq.origem
                                when 'E' then 'Entrega'
                                when 'S' then 'Serviço'
                                when 'C' then 'Coleta'
                            end as 'origem_',
                            case carq.origem
                                when 'E' then 'fa-arrow-circle-down'
                                when 'S' then 'fa-arrow-circle-up'
                                when 'C' then 'fa-arrow-circle-up'
                            end as 'icone',
                            case carq.origem
                                when 'E' then '#86c43b'
                                when 'S' then '#052055'
                                when 'C' then '#052055'
                            end as 'cor',
                            carq.arquivo,
                            carq.situacao,
                            carq.nome_original, 
                            carq.data_competencia, 
                            date_format(carq.data_competencia, '%d/%m/%Y') as 'data_competencia_', 
                            carq.data_vencimento, 
                            date_format(carq.data_vencimento, '%d/%m/%Y') as 'data_vencimento_', 
                            date_format(carq.data_c, '%d/%m/%Y') as 'data_c_', 
                            date_format(carq.data_a,'%d/%m/%Y') as 'data_a_',

                            ccli.nome as 'cliente',
                            ccli.cod_interno,
                            ccli.documento,

                            cpas.cod_pasta,
                            cpas.nome as 'cod_pasta'
                            "
						);
        
        $this->db->from('cad_arquivo carq');
        $this->db->join('cad_cliente ccli', "ccli.cod_cliente = carq.cod_cliente",'inner');
        $this->db->join('cad_evento ceve', "ceve.cod_evento = carq.cod_evento",'left');        
        $this->db->join('cad_pasta cpas', "cpas.cod_pasta = ceve.cod_pasta",'left');        

        if (array_key_exists("cod_cliente", $where)) $this->db->where("carq.cod_cliente = '{$where["cod_cliente"]}'"); 
        if (array_key_exists("cod_coleta", $where)) $this->db->where("carq.cod_coleta = '{$where["cod_coleta"]}'"); 
        if (array_key_exists("cod_pasta", $where)) $this->db->where("carq.cod_pasta = '{$where["cod_pasta"]}'"); 
        
        $this->db->order_by("carq.data_c", "asc");
		
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function download($arquivo,$zip=false) {

        $qtd = count($arquivo);
        
        log_message('debug', "ARQUIVO ARRAY:$qtd");

        foreach($arquivo as $value){

            $this->db->select(" 'arquivo.download',
                                carq.nome_original,
                                ccli.cod_interno"
                            );
            
            $this->db->from('cad_arquivo carq');
            $this->db->join('cad_cliente ccli', "ccli.cod_cliente = carq.cod_cliente",'inner');
            
            $this->db->where("arquivo = '{$value}'");
            
            $query = $this->db->get(); 
            $row = $query->row();

            $path[] = $this->functions->cliente_folder($row->cod_interno)."{$value}";
            $filename[] = $row->nome_original;
        }

        if ($qtd==1) {

            header("Content-Length: " . filesize($path[0]));
            header('Content-Disposition: attachment; filename="' . $filename[0] . '"');
            readfile($path[0]);
            flush();

        } else {
            log_message('debug', "ARQUIVO ZIP");
            $zipname = 'bah.zip';
            $zip = new ZipArchive;
            $zip->open($zipname, ZipArchive::CREATE);
            for ($i=0;$i<$qtd;$i++){
                $zip->addFile($path[$i],$filename[$i]);
            }
            $zip->close();

            header('Content-Type: application/zip');
            header("Content-Length: " . filesize($zipname));
            header('Content-Disposition: attachment; filename="' . $zipname . '"');
            readfile($zipname);
            flush();

        }

        exit;
    }
    
    public function excluir($arquivo) {
        
        $this->db->select(" 'arquivo.download',
						  	carq.nome_original,
                            ccli.cod_interno"
						);
        
        $this->db->from('cad_arquivo carq');
        $this->db->join('cad_cliente ccli', "ccli.cod_cliente = carq.cod_cliente",'inner');
        
		$this->db->where("arquivo = '{$arquivo}'");
		
		$query = $this->db->get(); 
        $row = $query->row();

        $path = $this->functions->cliente_folder($row->cod_interno)."{$arquivo}";
        
        unlink($path);

        $this->db->where("arquivo = '{$arquivo}'");
		$this->db->delete('cad_arquivo');
        
        echo $path;
    }

}

