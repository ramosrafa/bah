<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Formulario_model extends CI_Model {
    
    private $data = array(),
            $data_campos = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	//Retorna a quantidade total de registros da tabela
    function contar(){
		return $this->db->count_all('cad_formulario');
	}

    public function editar($cod_formulario) {
		$this->db->select(" 'formulario.editar',
						  	cfor.cod_formulario,
							cfor.cod_pasta,
							cfor.nome,
							cfor.texto".$this->functions->sql_auditoria("cfor")
						);
        
        $this->db->from('cad_formulario cfor');
        
		$this->db->where("cod_formulario = '{$cod_formulario}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'formulario.listar',
                            cfor.cod_formulario,
                            cfor.cod_pasta,
							cfor.nome,
                            cpas.nome as 'cod_pasta'"
							,FALSE
						);
        
        $this->db->from('cad_formulario cfor');
        $this->db->join('cad_pasta cpas', "cfor.cod_pasta = cpas.cod_pasta",'left');

        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("cfor.nome like '%{$busca}%'");
        
        $orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"nome";
        $orderby_order = $this->input->get_post('orderby_order');
        $this->db->order_by($orderby_column, $orderby_order);

        if (LIMIT<>0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    
    public function listar_select() {
		$this->db->select(" 'formulario.listar_select',
						  	cfor.cod_formulario,
							cfor.nome"
							,FALSE
						);
        
        $this->db->from('cad_formulario cfor');

        $this->db->order_by("cfor.nome", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_campos($cod_formulario) {
		$this->db->select(" 'formulario.listar_campos',
						  	cfc.cod_formulariocampo,
						  	cfc.cod_formulario,
							cfc.nome,
                            cfc.tipo,
                            cfc.label,
                            cfc.requerido,
                            cfc.opcoes,
                            cfc.ajuda,
                            cfc.ordem"
							,FALSE
						);
        
        $this->db->from('cad_formulariocampo cfc');
        $this->db->join('cad_formulario cfor', "cfor.cod_formulario = cfc.cod_formulario",'left');
        $this->db->where("cfc.cod_formulario = '{$cod_formulario}'");
        $this->db->order_by("cfc.ordem*1", "asc",FALSE);
        
		$query = $this->db->get(); 
		return $query->result_array();

    }

    public function inserir() {
        
        $this->data["cod_pasta"]=$this->input->get_post('cod_pasta');
        $this->data["nome"]=$this->input->get_post('nome');
        $this->data["texto"]=$this->input->get_post('texto');
        
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_formulario', $this->data);

        $insert_id = $this->db->insert_id();
        
        $this->salvar_campos($insert_id);
        
    }

    public function salvar($cod_formulario) {
        
        $this->data["cod_pasta"]=$this->input->get_post('cod_pasta');
        $this->data["nome"]=$this->input->get_post('nome');
        $this->data["texto"]=$this->input->get_post('texto');

        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->where('cod_formulario = ',$cod_formulario);
		$this->db->update('cad_formulario', $this->data);
        
        $this->salvar_campos($cod_formulario);
    }

    public function salvar_campos($cod_formulario) {
        $this->db->where("cod_formulario", $cod_formulario);
		$this->db->delete('cad_formulariocampo');
        
        $campo_nome = $this->input->get_post("campo_nome");
        $campo_tipo = $this->input->get_post("campo_tipo");
        $campo_label = $this->input->get_post("campo_label");
        $campo_requerido = $this->input->get_post("campo_requerido");
        $campo_opcoes = $this->input->get_post("campo_opcoes");
        $campo_ajuda = $this->input->get_post("campo_ajuda");
        $campo_ordem = $this->input->get_post("campo_ordem");
        foreach( $campo_nome as $key => $n ) {
            if ($campo_nome[$key]<>""){
                unset($data_campos);
                $data_campos = array();

                $this->data_campos["cod_formulario"]=$cod_formulario;
                $this->data_campos["nome"]=$campo_nome[$key];
                $this->data_campos["tipo"]=$campo_tipo[$key];
                $this->data_campos["label"]=$campo_label[$key];
                $this->data_campos["requerido"]=$campo_requerido[$key];
                $this->data_campos["opcoes"]=$campo_opcoes[$key];
                $this->data_campos["ajuda"]=$campo_ajuda[$key];
                $this->data_campos["ordem"]=$campo_ordem[$key];

                $this->db->insert('cad_formulariocampo', $this->data_campos);
            }     
        }     
    }

    public function excluir($cod_formulario) {

        $this->db->where("cod_formulario", $cod_formulario);
		$this->db->delete('cad_formulariocampo');
        
        $this->db->where("cod_formulario", $cod_formulario);
		$this->db->delete('cad_formulario');
        
    }

       
}

