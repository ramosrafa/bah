<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class log_model extends CI_Model {
    
    private $data = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	public function ver($cod_log="") {
		$this->db->select(" 'log.editar',
						  	clog.cod_log,
						  	clog.objeto,
						  	clog.acao,
						  	clog.log,
						  	clog.cod_usuario_c,
                            clog.data_c,
                            date_format(clog.data_c,'%d/%m/%Y %H:%i') as 'data_c_',
                            cusu.nome as 'usuario'
                            "
						);
        
        $this->db->from('cad_log clog');
        $this->db->join('cad_usuario cusu', "cusu.cod_usuario = clog.cod_usuario_c",'left');
        
        $this->db->where("cod_log like '%{$cod_log}%'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'log.listar',
                            clog.cod_log,
                            clog.objeto,
                            clog.acao,
                            clog.log,
                            clog.cod_usuario_c,
                            clog.data_c,
                            date_format(clog.data_c,'%d/%m/%Y %H:%i') as 'data_c_',
                            cusu.nome as 'usuario'"
						);
        
        $this->db->from('cad_log clog');
        $this->db->join('cad_usuario cusu', "cusu.cod_usuario = clog.cod_usuario_c",'left');

        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("clog.objeto like '%{$busca}%' or cusu.nome like '%{$busca}%' or cusu.email like '%{$busca}%'");
        
        $orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"objeto";
        $orderby_order = $this->input->get_post('orderby_order');
        $this->db->order_by($orderby_column, $orderby_order);
        
        if (LIMIT<>0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
		
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function inserir($objeto="",$acao="",$log="") {
        
        $this->data["objeto"]=$objeto;
        $this->data["acao"]=$acao;
        $this->data["log"]=$log;
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');        
        $this->data['data_c']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_log', $this->data);
        
    }


}

