<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Cliente_model extends CI_Model {
    
    private $data = array(),
            $data_cliente = array(),
            $data_evento = array(),
            $data_quadro = array(),
            $data_alvaras = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	//Retorna a quantidade total de registros da tabela
    function contar(){
		return $this->db->count_all('cad_cliente');
	}

    public function editar($cod_cliente="",$cod_interno="") {
		$this->db->select(" 'cliente.editar',
						  	ccli.cod_cliente,
						  	ccli.cod_clientematriz,
						  	ccli.cod_interno,
                            ccli.cod_regime,
                            ccli.cod_certificadotipo,
                            ccli.ativo,
						  	ccli.nome,
						  	ccli.razao_social,
						  	ccli.documento,
						  	ccli.ie,
						  	ccli.im,
						  	ccli.endereco,
						  	ccli.cidade,
						  	ccli.uf,
						  	ccli.cep,
                            
						  	ccli.certificado,
						  	ccli.certificado_vencimento,
                            concat(mid(ccli.certificado_vencimento,9,2),'/',mid(ccli.certificado_vencimento,6,2),'/',mid(ccli.certificado_vencimento,1,4)) as 'certificado_vencimento_',
                            ccli.certificado_senha,
                            ccli.certificado_posse,
                            
                            ccli.procuracao,
                            ccli.procuracao_vencimento,
                            date_format(ccli.procuracao_vencimento,'%d/%m/%Y') as 'procuracao_vencimento_',
						  	ccli.email,
						  	ccli.telefone,
						  	ccli.folder,
						  	ccli.atividade_holding,
						  	ccli.atividade_aluguel,
						  	ccli.atividade_industria,
						  	ccli.atividade_comercio,
						  	ccli.atividade_servico,
                            
                            ccli.estrategia,
                            ccli.estrategia_sobreempresa,
                            ccli.estrategia_site,
                            ccli.cod_estrategia_regimetributario,
                            date_format(ccli.estrategia_regimetributariodata,'%d/%m/%Y') as 'estrategia_regimetributariodata',
                            ccli.estrategia_numeromatriz,
                            ccli.estrategia_grupoempresarial,
                            ccli.estrategia_numeroprofissionais,
                            ccli.estrategia_quantidadesocios,
                            ccli.estrategia_sociosoutraempresa,
                            ccli.estrategia_atividades,
                            ccli.estrategia_faturamento,
                            ccli.estrategia_sobreprojeto,
                            ccli.estrategia_produtoservico,
                            ccli.estrategia_pessoacontato,
                            ccli.estrategia_objetivoestrategia,
                            ccli.estrategia_estagioqualificacao,
                            ccli.estrategia_kpicontabilidade,
                            ccli.estrategia_kpitributario,
                            ccli.estrategia_kpitrabalhista,
                            ccli.estrategia_kpisocietario,
                            ccli.estrategia_kpijuridico,
                            ccli.estrategia_kpiqualificacao,
                            ccli.estrategia_objetivoestagioatual,
                            ccli.estrategia_principaisclientes,
                            ccli.estrategia_principaisfornecedores,
                            ccli.estrategia_erp,
                            ccli.estrategia_consideracaoconsultor1,
                            date_format(ccli.estrategia_consideracaodata1,'%d/%m/%Y') as 'estrategia_consideracaodata1',
                            ccli.estrategia_consideracaoconsultor2,
                            date_format(ccli.estrategia_consideracaodata2,'%d/%m/%Y') as 'estrategia_consideracaodata2',
                            ccli.estrategia_consideracaoconsultor3,
                            date_format(ccli.estrategia_consideracaodata3,'%d/%m/%Y') as 'estrategia_consideracaodata3',
                            ccli.estrategia_modulos,
                            ccli.estrategia_concorrentes,
                            ccli.estrategia_linksmidias,
                            ccli.estrategia_blog,
                            ccli.estrategia_quiz,
                            cr.nome as 'regimetributario',
                            cct.nome as 'certificadotipo'".$this->functions->sql_auditoria("ccli")
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->join('cad_regime cr', "cr.cod_regime = ccli.cod_regime",'left');
        $this->db->join('cad_certificadotipo cct', "cct.cod_certificadotipo = ccli.cod_certificadotipo",'left');
        
		if ($cod_cliente) $this->db->where("cod_cliente = '{$cod_cliente}'");
		if ($cod_interno) $this->db->where("cod_interno = '{$cod_interno}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function codigo($cod_cliente="",$cod_interno="") {
		$this->db->select(" 'cliente.cod_interno',
						  	ccli.cod_cliente,
						  	ccli.cod_interno"
						);
        
        $this->db->from('cad_cliente ccli');
        
		if ($cod_cliente) $this->db->where("cod_cliente = '{$cod_cliente}'");
		if ($cod_interno) $this->db->where("cod_interno = '{$cod_interno}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }
    
    public function situacao_certificado($cod_cliente="") {
		$this->db->select(" 'cliente.situacao_certificado',
                            ccli.certificado,
                            ccli.certificado_vencimento,
                            concat(mid(ccli.certificado_vencimento,9,2),'/',mid(ccli.certificado_vencimento,6,2),'/',mid(ccli.certificado_vencimento,1,4)) as 'certificado_vencimento_',
                            ccli.certificado_posse,
                            cct.nome as 'certificadotipo',
                            case
                            when certificado_vencimento is not null and certificado_vencimento <> '0000-00-00' and datediff(certificado_vencimento, now()) >= 0 and datediff(certificado_vencimento, now()) <= 10 then 'Próximo ao vencimento' 
                            when certificado_vencimento is not null and certificado_vencimento <> '0000-00-00' and datediff(certificado_vencimento, now()) > 10 then 'ok' 
                            when certificado_vencimento is not null and certificado_vencimento <> '0000-00-00' and datediff(certificado_vencimento, now()) < 0 then 'Vencido' 
                            when certificado_vencimento is null or certificado_vencimento = '0000-00-00' then 'Sem vencimento' 
                            end as 'situacao', 
                            case 
                            when certificado_vencimento is not null and certificado_vencimento <> '0000-00-00' and datediff(certificado_vencimento, now()) >= 0 and datediff(certificado_vencimento, now()) <= 10 then 'warning' 
                            when certificado_vencimento is not null and certificado_vencimento <> '0000-00-00' and datediff(certificado_vencimento, now()) > 10 then 'success' 
                            when certificado_vencimento is not null and certificado_vencimento <> '0000-00-00' and datediff(certificado_vencimento, now()) < 0 then 'danger' 
                            when certificado_vencimento is null or certificado_vencimento = '0000-00-00' then 'warning'  
                            end as 'classe',"
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->join('cad_certificadotipo cct', "cct.cod_certificadotipo = ccli.cod_certificadotipo",'left');

		if ($cod_cliente) $this->db->where("ccli.cod_cliente = '{$cod_cliente}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0,$listar_filial='S') {
        
		$this->db->select(" 'cliente.listar',
						  	ccli.cod_cliente,
						  	ccli.cod_clientematriz,
						  	ccli.cod_interno,
						  	ccli.ativo,
						  	concat(ccli.nome,' (',ccli.cod_interno,')') as 'nome',
						  	case when length (ccli.documento)<= 12 then insert( insert( insert( ccli.documento, 10, 0, '-' ), 7, 0, '.' ), 4, 0, '.' ) else insert( insert( insert( insert( ccli.documento, 13, 0, '-' ), 9, 0, '/' ), 6, 0, '.' ), 3, 0, '.' ) end as 'documento',
                            cr.nome as 'regime',
                            ccli.email,
                            ccli.telefone,
                            (select group_concat(concat_ws('~',ccli_.cod_cliente,ccli_.documento,ccli_.nome) SEPARATOR '#') from cad_cliente ccli_ where ccli_.cod_clientematriz = ccli.cod_cliente) as 'filiais',
                            (select cucf.cod_cliente from cad_usuarioclientefavorito cucf where cucf.cod_cliente = ccli.cod_cliente and cucf.cod_usuario = '1') as 'favorito'"
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->join('cad_regime cr', "cr.cod_regime = ccli.cod_regime",'left');
        $this->db->where("(ccli.cod_cliente in (".$this->session->userdata('clientes_codigos')."0) or '".$this->session->userdata('admin')."'='S')");   
        if ($listar_filial=="N") $this->db->where("(ccli.cod_clientematriz IS NULL or ccli.cod_clientematriz = 0)");        

        $busca = $this->input->get_post('busca');
        
        if ($this->input->get_post('busca_inativo')<>"S"){
            $this->db->where("(ccli.ativo = 'S')"); 
        }

        if ($busca) $this->db->where("ccli.nome like '%{$busca}%'");
        
        $orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"nome";
        $orderby_order = $this->input->get_post('orderby_order');
        $this->db->order_by($orderby_column, $orderby_order);

        if (LIMIT<>0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_select() {
        
		$this->db->select(" 'cliente.listar',
						  	ccli.cod_cliente,
						  	ccli.cod_interno,
						  	concat(ccli.nome,' (',ccli.cod_interno,')') as 'nome',
                            ccli.email,
                            ccli.telefone"
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->where("(ccli.cod_cliente in (".$this->session->userdata('clientes_codigos')."0) or '".$this->session->userdata('admin')."'='S')");
        $this->db->where("ccli.ativo = 'S'");        
        $this->db->order_by("ccli.nome", "asc");

		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_ativos() {
        
		$this->db->select(" 'cliente.listar',
						  	ccli.cod_cliente,
						  	ccli.cod_interno,
						  	nome as 'cliente',
						  	razao_social,
                            ccli.email,
                            ccli.telefone"
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->where("ccli.ativo = 'S'");        
        $this->db->order_by("ccli.nome", "asc");

		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_cidades($cod_cliente="") {
        
		$this->db->select(" 'cliente.listar_cidades',
						  	ccli.uf,
						  	ccli.cidade"
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->where("ccli.uf <> '' and ccli.cidade<>''");        
        if ($cod_cliente<>"") $this->db->where("ccli.cod_cliente = '{$cod_cliente}'");
        $this->db->group_by('ccli.uf,ccli.cidade'); 

		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_drive() {
        
		$this->db->select(" 'cliente.listar_drive',
						  	ccli.cod_cliente,
                            ccli.cod_interno,
                            ccli.nome"
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->where("(ccli.cod_cliente in (".$this->session->userdata('clientes_codigos')."0) or '".$this->session->userdata('admin')."'='S')");
        $this->db->where("ccli.ativo = 'S'");   
        
        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("ccli.nome like '%{$busca}%'");

        $this->db->order_by("ccli.nome", "ASC");

		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_clienteusuario($cod_usuario="") {
        
		$this->db->select(" 'cliente.listar_clienteusuario',
						  	cucli.cod_cliente,
						  	ccli.cod_interno,
						  	ccli.ativo,
                            ccli.nome,
                            ccli.uf,
                            ccli.cidade"
							,FALSE
						);
        
        $this->db->from('cad_usuariocliente cucli');
        $this->db->join('cad_cliente ccli', "cucli.cod_cliente = ccli.cod_cliente",'inner');
        $this->db->where("cucli.cod_usuario = '{$cod_usuario}'"); 
           
		$query = $this->db->get(); 
		return $query->result_array();    
    }    
    
    public function listar_filiais($cod_clientematriz="") {
        
		$this->db->select(" 'cliente.listar_filiais',
                            ccli.cod_cliente,
                            ccli.cod_interno,
                            ccli.nome,
                            ccli.documento,
                            ccli.ativo"
							,FALSE
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->where("ccli.cod_clientematriz = '{$cod_clientematriz}'"); 
        $this->db->where("ccli.ativo = 'S'");        
        $this->db->order_by("ccli.nome", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }    

    public function listar_quadro($cod_cliente="") {
        
		$this->db->select(" 'cliente.listar_quadro',
						  	ccq.cod_clientequadro,
						  	ccq.nome,
						  	ccq.documento,
						  	ccq.ordem,
						  	ccq.participacao,
						  	ccq.quotas,
                            ccq.data_registro,
						  	date_format(ccq.data_registro,'%d/%m/%Y') as 'data_registro_',
                            ccq.data_inicio,
						  	date_format(ccq.data_inicio,'%d/%m/%Y') as 'data_inicio_',
						  	ccq.data_saida,
						  	date_format(ccq.data_saida,'%d/%m/%Y') as 'data_saida_',
                            ccq.responsavel"
							,FALSE
						);
        
        $this->db->from('cad_clientequadro ccq');
        $this->db->where("ccq.cod_cliente = '{$cod_cliente}'"); 
        $this->db->order_by("ccq.ordem", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }    

    public function listar_alvaras($cod_cliente="") {
        
		$this->db->select(" 'cliente.listar_alvaras',
						  	cca.cod_clientealvara,
                            cca.cod_alvara,
						  	cca.nome,
						  	cca.numero,
						  	cca.vencimento,
						  	cca.data_vencimento,
						  	date_format(cca.data_vencimento,'%d/%m/%Y') as 'data_vencimento_',
						  	cca.data_pagamento,
						  	date_format(cca.data_pagamento,'%m/%Y') as 'data_pagamento_',
						  	cca.valor"
							,FALSE
						);
        
        $this->db->from('cad_clientealvara cca');
        $this->db->where("cca.cod_cliente = '{$cod_cliente}'"); 
        $this->db->order_by("cca.data_vencimento", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }    

    public function listar_servicosdocliente($cod_cliente="") {
        
		$this->db->select(" 'cliente.listar_servicosdocliente',
						  	cser.cod_servico,
						  	cser.cod_tarefa,
						  	cser.cod_cliente,
                            cser.data_inicio,
                            date_format(cser.data_inicio,'%d/%m/%Y') as 'data_inicio_',
                            cser.data_previsao,
                            date_format(cser.data_previsao,'%d/%m/%Y') as 'data_previsao_',
                            cser.data_conclusao,
                            date_format(cser.data_conclusao,'%d/%m/%Y') as 'data_conclusao_',
                            ctar.nome as 'tarefa'
                            "
							,FALSE
						);
        
        $this->db->from('cad_servico cser');
        $this->db->join('cad_tarefa ctar', "ctar.cod_tarefa = cser.cod_tarefa",'inner');
        $this->db->where("cser.cod_cliente = '{$cod_cliente}'"); 
        $this->db->order_by("cser.data_inicio", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }  

    public function listar_coleta($cod_cliente="") {
        
		$this->db->select(" 'cliente.listar_coleta',
						  	ccol.cod_coleta,
                            ccol.data_solicitacao,
                            date_format(ccol.data_solicitacao,'%d/%m/%Y') as 'data_solicitacao_',
                            date_format(ccol.data_vencimento,'%d/%m/%Y') as 'data_vencimento_',
                            ccol.texto
                            "
							,FALSE
						);
        
        $this->db->from('cad_coleta ccol');
        $this->db->join('cad_coletacliente ccolcli', "(ccolcli.cod_coleta = ccol.cod_coleta and ccolcli.cod_cliente = '$cod_cliente')",'inner');
        $this->db->order_by("ccol.data_solicitacao", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }  

    public function listar_semusuario() {
        
        $this->db->select(" 'cliente.listar_semusuario',
                            '0' as 'tipo',
						  	ccli.cod_cliente,
							ccli.nome"
							,FALSE
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->where("not exists (select * from cad_usuariocliente cucli_ where cucli_.cod_cliente = ccli.cod_cliente)", NULL, FALSE);        
        $this->db->where("(ccli.cod_cliente in (".$this->session->userdata('clientes_codigos')."0) or '".$this->session->userdata('admin')."'='S')");              
        $this->db->where("ccli.ativo = 'S'");        
        $this->db->get(); 
        $query1 = $this->db->last_query();

        $this->db->select(" 'cliente.listar_semusuario',
                            '1' as 'tipo',
                            ccli.cod_cliente,
                            ccli.nome"
                            ,FALSE
                        );

        $this->db->from('cad_cliente ccli');
        $this->db->where("not exists (select * from cad_usuariocliente cucli_ inner join cad_usuario cusu_ on cusu_.cod_usuario = cucli_.cod_usuario  where cucli_.cod_cliente = ccli.cod_cliente and cusu_.config_receberemail='S')", NULL, FALSE);        
        $this->db->where("(ccli.cod_cliente in (".$this->session->userdata('clientes_codigos')."0) or '".$this->session->userdata('admin')."'='S')");
        $this->db->where("ccli.ativo = 'S'");        
        $this->db->get(); 
        $query2 = $this->db->last_query();

        $query = $this->db->query($query1." UNION ".$query2);

		return $query->result_array();    
    }    
   
    
    public function listar_certificadovencido($cron=FALSE) {
        
		$this->db->select(" 'cliente.listar_certificadovencido',
						  	ccli.cod_cliente,
						  	ccli.cod_interno,
						  	ccli.nome,
						  	ccli.certificado,
						  	ccli.certificado_vencimento,
						  	ccli.certificado_senha,
						  	case ccli.certificado_posse
                            when 'T' then 'TripleA'
                            when 'C' then 'Cliente'
                            end as 'certificado_posse',
                            concat(mid(certificado_vencimento,9,2),'/',mid(certificado_vencimento,6,2),'/',mid(certificado_vencimento,1,4)) as 'certificado_vencimento_',
                            case 
                            when certificado_vencimento is not null and certificado_vencimento <> '0000-00-00' and datediff(certificado_vencimento, now()) >= 0 and datediff(certificado_vencimento, now()) <= 30 then 'Próximo ao vencimento' 
                            when certificado_vencimento is not null and certificado_vencimento <> '0000-00-00' and datediff(certificado_vencimento, now()) > 30 then 'ok' 
                            when certificado_vencimento is not null and certificado_vencimento <> '0000-00-00' and datediff(certificado_vencimento, now()) < 0 then 'Vencido' 
                            when certificado_vencimento is null or certificado_vencimento = '0000-00-00' then 'Sem vencimento' 
                            end as 'situacao', 
                            case 
                            when certificado_vencimento is not null and certificado_vencimento <> '0000-00-00' and datediff(certificado_vencimento, now()) >= 0 and datediff(certificado_vencimento, now()) <= 30 then 'warning' 
                            when certificado_vencimento is not null and certificado_vencimento <> '0000-00-00' and datediff(certificado_vencimento, now()) > 30 then 'success' 
                            when certificado_vencimento is not null and certificado_vencimento <> '0000-00-00' and datediff(certificado_vencimento, now()) < 0 then 'danger' 
                            when certificado_vencimento is null or certificado_vencimento = '0000-00-00' then 'warning'  
                            end as 'classe',
                            "
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->where("(ccli.certificado is NULL or ccli.certificado = 'S')");        
        if (!$cron){
            $this->db->where("(ccli.cod_cliente in (".$this->session->userdata('clientes_codigos')."0) or '".$this->session->userdata('admin')."'='S')"); 
        }else{
            $this->db->where("datediff(certificado_vencimento, now()) <= 30");        
        }
        $this->db->where("ccli.ativo = 'S'");        
        $this->db->order_by("ccli.certificado_vencimento", "asc");

        $query = $this->db->get();
        
        $dados = $query->result_array();

        $retorno = array();        
        foreach($dados as $value){
            $arquivo = "./data/".md5($value["cod_interno"])."/certificado.pdf";
            $arquivo1 = "./data/".md5($value["cod_interno"])."/certificado.p12";
            $arquivo2 = "./data/".md5($value["cod_interno"])."/certificado.pfx";
            
            if (file_exists($arquivo)){
                $arquivo = $arquivo;
            } elseif (file_exists($arquivo1)){
                $arquivo = $arquivo1;
            } elseif (file_exists($arquivo2)){
                $arquivo = $arquivo2;
            } else{
                $arquivo = "Sem arquivo";
            }
            
            if ($arquivo<>"" and $value["situacao"]<>"ok"){
                array_push($retorno, array('cod_cliente'=>$value["cod_cliente"],'nome'=>$value["nome"],'certificado_posse'=>$value["certificado_posse"],'certificado_senha'=>$value["certificado_senha"],'certificado_vencimento_'=>$value["certificado_vencimento_"],'situacao'=>$value["situacao"],'classe'=>$value["classe"],'arquivo'=>$arquivo));
            }
        }
        
        return $retorno;
            
    }

    public function listar_procuracao() {
        
		$this->db->select(" 'cliente.listar_procuracao',
                            ccli.cod_cliente,
						  	ccli.cod_interno,
						  	ccli.nome,
                            ccli.procuracao,
                            case
                            when ccli.procuracao = 'S' then 'success'
                            when ccli.procuracao <> 'S' then 'danger'
                            end as 'procuracao_classe',
                            ccli.procuracao_vencimento,
                            date_format(ccli.procuracao_vencimento,'%d/%m/%Y') as 'procuracao_vencimento_',
                            case
                            when ccli.procuracao_vencimento is not null and ccli.procuracao_vencimento <> '0000-00-00' and datediff(ccli.procuracao_vencimento, now()) >= 0 and datediff(ccli.procuracao_vencimento, now()) <= 10 then 'Próximo ao vencimento' 
                            when ccli.procuracao_vencimento is not null and ccli.procuracao_vencimento <> '0000-00-00' and datediff(ccli.procuracao_vencimento, now()) > 10 then 'ok' 
                            when ccli.procuracao_vencimento is not null and ccli.procuracao_vencimento <> '0000-00-00' and datediff(ccli.procuracao_vencimento, now()) < 0 then 'Vencido' 
                            when ccli.procuracao_vencimento is null or ccli.procuracao_vencimento = '0000-00-00' then 'Sem vencimento' 
                            end as 'situacao', 
                            case 
                            when ccli.procuracao_vencimento is not null and ccli.procuracao_vencimento <> '0000-00-00' and datediff(ccli.procuracao_vencimento, now()) >= 0 and datediff(ccli.procuracao_vencimento, now()) <= 10 then 'warning' 
                            when ccli.procuracao_vencimento is not null and ccli.procuracao_vencimento <> '0000-00-00' and datediff(ccli.procuracao_vencimento, now()) > 10 then 'success' 
                            when ccli.procuracao_vencimento is not null and ccli.procuracao_vencimento <> '0000-00-00' and datediff(ccli.procuracao_vencimento, now()) < 0 then 'danger' 
                            when ccli.procuracao_vencimento is null or ccli.procuracao_vencimento = '0000-00-00' then 'warning'  
                            end as 'classe',"
							,FALSE
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->where("(ccli.cod_cliente in (".$this->session->userdata('clientes_codigos')."0) or '".$this->session->userdata('admin')."'='S')");         
        $this->db->where("ccli.ativo = 'S'");        
        $this->db->order_by("ccli.procuracao_vencimento", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    } 

    public function inserir() {
        
        $cod_clientematriz = $this->input->get_post('cod_clientematriz');
        $cod_interno = $this->input->get_post('cod_interno');
        $cod_regime = $this->input->get_post('cod_regime');
        $cod_certificadotipo = $this->input->get_post('cod_certificadotipo');
        $ativo = $this->input->get_post('ativo');
        $nome = $this->input->get_post('nome');
        $razao_social = $this->input->get_post('razao_social');
        $documento = $this->input->get_post('documento');
        $ie = $this->input->get_post('ie');
        $im = $this->input->get_post('im');
        $endereco = $this->input->get_post('endereco');
        $cidade = $this->input->get_post('cidade');
        $uf = $this->input->get_post('uf');
        $cep = $this->input->get_post('cep');
        $certificado = $this->input->get_post('certificado');
        $certificado_vencimento = $this->input->get_post('certificado_vencimento');
        $certificado_vencimento = substr($certificado_vencimento,6,4)."-".substr($certificado_vencimento,3,2)."-".substr($certificado_vencimento,0,2);
        $certificado_senha = $this->input->get_post('certificado_senha');
        $certificado_posse = $this->input->get_post('certificado_posse');
        $procuracao = $this->input->get_post('procuracao');
        $procuracao_vencimento = $this->input->get_post('procuracao_vencimento');
        $procuracao_vencimento = substr($procuracao_vencimento,6,4)."-".substr($procuracao_vencimento,3,2)."-".substr($procuracao_vencimento,0,2);
        $email = $this->input->get_post('email');
        $telefone = $this->input->get_post('telefone');
        $atividade_holding = $this->input->get_post('atividade_holding');
        $atividade_aluguel = $this->input->get_post('atividade_aluguel');
        $atividade_industria = $this->input->get_post('atividade_industria');
        $atividade_comercio = $this->input->get_post('atividade_comercio');
        $atividade_servico = $this->input->get_post('atividade_servico');

        $this->data["cod_clientematriz"]=$cod_clientematriz;
        $this->data["cod_interno"]=$cod_interno;
        $this->data["cod_regime"]=$cod_regime;
        $this->data["cod_certificadotipo"]=$cod_certificadotipo;
        $this->data["ativo"]=$ativo;
        $this->data["nome"]=$nome;
        $this->data["razao_social"]=$razao_social;
        $this->data["documento"]=$documento;
        $this->data["ie"]=$ie;
        $this->data["im"]=$im;
        $this->data["endereco"]=$endereco;
        $this->data["cidade"]=$cidade;
        $this->data["uf"]=$uf;
        $this->data["cep"]=$cep;
        $this->data["certificado"]=$certificado;
        $this->data["certificado_vencimento"]=$certificado_vencimento;
        $this->data["certificado_senha"]=$certificado_senha;
        $this->data["certificado_posse"]=$certificado_posse;
        $this->data["procuracao"]=$procuracao;
        $this->data["procuracao_vencimento"]=$procuracao_vencimento;
        $this->data["email"]=$email;
        $this->data["telefone"]=$telefone;
        $this->data["atividade_holding"]=$atividade_holding;
        $this->data["atividade_aluguel"]=$atividade_aluguel;
        $this->data["atividade_industria"]=$atividade_industria;
        $this->data["atividade_comercio"]=$atividade_comercio;
        $this->data["atividade_servico"]=$atividade_servico;
        
        $this->data["estrategia"]=$this->input->get_post('estrategia');
        $this->data["estrategia_sobreempresa"]=$this->input->get_post('estrategia_sobreempresa');
        $this->data["estrategia_site"]=$this->input->get_post('estrategia_site');
        $this->data["cod_estrategia_regimetributario"]=$this->input->get_post('cod_estrategia_regimetributario');
        $this->data["estrategia_regimetributariodata"]=substr($this->input->get_post('estrategia_regimetributariodata'),6,4)."-".substr($this->input->get_post('estrategia_regimetributariodata'),3,2)."-".substr($this->input->get_post('estrategia_regimetributariodata'),0,2); 
        $this->data["estrategia_numeromatriz"]=$this->input->get_post('estrategia_numeromatriz');
        $this->data["estrategia_grupoempresarial"]=$this->input->get_post('estrategia_grupoempresarial');
        $this->data["estrategia_numeroprofissionais"]=$this->input->get_post('estrategia_numeroprofissionais');
        $this->data["estrategia_quantidadesocios"]=$this->input->get_post('estrategia_quantidadesocios');
        $this->data["estrategia_sociosoutraempresa"]=$this->input->get_post('estrategia_sociosoutraempresa');
        $this->data["estrategia_atividades"]=$this->input->get_post('estrategia_atividades');
        $this->data["estrategia_faturamento"]=$this->input->get_post('estrategia_faturamento');
        $this->data["estrategia_sobreprojeto"]=$this->input->get_post('estrategia_sobreprojeto');
        $this->data["estrategia_produtoservico"]=$this->input->get_post('estrategia_produtoservico');
        $this->data["estrategia_pessoacontato"]=$this->input->get_post('estrategia_pessoacontato');
        $this->data["estrategia_objetivoestrategia"]=is_array($this->input->get_post('estrategia_objetivoestrategia'))?implode("#", $this->input->get_post('estrategia_objetivoestrategia')):"";
        $this->data["estrategia_estagioqualificacao"]=$this->input->get_post('estrategia_estagioqualificacao');
        $this->data["estrategia_kpicontabilidade"]=is_array($this->input->get_post('estrategia_kpicontabilidade'))?implode("#", $this->input->get_post('estrategia_kpicontabilidade')):"";
        $this->data["estrategia_kpitributario"]=is_array($this->input->get_post('estrategia_kpitributario'))?implode("#", $this->input->get_post('estrategia_kpitributario')):"";
        $this->data["estrategia_kpitrabalhista"]= is_array($this->input->get_post('estrategia_kpitrabalhista'))?implode("#", $this->input->get_post('estrategia_kpitrabalhista')):"";
        $this->data["estrategia_kpisocietario"]=is_array($this->input->get_post('estrategia_kpisocietario'))?implode("#", $this->input->get_post('estrategia_kpisocietario')):"";
        $this->data["estrategia_kpijuridico"]=is_array($this->input->get_post('estrategia_kpijuridico'))?implode("#", $this->input->get_post('estrategia_kpijuridico')):"";
        $this->data["estrategia_kpiqualificacao"]=is_array($this->input->get_post('estrategia_kpiqualificacao'))?implode("#", $this->input->get_post('estrategia_kpiqualificacao')):"";
        $this->data["estrategia_objetivoestagioatual"]=is_array($this->input->get_post('estrategia_objetivoestagioatual'))?implode("#", $this->input->get_post('estrategia_objetivoestagioatual')):"";
        $this->data["estrategia_principaisclientes"]=$this->input->get_post('estrategia_principaisclientes');
        $this->data["estrategia_principaisfornecedores"]=$this->input->get_post('estrategia_principaisfornecedores');
        $this->data["estrategia_erp"]=is_array($this->input->get_post('estrategia_erp'))?implode("#", $this->input->get_post('estrategia_erp')):"";
        $this->data["estrategia_consideracaoconsultor1"]=$this->input->get_post('estrategia_consideracaoconsultor1');
        $this->data["estrategia_consideracaodata1"]=substr($this->input->get_post('estrategia_consideracaodata1'),6,4)."-".substr($this->input->get_post('estrategia_consideracaodata1'),3,2)."-".substr($this->input->get_post('estrategia_consideracaodata1'),0,2); 
        $this->data["estrategia_consideracaoconsultor2"]=$this->input->get_post('estrategia_consideracaoconsultor2');
        $this->data["estrategia_consideracaodata2"]=substr($this->input->get_post('estrategia_consideracaodata2'),6,4)."-".substr($this->input->get_post('estrategia_consideracaodata2'),3,2)."-".substr($this->input->get_post('estrategia_consideracaodata2'),0,2); 
        $this->data["estrategia_consideracaoconsultor3"]=$this->input->get_post('estrategia_consideracaoconsultor3');
        $this->data["estrategia_consideracaodata3"]=substr($this->input->get_post('estrategia_consideracaodata3'),6,4)."-".substr($this->input->get_post('estrategia_consideracaodata3'),3,2)."-".substr($this->input->get_post('estrategia_consideracaodata3'),0,2); 
        $this->data["estrategia_modulos"]=$this->input->get_post('estrategia_modulos');
        $this->data["estrategia_concorrentes"]=$this->input->get_post('estrategia_concorrentes');
        $this->data["estrategia_linksmidias"]=$this->input->get_post('estrategia_linksmidias');
        $this->data["estrategia_blog"]=$this->input->get_post('estrategia_blog');
        $this->data["estrategia_quiz"]=$this->input->get_post('estrategia_quiz');
        
        $this->data["folder"]=md5($cod_interno);
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_cliente', $this->data);
        $insert_id = $this->db->insert_id();

        //Salva o cliente para o usuário que o criou
        $this->data_cliente["cod_usuario"]=$this->session->userdata('cod_usuario');
        $this->data_cliente["cod_cliente"]=$insert_id;
        $this->data_cliente['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data_cliente['data_c']=date('Y-m-d H:i:s');       
        $this->data_cliente['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data_cliente['data_a']=date('Y-m-d H:i:s');       
        $this->db->insert('cad_usuariocliente', $this->data_cliente);
        
        $this->salvar_evento($insert_id);
        $this->salvar_quadro($insert_id);
        $this->salvar_alvaras($insert_id);
        
        //Upload do termo de uso
        $name = $_FILES['termodeuso']['name'];
        $size = $_FILES['termodeuso']['size'];    
        $tmp_name = $_FILES['termodeuso']['tmp_name'];
        
        $folder = $this->functions->cliente_folder($cod_interno);
        $arquivo = "{$folder}termodeuso.pdf";
        
        move_uploaded_file($tmp_name, $arquivo);
        
        //Upload do certificado
        $name = $_FILES['certificado']['name'];
        $size = $_FILES['certificado']['size'];    
        $tmp_name = $_FILES['certificado']['tmp_name'];
        $ext = pathinfo($name, PATHINFO_EXTENSION);        
        
        $folder = $this->functions->cliente_folder($cod_interno);
        $arquivo = "{$folder}certificado.{$ext}";
        
        move_uploaded_file($tmp_name, $arquivo);
        
        //Upload do estrategia_anexoestagioqualificacao
        $name = $_FILES['estrategia_anexoestagioqualificacao']['name'];
        $size = $_FILES['estrategia_anexoestagioqualificacao']['size'];    
        $tmp_name = $_FILES['estrategia_anexoestagioqualificacao']['tmp_name'];
        $ext = pathinfo($name, PATHINFO_EXTENSION);        
        
        $folder = $this->functions->cliente_folder($cod_interno);
        $arquivo = "{$folder}estrategia_anexoestagioqualificacao.{$ext}";
        
        move_uploaded_file($tmp_name, $arquivo);
        
        //Upload do estrategia_anexokpijuridico
        $name = $_FILES['estrategia_anexokpijuridico']['name'];
        $size = $_FILES['estrategia_anexokpijuridico']['size'];    
        $tmp_name = $_FILES['estrategia_anexokpijuridico']['tmp_name'];
        $ext = pathinfo($name, PATHINFO_EXTENSION);        
        
        $folder = $this->functions->cliente_folder($cod_interno);
        $arquivo = "{$folder}estrategia_anexokpijuridico.{$ext}";
        
        move_uploaded_file($tmp_name, $arquivo);
        
    }

    public function salvar($cod_cliente) {
        
        $cod_clientematriz = $this->input->get_post('cod_clientematriz');
        $cod_interno = $this->input->get_post('cod_interno');
        $cod_regime = $this->input->get_post('cod_regime');
        $cod_certificadotipo = $this->input->get_post('cod_certificadotipo');
        $ativo = $this->input->get_post('ativo');
        $nome = $this->input->get_post('nome');
        $razao_social = $this->input->get_post('razao_social');
        $documento = $this->input->get_post('documento');
        $ie = $this->input->get_post('ie');
        $im = $this->input->get_post('im');
        $endereco = $this->input->get_post('endereco');
        $cidade = $this->input->get_post('cidade');
        $uf = $this->input->get_post('uf');
        $cep = $this->input->get_post('cep');
        $certificado = $this->input->get_post('certificado');
        $certificado_vencimento = $this->input->get_post('certificado_vencimento');
        $certificado_vencimento = substr($certificado_vencimento,6,4)."-".substr($certificado_vencimento,3,2)."-".substr($certificado_vencimento,0,2);
        $certificado_senha = $this->input->get_post('certificado_senha');
        $certificado_posse = $this->input->get_post('certificado_posse');
        $procuracao = $this->input->get_post('procuracao');
        $procuracao_vencimento = $this->input->get_post('procuracao_vencimento');
        $procuracao_vencimento = substr($procuracao_vencimento,6,4)."-".substr($procuracao_vencimento,3,2)."-".substr($procuracao_vencimento,0,2);
        $email = $this->input->get_post('email');
        $telefone = $this->input->get_post('telefone');
        $atividade_holding = $this->input->get_post('atividade_holding');
        $atividade_aluguel = $this->input->get_post('atividade_aluguel');
        $atividade_industria = $this->input->get_post('atividade_industria');
        $atividade_comercio = $this->input->get_post('atividade_comercio');
        $atividade_servico = $this->input->get_post('atividade_servico');

        $this->data["cod_clientematriz"]=$cod_clientematriz;
        $this->data["cod_interno"]=$cod_interno;
        $this->data["cod_regime"]=$cod_regime;
        $this->data["cod_certificadotipo"]=$cod_certificadotipo;
        $this->data["ativo"]=$ativo;
        $this->data["nome"]=$nome;
        $this->data["razao_social"]=$razao_social;
        $this->data["documento"]=$documento;
        $this->data["ie"]=$ie;
        $this->data["im"]=$im;
        $this->data["endereco"]=$endereco;
        $this->data["cidade"]=$cidade;
        $this->data["uf"]=$uf;
        $this->data["cep"]=$cep;
        $this->data["certificado"]=$certificado;
        $this->data["certificado_vencimento"]=$certificado_vencimento;
        $this->data["certificado_senha"]=$certificado_senha;
        $this->data["certificado_posse"]=$certificado_posse;
        $this->data["procuracao"]=$procuracao;
        $this->data["procuracao_vencimento"]=$procuracao_vencimento;
        $this->data["email"]=$email;
        $this->data["telefone"]=$telefone;
        $this->data["atividade_holding"]=$atividade_holding;
        $this->data["atividade_aluguel"]=$atividade_aluguel;
        $this->data["atividade_industria"]=$atividade_industria;
        $this->data["atividade_comercio"]=$atividade_comercio;
        $this->data["atividade_servico"]=$atividade_servico;
        
        $this->data["estrategia"]=$this->input->get_post('estrategia');
        $this->data["estrategia_sobreempresa"]=$this->input->get_post('estrategia_sobreempresa');
        $this->data["estrategia_site"]=$this->input->get_post('estrategia_site');
        $this->data["cod_estrategia_regimetributario"]=$this->input->get_post('cod_estrategia_regimetributario');
        $this->data["estrategia_regimetributariodata"]=substr($this->input->get_post('estrategia_regimetributariodata'),6,4)."-".substr($this->input->get_post('estrategia_regimetributariodata'),3,2)."-".substr($this->input->get_post('estrategia_regimetributariodata'),0,2); 
        $this->data["estrategia_numeromatriz"]=$this->input->get_post('estrategia_numeromatriz');
        $this->data["estrategia_grupoempresarial"]=$this->input->get_post('estrategia_grupoempresarial');
        $this->data["estrategia_numeroprofissionais"]=$this->input->get_post('estrategia_numeroprofissionais');
        $this->data["estrategia_quantidadesocios"]=$this->input->get_post('estrategia_quantidadesocios');
        $this->data["estrategia_sociosoutraempresa"]=$this->input->get_post('estrategia_sociosoutraempresa');
        $this->data["estrategia_atividades"]=$this->input->get_post('estrategia_atividades');
        $this->data["estrategia_faturamento"]=$this->input->get_post('estrategia_faturamento');
        $this->data["estrategia_sobreprojeto"]=$this->input->get_post('estrategia_sobreprojeto');
        $this->data["estrategia_produtoservico"]=$this->input->get_post('estrategia_produtoservico');
        $this->data["estrategia_pessoacontato"]=$this->input->get_post('estrategia_pessoacontato');
        $this->data["estrategia_objetivoestrategia"]=is_array($this->input->get_post('estrategia_objetivoestrategia'))?implode("#", $this->input->get_post('estrategia_objetivoestrategia')):"";
        $this->data["estrategia_estagioqualificacao"]=$this->input->get_post('estrategia_estagioqualificacao');
        $this->data["estrategia_kpicontabilidade"]=is_array($this->input->get_post('estrategia_kpicontabilidade'))?implode("#", $this->input->get_post('estrategia_kpicontabilidade')):"";
        $this->data["estrategia_kpitributario"]=is_array($this->input->get_post('estrategia_kpitributario'))?implode("#", $this->input->get_post('estrategia_kpitributario')):"";
        $this->data["estrategia_kpitrabalhista"]= is_array($this->input->get_post('estrategia_kpitrabalhista'))?implode("#", $this->input->get_post('estrategia_kpitrabalhista')):"";
        $this->data["estrategia_kpisocietario"]=is_array($this->input->get_post('estrategia_kpisocietario'))?implode("#", $this->input->get_post('estrategia_kpisocietario')):"";
        $this->data["estrategia_kpijuridico"]=is_array($this->input->get_post('estrategia_kpijuridico'))?implode("#", $this->input->get_post('estrategia_kpijuridico')):"";
        $this->data["estrategia_kpiqualificacao"]=is_array($this->input->get_post('estrategia_kpiqualificacao'))?implode("#", $this->input->get_post('estrategia_kpiqualificacao')):"";
        $this->data["estrategia_objetivoestagioatual"]=is_array($this->input->get_post('estrategia_objetivoestagioatual'))?implode("#", $this->input->get_post('estrategia_objetivoestagioatual')):"";
        $this->data["estrategia_principaisclientes"]=$this->input->get_post('estrategia_principaisclientes');
        $this->data["estrategia_principaisfornecedores"]=$this->input->get_post('estrategia_principaisfornecedores');
        $this->data["estrategia_erp"]=is_array($this->input->get_post('estrategia_erp'))?implode("#", $this->input->get_post('estrategia_erp')):"";
        $this->data["estrategia_consideracaoconsultor1"]=$this->input->get_post('estrategia_consideracaoconsultor1');
        $this->data["estrategia_consideracaodata1"]=substr($this->input->get_post('estrategia_consideracaodata1'),6,4)."-".substr($this->input->get_post('estrategia_consideracaodata1'),3,2)."-".substr($this->input->get_post('estrategia_consideracaodata1'),0,2); 
        $this->data["estrategia_consideracaoconsultor2"]=$this->input->get_post('estrategia_consideracaoconsultor2');
        $this->data["estrategia_consideracaodata2"]=substr($this->input->get_post('estrategia_consideracaodata2'),6,4)."-".substr($this->input->get_post('estrategia_consideracaodata2'),3,2)."-".substr($this->input->get_post('estrategia_consideracaodata2'),0,2); 
        $this->data["estrategia_consideracaoconsultor3"]=$this->input->get_post('estrategia_consideracaoconsultor3');
        $this->data["estrategia_consideracaodata3"]=substr($this->input->get_post('estrategia_consideracaodata3'),6,4)."-".substr($this->input->get_post('estrategia_consideracaodata3'),3,2)."-".substr($this->input->get_post('estrategia_consideracaodata3'),0,2); 
        $this->data["estrategia_modulos"]=$this->input->get_post('estrategia_modulos');
        $this->data["estrategia_concorrentes"]=$this->input->get_post('estrategia_concorrentes');
        $this->data["estrategia_linksmidias"]=$this->input->get_post('estrategia_linksmidias');
        $this->data["estrategia_blog"]=$this->input->get_post('estrategia_blog');
        $this->data["estrategia_quiz"]=$this->input->get_post('estrategia_quiz');
        
        $this->data["folder"]=md5($cod_interno);
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       
		
		$this->db->where('cod_cliente = ',$cod_cliente);
		$this->db->update('cad_cliente', $this->data);
        
        $this->salvar_evento($cod_cliente);
        $this->salvar_quadro($cod_cliente);
        $this->salvar_alvaras($cod_cliente);
        
        //Upload do termo de uso
        $name = $_FILES['termodeuso']['name'];
        $size = $_FILES['termodeuso']['size'];    
        $tmp_name = $_FILES['termodeuso']['tmp_name'];
        
        $folder = $this->functions->cliente_folder($cod_interno);
        $arquivo = "{$folder}termodeuso.pdf";
        
        move_uploaded_file($tmp_name, $arquivo);
        
        //Upload do certificado
        $name = $_FILES['certificado']['name'];
        $size = $_FILES['certificado']['size'];    
        $tmp_name = $_FILES['certificado']['tmp_name'];
        $ext = pathinfo($name, PATHINFO_EXTENSION);        
        
        $folder = $this->functions->cliente_folder($cod_interno);
        $arquivo = "{$folder}certificado.{$ext}";
        
        move_uploaded_file($tmp_name, $arquivo);
        
        //Upload do estrategia_anexoestagioqualificacao
        $name = $_FILES['estrategia_anexoestagioqualificacao']['name'];
        $size = $_FILES['estrategia_anexoestagioqualificacao']['size'];    
        $tmp_name = $_FILES['estrategia_anexoestagioqualificacao']['tmp_name'];
        $ext = pathinfo($name, PATHINFO_EXTENSION);        
        
        $folder = $this->functions->cliente_folder($cod_interno);
        $arquivo = "{$folder}estrategia_anexoestagioqualificacao.{$ext}";
        
        move_uploaded_file($tmp_name, $arquivo);
        
        //Upload do estrategia_anexokpijuridico
        $name = $_FILES['estrategia_anexokpijuridico']['name'];
        $size = $_FILES['estrategia_anexokpijuridico']['size'];    
        $tmp_name = $_FILES['estrategia_anexokpijuridico']['tmp_name'];
        $ext = pathinfo($name, PATHINFO_EXTENSION);        
        
        $folder = $this->functions->cliente_folder($cod_interno);
        $arquivo = "{$folder}estrategia_anexokpijuridico.{$ext}";
        
        move_uploaded_file($tmp_name, $arquivo);        
        
    }

    public function salvar_quadro($cod_cliente) {
        $this->db->where("cod_cliente", $cod_cliente);
		$this->db->delete('cad_clientequadro');

        $quadro_nome = $this->input->get_post("quadro_nome");
        $quadro_documento = $this->input->get_post("quadro_documento");
        $quadro_ordem = $this->input->get_post("quadro_ordem");
        $quadro_participacao = $this->input->get_post("quadro_participacao");
        $quadro_quotas = $this->input->get_post("quadro_quotas");
        $quadro_data_registro = $this->input->get_post("quadro_data_registro");
        $quadro_data_inicio = $this->input->get_post("quadro_data_inicio");
        $quadro_data_saida = $this->input->get_post("quadro_data_saida");
        $quadro_responsavel = $this->input->get_post("quadro_responsavel");
        
        if ($quadro_nome==NULL) return;

        foreach( $quadro_nome as $key => $n ) {
            if($quadro_nome[$key]<>""){
                $this->data_quadro[] = array('cod_cliente'=>$cod_cliente,
                                             'nome'=>$quadro_nome[$key],
                                             'documento'=>$quadro_documento[$key],
                                             'ordem'=>$quadro_ordem[$key],
                                             'participacao'=>$quadro_participacao[$key],
                                             'quotas'=>$quadro_quotas[$key],
                                             'data_registro'=>substr($quadro_data_registro[$key],6,4)."-".substr($quadro_data_registro[$key],3,2)."-".substr($quadro_data_registro[$key],0,2),
                                             'data_inicio'=>substr($quadro_data_inicio[$key],6,4)."-".substr($quadro_data_inicio[$key],3,2)."-".substr($quadro_data_inicio[$key],0,2),
                                             'data_saida'=>substr($quadro_data_saida[$key],6,4)."-".substr($quadro_data_saida[$key],3,2)."-".substr($quadro_data_saida[$key],0,2),
                                             'responsavel'=>$quadro_responsavel[$key],
                                              );
            }
        }     
        //print_r($this->data_quadro);exit;
        if (!empty($this->data_quadro)) $this->db->insert_batch('cad_clientequadro', $this->data_quadro);
    }

    public function salvar_alvaras($cod_cliente) {
        log_message('debug', "salvar_alvaras: $cod_cliente");
        $this->db->where("cod_cliente", $cod_cliente);
		$this->db->delete('cad_clientealvara');

        $alvara_cod_alvara = $this->input->get_post("alvara_cod_alvara");
        $alvara_nome = $this->input->get_post("alvara_nome");
        $alvara_numero = $this->input->get_post("alvara_numero");
        $alvara_vencimento = $this->input->get_post("alvara_vencimento");
        $alvara_data_vencimento = $this->input->get_post("alvara_data_vencimento");
        $alvara_data_pagamento = $this->input->get_post("alvara_data_pagamento");
        //$alvara_valor = $this->input->get_post("alvara_valor");
        
        if ($alvara_numero==NULL) return;

        foreach( $alvara_numero as $key => $n ) {
            if($alvara_numero[$key]<>""){
                $this->data_alvaras[] = array('cod_cliente'=>$cod_cliente,
                                             'cod_alvara'=>$alvara_cod_alvara[$key],
                                             'nome'=>$alvara_nome[$key],
                                             'numero'=>$alvara_numero[$key],
                                             'vencimento'=>$alvara_vencimento[$key],
                                             'data_vencimento'=>substr($alvara_data_vencimento[$key],6,4)."-".substr($alvara_data_vencimento[$key],3,2)."-".substr($alvara_data_vencimento[$key],0,2),
                                             'data_pagamento'=>substr($alvara_data_pagamento[$key],3,4)."-".substr($alvara_data_pagamento[$key],0,2)."-01",
                                             //'valor'=>$alvara_valor[$key],
                                              );
            }
        }     
        print_r($this->data_alvaras);
        if (!empty($this->data_alvaras)) $this->db->insert_batch('cad_clientealvara', $this->data_alvaras);
    }

    public function salvar_evento($cod_cliente) {
        
        $cliente_evento = $this->input->get_post('cliente_evento');
        
        $this->db->where("cod_cliente", $cod_cliente);
		$this->db->delete('cad_clienteevento');
        if ($cliente_evento){
            foreach($cliente_evento as $value){
                $this->data_evento["cod_cliente"]=$cod_cliente;
                $this->data_evento["cod_evento"]=$value;
                $this->data_evento['cod_usuario_c']=$this->session->userdata('cod_usuario');       
                $this->data_evento['data_c']=date('Y-m-d H:i:s');       
                $this->data_evento['cod_usuario_a']=$this->session->userdata('cod_usuario');       
                $this->data_evento['data_a']=date('Y-m-d H:i:s');       

                $this->db->insert('cad_clienteevento', $this->data_evento);
            }
        }
    }

    public function associar_evento($associar,$cod_cliente,$cod_evento) {
        
        if ($associar=="A"){
            $this->data_evento["cod_cliente"]=$cod_cliente;
            $this->data_evento["cod_evento"]=$cod_evento;
            $this->data_evento['cod_usuario_c']=$this->session->userdata('cod_usuario');       
            $this->data_evento['data_c']=date('Y-m-d H:i:s');       
            $this->data_evento['cod_usuario_a']=$this->session->userdata('cod_usuario');       
            $this->data_evento['data_a']=date('Y-m-d H:i:s');       

            $this->db->insert('cad_clienteevento', $this->data_evento);
            
            return json_encode(array('op'=>'1', 'msg' => "Associado {$cod_evento} ao cliente {$cod_cliente}", 'cod_clienteevento' => $this->db->insert_id()));

        } elseif ($associar=="R") {
            $this->db->where("cod_cliente", $cod_cliente);
            $this->db->where("cod_evento", $cod_evento);
            $this->db->delete('cad_clienteevento');

            return json_encode(array('op'=>'1', 'msg' => "Removido {$cod_evento} do cliente {$cod_cliente}"));
        }

    }

    public function excluir($cod_cliente) {

        //Faz backup da conta nas tabelas auxiliares
		$this->db->query("insert cad_cliente_ select * from cad_cliente where cod_cliente = '{$cod_cliente}'");
		$this->db->query("insert cad_clientealvara_ select * from cad_clientealvara where cod_cliente = '{$cod_cliente}'");
		$this->db->query("insert cad_clienteevento_ select * from cad_clienteevento where cod_cliente = '{$cod_cliente}'");
		$this->db->query("insert cad_clientenotificacao_ select * from cad_clientenotificacao where cod_cliente = '{$cod_cliente}'");
		$this->db->query("insert cad_clientequadro_ select * from cad_clientequadro_ where cod_cliente = '{$cod_cliente}'");
		$this->db->query("insert cad_arquivo_ select * from cad_arquivo where cod_cliente = '{$cod_cliente}'");
		$this->db->query("insert cad_funcionario_ select * from cad_funcionario where cod_cliente = '{$cod_cliente}'");
        $this->db->query("update cad_cliente_ set cod_usuario_e = '".$this->session->userdata('cod_usuario')."', data_e = '".date('Y-m-d H:i:s')."' where cod_cliente = '{$cod_cliente}'");
		
		//Deleta os registros
		$this->db->query("delete from cad_cliente where cod_cliente = '{$cod_cliente}'");
		$this->db->query("delete from cad_clientealvara where cod_cliente = '{$cod_cliente}'");
		$this->db->query("delete from cad_clienteevento where cod_cliente = '{$cod_cliente}'");
		$this->db->query("delete from cad_clientenotificacao where cod_cliente = '{$cod_cliente}'");
		$this->db->query("delete from cad_clientequadro where cod_cliente = '{$cod_cliente}'");
		$this->db->query("delete from cad_funcionario where cod_cliente = '{$cod_cliente}'");
		$this->db->query("delete from cad_arquivo where cod_cliente = '{$cod_cliente}'");
        
    }    
    public function evento($data="",$cod_cliente="",$busca_cliente="",$busca_evento="") {

        $retorno = array();        

        $data = explode("/",$data);
        $mes = $data[1];
        $ano = $data[2];

        $retorno["mes"] = $mes;
        $retorno["ano"] = $ano;

        //Clientes
        $this->db->select(" 'cliente.evento',
						  	ccli.cod_cliente,
                            ccli.cod_interno,
                            concat(mid(nome,1,30),'...')  as 'nome',
                            ccli.uf,
                            ccli.cidade"
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->where("(ccli.cod_cliente in (".$this->session->userdata('clientes_codigos')."0) or '".$this->session->userdata('admin')."'='S')");        
        $this->db->where("ccli.ativo = 'S'");    
        if ($cod_cliente<>"") $this->db->where("ccli.cod_cliente = '{$cod_cliente}'");    
        if ($busca_cliente<>"") $this->db->where("ccli.nome like '%{$busca_cliente}%'");
        $this->db->order_by("ccli.nome", "asc");
		
        $query = $this->db->get(); 
        $cliente = $query->result_array(); 

        $retorno ["cliente"] = $cliente;

        //Eventos
        $this->db->select(" 'cliente.evento',
						  	ceve.cod_evento,
                            ceve.nome,
                            ceve.sigla"
						);
        
        $this->db->from('cad_evento ceve');
        if ($busca_evento<>"") $this->db->where("(ceve.nome like '%{$busca_evento}%' or ceve.sigla like '%{$busca_evento}%')");
        $this->db->order_by("ceve.sigla", "asc");
		
        $query = $this->db->get(); 
        $evento = $query->result_array(); 

        foreach($evento as $value){
            $retorno["evento"][$value["cod_evento"]]=array(   
                                                            'cod_evento'=>$value["cod_evento"],
                                                            'nome'=>$value["nome"],
                                                            'sigla'=>$value["sigla"],
                                                            'dia'=>'',
                                                            'calendario'=>''
                                                        );            
        }

        //Feriados
        $this->db->select(" 'cliente.evento',
                            cfer.cod_feriado as 'cod',
                            cfer.nome,
                            cfer.uf,
                            cfer.cidade,
                            date_format(concat({$ano},'-',month(cfer.data),'-',day(cfer.data)), '%Y-%m-%d') as 'data',
                            date_format(concat({$ano},'-',month(cfer.data),'-',day(cfer.data)), '%d/%m/%Y') as 'data_'
                            "
        );

        $this->db->from('cad_feriado cfer');
        $this->db->where("month(cfer.data)='{$mes}'");

        $query = $this->db->get(); 
        $feriado = $query->result_array(); 

        $retorno ["feriado"] = $feriado;

        //Eventos dos clientes
        $this->db->select(" 'cliente.evento', 
                            ccev.cod_clienteevento, 
                            ceve.cod_evento, 
                            ccev.cod_cliente, 
                            ceve.cod_pasta, 
                            ceve.tipo, 
                            ceve.calendario, 
                            ceve.nome,
                            ceve.sigla,
                            ceve.vencimento,
						  	ceve.vencimento_dias,
						  	ceve.vencimento_diastipo,
						  	ceve.vencimento_meses,
                            ceve.vencimento_ajuste,
                            ceve.texto,
                            ccli.uf,
                            ccli.cidade,
                            date_format(carq.data_c,'%d/%m/%Y') as 'data_entrega',
                            date_format(carq.data_c,'%d/%m/%Y') as 'data_entrega_',
                            date_format(carq.data_vencimento,'%d/%m/%Y') as 'data_vencimento',
                            date_format(carq.data_vencimento,'%d/%m/%Y') as 'data_vencimento_',
                            carq.arquivo,
                            cpas.nome as 'pasta'
                            "
						);
        
        $this->db->from('cad_evento ceve');
        $this->db->join('cad_clienteevento ccev', "ccev.cod_evento = ceve.cod_evento and (ccev.cod_cliente = '{$cod_cliente}' or '{$cod_cliente}'='')",'inner');
        $this->db->join('cad_cliente ccli', "ccli.cod_cliente = ccev.cod_cliente",'inner');
        $this->db->join('cad_arquivo carq', "(carq.cod_cliente = ccli.cod_cliente and carq.cod_evento = ceve.cod_evento and month(carq.data_vencimento)='{$mes}' and year(carq.data_vencimento)='{$ano}')",'left');
        $this->db->join('cad_pasta cpas', "cpas.cod_pasta = ceve.cod_pasta",'inner');
        $this->db->where("mes_{$mes}='S'");
        if ($busca_cliente<>"") $this->db->where("ccli.nome like '%{$busca_cliente}%'");
        if ($busca_evento<>"") $this->db->where("(ceve.nome like '%{$busca_evento}%' or ceve.sigla like '%{$busca_evento}%')");
        
        $query = $this->db->get(); 
        $evento_cliente = $query->result_array(); 

        foreach($evento_cliente as $value){

            $data_vencimento=mktime(0, 0, 0, $mes, 1, $ano);
            
            $var['data_vencimento'] = date('Y-m-d', $data_vencimento);
            $var['vencimento_dias'] = $value['vencimento_dias'];
            $var['vencimento_diastipo'] = $value['vencimento_diastipo'];
            $var['vencimento_meses'] = 0;
            $var['vencimento_ajuste'] = $value['vencimento_ajuste'];

            $data_vencimento = $this->functions->evento_calculavencimento($var,$feriado,$value["uf"],$value["cidade"]);
            
            $retorno["evento_cliente"][$value["cod_cliente"]][$value["cod_evento"]]=array(   
                                                                                            'cod_clienteevento'=>$value["cod_clienteevento"],
                                                                                            'tipo'=>$value["tipo"],
                                                                                            'dia'=>date('d', $data_vencimento),
                                                                                            'dia'=>date('d', $data_vencimento),
                                                                                            'data_vencimento'=>date('Y-m-d', $data_vencimento),
                                                                                            'data_vencimento_'=>date('d-m-Y', $data_vencimento),
                                                                                            'data_entrega'=>$value["data_entrega"],
                                                                                            'data_entrega_'=>$value["data_entrega_"],
                                                                                            'nome'=>$value["nome"],
                                                                                            'arquivo'=>$value["arquivo"],
                                                                                            'pasta'=>$value["pasta"],
                                                                                            'calendario'=>$value["calendario"]
                                                                                        );
            $retorno["evento"][$value["cod_evento"]]["dia"]=date('d', $data_vencimento);
            $retorno["evento"][$value["cod_evento"]]["calendario"]=$value["calendario"];
            
        }

        //Coletas manuais
        $this->db->select(" 'cliente.evento',
                            ccol.cod_coleta as 'cod',
                            'C' as 'tipo',
                            ccol.cod_pasta,
                            ccol.texto as 'nome',
                            ccol.texto,
                            ccol.data_vencimento,
                            date_format(ccol.data_vencimento, '%d/%m/%Y') as 'data_vencimento_',
                            cpas.nome as 'pasta'
                            "
                        );

        $this->db->from('cad_coleta ccol');
        $this->db->join('cad_coletacliente ccolcli', "(ccolcli.cod_coleta = ccol.cod_coleta and ccolcli.cod_cliente = '{$cod_cliente}')",'inner');
        $this->db->join('cad_pasta cpas', "cpas.cod_pasta = ccol.cod_pasta",'inner');
        $this->db->where("ccol.cod_evento is null");
        $this->db->where("month(ccol.data_vencimento)='{$mes}'");
        $this->db->where("year(ccol.data_vencimento)='{$ano}'");
        $this->db->order_by("ccol.data_vencimento", "asc");

        $query = $this->db->get(); 
        $coleta = $query->result_array(); 

        $retorno ["coleta"] = $coleta;

        //var_dump( $retorno["coleta"]);
        //var_dump($retorno["evento_cliente"]);     
        //exit;   
        return $retorno;
    }

    /*
    public function listar_calendario($cod_cliente,$mes="",$ano="") {

        $retorno = array();        

        //Feriados
        $this->db->select(" 'cliente.listar_calendario',
                            cfer.cod_feriado as 'cod',
                            cfer.nome,
                            date_format(concat({$ano},'-',month(cfer.data),'-',day(cfer.data)), '%Y-%m-%d') as 'data',
                            date_format(concat({$ano},'-',month(cfer.data),'-',day(cfer.data)), '%d/%m/%Y') as 'data_'
                            "
                        );

        $this->db->from('cad_feriado cfer');
        $this->db->where("month(cfer.data)='{$mes}'");

        $query = $this->db->get(); 
        $feriado = $query->result_array(); 
        
        foreach($feriado as $value){
            $retorno[]=array(   'origem'=>"feriado",
                                'cod'=>$value["cod"],
                                'tipo'=>'',
                                'nome'=>$value["nome"],
                                'texto'=>'',
                                'data_vencimento'=>$value["data"],
                                'data_vencimento_'=>$value["data_"],
                                'pasta'=>'',
                                'arquivo'=>"",
                                'caminho'=>"",
                            );
        }        

        //Eventos
        $this->db->select(" 'cliente.listar_calendario', 
                            ceve.cod_evento as 'cod', 
                            ccev.cod_cliente, 
                            ceve.cod_pasta, 
                            ceve.tipo, 
                            ceve.nome,
                            ceve.vencimento,
						  	ceve.vencimento_dias,
						  	ceve.vencimento_diastipo,
						  	ceve.vencimento_meses,
                            ceve.vencimento_ajuste,
                            ceve.texto, 
                            cpas.nome as 'pasta',
                            ccli.uf,
                            ccli.cidade,
                            date_format(carq.data_c,'%d/%m/%Y') as 'data_entrega_',
                            date_format(carq.data_vencimento,'%d/%m/%Y') as 'data_vencimento_',
                            carq.arquivo
                            "
						);
        
        $this->db->from('cad_evento ceve');
        $this->db->join('cad_clienteevento ccev', "ccev.cod_evento = ceve.cod_evento and ccev.cod_cliente = {$cod_cliente}",'inner');
        $this->db->join('cad_cliente ccli', "ccli.cod_cliente = ccev.cod_cliente",'inner');
        $this->db->join('cad_pasta cpas', "cpas.cod_pasta = ceve.cod_pasta",'inner');
        $this->db->join('cad_arquivo carq', "(carq.cod_cliente = ccli.cod_cliente and carq.cod_evento = ceve.cod_evento and month(carq.data_vencimento)='{$mes}' and year(carq.data_vencimento)='{$ano}')",'left');
        $this->db->where("ceve.calendario = 'S'");
        $this->db->where("mes_{$mes}='S'");
        $this->db->order_by("carq.data_vencimento", "asc");
        
        $query = $this->db->get(); 
        $eventos = $query->result_array(); 

        foreach($eventos as $value){

            $data_vencimento=mktime(0, 0, 0, $mes, 1, $ano);
            
            $var['data_vencimento'] = date('Y-m-d', $data_vencimento);
            $var['vencimento_dias'] = $value['vencimento_dias'];
            $var['vencimento_diastipo'] = $value['vencimento_diastipo'];
            $var['vencimento_meses'] = 0;
            $var['vencimento_ajuste'] = $value['vencimento_ajuste'];

            $data_vencimento = $this->functions->evento_calculavencimento($var,$feriado,$value['uf'],$value['cidade']);
            
            $retorno[]=array(   'origem'=>"evento",
                                'cod'=>$value["cod"],
                                'tipo'=>$value["tipo"],
                                'nome'=>$value["nome"],
                                'texto'=>$value["texto"],
                                'data_vencimento'=>date('Y-m-d', $data_vencimento),
                                'data_vencimento_'=>date('d-m-Y', $data_vencimento),
                                'pasta'=>$value["pasta"],
                                'arquivo'=>$value['arquivo'],
                                'caminho'=>"",
                            );
        }

        //Verifica se existem coletas manuais
        $this->db->select(" 'cliente.listar_calendario',
                ccol.cod_coleta as 'cod',
                'C' as 'tipo',
                ccol.cod_pasta,
                ccol.texto as 'nome',
                ccol.texto,
                ccol.data_vencimento,
                date_format(ccol.data_vencimento, '%d/%m/%Y') as 'data_vencimento_',
                cpas.nome as 'pasta'
                "
            );

        $this->db->from('cad_coleta ccol');
        $this->db->join('cad_coletacliente ccolcli', "(ccolcli.cod_coleta = ccol.cod_coleta and ccolcli.cod_cliente = '{$cod_cliente}')",'inner');
        $this->db->join('cad_pasta cpas', "cpas.cod_pasta = ccol.cod_pasta",'inner');
        $this->db->where("ccol.cod_evento is null");
        $this->db->where("month(ccol.data_vencimento)='{$mes}'");
        $this->db->where("year(ccol.data_vencimento)='{$ano}'");
        $this->db->order_by("ccol.data_vencimento", "asc");

        $query = $this->db->get(); 
        $dados = $query->result_array(); 

        foreach($dados as $value){
            $retorno[]=array(   'origem'=>"coleta",
                                'cod'=>$value["cod"],
                                'tipo'=>$value["tipo"],
                                'nome'=>$value["nome"],
                                'texto'=>$value["texto"],
                                'data_vencimento'=>$value["data_vencimento"],
                                'data_vencimento_'=>$value["data_vencimento_"],
                                'pasta'=>$value["pasta"],
                                'arquivo'=>"",
                                'caminho'=>"",
                            );
        }

        //var_dump($retorno);exit;
        return $retorno;

    }
    */

    public function cliente_possuievento($cod_cliente="",$cod_evento="") {
        
		$this->db->select(" 'cliente.cliente_possuievento',
                            ccev.cod_cliente,
                            ccev.cod_evento"
						);
        
        $this->db->from('cad_clienteevento ccev');
        $this->db->where("ccev.cod_cliente", $cod_cliente);
        $this->db->where("ccev.cod_evento", $cod_evento);		
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }
    
    public function cliente_evento($cod_cliente="") {
        
		$this->db->select(" 'cliente.cliente_evento',
                            ceve.cod_evento,
                            ceve.cod_pasta,
                            ceve.nome as 'evento',
                            ceve.sigla,
                            ccev.cod_cliente,
                            cpas.nome as 'cod_pasta'"
						);
        
        $this->db->from('cad_evento ceve');
		$this->db->join('cad_clienteevento ccev', "(ccev.cod_evento = ceve.cod_evento and ccev.cod_cliente = '{$cod_cliente}')",'left');
		$this->db->join('cad_pasta cpas', "cpas.cod_pasta = ceve.cod_pasta",'left');
        
        $this->db->order_by("cpas.nome", "asc");
        $this->db->order_by("ceve.nome", "asc");
		
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function favoritar($cod_cliente) {
        
        $this->data["cod_cliente"]=$cod_cliente;
        $this->data["cod_usuario"]=$this->session->userdata('cod_usuario');
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       
		
        $this->db->insert('cad_usuarioclientefavorito', $this->data);
        
    }    
    
    public function desfavoritar($cod_cliente) {
        
        $this->db->where("cod_cliente", $cod_cliente);
        $this->db->where("cod_usuario", $this->session->userdata('cod_usuario'));
		$this->db->delete('cad_usuarioclientefavorito');
        
    }    
    
    public function estrategia($cod_cliente) {
        
        $this->db->select(" 'cliente.relatorio_estrategia',
						  	ccli.cod_cliente,
						  	ccli.cod_interno,
                            ccli.cod_regime,
						  	ccli.nome,
						  	ccli.razao_social,
						  	ccli.documento,
                            case when length (ccli.documento)<= 12 then insert( insert( insert( ccli.documento, 10, 0, '-' ), 7, 0, '.' ), 4, 0, '.' ) else insert( insert( insert( insert( ccli.documento, 13, 0, '-' ), 9, 0, '/' ), 6, 0, '.' ), 3, 0, '.' ) end as 'documento_',
						  	ccli.ie,
						  	ccli.im,
						  	ccli.certificado_vencimento,
						  	ccli.certificado_posse,
                            concat(mid(certificado_vencimento,9,2),'/',mid(certificado_vencimento,6,2),'/',mid(certificado_vencimento,1,4)) as 'certificado_vencimento_',
						  	ccli.email,
						  	ccli.telefone,
						  	ccli.folder,
						  	ccli.atividade_holding,
						  	ccli.atividade_aluguel,
						  	ccli.atividade_industria,
						  	ccli.atividade_comercio,
						  	ccli.atividade_servico,
                            
                            ccli.estrategia,
                            ccli.estrategia_sobreempresa,
                            ccli.estrategia_site,
                            ccli.cod_estrategia_regimetributario,
                            date_format(ccli.estrategia_regimetributariodata,'%d/%m/%Y') as 'estrategia_regimetributariodata',
                            ccli.estrategia_numeromatriz,
                            ccli.estrategia_grupoempresarial,
                            ccli.estrategia_numeroprofissionais,
                            ccli.estrategia_quantidadesocios,
                            ccli.estrategia_sociosoutraempresa,
                            ccli.estrategia_atividades,
                            ccli.estrategia_faturamento,
                            ccli.estrategia_sobreprojeto,
                            ccli.estrategia_produtoservico,
                            ccli.estrategia_pessoacontato,
                            ccli.estrategia_objetivoestrategia,
                            ccli.estrategia_estagioqualificacao,
                            ccli.estrategia_kpicontabilidade,
                            ccli.estrategia_kpitributario,
                            ccli.estrategia_kpitrabalhista,
                            ccli.estrategia_kpisocietario,
                            ccli.estrategia_kpijuridico,
                            ccli.estrategia_kpiqualificacao,
                            ccli.estrategia_objetivoestagioatual,
                            ccli.estrategia_principaisclientes,
                            ccli.estrategia_principaisfornecedores,
                            ccli.estrategia_erp,
                            ccli.estrategia_consideracaoconsultor1,
                            date_format(ccli.estrategia_consideracaodata1,'%d/%m/%Y') as 'estrategia_consideracaodata1',
                            ccli.estrategia_consideracaoconsultor2,
                            date_format(ccli.estrategia_consideracaodata2,'%d/%m/%Y') as 'estrategia_consideracaodata2',
                            ccli.estrategia_consideracaoconsultor3,
                            date_format(ccli.estrategia_consideracaodata3,'%d/%m/%Y') as 'estrategia_consideracaodata3',
                            ccli.estrategia_modulos,
                            ccli.estrategia_concorrentes,
                            ccli.estrategia_linksmidias,
                            ccli.estrategia_blog,
                            ccli.estrategia_quiz,
                            
                            cct.nome as 'certificadotipo',
                            cr.nome as 'regimetributario',
                            cr1.nome as 'estrategia_regimetributario'
                            "
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->join('cad_certificadotipo cct', "cct.cod_certificadotipo = ccli.cod_certificadotipo",'left');
        $this->db->join('cad_regime cr', "cr.cod_regime = ccli.cod_regime",'left');
        $this->db->join('cad_regime cr1', "cr1.cod_regime = ccli.cod_estrategia_regimetributario",'left');
        
        $this->db->where("ccli.cod_cliente = '{$cod_cliente}'");
                
		$query = $this->db->get(); 
        return $query->row_array();
    }
    
    public function relatorio_regimetributario() {
        
        $this->db->select(" 'cliente.relatorio_regimetributario',
						  	ccli.cod_cliente,
						  	ccli.cod_interno,
                            ccli.cod_regime,
						  	ccli.nome,
						  	ccli.razao_social,
						  	ccli.documento,
                            case when length (ccli.documento)<= 12 then insert( insert( insert( ccli.documento, 10, 0, '-' ), 7, 0, '.' ), 4, 0, '.' ) else insert( insert( insert( insert( ccli.documento, 13, 0, '-' ), 9, 0, '/' ), 6, 0, '.' ), 3, 0, '.' ) end as 'documento_',
						  	ccli.ie,
						  	ccli.im,
                            date_format(ccli.estrategia_regimetributariodata,'%d/%m/%Y') as 'estrategia_regimetributariodata',
                            cr.nome as 'regime'
                            "
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->join('cad_regime cr', "cr.cod_regime = ccli.cod_estrategia_regimetributario",'left');
        $this->db->order_by("ccli.estrategia_regimetributariodata", "asc");                
        
		$query = $this->db->get(); 
		return $query->result_array();
    }
    
    public function relatorio_alvaras() {
        
        $this->db->select(" 'cliente.relatorio_alvaras',
						  	cca.cod_clientealvara,
						  	cca.nome,
						  	cca.numero,
						  	cca.data_vencimento,
						  	date_format(cca.data_vencimento,'%d/%m/%Y') as 'data_vencimento_',
						  	cca.data_pagamento,
						  	date_format(cca.data_pagamento,'%d/%m/%Y') as 'data_pagamento_',
						  	cca.valor,
                            ccli.nome as 'cliente',
                            calv.nome as 'alvara'"
							,FALSE
						);
        
        $this->db->from('cad_clientealvara cca');
        $this->db->join('cad_cliente ccli', "ccli.cod_cliente = cca.cod_cliente",'left');
        $this->db->join('cad_alvara calv', "cca.cod_alvara = calv.cod_alvara",'left');
        $this->db->where("cca.data_vencimento<=now() and (cca.data_pagamento is null or cca.data_pagamento= '0000-00-00')");
        $this->db->order_by("cca.data_vencimento", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();
    }
    
    public function relatorio_quadro($cod_cliente="") {
        
        $this->db->select(" 'cliente.relatorio_quadro',
						  	ccq.cod_clientequadro,
						  	ccq.nome,
						  	ccq.documento,
						  	ccq.ordem,
						  	ccq.participacao,
						  	ccq.quotas,
                            ccq.data_registro,
						  	date_format(ccq.data_registro,'%d/%m/%Y') as 'data_registro_',
                            ccq.data_inicio,
						  	date_format(ccq.data_inicio,'%d/%m/%Y') as 'data_inicio_',
						  	ccq.data_saida,
						  	date_format(ccq.data_saida,'%d/%m/%Y') as 'data_saida_',
                            ccq.responsavel,
                            ccli.nome as 'cliente'"
							,FALSE
						);
        
        $this->db->from('cad_clientequadro ccq');
        $this->db->join('cad_cliente ccli', "ccli.cod_cliente = ccq.cod_cliente",'left');

        if ($cod_cliente<>"") $this->db->where("ccq.cod_cliente = '{$cod_cliente}'");

        $this->db->order_by("ccli.nome,ccq.ordem", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();
    }
    
    public function comunicar_estrategia($cod_cliente) {
        
        $nome = $this->input->get_post('nome');
        
        //Envia email
        $this->db->select(" 'cliente.comunicar_estrategia',
                            ccli.folder,
                            cusu.nome,
                            cusu.email,
                            ccli.nome as 'cliente',
                            ccli.razao_social"
						);
        
        $this->db->from('cad_usuario cusu');
        $this->db->join('cad_usuariocliente cucli', "cucli.cod_usuario = cusu.cod_usuario",'inner');
        $this->db->join('cad_cliente ccli', "ccli.cod_cliente = cucli.cod_cliente",'inner');

        $this->db->where("cucli.cod_cliente = '{$cod_cliente}'");
        $this->db->where("cusu.email <> '' and cusu.config_receberemail = 'S'");
        $this->db->group_by('cusu.cod_usuario');        
        
        $query = $this->db->get();
        $dados = $query->result_array();
        
        $link = LOCAL."cliente/relatorio_estrategia/{$cod_cliente}";
        
        foreach($dados as $value){

            $template = $this->Template_model->editar(5);

            $myemail = new Myemail;
            $myemail->cod_cliente(0);
            $myemail->from(TITULO, EMAIL_NR);
            $myemail->to("BAH",$value["email"]);
            $myemail->subject($template["titulo"]);
            $myemail->body($template["texto"]);
            $myemail->custom(array("%USUARIO%"=>$value["nome"],"%CLIENTE%"=>$value["cliente"],"%RAZAOSOCIAL%"=>$value["razao_social"]));
            $myemail->enviar();

        }        
        
    }
    
    //JSON

    public function json_clientelistar($cod_usuario="") {
        
		$this->db->select(" 'cliente.json_clientelistar',
						  	cucli.cod_cliente,
							ccli.nome"
							,FALSE
						);
        
        $this->db->from('cad_usuariocliente cucli');
        $this->db->join('cad_cliente ccli', "cucli.cod_cliente = ccli.cod_cliente",'inner');
        
        $this->db->where("cucli.cod_usuario = '{$cod_usuario}'"); 
           
		$query = $this->db->get(); 
		return json_encode($query->result_array());    
        
    }    
    
    public function ajax_painel($mes,$ano,$cod_usuario,$cod_cliente) {   
        
        $this->db->select(" 'cliente.ajax_painel',
                            ccli.cod_cliente, 
                            ccli.cod_interno, 
                            concat(ccli.nome,' (',ccli.cod_interno,')') as 'cliente',
                            ceve.nome as 'evento',
                            ceve.sigla,
                            date_format(carq.data_competencia,'%d/%m/%Y') as 'data_competencia',
                            date_format(carq.data_vencimento,'%d/%m/%Y') as 'data_vencimento',
                            cpas.nome as 'cod_pasta',
                            date_format(carq.data_a,'%d/%m/%Y') as 'data_a',
                            carq.arquivo,
                            carq.nome_original,
                            carq.situacao,
                            '0' as 'favorito',
                            concat('".LOCAL."data/',md5(ccli.cod_interno),'/',carq.arquivo) as 'caminho'"
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->join('cad_clienteevento ccev', "ccli.cod_cliente = ccev.cod_cliente",'inner');
        $this->db->join('cad_evento ceve', "ceve.cod_evento = ccev.cod_evento",'left');        
        $this->db->join('cad_pasta cpas', "cpas.cod_pasta = ceve.cod_pasta",'left');        
        $this->db->join('cad_usuariopasta cupas', "(cupas.cod_pasta = ceve.cod_pasta and cupas.cod_usuario = '{$cod_usuario}')",'inner');         
        $this->db->join('cad_arquivo carq', "(carq.cod_cliente = ccli.cod_cliente and carq.cod_evento = ceve.cod_evento and month(carq.data_vencimento)={$mes} and year(carq.data_vencimento)={$ano})",'inner');  
        
        $this->db->where("ccli.cod_cliente = {$cod_cliente}");
        $this->db->where("month(carq.data_vencimento) = {$mes}");  
        
        $this->db->order_by("ceve.nome", "asc");
        $this->db->order_by("carq.data_vencimento", "desc");
		
		$query = $this->db->get(); 
		return json_encode($query->result_array());
    }
    
    public function json_buscalistar($cod_usuario="") {
        
        $referencia = explode("~", $this->input->get_post('referencia'));
        $mes = $referencia[0];
        $ano = $referencia[1];
		$cod_pasta = $this->input->get_post('cod_pasta');
		$documento = $this->input->get_post('documento');
        
        $this->db->select(" 'cliente.json_buscalistar{$cod_pasta}',
                            ccli.cod_cliente, 
                            ccli.cod_interno, 
                            concat(ccli.nome,' (',ccli.cod_interno,')') as 'cliente',
                            ceve.nome as 'evento',
                            ceve.sigla,
                            date_format(carq.data_competencia,'%d/%m/%Y') as 'data_competencia',
                            date_format(carq.data_vencimento,'%d/%m/%Y') as 'data_vencimento',
                            cpas.nome as 'cod_pasta',
                            date_format(carq.data_a,'%d/%m/%Y') as 'data_a',
                            carq.arquivo,
                            carq.nome_original,
                            carq.situacao,
                            concat('".LOCAL."data/',md5(ccli.cod_interno),'/',carq.arquivo) as 'caminho'"
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->join('cad_clienteevento ccev', "ccli.cod_cliente = ccev.cod_cliente",'inner'); 
        $this->db->join('cad_evento ceve', "ceve.cod_evento = ccev.cod_evento",'left');        
        $this->db->join('cad_pasta cpas', "cpas.cod_pasta = ceve.cod_pasta",'left');        
        $this->db->join('cad_arquivo carq', "(carq.cod_cliente = ccli.cod_cliente and carq.cod_evento = ceve.cod_evento and month(carq.data_vencimento)={$mes} and year(carq.data_vencimento)={$ano})",'inner');        
        
        $this->db->where("ccli.cod_cliente in (select cucli_.cod_cliente from cad_usuariocliente cucli_ where cucli_.cod_usuario = '{$cod_usuario}')");
        $this->db->where("month(carq.data_vencimento) = {$mes}"); 
        $this->db->where("(ceve.nome like '%{$evento}%' or carq.nome_original like '%{$evento}%')");           
        if ($cod_pasta<>"0") $this->db->where("cpas.cod_pasta = {$cod_pasta}");             
        
        $this->db->order_by("ceve.nome", "asc");
        $this->db->order_by("carq.data_vencimento", "desc");
		
		$query = $this->db->get(); 
		return json_encode($query->result_array());          
        
        
    }
        
    public function json_uploadlistar($cod_usuario="",$cod_pasta="") {

        $this->db->select(" 'cliente.json_uploadlistar',
                            ccli.cod_cliente, 
                            ccli.cod_interno, 
                            cpas.cod_pasta, 
                            concat(ccli.nome,' (',ccli.cod_interno,')') as 'cliente',
                            ceve.nome as 'evento',
                            ceve.sigla,
                            date_format(carq.data_vencimento, '%d/%m/%Y') as 'data_vencimento',
                            cpas.nome as 'cod_pasta',
                            date_format(carq.data_a,'%d/%m/%Y') as 'data_a',
                            carq.arquivo,
                            carq.nome_original,
                            carq.situacao,
                            concat('".LOCAL."data/',md5(ccli.cod_interno),'/',carq.arquivo) as 'caminho'"
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->join('cad_clienteevento ccev', "ccli.cod_cliente = ccev.cod_cliente",'inner');
        $this->db->join('cad_evento ceve', "ceve.cod_evento = ccev.cod_evento",'inner');        
        $this->db->join('cad_pasta cpas', "cpas.cod_pasta = ceve.cod_pasta",'inner');        
        $this->db->join('cad_arquivo carq', "(carq.cod_cliente = ccli.cod_cliente and carq.cod_evento = ceve.cod_evento)",'inner');        
        
        $this->db->where("ccli.cod_cliente in (select cucli_.cod_cliente from cad_usuariocliente cucli_ where cucli_.cod_usuario = '{$cod_usuario}')");  
        $this->db->where("ceve.cod_pasta = '{$cod_pasta}'");         
        
        $this->db->order_by("ceve.nome", "asc");
        $this->db->order_by("carq.data_vencimento", "desc"); 
		
		$query = $this->db->get(); 
		return json_encode($query->result_array());
        
    }    

    
    public function json_clientesalvardevice($cod_cliente,$id,$platform) { 
        $this->data["device_id"]=$id;
        $this->data["device_platform"]=$platform;
		
		$this->db->where('cod_usuario = ',$cod_cliente);
		return $this->db->update('cad_usuario', $this->data);  
    }
    
    
}

