<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Coleta_model extends CI_Model {
    
    private $data = array(),
            $data_cliente = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	//Retorna a quantidade total de registros da tabela
    function contar(){
		return $this->db->count_all('cad_coleta');
	}

    public function editar($cod_coleta) {
		$this->db->select(" 'coleta.editar',
						  	ccol.cod_coleta,
						  	ccol.cod_pasta,
						  	ccol.cod_formulario,
                            ccol.data_solicitacao,
                            date_format(ccol.data_solicitacao, '%d/%m/%Y') as 'data_solicitacao_',
                            ccol.data_vencimento,
                            date_format(ccol.data_vencimento, '%d/%m/%Y') as 'data_vencimento_',
                            ccol.texto".$this->functions->sql_auditoria("ccol")
						);
        
        $this->db->from('cad_coleta ccol');
        $this->db->where("cod_coleta = '{$cod_coleta}'");

		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function existe($cod_evento,$data_vencimento) {
		$this->db->select(" 'coleta.existe',
						  	ccol.cod_coleta".$this->functions->sql_auditoria("ccol")
						);
        
        $this->db->from('cad_coleta ccol');
        $this->db->join('cad_coletacliente ccolcli', "(ccolcli.cod_coleta = ccol.cod_coleta and ccolcli.cod_cliente = '".$this->session->userdata('cliente')."')",'inner');
        $this->db->where("cod_evento = '{$cod_evento}'");
        $this->db->where("data_vencimento = '{$data_vencimento}'");

		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function ver($cod_coleta) {
		$this->db->select(" 'coleta.ver',
						  	ccol.cod_coleta,
						  	ccol.cod_pasta,
						  	ccol.cod_formulario,
                            ccol.data_solicitacao,
                            date_format(ccol.data_solicitacao, '%d/%m/%Y') as 'data_solicitacao_',
                            ccol.data_vencimento,
                            date_format(ccol.data_vencimento, '%d/%m/%Y') as 'data_vencimento_',
                            ccol.texto,
                            ccolcli.cod_coletacliente,
                            ccolcli.formulario,
                            ccolcli.arquivo,
                            ccolcli.arquivo_original,
                            ccli.cod_cliente,
                            ccli.cod_interno,
                            cpas.nome as 'cod_pasta',
                            cfor.nome as 'formulario_titulo'"
						);
        
        $this->db->from('cad_coleta ccol');
        $this->db->join('cad_coletacliente ccolcli', "ccolcli.cod_coleta = ccol.cod_coleta",'inner');
        $this->db->join('cad_cliente ccli', "ccli.cod_cliente = ccolcli.cod_cliente",'left');
        $this->db->join('cad_pasta cpas', "cpas.cod_pasta = ccol.cod_pasta",'left');
        $this->db->join('cad_formulario cfor', "cfor.cod_formulario = ccol.cod_formulario",'left');
        $this->db->where("ccol.cod_coleta = '{$cod_coleta}'");

        $query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'coleta.listar',
						  	ccol.cod_coleta,
						  	ccol.texto,
                            date_format(ccol.data_solicitacao,  '%d/%m/%Y') as 'data_solicitacao_',
                            date_format(ccol.data_vencimento, '%d/%m/%Y') as 'data_vencimento_',
                            cpas.nome as 'cod_pasta',
                            cfor.nome as 'formulario'
                            "
						);
        
        $this->db->from('cad_coleta ccol');
        $this->db->join('cad_coletacliente ccolcli', "ccolcli.cod_coleta = ccol.cod_coleta",'left');
        $this->db->join('cad_cliente ccli', "ccolcli.cod_cliente = ccli.cod_cliente",'left');
        $this->db->join('cad_pasta cpas', "cpas.cod_pasta = ccol.cod_pasta",'left');
        $this->db->join('cad_formulario cfor', "cfor.cod_formulario = ccol.cod_formulario",'left');
        
        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("ccli.nome like '%{$busca}%'");

        if ($this->session->userdata('tipo')=="C"){
            $this->db->where("ccolcli.cod_cliente = '".$this->session->userdata('cliente')."'");        
        } elseif ($this->session->userdata('tipo')=="T"){
            //$this->db->where("(ccol.cod_cliente=0 or ccol.cod_cliente in (select cucli_.cod_cliente from cad_usuariocliente cucli_ where cucli_.cod_usuario = '".$this->session->userdata('cod_usuario')."'))"); 
        }

        $this->db->group_by('ccol.cod_coleta');

        $orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"cod_pasta";
        $orderby_order = $this->input->get_post('orderby_order');
        $this->db->order_by($orderby_column, $orderby_order);

        if (LIMIT<>0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }
    public function listar_cliente($cod_coleta="") {
        
		$this->db->select(" 'coleta.listar_cliente',
                            ccli.cod_cliente,
                            ccli.cod_interno,
                            concat(ccli.nome,' (',ccli.cod_interno,')') as 'cliente',
                            ccolcli.arquivo,
                            ccolcli.arquivo_original,
                            ccolcli.formulario"
						);
        
        $this->db->from('cad_coletacliente ccolcli');
		$this->db->join('cad_cliente ccli', "ccli.cod_cliente = ccolcli.cod_cliente",'left');
        
        $this->db->where("ccolcli.cod_coleta = {$cod_coleta}");
        
        $this->db->order_by("ccli.nome", "asc");
		
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function inserir() {
        
        $this->data["cod_pasta"]=$this->input->get_post('cod_pasta');
        $this->data["cod_formulario"]=$this->input->get_post('cod_formulario');
        $this->data["data_solicitacao"]=substr($this->input->get_post('data_solicitacao'),6,4)."-".substr($this->input->get_post('data_solicitacao'),3,2)."-".substr($this->input->get_post('data_solicitacao'),0,2); 
        $this->data["data_vencimento"]=substr($this->input->get_post('data_vencimento'),6,4)."-".substr($this->input->get_post('data_vencimento'),3,2)."-".substr($this->input->get_post('data_vencimento'),0,2); 
        $this->data["texto"]=$this->input->get_post('texto');

        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_coleta', $this->data);
        $insert_id = $this->db->insert_id();

        $this->salvar_cliente($insert_id);
        $this->enviar_aviso($insert_id,$this->data["texto"]);
    }

    public function inserir_evento($cod_evento,$data_vencimento) {

        $evento = $this->Evento_model->editar($cod_evento);
        
        $this->data["cod_evento"]=$cod_evento;
        $this->data["cod_pasta"]=$evento["cod_pasta"];
        $this->data["data_solicitacao"]=$data_vencimento;
        $this->data["data_vencimento"]=$data_vencimento;
        $this->data["texto"]=$evento["texto"];

        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_coleta', $this->data);
        $insert_id = $this->db->insert_id();

        $this->data_cliente["cod_coleta"]=$insert_id;
        $this->data_cliente["cod_cliente"]=$this->session->userdata('cliente');
        $this->data_cliente['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data_cliente['data_c']=date('Y-m-d H:i:s');       
        $this->data_cliente['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data_cliente['data_a']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_coletacliente', $this->data_cliente);

        return $insert_id;
    }

    public function salvar($cod_coleta) {
        
        $this->data["cod_pasta"]=$this->input->get_post('cod_pasta');
        $this->data["cod_formulario"]=$this->input->get_post('cod_formulario');
        $this->data["data_solicitacao"]=substr($this->input->get_post('data_solicitacao'),6,4)."-".substr($this->input->get_post('data_solicitacao'),3,2)."-".substr($this->input->get_post('data_solicitacao'),0,2); 
        $this->data["data_vencimento"]=substr($this->input->get_post('data_vencimento'),6,4)."-".substr($this->input->get_post('data_vencimento'),3,2)."-".substr($this->input->get_post('data_vencimento'),0,2);
        $this->data["texto"]=$this->input->get_post('texto');

        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->where('cod_coleta = ',$cod_coleta);
        $this->db->update('cad_coleta', $this->data);
        
        $this->salvar_cliente($cod_coleta);
    }

    public function salvar_cliente($cod_coleta) {
        
        $coleta_cliente = $this->input->get_post('coleta_cliente'); 
        
        $this->db->where("cod_coleta", $cod_coleta);
		$this->db->delete('cad_coletacliente');
        
        if ($coleta_cliente){
            foreach($coleta_cliente as $value){
                $this->data_cliente["cod_coleta"]=$cod_coleta;
                $this->data_cliente["cod_cliente"]=$value;
                $this->data_cliente['cod_usuario_c']=$this->session->userdata('cod_usuario');       
                $this->data_cliente['data_c']=date('Y-m-d H:i:s');       
                $this->data_cliente['cod_usuario_a']=$this->session->userdata('cod_usuario');       
                $this->data_cliente['data_a']=date('Y-m-d H:i:s');       

                $this->db->insert('cad_coletacliente', $this->data_cliente);
            }
        }
    }

    public function enviar_aviso($cod_coleta,$texto) {
        
        log_message('debug', "COLETA.enviar_aviso");

        //Envia e-mail e msg no APP para o cliente
		$this->db->select(" 'coleta.enviar_aviso',
                            cusu.email,
                            cusu.nome,
                            cusu.device_id,
						  	cusu.device_platform,
                            ccli.nome as 'cliente',
                            ccli.razao_social"
						);
        
        $this->db->from('cad_usuario cusu');
        $this->db->join('cad_usuariocliente cucli', "cucli.cod_usuario = cusu.cod_usuario",'inner');
        $this->db->join('cad_coletacliente ccolcli', "ccolcli.cod_cliente = cucli.cod_cliente",'inner');
        $this->db->join('cad_cliente ccli', "ccli.cod_cliente = cucli.cod_cliente",'inner');
        $this->db->where("cusu.tipo ='C' and cusu.config_receberemail = 'S' and ccolcli.cod_coleta = '{$cod_coleta}'");

        $query = $this->db->get();
        $dados = $query->result_array();

        $template = $this->Template_model->editar(2);

        foreach($dados as $value){

            log_message('debug', "COLETA.enviar_aviso nome:{$value["nome"]} email:{$value["email"]}");

            if ($value["device_id"]<>NULL and $value["device_platform"]<>NULL){
                //$this->functions->app_push($value["device_id"],$value["device_platform"],TITULO,"Solicitação de Coleta");
            }

            $myemail = new Myemail;
            $myemail->cod_cliente(0);
            $myemail->from(TITULO, EMAIL_NR);
            $myemail->to("BAH",$value["email"]);
            $myemail->subject($template["titulo"]);
            $myemail->body($template["texto"]);
            $myemail->custom(array("%CLIENTE%"=>$value["cliente"],"%RAZAOSOCIAL%"=>$value["razao_social"],"%USUARIO%"=>$value["nome"],"%TEXTO%"=>$texto));
            $myemail->enviar();
        }
    }

    public function enviar($cod_coleta) {

        $cod_coletacliente = $this->input->get_post("cod_coletacliente");
        $cod_coleta = $this->input->get_post("cod_coleta");
        $cod_pasta = $this->input->get_post("cod_pasta");
        $cod_interno = $this->input->get_post("cod_interno");
    
        //Salva formulário
        $formulario = "";
        $count=0;
        foreach ($this->input->post() as $key => $value) {
            if (strpos($key,"formulario_")===0){
                //echo "<br>key $key; value:$value;";
                $titulo = str_replace("formulario_","",$key);
                $titulo = str_replace("_"," ",$titulo);
                $titulo = ucfirst($titulo);
                $count++;
                $formulario .= "<br><strong>$count. $titulo</strong>:<br> $value<br>\n";
            }
            $this->db->query("update cad_coletacliente set formulario = '{$formulario}' WHERE `cod_coletacliente` = '{$cod_coletacliente}'");
        }
        //echo "update cad_coletacliente set formulario = '{$formulario}' WHERE `cod_coletacliente` = '{$cod_coletacliente}'";
        //echo $formulario;
        //exit;

        $obs = "";
        $agrupador = date("YmdGi");
        foreach($_FILES['files']['name'] as $key => $array_value){     
            $erro = "";
            $name = $_FILES['files']['name'][$key];
            $size = $_FILES['files']['size'][$key];    
            $tmp_name = $_FILES['files']['tmp_name'][$key];    

            $folder = $this->functions->cliente_folder($this->input->get_post('cod_interno'));
            
            $arquivo = md5($name);
            $arquivo = "{$folder}/$arquivo";
            
            move_uploaded_file($tmp_name, $arquivo);
            //log_message('debug', "upload_model.enviar move_uploaded_file {$tmp_name}, {$arquivo}");

            $this->db->query("insert into cad_arquivo (
                                                    cod_cliente, 
                                                    cod_evento,
                                                    cod_coleta,
                                                    cod_pasta,
                                                    origem,
                                                    nome_original,
                                                    arquivo,
                                                    data_competencia,
                                                    data_vencimento,
                                                    situacao,
                                                    obs,
                                                    agrupador,
                                                    cod_usuario_c,
                                                    data_c,
                                                    cod_usuario_a,
                                                    data_a)
                                                    values(
                                                    '".$this->session->userdata('cliente')."', 
                                                    '',
                                                    '".$cod_coleta."',
                                                    '".$cod_pasta."',
                                                    'C',
                                                    '".strtoupper($name)."',
                                                    '".md5($name)."',
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    '".$agrupador."',
                                                    '".$this->session->userdata('cod_usuario')."',
                                                    '".date('Y-m-d H:i:s')."',
                                                    '".$this->session->userdata('cod_usuario')."',
                                                    '".date('Y-m-d H:i:s')."'

                                                    )
                                                    on duplicate key update 
                                                    cod_cliente = '".$this->session->userdata('cliente')."',
                                                    cod_evento = '',
                                                    cod_coleta = '".$cod_coleta."',
                                                    origem = 'C',
                                                    nome_original = '".strtoupper($name)."',
                                                    arquivo = '".md5($name)."',
                                                    data_competencia = '',
                                                    data_vencimento = '',
                                                    obs = '".$obs."',
                                                    agrupador = '".$agrupador."',
                                                    cod_usuario_a = '".$this->session->userdata('cod_usuario')."',
                                                    data_a = '".date('Y-m-d H:i:s')."'

                                                    ");
        }   
    }

    public function excluir($cod_coleta) {

        $this->db->where("cod_coleta", $cod_coleta);
		$this->db->delete('cad_coletacliente');

        $this->db->where("cod_coleta", $cod_coleta);
		$this->db->delete('cad_coleta');
        
    }

    public function coleta_cliente($cod_coleta="") {
        
		$this->db->select(" 'coleta.coleta_cliente',
                            ccli.cod_cliente,
                            ccli.nome as 'cliente',
                            ccolcli.cod_coleta"
						);
        
        $this->db->from('cad_cliente ccli');
		$this->db->join('cad_coletacliente ccolcli', "(ccolcli.cod_cliente = ccli.cod_cliente and ccolcli.cod_coleta = '{$cod_coleta}')",'left');
        
        $this->db->where("(ccli.cod_cliente in (".$this->session->userdata('clientes_codigos')."0) or '".$this->session->userdata('admin')."'='S')");
        
        $this->db->order_by("ccli.nome", "asc");
		
		$query = $this->db->get(); 
		return $query->result_array();    
    }

}

