<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Banco_model extends CI_Model {
    
    private $data = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	public function editar($cod_banco) {
		$this->db->select(" 'banco.editar',
						  	cban.cod_banco,
							cban.nome,
                            cban.febraban".$this->functions->sql_auditoria("cban")
						);
        
        $this->db->from('cad_banco cban');
        
		$this->db->where("cod_banco = '{$cod_banco}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'banco.listar',
						  	cban.cod_banco,
							cban.nome,
							cban.febraban"
							,FALSE
						);
        
        $this->db->from('cad_banco cban');

        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("cban.nome like '%{$busca}%'");
        
        $orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"nome";
        $orderby_order = $this->input->get_post('orderby_order');
        $this->db->order_by($orderby_column, $orderby_order);

        if (LIMIT<> 0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_select() {
		$this->db->select(" 'banco.listar',
						  	cban.cod_banco,
							cban.nome"
							,FALSE
						);
        
        $this->db->from('cad_banco cban');

        $this->db->order_by("cban.nome", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function inserir() {
        
        $nome = $this->input->get_post('nome');
        $febraban = $this->input->get_post('febraban');

        $this->data["nome"]=$nome;
        $this->data["febraban"]=$febraban;
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_banco', $this->data);

        
    }

    public function salvar($cod_banco) {
        
        $nome = $this->input->get_post('nome');
        $febraban = $this->input->get_post('febraban');

        $this->data["nome"]=$nome;
        $this->data["febraban"]=$febraban;
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->where('cod_banco = ',$cod_banco);
		$this->db->update('cad_banco', $this->data);
        
    }

    public function excluir($cod_banco) {

        $this->db->where("cod_banco", $cod_banco);
		$this->db->delete('cad_banco');
        
    }

}

