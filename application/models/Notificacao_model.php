<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Notificacao_model extends CI_Model {
    
    private $data = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	//Retorna a quantidade total de registros da tabela
    function contar(){
		return $this->db->count_all('cad_notificacao');
	}

    public function editar($cod_notificacao="") {
		$this->db->select(" 'notificacao.editar',
						  	cnot.cod_notificacao,
						  	cnot.cod_cliente,
						  	cnot.titulo,
							cnot.texto".$this->functions->sql_auditoria("cnot")
						);
        
        $this->db->from('cad_notificacao cnot');
        
		if ($cod_notificacao) $this->db->where("cod_notificacao = '{$cod_notificacao}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function ultima() {
		$this->db->select(" 'notificacao.ultima',
						  	cnot.cod_notificacao,
						  	cnot.cod_cliente,
						  	cnot.titulo,
							cnot.texto,
                            cnot.data_a"
						);
        
        $this->db->from('cad_usuarionotificacao cun');
        $this->db->join('cad_notificacao cnot', "cnot.cod_notificacao = cun.cod_notificacao",'inner');
        
		//$this->db->where("cnot.cod_cliente is null or cnot.cod_cliente = 0 or cnot.cod_cliente = '".$this->session->userdata('cod_cliente')."'");
		$this->db->where("cun.cod_usuario = '".$this->session->userdata('cod_usuario')."'");
		$this->db->where("cun.lida is null");
        
        $this->db->order_by("cnot.cod_notificacao", "desc");
        
        $this->db->limit(1, 0);
        
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'notificacao.listar',
						  	cnot.cod_notificacao,
						  	cnot.cod_cliente,
						  	cnot.titulo,
							cnot.texto,
                            concat(ccli.nome,' (',ccli.cod_interno,')') as 'cliente'"
						);
        
        $this->db->from('cad_notificacao cnot');
		$this->db->join('cad_cliente ccli', "ccli.cod_cliente = cnot.cod_cliente",'left');

        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("cnot.titulo like '%{$busca}%' or ccli.nome like '%{$busca}%'");

        if ($this->session->userdata('tipo')=="C"){
            $this->db->where("cnot.cod_cliente = '".$this->session->userdata('cliente')."'");        
        } elseif ($this->session->userdata('tipo')=="T"){
            $this->db->where("(cnot.cod_cliente=0 or ccli.cod_cliente in (select cucli_.cod_cliente from cad_usuariocliente cucli_ where cucli_.cod_usuario = '".$this->session->userdata('cod_usuario')."'))"); 
        }
        
        $orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"titulo";
        $orderby_order = $this->input->get_post('orderby_order');
        $this->db->order_by($orderby_column, $orderby_order);
		
        if (LIMIT<>0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_notificacaodocliente($cod_cliente="") {
		$this->db->select(" 'notificacao.listar_notificacaodocliente',
						  	cnot.cod_notificacao,
						  	cnot.cod_cliente,
                            cnot.titulo,
                            date_format(cnot.data_c,'%d/%m/%Y') as 'data_c_',"
						);
        
        $this->db->from('cad_notificacao cnot');
		$this->db->join('cad_cliente ccli', "ccli.cod_cliente = cnot.cod_cliente",'left');
        $this->db->where("cnot.cod_cliente = '{$cod_cliente}'");        
        $this->db->order_by("cnot.cod_notificacao", "desc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_ultimas($cod_cliente="") {
        $this->db->select(" 'notificacao.listar_ultimas',
						  	cnot.cod_notificacao,
						  	cnot.cod_cliente,
						  	cnot.titulo,
							cnot.texto,
                            cnot.data_a"
						);
        
        $this->db->from('cad_usuarionotificacao cun');
        $this->db->join('cad_notificacao cnot', "cnot.cod_notificacao = cun.cod_notificacao",'inner');
        
		$this->db->where("cun.cod_usuario = '".$this->session->userdata('cod_usuario')."'");
		$this->db->where("cnot.cod_cliente = '{$cod_cliente}'");
        
        $this->db->order_by("cnot.cod_notificacao", "desc");
        
        $this->db->limit(4, 0);
        
		$query = $this->db->get(); 
        return $query->result_array();	

    }

    public function cliente() {
		$this->db->select(" 'notificacao.cliente',
						  	cnot.cod_notificacao,
						  	cnot.cod_cliente,
						  	cnot.titulo,
							cnot.texto,
                            cnot.data_a"
						);
        
        $this->db->from('cad_usuarionotificacao cun');
        $this->db->join('cad_notificacao cnot', "cnot.cod_notificacao = cun.cod_notificacao",'inner');
        
		$this->db->where("cun.cod_usuario = '".$this->session->userdata('cod_usuario')."'");
		//$this->db->where("cun.lida is null");
        
        $this->db->order_by("cnot.cod_notificacao", "desc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_quemleu($cod_notificacao) {
		$this->db->select(" 'notificacao.listar_quemleu',
							cusu.nome,
							cusu.tipo,
							cusu.usuario,
							cun.lida,
                            date_format(cun.data_a,'%d/%m/%Y') as 'data_a'"
						);
        
        $this->db->from('cad_usuarionotificacao cun');
		$this->db->join('cad_usuario cusu', "cusu.cod_usuario = cun.cod_usuario",'inner');
        
        $this->db->where("cun.cod_notificacao = '{$cod_notificacao}'");
        
        $this->db->order_by("cusu.usuario", "asc");
        $this->db->order_by("cun.data_a", "asc");
		
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function inserir() {
        
        $cod_cliente = $this->input->get_post('cod_cliente');
        $titulo = $this->input->get_post('titulo');
        $texto = $this->input->get_post('texto');
        
        $this->data["cod_cliente"]=$cod_cliente;
        $this->data["titulo"]=$titulo;
        $this->data["texto"]=$texto;
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_notificacao', $this->data);  
        $insert_id = $this->db->insert_id();
        
        $this->db->query("insert into cad_usuarionotificacao (cod_usuario,cod_notificacao) select cucli.cod_usuario,{$insert_id} from cad_usuariocliente cucli where '{$cod_cliente}'='NULL' or cucli.cod_cliente = '{$cod_cliente}'");

        $template = $this->Template_model->editar(10);

        $usuarios = $this->Usuario_model->listar_usuarioscliente($cod_cliente);
        foreach($usuarios as $value_){
            if ($value_["config_receberemail"]=="S"){

                $myemail = new Myemail;
                $myemail->cod_cliente(0);
                $myemail->from(TITULO, EMAIL_NR);
                $myemail->to("BAH","ramosrafa18@gmail.com");
                $myemail->subject($template["titulo"]);
                $myemail->body($template["texto"]);
                $myemail->custom(array("%USUARIO%"=>$value["nome"]));
                $myemail->enviar();
            }
        }        
        /*Envia notificacao
		$this->db->select(" 'notificacao.inserir',
                            cusu.device_id,
						  	cusu.device_platform"
						);
        
        $this->db->from('cad_usuario cusu');
        $this->db->join('cad_usuarionotificacao cun', "(cun.cod_usuario = cusu.cod_usuario and cun.cod_notificacao = '{$insert_id}')",'inner');
        
        $this->db->where("cusu.device_id is not null");
        $this->db->where("cusu.device_platform is not null");
        
        $query = $this->db->get();
        
        $dados = $query->result_array();
        
                
        foreach($dados as $value){
            log_message('debug', "push: ".$value["device_id"].$value["device_platform"].TITULO,"Você tem uma nova notificação");
            
            $this->functions->app_push($value["device_id"],$value["device_platform"],TITULO,"Você tem uma nova notificação");
        }
        */
        
    }

    public function salvar($cod_notificacao) {
        
        $cod_cliente = $this->input->get_post('cod_cliente');
        $titulo = $this->input->get_post('titulo');
        $texto = $this->input->get_post('texto');

        $this->data["cod_cliente"]=$cod_cliente;
        $this->data["titulo"]=$titulo;
        $this->data["texto"]=$texto;
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');   
        $this->data['data_a']=date('Y-m-d H:i:s');       
		
		$this->db->where('cod_notificacao = ',$cod_notificacao);
		$this->db->update('cad_notificacao', $this->data);
        
        $this->db->query("delete from cad_usuarionotificacao where cod_notificacao = '{$cod_notificacao}'");
        $this->db->query("insert into cad_usuarionotificacao (cod_usuario,cod_notificacao,cod_usuario_c,data_c) select cucli.cod_usuario,{$cod_notificacao},".$this->session->userdata('cod_usuario').",'".date('Y-m-d H:i:s')."' from cad_usuariocliente cucli where '{$cod_cliente}'='NULL' or cucli.cod_cliente = '{$cod_cliente}'");
        
    }

    public function excluir($cod_notificacao) {

        $this->db->query("delete from cad_usuarionotificacao where cod_notificacao = '{$cod_notificacao}'"); 
        
        $this->db->where("cod_notificacao", $cod_notificacao);
		$this->db->delete('cad_notificacao');
        
    }

    public function lida($cod_notificacao) {
        
        $this->data["lida"]="S";
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       
		
		$this->db->where('cod_usuario = ',$this->session->userdata('cod_usuario'));
		$this->db->where('cod_notificacao = ',$cod_notificacao);
		$this->db->update('cad_usuarionotificacao', $this->data);
        
    }

    //JSON

    public function ajax_ultima($cod_usuario=0) {

        $this->db->select(" 'notificacao.ultima',
						  	cnot.cod_notificacao,
						  	cnot.cod_cliente,
						  	cnot.titulo,
							cnot.texto,
                            date_format(cnot.data_a,'%d/%m/%Y') as 'data_a'"
						);
        
        $this->db->from('cad_usuarionotificacao cun');
        $this->db->join('cad_notificacao cnot', "cnot.cod_notificacao = cun.cod_notificacao",'inner');
        
		$this->db->where("cun.cod_usuario = '{$cod_usuario}'");
		$this->db->where("cun.lida is null");
        
        $this->db->order_by("cnot.cod_notificacao", "desc"); 
        
        $this->db->limit(1, 0);
        
		$query = $this->db->get(); 
		return json_encode($query->result_array());          
    }
    
    public function ajax_notificacaolistar($cod_usuario=0) { 
        $this->db->select(" 'notificacao.ultima',
						  	cnot.cod_notificacao,
						  	cnot.cod_cliente,
						  	cnot.titulo,
							cnot.texto,
                            date_format(cnot.data_a,'%d/%m/%Y') as 'data_a'"
						);
        
        $this->db->from('cad_usuarionotificacao cun');
        $this->db->join('cad_notificacao cnot', "cnot.cod_notificacao = cun.cod_notificacao",'inner');
        
		$this->db->where("cun.cod_usuario = '{$cod_usuario}'");
        
        $this->db->order_by("cnot.cod_notificacao", "desc"); 
        
        $this->db->limit(10, 0);
        
		$query = $this->db->get(); 
		return json_encode($query->result_array());     
    }
    
    public function json_lida($cod_notificacao,$cod_usuario) {
        
        $this->data["lida"]="S";
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       
		
		$this->db->where('cod_notificacao = ',$cod_notificacao);
		$this->db->where('cod_usuario = ',$cod_usuario);
		return $this->db->update('cad_usuarionotificacao', $this->data);
        
    }

    public function json_inserir($cod_cliente) {
        
		$cod_pasta = $this->input->get_post('cod_pasta');
		$nome = $this->input->get_post('nome');
		$email = $this->input->get_post('email');
		$texto = $this->input->get_post('mensagem');		

        $cliente = $this->Cliente_model->editar($cod_cliente);        
        
        log_message('debug', $cliente["nome"]." (".$cod_pasta."): ".$texto);
        
        $this->data["cod_cliente"]=1;
        $this->data["cod_pasta"]=$cod_pasta;
        $this->data["titulo"]="Suporte ".$cliente["nome"];
        $this->data["texto"]=$cliente["nome"]." (".$cod_pasta."): ".$texto; 
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_notificacao', $this->data); 
        $insert_id = $this->db->insert_id();
        
        return $this->db->query("insert into cad_usuarionotificacao (cod_usuario,cod_notificacao,cod_usuario_c,data_c) select cucli.cod_usuario,{$insert_id},".$this->session->userdata('cod_usuario').",'".date('Y-m-d H:i:s')."' from cad_usuariocliente cucli where '{$cod_cliente}'='NULL' or cucli.cod_cliente = '{$cod_cliente}'");
        
    }
    
    
}

