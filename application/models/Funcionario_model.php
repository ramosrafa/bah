<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Funcionario_model extends CI_Model {
    
    private $data = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	public function editar($cod_funcionario) {
		$this->db->select(" 'funcionario.editar',
						  	cfun.cod_funcionario,
						  	cfun.cod_cliente,
							cfun.nome,
                            cfun.documento,
                            cfun.data_nascimento,
                            cfun.estado_civil,
                            cfun.municipio,
                            cfun.rg,
                            cfun.emissor,
                            cfun.data_emissao1,
                            cfun.data_emissao2,
                            cfun.data_emissao3,
                            cfun.endereco,
                            cfun.complemento,
                            cfun.cidade,
                            cfun.bairro,
                            cfun.uf,
                            cfun.cep,
                            cfun.email,
                            cfun.telefone,
                            cfun.titulo_eleitor,
                            cfun.zona,
                            cfun.secao,
                            cfun.carteira,
                            cfun.serie_carteira,
                            cfun.uf_carteira,
                            cfun.escolaridade,
                            cfun.pis,
                            cfun.data_pis,
                            cfun.empresa,
                            cfun.funcao,
                            cfun.vale,
                            cfun.salario,
                            cfun.periodo,
                            cfun.data_admissao,
                            cfun.data_demissao,
                            concat(ccli.nome,' (',ccli.cod_interno,')') as 'cliente'".$this->functions->sql_auditoria("cfun")
						);
        
        $this->db->from('cad_funcionario cfun');
        $this->db->join('cad_cliente ccli', "ccli.cod_cliente = cfun.cod_cliente",'left');
		$this->db->where("cod_funcionario = '{$cod_funcionario}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'funcionario.listar',
						  	cfun.cod_funcionario,
                            cfun.nome,
                            cfun.documento,
                            cfun.data_nascimento,
                            date_format(cfun.data_nascimento, '%d/%m/%Y') as 'data_nascimento', 
                            cfun.estado_civil,
                            cfun.municipio,
                            cfun.rg,
                            cfun.emissor,
                            cfun.data_emissao1,
                            date_format(cfun.data_emissao1, '%d/%m/%Y') as 'data_emissao1_', 
                            cfun.data_emissao2,
                            date_format(cfun.data_emissao2, '%d/%m/%Y') as 'data_emissao2_', 
                            cfun.data_emissao3,
                            date_format(cfun.data_emissao3, '%d/%m/%Y') as 'data_emissao3_', 
                            cfun.endereco,
                            cfun.complemento,
                            cfun.cidade,
                            cfun.bairro,
                            cfun.uf,
                            cfun.cep,
                            cfun.email,
                            cfun.telefone,
                            cfun.titulo_eleitor,
                            cfun.zona,
                            cfun.secao,
                            cfun.carteira,
                            cfun.serie_carteira,
                            cfun.uf_carteira,
                            cfun.escolaridade,
                            cfun.pis,
                            cfun.data_pis,
                            date_format(cfun.data_pis, '%d/%m/%Y') as 'data_pis_', 
                            cfun.empresa,
                            cfun.funcao,
                            cfun.vale,
                            cfun.salario,
                            cfun.periodo,
                            cfun.data_admissao,
                            date_format(cfun.data_admissao, '%d/%m/%Y') as 'data_admissao_',
                            cfun.data_demissao,
                            date_format(cfun.data_demissao, '%d/%m/%Y') as 'data_demissao_',
                            concat(ccli.nome,' (',ccli.cod_interno,')') as 'cliente'"
							,FALSE
						);
        
        $this->db->from('cad_funcionario cfun');
        $this->db->join('cad_cliente ccli', "ccli.cod_cliente = cfun.cod_cliente",'left');

        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("cfun.nome like '%{$busca}%'");
        
        if ($this->session->userdata('tipo')=="C"){
            $this->db->where("cfun.cod_cliente = '".$this->session->userdata('cliente')."'");        
        } elseif ($this->session->userdata('tipo')=="T"){
            $this->db->where("(cfun.cod_cliente=0 or cfun.cod_cliente in (select cucli_.cod_cliente from cad_usuariocliente cucli_ where cucli_.cod_usuario = '".$this->session->userdata('cod_usuario')."'))"); 
        }

        $orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"nome";
        $orderby_order = $this->input->get_post('orderby_order');
        $this->db->order_by($orderby_column, $orderby_order);

        if (LIMIT<> 0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_funcionariodocliente($cod_cliente) {
		$this->db->select(" 'funcionario.listar_funcionariodocliente',
						  	cfun.cod_funcionario,
                            cfun.nome,
                            cfun.documento,
                            cfun.data_admissao,
                            date_format(cfun.data_admissao, '%d/%m/%Y') as 'data_admissao_',
                            cfun.data_demissao,
							date_format(cfun.data_demissao, '%d/%m/%Y') as 'data_demissao_' "
							,FALSE
						);
        
        $this->db->from('cad_funcionario cfun');

        $this->db->where("cfun.cod_cliente = '{$cod_cliente}'");        

        $this->db->order_by("cfun.nome", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_select() {
		$this->db->select(" 'funcionario.listar',
						  	cfun.cod_funcionario,
							cfun.nome"
							,FALSE
						);
        
        $this->db->from('cad_funcionario cfun');

        $this->db->order_by("cfun.nome", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function inserir() {
        
        $cod_cliente = $this->input->get_post('cod_cliente');
        $nome = $this->input->get_post('nome');
        $documento = $this->input->get_post('documento');
        $data_nascimento = substr($this->input->get_post('data_nascimento'),6,4)."-".substr($this->input->get_post('data_nascimento'),3,2)."-".substr($this->input->get_post('data_nascimento'),0,2);
        $estado_civil = $this->input->get_post('estado_civil');
        $municipio = $this->input->get_post('municipio');
        $rg = $this->input->get_post('rg');
        $emissor = $this->input->get_post('emissor');
        $data_emissao1 = substr($this->input->get_post('data_emissao1'),6,4)."-".substr($this->input->get_post('data_emissao1'),3,2)."-".substr($this->input->get_post('data_emissao1'),0,2);
        $data_emissao2 = substr($this->input->get_post('data_emissao2'),6,4)."-".substr($this->input->get_post('data_emissao2'),3,2)."-".substr($this->input->get_post('data_emissao2'),0,2);
        $data_emissao3 = substr($this->input->get_post('data_emissao3'),6,4)."-".substr($this->input->get_post('data_emissao3'),3,2)."-".substr($this->input->get_post('data_emissao3'),0,2);
        $endereco = $this->input->get_post('endereco');
        $complemento = $this->input->get_post('complemento');
        $cidade = $this->input->get_post('cidade');
        $bairro = $this->input->get_post('bairro');
        $uf = $this->input->get_post('uf');
        $cep = $this->input->get_post('cep');
        $email = $this->input->get_post('email');
        $telefone = $this->input->get_post('telefone');
        $titulo_eleitor = $this->input->get_post('titulo_eleitor');
        $zona = $this->input->get_post('zona');
        $secao = $this->input->get_post('secao');
        $carteira = $this->input->get_post('carteira');
        $serie_carteira = $this->input->get_post('serie_carteira');
        $uf_carteira = $this->input->get_post('uf_carteira');
        $escolaridade = $this->input->get_post('escolaridade');
        $pis = $this->input->get_post('pis');
        $data_pis = substr($this->input->get_post('data_pis'),6,4)."-".substr($this->input->get_post('data_pis'),3,2)."-".substr($this->input->get_post('data_pis'),0,2);
        $empresa = $this->input->get_post('empresa');
        $funcao = $this->input->get_post('funcao');
        $vale = $this->input->get_post('vale');
        $salario = $this->input->get_post('salario');
        $periodo = $this->input->get_post('periodo');
        $data_admissao=substr($this->input->get_post('data_admissao'),6,4)."-".substr($this->input->get_post('data_admissao'),3,2)."-".substr($this->input->get_post('data_admissao'),0,2); 
        $data_demissao=substr($this->input->get_post('data_demissao'),6,4)."-".substr($this->input->get_post('data_demissao'),3,2)."-".substr($this->input->get_post('data_demissao'),0,2); 

        $this->data["cod_cliente"]=$cod_cliente;
        $this->data["nome"]=$nome;
        $this->data["documento"]=$documento;
        $this->data["data_nascimento"]=$data_nascimento;
        $this->data["estado_civil"]=$estado_civil;
        $this->data["municipio"]=$municipio;
        $this->data["rg"]=$rg;
        $this->data["emissor"]=$emissor;
        $this->data["data_emissao1"]=$data_emissao1;
        $this->data["data_emissao2"]=$data_emissao2;
        $this->data["data_emissao3"]=$data_emissao3;
        $this->data["endereco"]=$endereco;
        $this->data["complemento"]=$complemento;
        $this->data["cidade"]=$cidade;
        $this->data["bairro"]=$bairro;
        $this->data["uf"]=$uf;
        $this->data["cep"]=$cep;
        $this->data["email"]=$email;
        $this->data["telefone"]=$telefone;
        $this->data["titulo_eleitor"]=$titulo_eleitor;
        $this->data["zona"]=$zona;
        $this->data["secao"]=$secao;
        $this->data["carteira"]=$carteira;
        $this->data["serie_carteira"]=$serie_carteira;
        $this->data["uf_carteira"]=$uf_carteira;
        $this->data["escolaridade"]=$escolaridade;
        $this->data["pis"]=$pis;
        $this->data["data_pis"]=$data_pis;
        $this->data["empresa"]=$empresa;
        $this->data["funcao"]=$funcao;
        $this->data["vale"]=$vale;
        $this->data["salario"]=$salario;
        $this->data["periodo"]=$periodo;
        $this->data["data_admissao"]=$data_admissao;
        $this->data["data_demissao"]=$data_demissao;
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_funcionario', $this->data);

        
    }

    public function salvar($cod_funcionario) {
        
        $cod_cliente = $this->input->get_post('cod_cliente');
        $nome = $this->input->get_post('nome');
        $documento = $this->input->get_post('documento');
        $data_nascimento = substr($this->input->get_post('data_nascimento'),6,4)."-".substr($this->input->get_post('data_nascimento'),3,2)."-".substr($this->input->get_post('data_nascimento'),0,2);
        $estado_civil = $this->input->get_post('estado_civil');
        $municipio = $this->input->get_post('municipio');
        $rg = $this->input->get_post('rg');
        $emissor = $this->input->get_post('emissor');
        $data_emissao1 = substr($this->input->get_post('data_emissao1'),6,4)."-".substr($this->input->get_post('data_emissao1'),3,2)."-".substr($this->input->get_post('data_emissao1'),0,2);
        $data_emissao2 = substr($this->input->get_post('data_emissao2'),6,4)."-".substr($this->input->get_post('data_emissao2'),3,2)."-".substr($this->input->get_post('data_emissao2'),0,2);
        $data_emissao3 = substr($this->input->get_post('data_emissao3'),6,4)."-".substr($this->input->get_post('data_emissao3'),3,2)."-".substr($this->input->get_post('data_emissao2'),0,2);
        $endereco = $this->input->get_post('endereco');
        $complemento = $this->input->get_post('complemento');
        $cidade = $this->input->get_post('cidade');
        $bairro = $this->input->get_post('bairro');
        $uf = $this->input->get_post('uf');
        $cep = $this->input->get_post('cep');
        $email = $this->input->get_post('email');
        $telefone = $this->input->get_post('telefone');
        $titulo_eleitor = $this->input->get_post('titulo_eleitor');
        $zona = $this->input->get_post('zona');
        $secao = $this->input->get_post('secao');
        $carteira = $this->input->get_post('carteira');
        $serie_carteira = $this->input->get_post('serie_carteira');
        $uf_carteira = $this->input->get_post('uf_carteira');
        $escolaridade = $this->input->get_post('escolaridade');
        $pis = $this->input->get_post('pis');
        $data_pis = substr($this->input->get_post('data_pis'),6,4)."-".substr($this->input->get_post('data_pis'),3,2)."-".substr($this->input->get_post('data_pis'),0,2);
        $empresa = $this->input->get_post('empresa');
        $funcao = $this->input->get_post('funcao');
        $vale = $this->input->get_post('vale');
        $salario = $this->input->get_post('salario');
        $periodo = $this->input->get_post('periodo');
        $data_admissao=substr($this->input->get_post('data_admissao'),6,4)."-".substr($this->input->get_post('data_admissao'),3,2)."-".substr($this->input->get_post('data_admissao'),0,2); 
        $data_demissao=substr($this->input->get_post('data_demissao'),6,4)."-".substr($this->input->get_post('data_demissao'),3,2)."-".substr($this->input->get_post('data_demissao'),0,2); 

        $this->data["cod_cliente"]=$cod_cliente;
        $this->data["nome"]=$nome;
        $this->data["documento"]=$documento;
        $this->data["data_nascimento"]=$data_nascimento;
        $this->data["estado_civil"]=$estado_civil;
        $this->data["municipio"]=$municipio;
        $this->data["rg"]=$rg;
        $this->data["emissor"]=$emissor;
        $this->data["data_emissao1"]=$data_emissao1;
        $this->data["data_emissao2"]=$data_emissao2;
        $this->data["data_emissao3"]=$data_emissao3;
        $this->data["endereco"]=$endereco;
        $this->data["complemento"]=$complemento;
        $this->data["cidade"]=$cidade;
        $this->data["bairro"]=$bairro;
        $this->data["uf"]=$uf;
        $this->data["cep"]=$cep;
        $this->data["email"]=$email;
        $this->data["telefone"]=$telefone;
        $this->data["titulo_eleitor"]=$titulo_eleitor;
        $this->data["zona"]=$zona;
        $this->data["secao"]=$secao;
        $this->data["carteira"]=$carteira;
        $this->data["serie_carteira"]=$serie_carteira;
        $this->data["uf_carteira"]=$uf_carteira;
        $this->data["escolaridade"]=$escolaridade;
        $this->data["pis"]=$pis;
        $this->data["data_pis"]=$data_pis;
        $this->data["empresa"]=$empresa;
        $this->data["funcao"]=$funcao;
        $this->data["vale"]=$vale;
        $this->data["salario"]=$salario;
        $this->data["periodo"]=$periodo;
        $this->data["data_admissao"]=$data_admissao;
        $this->data["data_demissao"]=$data_demissao;
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->where('cod_funcionario = ',$cod_funcionario);
		$this->db->update('cad_funcionario', $this->data);
        
    }

    public function excluir($cod_funcionario) {

        $this->db->where("cod_funcionario", $cod_funcionario);
		$this->db->delete('cad_funcionario');
        
    }

}

