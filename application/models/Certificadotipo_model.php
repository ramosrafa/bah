<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Certificadotipo_model extends CI_Model {
    
    private $data = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	//Retorna a quantidade total de registros da tabela
    function contar(){
		return $this->db->count_all('cad_certificadotipo');
	}

    public function editar($cod_certificadotipo) {
		$this->db->select(" 'certificadotipo.editar',
						  	cct.cod_certificadotipo,
							cct.nome".$this->functions->sql_auditoria("cct")
						);
        
        $this->db->from('cad_certificadotipo cct');
        
		$this->db->where("cod_certificadotipo = '{$cod_certificadotipo}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'certificadotipo.listar',
						  	cct.cod_certificadotipo,
							cct.nome"
							,FALSE
						);
        
        $this->db->from('cad_certificadotipo cct');

        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("cct.nome like '%{$busca}%'");
        
        $this->db->order_by("cct.nome", "asc");

        if (LIMIT<> 0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_select() {
		$this->db->select(" 'certificadotipo.listar',
						  	cct.cod_certificadotipo,
							cct.nome"
							,FALSE
						);
        
        $this->db->from('cad_certificadotipo cct');

        $this->db->order_by("cct.nome", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function inserir() {
        
        $nome = $this->input->get_post('nome');
        $texto = $this->input->get_post('texto');

        $this->data["nome"]=$nome;
        $this->data["texto"]=$texto;
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_certificadotipo', $this->data);

        
    }

    public function salvar($cod_certificadotipo) {
        
        $nome = $this->input->get_post('nome');
        $texto = $this->input->get_post('texto');

        $this->data["nome"]=$nome;
        $this->data["texto"]=$texto;
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->where('cod_certificadotipo = ',$cod_certificadotipo);
		$this->db->update('cad_certificadotipo', $this->data);
        
    }

    public function excluir($cod_certificadotipo) {

        $this->db->where("cod_certificadotipo", $cod_certificadotipo);
		$this->db->delete('cad_certificadotipo');
        
    }
}

