<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Servico_model extends CI_Model {
    
    private $data = array(),
            $data_etapas = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	//Retorna a quantidade total de registros da tabela
    function contar(){
		return $this->db->count_all('cad_servico');
	}

    public function editar($cod_servico) {
		$this->db->select(" 'servico.editar',
						  	cser.cod_servico,
						  	cser.cod_tarefa,
							cser.cod_cliente,
							cser.cod_pasta,
							cser.data_inicio,
                            date_format(cser.data_inicio, '%d/%m/%Y') as 'data_inicio_', 
							cser.data_previsao,
                            date_format(cser.data_previsao, '%d/%m/%Y') as 'data_previsao_', 
							cser.data_conclusao,
                            date_format(cser.data_conclusao, '%d/%m/%Y') as 'data_conclusao_',
							cser.data_suspensao,
                            date_format(cser.data_suspensao, '%d/%m/%Y') as 'data_suspensao_',
                            cser.texto,
                            ctar.nome as 'tarefa',
                            ccli.nome as 'cliente',
                            cpas.nome as 'cod_pasta',
                            ccli.cod_interno,
                            ccli.folder".$this->functions->sql_auditoria("cser")
						);
        
        $this->db->from('cad_servico cser');
        $this->db->join('cad_cliente ccli', "ccli.cod_cliente = cser.cod_cliente",'left');
        $this->db->join('cad_pasta cpas', "cpas.cod_pasta = cser.cod_pasta",'left');
        $this->db->join('cad_tarefa ctar', "ctar.cod_tarefa = cser.cod_tarefa",'left');
        
		$this->db->where("cod_servico = '{$cod_servico}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'servico.listar',
                            cser.cod_servico,
                            cser.cod_tarefa,
                            cser.cod_cliente,
                            cser.cod_pasta,
							date_format(cser.data_inicio, '%d/%m/%Y') as 'data_inicio_', 
                            date_format(cser.data_previsao, '%d/%m/%Y') as 'data_previsao_', 
                            date_format(cser.data_conclusao, '%d/%m/%Y') as 'data_conclusao_', 
                            date_format(cser.data_suspensao, '%d/%m/%Y') as 'data_suspensao_', 
							cser.texto,
                            ctar.nome as 'tarefa',
                            ccli.cod_interno,
                            concat(ccli.nome,' (',ccli.cod_interno,')') as 'cliente',
                            cpas.nome as 'cod_pasta',
                            "
							,FALSE
						);
        
        $this->db->from('cad_servico cser');
        $this->db->join('cad_tarefa ctar', "ctar.cod_tarefa = cser.cod_tarefa",'left');
        $this->db->join('cad_cliente ccli', "ccli.cod_cliente = cser.cod_cliente",'left');
        $this->db->join('cad_pasta cpas', "cpas.cod_pasta = cser.cod_pasta",'left');

        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("
                                        ccli.nome like '%{$busca}%' 
                                        or cpas.nome like '%{$busca}%'
                                        or ctar.nome like '%{$busca}%'
                                    ");

        if ($this->input->get_post('busca_concluido')<>"S"){
            $this->db->where("(cser.data_conclusao is null or cser.data_conclusao = '0000-00-00')");
        }
        
        if ($this->input->get_post('busca_suspenso')<>"S"){
            $this->db->where("(cser.data_suspensao is null or cser.data_suspensao = '0000-00-00')");
        }
        
        if ($this->session->userdata('tipo')=="C"){
            $this->db->where("cser.cod_cliente = '".$this->session->userdata('cliente')."'");        
        } elseif ($this->session->userdata('tipo')=="T"){
            $this->db->where("(cser.cod_cliente=0 or cser.cod_cliente in (select cucli_.cod_cliente from cad_usuariocliente cucli_ where cucli_.cod_usuario = '".$this->session->userdata('cod_usuario')."'))"); 
        }
        
        $orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"tarefa";
        $orderby_order = $this->input->get_post('orderby_order');
        $this->db->order_by($orderby_column, $orderby_order);

        if (LIMIT<>0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_servicovencido() {
        $this->db->select(" 'servico.listar_servicovencido',
                            cser.cod_servico,
                            cser.cod_tarefa,
                            cser.cod_cliente,
							date_format(cser.data_inicio, '%d/%m/%Y') as 'data_inicio_', 
                            date_format(cser.data_previsao, '%d/%m/%Y') as 'data_previsao_', 
                            date_format(cser.data_conclusao, '%d/%m/%Y') as 'data_conclusao_',
                            date_format(cser.data_suspensao, '%d/%m/%Y') as 'data_suspensao_',
							cser.texto,
                            ctar.nome as 'tarefa',
                            ccli.nome as 'cliente',
                            case 
                            when cser.data_previsao is not null and cser.data_previsao <> '0000-00-00' and datediff(cser.data_previsao, now()) >= 0 and datediff(cser.data_previsao, now()) <= 10 then 'Próximo ao vencimento' 
                            when cser.data_previsao is not null and cser.data_previsao <> '0000-00-00' and datediff(cser.data_previsao, now()) > 10 then 'ok' 
                            when cser.data_previsao is not null and cser.data_previsao <> '0000-00-00' and datediff(cser.data_previsao, now()) < 0 then 'Vencido' 
                            when cser.data_previsao is null or cser.data_previsao = '0000-00-00' then 'Sem previsão' 
                            end as 'situacao', 
                            case 
                            when cser.data_previsao is not null and cser.data_previsao <> '0000-00-00' and datediff(cser.data_previsao, now()) >= 0 and datediff(cser.data_previsao, now()) <= 10 then 'warning' 
                            when cser.data_previsao is not null and cser.data_previsao <> '0000-00-00' and datediff(cser.data_previsao, now()) > 10 then 'success' 
                            when cser.data_previsao is not null and cser.data_previsao <> '0000-00-00' and datediff(cser.data_previsao, now()) < 0 then 'danger' 
                            when cser.data_previsao is null or cser.data_previsao = '0000-00-00' then 'warning'  
                            end as 'classe' 

                            "
							,FALSE
						);
        
        $this->db->from('cad_servico cser');
        $this->db->join('cad_tarefa ctar', "ctar.cod_tarefa = cser.cod_tarefa",'left');
        $this->db->join('cad_cliente ccli', "ccli.cod_cliente = cser.cod_cliente",'left');

        $this->db->where("cser.data_previsao<=now() and (cser.data_conclusao is null or cser.data_conclusao = '0000-00-00')");
        $this->db->where("cser.data_previsao<=now() and (cser.data_suspensao is null or cser.data_suspensao = '0000-00-00')");

        if ($this->session->userdata('tipo')=="C"){
            $this->db->where("cser.cod_cliente = '".$this->session->userdata('cliente')."'");        
        }

        if ($this->session->userdata('tipo')=="T"){
            $this->db->where("(ccli.cod_cliente in (".$this->session->userdata('clientes_codigos')."0) or '".$this->session->userdata('admin')."'='S')");    
        }
        
        $this->db->order_by("ccli.nome", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_etapas($cod_servico) {
		$this->db->select(" 'servico.listar_etapas',
						  	cse.cod_servicoetapa,
						  	cse.cod_tarefaetapa,
						  	cse.cod_servico,
                            cse.tipo,
                            case cse.tipo
                            when 'C' then 'Cliente'
                            when 'T' then 'TripleAIE'
                            end as 'tipo_',
						  	cse.ordem,
							cse.nome,
							cse.data_inicio,
                            date_format(cse.data_inicio, '%d/%m/%Y') as 'data_inicio_', 
							cse.data_previsao,
                            date_format(cse.data_previsao, '%d/%m/%Y') as 'data_previsao_', 
							cse.data_conclusao,
                            date_format(cse.data_conclusao, '%d/%m/%Y') as 'data_conclusao_',
                            cse.texto,
                            cse.previsao,
                            cse.arquivo,
                            cse.arquivo_original"
							,FALSE
						);
        
        $this->db->from('cad_servicoetapa cse');
        
        $this->db->where("cod_servico = '{$cod_servico}'");

        $this->db->order_by("cse.ordem*1", "asc",FALSE);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_calendario($cod_cliente,$mes="",$ano="") {
        $this->db->select(" 'servico.listar_calendario',
                            cser.cod_servico,
						  	cse.cod_servicoetapa,
                            cse.nome,
                            cse.data_previsao,
                            date_format(cse.data_previsao, '%d/%m/%Y') as 'data_previsao_'"
							,FALSE
						);
        
        $this->db->from('cad_servicoetapa cse');
        $this->db->join('cad_servico cser', "cser.cod_servico = cse.cod_servico",'left');
        
        $this->db->where("cser.cod_cliente = '{$cod_cliente}'");
        $this->db->where("cse.tipo = 'C'");

        if ($mes<>""){
            $this->db->where("month(cse.data_previsao)='{$mes}'");
        }
        if ($ano<>""){
            $this->db->where("year(cse.data_previsao)='{$ano}'");
        }


        $this->db->order_by("cse.data_previsao", "asc");
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function servico_data_previsao($cod_servico) {
		$this->db->select(" 'servico.servico_data_previsao',
						  	max(data_previsao) as 'data_previsao'"
							,FALSE
						);
        
        $this->db->from('cad_servicoetapa cse');
        
        $this->db->where("cod_servico = '{$cod_servico}'");
        
		$query = $this->db->get(); 
		return $query->row_array();
    }

    public function grafico_quantidademes($inicio=0) {
		$this->db->select(" date_format(cser.data_inicio,'%m-%Y') as 'mes', 
                            count(*) as 'qtd'
                            "
							,FALSE
						);
        
        $this->db->from("cad_servico cser");
        $this->db->group_by("date_format(cser.data_inicio,'%m-%Y')"); 

        $this->db->limit(10, 0);
        
		$query = $this->db->get(); 
		return json_encode($query->result_array());    
    }

    public function inserir() {
        
        $this->data["cod_tarefa"]=$this->input->get_post('cod_tarefa');
        $this->data["cod_cliente"]=$this->input->get_post('cod_cliente');
        $this->data["cod_pasta"]=$this->input->get_post('cod_pasta');
        $this->data["data_inicio"]=substr($this->input->get_post('data_inicio'),6,4)."-".substr($this->input->get_post('data_inicio'),3,2)."-".substr($this->input->get_post('data_inicio'),0,2); 
        $this->data["data_previsao"]=substr($this->input->get_post('data_previsao'),6,4)."-".substr($this->input->get_post('data_previsao'),3,2)."-".substr($this->input->get_post('data_previsao'),0,2); 
        $this->data["data_conclusao"]=substr($this->input->get_post('data_conclusao'),6,4)."-".substr($this->input->get_post('data_conclusao'),3,2)."-".substr($this->input->get_post('data_conclusao'),0,2); 
        $this->data["data_suspensao"]=substr($this->input->get_post('data_suspensao'),6,4)."-".substr($this->input->get_post('data_suspensao'),3,2)."-".substr($this->input->get_post('data_suspensao'),0,2); 
        $this->data["texto"]=$this->input->get_post('texto');
        
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_servico', $this->data);

        $insert_id = $this->db->insert_id();
        
        $this->salvar_etapas($insert_id,$this->input->get_post('cod_cliente'));
        
        $this->db->query("update cad_servico set data_previsao = (SELECT max(data_previsao) as 'data_previsao' FROM `cad_servicoetapa` `cse` WHERE `cod_servico` = '{$insert_id}') WHERE `cod_servico` = '{$insert_id}'");

        //Envia e-mail e msg no APP para o cliente
		$this->db->select(" 'coleta.inserir',
                            cusu.nome,
                            cusu.email,
                            cusu.device_id,
                            cusu.device_platform,
                            ccli.nome as 'cliente',
                            ccli.razao_social"
                            );

        $this->db->from('cad_usuario cusu');
        $this->db->join('cad_usuariocliente cucli', "cucli.cod_usuario = cusu.cod_usuario",'inner');
        $this->db->join('cad_cliente ccli', "ccli.cod_cliente = cucli.cod_cliente",'inner');
        $this->db->where("cusu.tipo ='C' and cusu.config_receberemail = 'S'");
        $this->db->where("cucli.cod_cliente ='".$this->data["cod_cliente"]."'");

        $query = $this->db->get();
        $dados = $query->result_array();

        foreach($dados as $value){

            if ($value["device_id"]<>NULL and $value["device_platform"]<>NULL){
                $this->functions->app_push($value["device_id"],$value["device_platform"],TITULO,"Novo Serviço");
            }

            $template = $this->Template_model->editar(6);

            $myemail = new Myemail;
            $myemail->cod_cliente(0);
            $myemail->from(TITULO, EMAIL_NR);
            $myemail->to("BAH",$value["email"]);
            $myemail->subject($template["titulo"]);
            $myemail->body($template["texto"]);
            $myemail->custom(array("%USUARIO%"=>$value["nome"],"%CLIENTE%"=>$value["cliente"],"%RAZAOSOCIAL%"=>$value["razao_social"],"%TEXTO%"=>$this->data["texto"]));
            $myemail->enviar();

        }
    }

    public function salvar($cod_servico) {
        
        $this->data["data_inicio"]=substr($this->input->get_post('data_inicio'),6,4)."-".substr($this->input->get_post('data_inicio'),3,2)."-".substr($this->input->get_post('data_inicio'),0,2); 
        $this->data["data_previsao"]=substr($this->input->get_post('data_previsao'),6,4)."-".substr($this->input->get_post('data_previsao'),3,2)."-".substr($this->input->get_post('data_previsao'),0,2); 
        $this->data["data_conclusao"]=substr($this->input->get_post('data_conclusao'),6,4)."-".substr($this->input->get_post('data_conclusao'),3,2)."-".substr($this->input->get_post('data_conclusao'),0,2);
        $this->data["data_suspensao"]=substr($this->input->get_post('data_suspensao'),6,4)."-".substr($this->input->get_post('data_suspensao'),3,2)."-".substr($this->input->get_post('data_suspensao'),0,2);
        $this->data["texto"]=$this->input->get_post('texto');

        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->where('cod_servico = ',$cod_servico);
		$this->db->update('cad_servico', $this->data);
        
        $this->salvar_etapas($cod_servico,$this->input->get_post('cod_cliente'));
        
        $this->db->query("update cad_servico set data_previsao = (SELECT max(data_previsao) as 'data_previsao' FROM `cad_servicoetapa` `cse` WHERE `cod_servico` = '{$cod_servico}') WHERE `cod_servico` = '{$cod_servico}'");
            
    }

    public function salvar_etapas($cod_servico,$cod_cliente) {
        $this->db->where("cod_servico", $cod_servico);
		$this->db->delete('cad_servicoetapa');
        
        $etapa_cod_servicoetapa = $this->input->get_post("etapa_cod_servicoetapa");
        $etapa_cod_tarefaetapa = $this->input->get_post("etapa_cod_tarefaetapa");
        $etapa_tipo = $this->input->get_post("etapa_tipo");
        $etapa_ordem = $this->input->get_post("etapa_ordem");
        $etapa_nome = $this->input->get_post("etapa_nome");
        $etapa_previsao = $this->input->get_post("etapa_previsao");
        $etapa_data_inicio = $this->input->get_post("etapa_data_inicio"); 
        $etapa_data_previsao = $this->input->get_post("etapa_data_previsao"); 
        $etapa_data_conclusao = $this->input->get_post("etapa_data_conclusao"); 
        $etapa_previsao = $this->input->get_post("etapa_previsao"); 
        $etapa_arquivo = $this->input->get_post("etapa_arquivo"); 
        $etapa_arquivoanexado = $this->input->get_post("etapa_arquivoanexado"); 
        $etapa_arquivoanexadooriginal = $this->input->get_post("etapa_arquivoanexadooriginal"); 
        $etapa_arquivotarefa = $this->input->get_post("etapa_arquivotarefa"); 
        foreach( $etapa_nome as $key => $n ) {
            if ($etapa_nome[$key]<>""){
                unset($data_etapas);
                $data_etapas = array();

                $this->data_etapas["cod_servico"]=$cod_servico;
                $this->data_etapas["cod_tarefaetapa"]=$etapa_cod_tarefaetapa[$key];
                $this->data_etapas["tipo"]=$etapa_tipo[$key];
                $this->data_etapas["ordem"]=$etapa_ordem[$key];
                $this->data_etapas["nome"]=$etapa_nome[$key];
                $this->data_etapas["data_inicio"]=substr($etapa_data_inicio[$key],6,4)."-".substr($etapa_data_inicio[$key],3,2)."-".substr($etapa_data_inicio[$key],0,2);
                $this->data_etapas["data_previsao"]=substr($etapa_data_previsao[$key],6,4)."-".substr($etapa_data_previsao[$key],3,2)."-".substr($etapa_data_previsao[$key],0,2);
                $this->data_etapas["data_conclusao"]=substr($etapa_data_conclusao[$key],6,4)."-".substr($etapa_data_conclusao[$key],3,2)."-".substr($etapa_data_conclusao[$key],0,2);
                $this->data_etapas["previsao"]=$etapa_previsao[$key];
                
                $this->db->insert('cad_servicoetapa', $this->data_etapas);

                $insert_id = $this->db->insert_id();

                $cod_interno = $this->input->get_post("cod_interno");

                //Upload do arquivo
                if ($_FILES['etapa_arquivo']['name'][$key]){
                    $name = $_FILES['etapa_arquivo']['name'][$key];
                    $size = $_FILES['etapa_arquivo']['size'][$key];    
                    $tmp_name = $_FILES['etapa_arquivo']['tmp_name'][$key];
                    $ext = pathinfo($name, PATHINFO_EXTENSION);        

                    $folder = $this->functions->cliente_folder($cod_interno);
                    $nome_arquivo = "servico{$cod_servico}-{$insert_id}.{$ext}";
                    $arquivo = strtolower("{$folder}servico{$cod_servico}-{$insert_id}.{$ext}");

                    move_uploaded_file($tmp_name, $arquivo);
                    
                    $this->db->query("update cad_servicoetapa set arquivo = '{$nome_arquivo}', arquivo_original = '{$name}' WHERE `cod_servicoetapa` = '{$insert_id}'");
                    
                }elseif($etapa_arquivoanexado[$key]<>""){
                    $this->db->query("update cad_servicoetapa set arquivo = '{$etapa_arquivoanexado[$key]}', arquivo_original = '{$etapa_arquivoanexadooriginal[$key]}' WHERE `cod_servicoetapa` = '{$insert_id}'");
                    log_message('debug', "arquivo existente servico:$etapa_arquivoanexado[$key]");
                }elseif($etapa_arquivotarefa[$key]<>"" and $etapa_arquivotarefa[$key]<>NULL){
                    $origem = "./data/tarefa/".$etapa_arquivotarefa[$key];

                    if (file_exists($origem)){
                        $folder = $this->functions->cliente_folder($cod_interno);
                        $ext = pathinfo($origem, PATHINFO_EXTENSION);
                        $nome_arquivo = "servico{$cod_servico}-{$insert_id}.{$ext}";
                        $destino = strtolower("{$folder}servico{$cod_servico}-{$insert_id}.{$ext}");

                        copy($origem,$destino);

                        $this->db->query("update cad_servicoetapa set arquivo = '{$nome_arquivo}' WHERE `cod_servicoetapa` = '{$insert_id}'");
                    }
                }
            }     
        }     
    }

    public function salvar_etapas_batch($cod_servico) {
        $this->db->where("cod_servico", $cod_servico);
		$this->db->delete('cad_servicoetapa');
        
        $etapa_cod_tarefaetapa = $this->input->get_post("etapa_cod_tarefaetapa");
        $etapa_ordem = $this->input->get_post("etapa_ordem");
        $etapa_nome = $this->input->get_post("etapa_nome");
        $etapa_previsao = $this->input->get_post("etapa_previsao");
        $etapa_data_inicio = $this->input->get_post("etapa_data_inicio"); 
        $etapa_data_previsao = $this->input->get_post("etapa_data_previsao"); 
        $etapa_data_conclusao = $this->input->get_post("etapa_data_conclusao"); 
        $etapa_previsao = $this->input->get_post("etapa_previsao"); 
        foreach( $etapa_nome as $key => $n ) {
            $this->data_etapas[] = array('cod_servico'=>$cod_servico,
                                            'cod_tarefaetapa'=>$etapa_cod_tarefaetapa[$key],
                                            'ordem'=>$etapa_ordem[$key],
                                            'nome'=>$etapa_nome[$key],
                                            'data_inicio'=>substr($etapa_data_inicio[$key],6,4)."-".substr($etapa_data_inicio[$key],3,2)."-".substr($etapa_data_inicio[$key],0,2), 
                                            'data_previsao'=>substr($etapa_data_previsao[$key],6,4)."-".substr($etapa_data_previsao[$key],3,2)."-".substr($etapa_data_previsao[$key],0,2),
                                            'data_conclusao'=>substr($etapa_data_conclusao[$key],6,4)."-".substr($etapa_data_conclusao[$key],3,2)."-".substr($etapa_data_conclusao[$key],0,2),
                                            'previsao'=>$etapa_previsao[$key],
                                          );
        }     
        //print_r($this->data_etapas);exit;
        if (!empty($this->data_etapas)) $this->db->insert_batch('cad_servicoetapa', $this->data_etapas);
    }

    public function excluir($cod_servico) {

        $this->db->where("cod_servico", $cod_servico);
		$this->db->delete('cad_servicoetapa');
        
        $this->db->where("cod_servico", $cod_servico);
		$this->db->delete('cad_servico');
        
    }

       
}

