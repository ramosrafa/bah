<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Link_model extends CI_Model {
		
	private $data = array();  
	
	function __construct() {
			parent::__construct(); 
			$this->load->dbutil();
    }
    function contar(){
		return $this->db->count_all('cad_link');
	}

	public function editar($cod_link) {
        $this->db->select(" 'link.editar',
							clink.cod_link,
							clink.nome,
							clink.texto,
							clink.link,
                            clink.uf,
                            clink.cidade ".$this->functions->sql_auditoria("clink")
					);
			
		$this->db->from('cad_link clink');
			
		$this->db->where("cod_link = '{$cod_link}'");
		
		$query = $this->db->get(); 
		return $query->row_array();		
	}

	public function listar($inicio=0) {
		$this->db->select(" 'link.listar',
							clink.cod_link,
							clink.nome,
							clink.texto,
							clink.link,
                            clink.uf,
                            clink.cidade "
							,FALSE
						);
			
		$this->db->from('cad_link clink');

		$busca = $this->input->get_post('busca');
		if ($busca) $this->db->where("clink.nome like '%{$busca}%'");
		
		$orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"nome";
		$orderby_order = $this->input->get_post('orderby_order');
		$this->db->order_by($orderby_column, $orderby_order);

		if (LIMIT<> 0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
			
		$query = $this->db->get(); 
		return $query->result_array();    
	}

	public function listar_select() {
		$this->db->select(" 'link.listar_select',
							clink.nome,
							clink.texto,
							clink.link,
                            clink.uf,
                            clink.cidade "
							,FALSE
						);
				
		$this->db->from('cad_link clink');

		$this->db->order_by("clink.nome", "asc");
				
		$query = $this->db->get(); 
		return $query->result_array();    
	}

	public function listar_localizacao() {
		$this->db->select(" 'link.listar_localizacao',
							clink.nome,
							clink.texto,
							clink.link,
                            clink.uf,
                            clink.cidade "
							,FALSE
						);
				
		$this->db->from('cad_link clink');
		$this->db->where("(clink.uf='' or clink.uf = '".$this->session->userdata('cliente_uf')."') and (clink.cidade='' or clink.cidade = '".$this->session->userdata('cliente_cidade')."') ");
		$this->db->order_by("clink.nome", "asc");
				
		$query = $this->db->get(); 
		return $query->result_array();    
	}

	public function inserir() {
			
        $nome = $this->input->get_post('nome');
		$texto = $this->input->get_post('texto');
        $link = $this->input->get_post('link');
        $uf = $this->input->get_post('uf');
        $cidade = $this->input->get_post('cidade');

		$this->data["nome"]=$nome;
		$this->data["texto"]=$texto;
		$this->data["link"]=$link;
        $this->data["uf"]=$uf;
        $this->data["cidade"]=$cidade;
		$this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
		$this->data['data_c']=date('Y-m-d H:i:s');       
		$this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
		$this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->insert('cad_link', $this->data);

	}

	public function salvar($cod_link) {
			
        $nome = $this->input->get_post('nome');
		$texto = $this->input->get_post('texto');
        $link = $this->input->get_post('link');
        $uf = $this->input->get_post('uf');
        $cidade = $this->input->get_post('cidade');

		$this->data["nome"]=$nome;
		$this->data["texto"]=$texto;
		$this->data["link"]=$link;
        $this->data["uf"]=$uf;
        $this->data["cidade"]=$cidade;
		$this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
		$this->data['data_a']=date('Y-m-d H:i:s');       

		$this->db->where('cod_link = ',$cod_link);
		$this->db->update('cad_link', $this->data);		
	}
	
	public function excluir($cod_link) {

		$this->db->where("cod_link", $cod_link);
		$this->db->delete('cad_link');
			
	}
}

