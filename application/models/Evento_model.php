<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido');

class Evento_model extends CI_Model {
    
    private $data = array();  
    
    function __construct() {
        parent::__construct();
        $this->load->dbutil();
    }

	//Retorna a quantidade total de registros da tabela
    function contar(){
		return $this->db->count_all('cad_evento');
	}

    public function editar($cod_evento="",$sigla="") {
		$this->db->select(" 'evento.editar',
						  	ceve.cod_evento,
						  	ceve.cod_pasta,
						  	ceve.tipo,
						  	ceve.calendario,
						  	ceve.nome,
						  	ceve.vencimento,
						  	ceve.vencimento_dias,
						  	ceve.vencimento_diastipo,
						  	ceve.vencimento_meses,
                            ceve.vencimento_ajuste,
                            ceve.mes_01,
                            ceve.mes_02,
                            ceve.mes_03,
                            ceve.mes_04,
                            ceve.mes_05,
                            ceve.mes_06,
                            ceve.mes_07,
                            ceve.mes_08,
                            ceve.mes_09,
                            ceve.mes_10,
                            ceve.mes_11,
                            ceve.mes_12,
						  	ceve.sigla,
                            ceve.texto,
                            cpas.nome as 'pasta'".$this->functions->sql_auditoria("ceve")
						);
        
        $this->db->from('cad_evento ceve');
        $this->db->join('cad_pasta cpas', "cpas.cod_pasta = ceve.cod_pasta",'left');

		if ($cod_evento) $this->db->where("cod_evento = '{$cod_evento}'");
		if ($sigla) $this->db->where("sigla = '{$sigla}'");
		
		$query = $this->db->get(); 
        return $query->row_array();		
    }

    public function listar($inicio=0) {
		$this->db->select(" 'evento.listar',
						  	ceve.cod_evento,
						  	ceve.cod_pasta,
						  	ceve.tipo,
						  	ceve.calendario,
						  	ceve.nome,
						  	ceve.vencimento,
						  	ceve.vencimento_dias,
						  	ceve.vencimento_diastipo,
						  	ceve.vencimento_meses,
						  	ceve.vencimento_ajuste,
                            ceve.mes_01,
                            ceve.mes_02,
                            ceve.mes_03,
                            ceve.mes_04,
                            ceve.mes_05,
                            ceve.mes_06,
                            ceve.mes_07,
                            ceve.mes_08,
                            ceve.mes_09,
                            ceve.mes_10,
                            ceve.mes_11,
                            ceve.mes_12,
						  	ceve.sigla,
							ceve.texto,
                            cpas.nome as 'cod_pasta'"
						);
        
        $this->db->from('cad_evento ceve');
		$this->db->join('cad_pasta cpas', "cpas.cod_pasta = ceve.cod_pasta",'left');

        $busca = $this->input->get_post('busca');
        if ($busca) $this->db->where("ceve.nome like '%{$busca}%' or ceve.sigla like '%{$busca}%'");
        
        $orderby_column = $this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):"nome";
        $orderby_order = $this->input->get_post('orderby_order');
        $this->db->order_by($orderby_column, $orderby_order);
        
        if (LIMIT<>0 and is_numeric($inicio)) $this->db->limit(LIMIT, $inicio);
		
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function listar_select($cod_pasta="") {
		$this->db->select(" 'evento.listar',
						  	ceve.cod_evento,
						  	ceve.cod_pasta,
						  	ceve.tipo,
						  	concat(ceve.nome,' (',ceve.sigla,')') as 'evento',
						  	ceve.vencimento,
						  	ceve.vencimento_dias,
						  	ceve.vencimento_diastipo,
						  	ceve.vencimento_meses,
						  	ceve.vencimento_ajuste,
						  	ceve.sigla,
							ceve.texto,
                            cpas.nome as 'cod_pasta'"
						);
        
        $this->db->from('cad_evento ceve');
		$this->db->join('cad_pasta cpas', "cpas.cod_pasta = ceve.cod_pasta",'left');
        
        if ($cod_pasta<>"") $this->db->where("ceve.cod_pasta = '{$cod_pasta}'");

        $this->db->order_by("ceve.nome", "asc");
		
		$query = $this->db->get(); 
		return $query->result_array();    
    }

    public function validar() {

        $cod_evento = $this->input->get_post('cod_evento');
        $sigla = $this->input->get_post('sigla');

        // Verifica se já existe o e-mail informado
        $this->db->select(" 'evento.validar',
                            ceve.cod_evento
                        ");

        $this->db->from('cad_evento ceve');
        
        $this->db->where("ceve.sigla = '{$sigla}' and ceve.cod_evento <> '{$cod_evento}'");
        
		$query = $this->db->get();
        
        if( $query->num_rows() == 0 ) return array('op'=>'1', 'msg' => "");
        if( $query->num_rows() > 0 ) return array('op'=>'0', 'msg' => "Sigla já cadastrada: {$sigla}" );
    }

    public function inserir() {
        
        $cod_pasta = $this->input->get_post('cod_pasta');
        $tipo = $this->input->get_post('tipo');
        $calendario = $this->input->get_post('calendario');
        $nome = $this->input->get_post('nome');
        $vencimento = $this->input->get_post('vencimento');
        $vencimento_dias = $this->input->get_post('vencimento_dias');
        $vencimento_diastipo = $this->input->get_post('vencimento_diastipo');
        $vencimento_meses = $this->input->get_post('vencimento_meses');
        $vencimento_ajuste = $this->input->get_post('vencimento_ajuste');
        $mes_01 = $this->input->get_post('mes_01');
        $mes_02 = $this->input->get_post('mes_02');
        $mes_03 = $this->input->get_post('mes_03');
        $mes_04 = $this->input->get_post('mes_04');
        $mes_05 = $this->input->get_post('mes_05');
        $mes_06 = $this->input->get_post('mes_06');
        $mes_07 = $this->input->get_post('mes_07');
        $mes_08 = $this->input->get_post('mes_08');
        $mes_09 = $this->input->get_post('mes_09');
        $mes_10 = $this->input->get_post('mes_10');
        $mes_11 = $this->input->get_post('mes_11');
        $mes_12 = $this->input->get_post('mes_12');
        $sigla = $this->input->get_post('sigla');
        $texto = $this->input->get_post('texto');

        $this->data["cod_pasta"]=$cod_pasta;
        $this->data["tipo"]=$tipo;
        $this->data["calendario"]=$calendario;
        $this->data["nome"]=$nome;
        $this->data["vencimento"]=$vencimento;
        $this->data["vencimento_dias"]=$vencimento_dias; 
        $this->data["vencimento_diastipo"]=$vencimento_diastipo; 
        $this->data["vencimento_meses"]=$vencimento_meses; 
        $this->data["vencimento_ajuste"]=$vencimento_ajuste; 
        $this->data["mes_01"]=$mes_01;
        $this->data["mes_02"]=$mes_02;
        $this->data["mes_03"]=$mes_03;
        $this->data["mes_04"]=$mes_04;
        $this->data["mes_05"]=$mes_05;
        $this->data["mes_06"]=$mes_06;
        $this->data["mes_07"]=$mes_07;
        $this->data["mes_08"]=$mes_08;
        $this->data["mes_09"]=$mes_09;
        $this->data["mes_10"]=$mes_10;
        $this->data["mes_11"]=$mes_11;
        $this->data["mes_12"]=$mes_12;
        $this->data["sigla"]=$sigla;
        $this->data["texto"]=$texto;
        $this->data['cod_usuario_c']=$this->session->userdata('cod_usuario');        
        $this->data['data_c']=date('Y-m-d H:i:s');       
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       

        $this->db->insert('cad_evento', $this->data);

        
    }

    public function salvar($cod_evento) {
        
        $cod_pasta = $this->input->get_post('cod_pasta');
        $tipo = $this->input->get_post('tipo');
        $calendario = $this->input->get_post('calendario');
        $nome = $this->input->get_post('nome');
        $vencimento = $this->input->get_post('vencimento');
        $vencimento_dias = $this->input->get_post('vencimento_dias');
        $vencimento_diastipo = $this->input->get_post('vencimento_diastipo');
        $vencimento_meses = $this->input->get_post('vencimento_meses');
        $vencimento_ajuste = $this->input->get_post('vencimento_ajuste');
        $mes_01 = $this->input->get_post('mes_01');
        $mes_02 = $this->input->get_post('mes_02');
        $mes_03 = $this->input->get_post('mes_03');
        $mes_04 = $this->input->get_post('mes_04');
        $mes_05 = $this->input->get_post('mes_05');
        $mes_06 = $this->input->get_post('mes_06');
        $mes_07 = $this->input->get_post('mes_07');
        $mes_08 = $this->input->get_post('mes_08');
        $mes_09 = $this->input->get_post('mes_09');
        $mes_10 = $this->input->get_post('mes_10');
        $mes_11 = $this->input->get_post('mes_11');
        $mes_12 = $this->input->get_post('mes_12');
        $sigla = $this->input->get_post('sigla');
        $texto = $this->input->get_post('texto');

        $this->data["cod_pasta"]=$cod_pasta;
        $this->data["tipo"]=$tipo;
        $this->data["calendario"]=$calendario;
        $this->data["nome"]=$nome;
        $this->data["vencimento"]=$vencimento;
        $this->data["vencimento_dias"]=$vencimento_dias; 
        $this->data["vencimento_diastipo"]=$vencimento_diastipo; 
        $this->data["vencimento_meses"]=$vencimento_meses; 
        $this->data["vencimento_ajuste"]=$vencimento_ajuste; 
        $this->data["mes_01"]=$mes_01;
        $this->data["mes_02"]=$mes_02;
        $this->data["mes_03"]=$mes_03;
        $this->data["mes_04"]=$mes_04;
        $this->data["mes_05"]=$mes_05;
        $this->data["mes_06"]=$mes_06;
        $this->data["mes_07"]=$mes_07;
        $this->data["mes_08"]=$mes_08;
        $this->data["mes_09"]=$mes_09;
        $this->data["mes_10"]=$mes_10;
        $this->data["mes_11"]=$mes_11;
        $this->data["mes_12"]=$mes_12;
        $this->data["sigla"]=$sigla;
        $this->data["texto"]=$texto;
        $this->data['cod_usuario_a']=$this->session->userdata('cod_usuario');       
        $this->data['data_a']=date('Y-m-d H:i:s');       
		
		$this->db->where('cod_evento = ',$cod_evento);
		$this->db->update('cad_evento', $this->data);
        
    }

    public function excluir($cod_evento) {

        $this->db->where("cod_evento", $cod_evento);
		$this->db->delete('cad_evento');
        
    }
    
    
    //JSON
    public function json_eventolistar() {

        $this->db->select(" 'evento_.json_eventolistar',
						  	ceve.nome,
                            ceve.sigla"
						);
        
        $this->db->from('cad_evento ceve');
        
        $this->db->order_by("ceve.nome", "asc");
        
        $query = $this->db->get(); 
		return json_encode($query->result_array());    
    }    

}