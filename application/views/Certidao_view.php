<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
?>

<div class="col-sm-12 div-form">
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                <label class="control-label" for="nome">Nome</label>  
                <input type="text" name="nome" id="nome" maxlength="100" value="<?=@$var_nome;?>" readonly="true" class="form-control">
            </div>            
        </div>  
        <div class="col-sm-2">
            <div class="form-group">
                <label class="control-label" for="documento">CPF/CNPJ</label>  
                <input type="text" name="documento" id="documento" maxlength="14" value="<?=@$var_documento;?>" readonly="true" class="form-control">
            </div>            
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label class="control-label" for="documento">Estado</label>  
                <input type="text" name="cliente_uf" id="cliente_uf" maxlength="14" value="<?=$this->session->userdata('cliente_uf');?>" readonly="true" class="form-control">
            </div>            
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label class="control-label" for="documento">Cidade</label>  
                <input type="text" name="cliente_cidade" id="cliente_cidade" maxlength="14" value="<?=$this->session->userdata('cliente_cidade');?>" readonly="true" class="form-control">
            </div>            
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <br><a href="#" class="btn" id="certidao-copiardocumento">Copiar para área de transferência</a>
            </div>            
        </div>
    </div>
</div>
<div class="col-sm-12 div-form">
    <div class="div-table">
        <div class="div-table-fixed">
            <table class="tablebah">
                <thead>
                    <tr>
                        <th width="20%">Certidão</th>
                        <th width="*">Descrição</th>
                        <th width="3%">&nbsp;</th>
                        <th width="3%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($dados_links as $value){
                            ?>
                            <tr>
                                <td><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>&nbsp;&nbsp;<?=$value["nome"]?></td>
                                <td><?=$value["texto"]?></td>
                                <td><a href="<?=$value["link"]?>" target="_blank" class="btn pull-right">Abrir site</a></td>
                            </tr>
                            <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>

    function Certidao(documento) {
        url = 'http://servicos.receita.fazenda.gov.br/Servicos/certidao/CNDConjuntaSegVia/ResultadoSegVia.asp?Origem=1&Tipo=1&NI='+documento+'&Senha=';
        //alert(url);
        var msgWindow;
        
        msgWindow=window.open(url);
        /*
        msgWindow.document.clear();
        msgWindow.document.write ("<HTML><BODY>")
        
        msgWindow.document.write ("<div align='center'>")
        
        msgWindow.document.write (msgWindow.document.getElementById("PRINCIPAL").innerHTML);
        
        msgWindow.document.write ("</div>")
        
        msgWindow.document.write ("</HTML></BODY>")
        msgWindow.document.close();
        */
        //msgWindow.focus();
        //msgWindow.print();
    }
</script>
