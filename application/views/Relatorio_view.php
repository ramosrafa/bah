<?php 
    if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
    
    $rel=0;
?>

<div class="col-sm-12">

    <?php if ($operacao=="diverso"){?>

        <div class="panel-group" id="accordion">

            <?php $rel++;?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$rel?>"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Alvarás Vencidos</a>
                    <a href="<?=LOCAL?>cliente\relatorio_alvaras" class="btn pull-right" target="_blank">Relatório</a>
                    <a href="<?=LOCAL?>cliente\relatorio_alvaras\csv" class="btn pull-right" target="_blank">Exportar</a>
                    </h4>
                </div>
                <div id="collapse<?=$rel?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <p>Lista dos alvarás vencidos por ordem de vencimento</p>
                    </div>
                </div>
            </div>

            <?php $rel++;?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$rel?>"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Certificados Digitais Vencidos</a>
                    <a href="<?=LOCAL?>cliente\relatorio_certificadosvencimento" class="btn pull-right" target="_blank">Relatório</a>
                    <a href="<?=LOCAL?>cliente\relatorio_certificadosvencimento\csv" class="btn pull-right" target="_blank">Exportar</a>
                    </h4>
                </div>
                <div id="collapse<?=$rel?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <p>Lista de vencimento dos certificados digitais dos clientes</p>
                    </div>
                </div>
            </div>

            <?php $rel++;?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$rel?>"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Clientes</a>
                    <a href="<?=LOCAL?>cliente\relatorio_cliente" class="btn pull-right" target="_blank">Relatório</a>
                    <a href="<?=LOCAL?>cliente\relatorio_cliente\csv" class="btn pull-right" target="_blank">Exportar</a>
                    </h4>
                </div>
                <div id="collapse<?=$rel?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <p>Lista dos clientes cadastrados no sistema</p>
                    </div>
                </div>
            </div>

            <?php $rel++;?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$rel?>"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Eventos</a>
                    <a href="<?=LOCAL?>evento\relatorio_evento" class="btn pull-right" target="_blank">Relatório</a>
                    <a href="<?=LOCAL?>evento\relatorio_evento\csv" class="btn pull-right" target="_blank">Exportar</a>
                    </h4>
                </div>
                <div id="collapse<?=$rel?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <p>Lista dos eventos cadastrados no sistema</p>
                    </div>
                </div>
            </div>

            <?php $rel++;?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$rel?>"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;E-mails</a>
                    <a class="a-acao btn pull-right" data-objeto="<?=LOCAL."email"?>" data-acao="relatorio_email" title="Relatório" target="_blank">Relatório</a>
                    <a class="a-acao btn pull-right" data-objeto="<?=LOCAL."email"?>" data-acao="relatorio_email" data-cod="csv" title="Relatório" target="_blank">Exportar</a>
                    </h4>
                </div>
                <div id="collapse<?=$rel?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <p>Lista de e-mails enviados pelo sistema</p>
                    </div>
                </div>
            </div>

            <?php $rel++;?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$rel?>"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Entregas</a>
                    <a class="a-acao btn pull-right" data-objeto="<?=LOCAL."cliente"?>" data-acao="relatorio_entregas" title="Relatório" target="_blank">Relatório</a>
                    <a class="a-acao btn pull-right" data-objeto="<?=LOCAL."cliente"?>" data-acao="relatorio_entregas" data-cod="csv" title="Relatório" target="_blank">Exportar</a>
                    </h4>
                </div>
                <div id="collapse<?=$rel?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <p>Lista de envios de documentos</p>

                    <!--    <div class="row">
                            <div class="col-sm-6">
                                <p>Nome <input type="text" name="entrega_nome" id="entrega_nome" maxlength="50" class=""></p>
                            </div>        
                            <div class="col-sm-6">
                                <p>Vencimento de <input type="text" name="entrega_datavencimentoinicio" id="entrega_datavencimentoinicio" maxlength="10" class="input-date">
                                a <input type="text" name="entrega_datavencimentofim" id="entrega_datavencimentofim" maxlength="10" class="input-date"></p>
                            </div>        
                        </div>        -->

                    </div>
                </div>
            </div>

            <?php $rel++;?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$rel?>"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Funcionários</a>
                    <a href="<?=LOCAL?>funcionario\relatorio_funcionario" class="btn pull-right" target="_blank">Relatório</a>
                    <a href="<?=LOCAL?>funcionario\relatorio_funcionario\csv" class="btn pull-right" target="_blank">Exportar</a>
                    </h4>
                </div>
                <div id="collapse<?=$rel?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <p>Lista de funcionários por cliente</p>
                    </div>
                </div>
            </div>

            <?php $rel++;?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$rel?>"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Procurações</a>
                    <a href="<?=LOCAL?>cliente\relatorio_procuracaovencimento" class="btn pull-right" target="_blank">Relatório</a>
                    <a href="<?=LOCAL?>cliente\relatorio_procuracaovencimento\csv" class="btn pull-right" target="_blank">Exportar</a>
                    </h4>
                </div>
                <div id="collapse<?=$rel?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <p>Lista de procurações dos clientes</p>
                    </div>
                </div>
            </div>

            <?php $rel++;?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$rel?>"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Quadro Societário</a>
                    <a href="<?=LOCAL?>cliente\relatorio_quadro" class="btn pull-right" target="_blank">Relatório</a>
                    <a href="<?=LOCAL?>cliente\relatorio_quadro\csv" class="btn pull-right" target="_blank">Exportar</a>
                    </h4>
                </div>
                <div id="collapse<?=$rel?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <p>Lista de clientes e seu quadro societário</p>
                    </div>
                </div>
            </div>

            <?php $rel++;?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$rel?>"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Regime Tributário</a>
                    <a href="<?=LOCAL?>cliente\relatorio_regimetributario" class="btn pull-right" target="_blank">Relatório</a>
                    <a href="<?=LOCAL?>cliente\relatorio_regimetributario\csv" class="btn pull-right" target="_blank">Exportar</a>
                    </h4>
                </div>
                <div id="collapse<?=$rel?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <p>Lista de vencimento do regime tributário</p>
                    </div>
                </div>
            </div>

            <?php $rel++;?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$rel?>"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Serviços Vencidos</a>
                    <a href="<?=LOCAL?>servico\relatorio_servicovencido" class="btn pull-right" target="_blank">Relatório</a>
                    <a href="<?=LOCAL?>servico\relatorio_servicovencido\csv" class="btn pull-right" target="_blank">Exportar</a>
                    </h4>
                </div>
                <div id="collapse<?=$rel?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <p>Lista de serviços vencidos</p>
                    </div>
                </div>
            </div>

            <?php $rel++;?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$rel?>"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Tarefas</a>
                    <a href="<?=LOCAL?>tarefa\relatorio_tarefa" class="btn pull-right" target="_blank">Relatório</a>
                    <a href="<?=LOCAL?>tarefa\relatorio_tarefa\csv" class="btn pull-right" target="_blank">Exportar</a>
                    </h4>
                </div>
                <div id="collapse<?=$rel?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <p>Lista de tarefas</p>
                    </div>
                </div>
            </div>

            <?php $rel++;?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$rel?>"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Usuários</a>
                    <a href="<?=LOCAL?>usuario\relatorio_usuario" class="btn pull-right" target="_blank">Relatório</a>
                    <a href="<?=LOCAL?>usuario\relatorio_usuario\csv" class="btn pull-right" target="_blank">Exportar</a>
                    </h4>
                </div>
                <div id="collapse<?=$rel?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        <p>Lista de usuários</p>
                    </div>
                </div>
            </div>

        </div>

    <?php }?>

    <?php if ($operacao=="cliente_evento"){?>

        <div class="col-sm-12 div-operacao">
            <div class="row">
                <a href="<?=LOCAL?>cliente\relatorio_evento" class="btn pull-right" target="_blank">Relatório</a>
                <a href="<?=LOCAL?>cliente\relatorio_evento\csv" class="btn pull-right" target="_blank">Exportar</a>
            </div>
        </div>
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="control-label" for="nome">Nome</label>  
                        <input type="text" name="nome" id="nome" maxlength="100" value="" class="form-control">
                    </div>            
                </div>            
            </div>
        </div>

    <?php }?>

</div>

