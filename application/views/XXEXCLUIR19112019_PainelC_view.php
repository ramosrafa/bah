<?php 

    if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 

    if( $this->session->userdata('termo_de_uso') <> true ) {
        ?>
        <div class="col-sm-12 div-form" id="termodeuso">
            <div class="col-termodeuso">
                <h1>Termos e Condições de Uso do BAH</h1>
                <div class="col-termodeuso_">

                    <br><br><strong>1.Definições</strong>

                    <br><br>Bem-vindo ao Triple A – Business Anywhere – BAH. O BAH é uma aplicação web desenvolvida pela Triple A Assessoria Contábil Ltda Ltda, doravante denominada "Triple A" que é detentora dos direitos autorais. O BAH fornece uma forma de comunicação e troca de documentos entre você, doravante denominado "Usuário" e o responsável pela contabilidade de sua empresa. 


                    <br><br><strong>2.Descrição do Serviço</strong>

                    <br><br>O acesso ao BAH pelo Usuário é fornecido pela Triple A que cadastra uma conta formada por e-mail e uma senha. Esta conta é fornecida pela Triple A ao Usuário que poderá fazer seu acesso ao BAH. No seu primeiro acesso o Usuário será obrigado a alterar sua senha, ler e estar de acordo com os Termos e Condições de Uso do BAH.

                    <br><br>Qualquer novo recurso que aumente ou aprimore as funcionalidades atualmente disponibilizadas, ou aclusão de novas funcionalidades no BAH, estarão automaticamente sujeitas aos Termos e Condições de Uso do BAH. O Usuário está ciente e concorda que o BAH é fornecido na forma como está disponibilizado. Para utilizar o Serviço, o Usuário deve obter, por si, acesso à "World Wide Web", seja diretamente ou através de dispositivos que possam disponibilizar o conteúdo existente na Web, pagando os valores cobrados por seu provedor de acesso, se este for o caso, e providenciando todo o equipamento necessário para efetuar sua conexão à World Wide Web,cluindo computador, modem ou outro dispositivo de acesso.

                    <br><br>A Triple A fornecerá treinamento, suporte e atendimento técnico ao Usuário.

                    <br><br><strong>3.Conta do Usuário, senha e segurança</strong>

                    <br><br>A Triple A não se responsabiliza pelo acesso não autorizado aos dados do Usuário devido à divulgação dos dados da conta pelo Usuário, mesmo que a obtenção das senhas ocorra de formavoluntária, devido a ação de hackers, softwares de log de teclado, cavalo de tróia, domínio do equipamento utilizado pelo Usuário para acesso ao BAH ou qualquer outra técnica empregada para obter o nome do usuário e senha para acesso.

                    <br><br><strong>4.Conduta do Usuário</strong>

                    <br><br>O Usuário responderá exclusivamente pelo conteúdo armazenado no BAH,clusive no tocante à licitude dos mesmos edenizará, de forma plena, regressivamente, a Triple A em caso de condenação judicial ou administrativa desta em função do conteúdo armazenado.

                    <br><br>É VEDADO ao Usuário utilizar o BAH para:

                    <br><br>·  carregar, transmitir, divulgar, exibir, enviar, ou de qualquer outra forma tornar disponível qualquer Conteúdo que seja ilegal,cluindo, mas não se limitando, que seja ofensivo à honra, quevada à privacidade de terceiros, que seja ameaçador, vulgar, obsceno, preconceituoso, racista ou de qualquer forma censurável;

                    <br><br>·  violar direitos de crianças e/ou adolescentes;

                    <br><br>·  assumir a "personalidade" de outra pessoa, física ou jurídica;

                    <br><br>·  forjar cabeçalhos, ou de qualquer outra forma manipular identificadores, a fim de disfarçar a origem de qualquer Conteúdo transmitido através do BAH;

                    <br><br>·  carregar, transmitir, divulgar, exibir, enviar, ou de qualquer outra forma tornar disponível qualquer Conteúdo sem que tenha o direito de fazê-lo de acordo com a lei , por força de contrato ou de relação de confiança (por exemplo, no caso deformaçõesternas, exclusivas ou confidenciais recebidas ou divulgadas com conseqüência de relação de emprego ou contrato de confidencialidade);

                    <br><br>·  carregar, transmitir, divulgar, exibir, enviar, ou de qualquer forma tornar disponível qualquer Conteúdo que viole qualquer patente, marca, segredo de negócio, direito autoral, direitos de propriedadetelectual, ou qualquer outro direito de terceiro;

                    <br><br>·  carregar, transmitir, divulgar, exibir, enviar, ou de qualquer forma tornar disponível qualquer Conteúdo que contenha vírus ou qualquer outro código, arquivo ou programa de computador com o propósito deterromper, destruir ou limitar a funcionalidade de qualquer software, hardware ou equipamento de telecomunicações;

                    <br><br>·  violar, sejatencionalmente ou não, qualquer norma legal municipal, estadual, nacional outernacional que sejategrada ao ordenamento jurídico brasileiro, ou ainda, que, por qualquer razão legal, deva ser no Brasil aplicada.
                    <br><br>A Triple A tratará com confidencialidade asformações e os dados contidos no BAH, guardando total sigilo perante terceiros, ressalvados os casos de ordem e/ou pedido e/ou determinação judicial de qualquer espécie ou de autoridades públicas a fim de esclarecer fatos e/ou circunstâncias e/oustruirvestigação,quérito e/ou denúncia em curso.


                    <br><br><strong>5.Indenização</strong>

                    <br><br>Caso o Usuáriofrinja qualquer das cláusulas do presente Termo, oufrinja quaisquer dos dispositivos referentes a proteção dos direitos autorais e de propriedade da Triple A, responderá por perdas e danos a serem apurados em ação própria.


                    <br><br><strong>6.Proibição de Revenda do Serviço</strong>

                    <br><br>O Usuário não poderá sub-licenciar, comercializar, ou locar o BAH.


                    <br><br><strong>7.Práticas Gerais sobre Uso e Armazenamento</strong>

                    <br><br>O Usuário estará limitado ao espaço de armazenamento e limites de trafego de dados disponibilizados pela Triple A a seu Cliente.

                    <br><br>A utilização do BAH pelo Usuário poderá ser cancelada a qualquer momento, sem aviso prévio, pela Triple.

                    <br><br>A utilização do BAH pelo Usuário também está condicionada à vigência do contrato firmado entre a Triple A e seu Cliente.

                    <br><br>Em qualquer das hipóteses de cancelamento da conta do Usuário, todos os dados poderão ser excluídos sem aviso prévio.

                    <br><br><strong>8.Modificações no Serviço</strong>

                    <br><br>A Triple A Assessoria Contábil Ltda reserva-se o direito de, a qualquer tempo, modificar ou descontinuar, temporariamente ou permanentemente, o BAH ou parte dele, com ou sem notificação. O Usuário concorda que a Triple A não será responsabilizada, nem terá qualquer obrigação adicional, implícita ou explícita, para com o Usuário ou terceiros em razão de qualquer modificação, suspensão ou descontinuação do BAH.

                    <br><br><strong>9.Limitação de Responsabilidade</strong>

                    <br><br>A Triple A não se responsabiliza por quaisquer perdas, danos e conseqüências do uso ou do usodevido dos produtos por si fornecidos, e isenta-se expressamente de quaisquer responsabilidades edenizações, lucros cessantes, prejuízos de quaisquer espécies, ou sob quaisquer espécies ou sobre quaisquer títulos, perdas de negócios, perda ou extravio de dados, defeitos de computador, equipamentos ou periféricos, ou quaisquer outros danos diretos oudiretos, decorrentes da prestação dos serviços, causados ao Usuário.

                    <br><br><strong>10.Avisos</strong>

                    <br><br>Qualquer aviso ao Usuário poderá ser feito através de e-mail ou por correio convencional, sem limitação de outras formas elegíveis pela Triple A, a seu critério. A Triple A poderá, também, fornecer avisos sobre modificações nos Termos e Condições de Uso do BAH ou qualquer outraformação por meio da exibição de notícias na área de notícias do BAH.

                    <br>
                </div>
                <br><br><a class="btn a-acao a-acao-termodeuso pull-right" data-objeto="<?=LOCAL."usuario"?>" data-acao="aceitar_termodeuso">Li as instruções que devo seguir para utilizar o BAH e aceito as condições.</a>
                <br>
            </div>
        </div>
        <?php
    }


    if (isset($dados_notificacao)) {
        extract ($dados_notificacao,EXTR_PREFIX_ALL, 'var');
        ?>
        <div class="col-sm-12 div-form">
            <div class="panel panel-custom">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-sm-1">
                            <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
                        </div>
                        <div class="col-sm-11">
                            <h1><?=$var_titulo ?></h1>
                            <blockquote><?=$var_texto ?></blockquote>
                            <a class="btn a-acao-painellida" data-objeto="<?=LOCAL ?>painel" data-acao="lida" data-cod="<?=$var_cod_notificacao ?>">Lida</a>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <a class="a-acao pull-right" data-objeto="<?=LOCAL."notificacao"?>" data-acao="cliente"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>&nbsp;Ver todas</a>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <?php
    }
?>



<div class="col-sm-12 div-form">
    <div class="row">     
            
        <?php 
            //Vencimento do certificado
            if (isset($dados_situacaocertificado)) {
                extract ($dados_situacaocertificado,EXTR_PREFIX_ALL, 'var');
                ?>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <br><br><h1>Certificado</h1>
                        </div>
                        <div class="col-sm-12">
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <ul class="list-group"><li class="list-group-item list-group-item-<?=$var_classe?>"></li></ul> &nbsp;&nbsp;&nbsp;Seu certificado <?=$var_certificadotipo?> com vencimento em '<?=$var_certificado_vencimento_?>' esta <strong><?=$var_situacao?></strong>
                    <br><br>
                </div>
                <?php
            }
        ?>

        <div class="col-sm-12">
            <div class="row">
                <div class="panel-group" id="accordion">

                    <?php
                        $warning=0;
                        $success=0;
                        $danger=0;
                        foreach($dados_servicos as $value){
                            if ($value["classe"]=="warning") $warning++;
                            if ($value["classe"]=="success") $success++;
                            if ($value["classe"]=="danger") $danger++;
                        }
                    ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse0"><span class="glyphicon glyphicon-chevron-right"></span>&nbsp;Serviços</a>
                            <ul class="list-group pull-right"><li class="list-group-item list-group-item-small list-group-item-danger"><?=$danger?></li></ul>
                            <ul class="list-group pull-right"><li class="list-group-item list-group-item-small list-group-item-warning"><?=$warning?></li></ul>
                            </h4>
                        </div>
                        <div id="collapse0" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="tablebah" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="*">Tarefa</th>
                                            <th width="*">Dt&nbsp;Início</th>
                                            <th width="*" class="tdoculta">Dt&nbsp;Previsão</th>
                                            <th width="*" class="tdoculta">Dt&nbsp;Conclusão</th>
                                            <th width="*" class="tdoculta">Situação</th>
                                            <th width="3%">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach($dados_servicos as $value){
                                                ?>
                                                <tr>
                                                    <td><?=$value["tarefa"]?></td>
                                                    <td><?=$value["data_inicio_"]?></td>
                                                    <td class="tdoculta"><?=$value["data_previsao_"]?></td>
                                                    <td class="tdoculta"><?=$value["data_conclusao_"]?></td>
                                                    <td class="tdoculta">
                                                        <ul class="list-group"><li class="list-group-item list-group-item-<?=$value["classe"]?>"><?=$value["situacao"]?></li></ul>
                                                    </td>
                                                    <?php if ($this->session->userdata('tipo')=="T"){ ?>
                                                        <td><a class="btn btn-editar a-acao" data-objeto="<?=LOCAL ?>servico" data-acao="editar" data-cod="<?=$value["cod_servico"] ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                                    <?php } ?>
                                                    <?php if ($this->session->userdata('tipo')=="C"){ ?>
                                                        <td><a class="btn btn-editar a-acao" data-objeto="<?=LOCAL ?>servico" data-acao="ver" data-cod="<?=$value["cod_servico"] ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                                    <?php } ?>
                                                </tr>
                                                <?php
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <br><br>
                            <div class="row">
                                <div class="col-sm-8">
                                    <input type="hidden" id="calendario_mes" name="calendario_mes" value="<?=$calendario_mes?>">
                                    <input type="hidden" id="calendario_ano" name="calendario_ano" value="<?=$calendario_ano?>">

                                    <h1>Calendário de Obrigações
                                    <a href="#" class="calendario_navegacao calendario_navegacao_avancar">
                                        &nbsp;mês posterior&nbsp;
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                                    <div class="calendario_navegacao calendario_navegacao_data">
                                        <?=@$calendario_mes?>/<?=@$calendario_ano?>
                                    </div>
                                    <a href="#" class="calendario_navegacao calendario_navegacao_voltar">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                        &nbsp;mês anterior&nbsp;
                                    </a>
                                    </h1>
                                </div>
                                <div class="col-sm-4">
                                    &nbsp;

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <hr>
                        </div>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-2">
                                    <ul class="list-group"><li class="list-group-item list-group-item-coleta"></li></ul> &nbsp;&nbsp;&nbsp;Coleta&nbsp;&nbsp;&nbsp;
                                </div>
                                <div class="col-sm-2">
                                    <ul class="list-group"><li class="list-group-item list-group-item-servico"></li></ul> &nbsp;&nbsp;&nbsp;Serviço&nbsp;&nbsp;&nbsp;
                                </div>
                                <div class="col-sm-2">
                                    <ul class="list-group"><li class="list-group-item list-group-item-upload"></li></ul> &nbsp;&nbsp;&nbsp;Obrigação&nbsp;&nbsp;&nbsp;
                                </div>
                            </div>
                            <br>
                        </div>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="row">

                                        <div class="col-sm-12">
                                            <div class="row calendario_titulo">
                                                <div class="calendario_titulo_diasemana">D</div>
                                                <div class="calendario_titulo_diasemana">S</div>
                                                <div class="calendario_titulo_diasemana">T</div>
                                                <div class="calendario_titulo_diasemana">Q</div>
                                                <div class="calendario_titulo_diasemana">Q</div>
                                                <div class="calendario_titulo_diasemana">S</div>
                                                <div class="calendario_titulo_diasemana">S</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row calendario_corpo">
                                                <?php
                                            
                                                //Calendário da coleta digital

                                                $ano = ($this->input->get_post('calendario_ano'))?$this->input->get_post('calendario_ano'):date('Y');
                                                $mes = ($this->input->get_post('calendario_mes'))?$this->input->get_post('calendario_mes'):date('m');

                                                $mes_anterior="";
                                                $ano_anterior="";
                                                $mes_posterior="";
                                                $ano_posterior="";

                                                $dia = date('d');

                                                //Trata mês para navegação
                                                if ($mes==1) {
                                                    $mes_anterior = 12;
                                                    $ano_anterior = $ano-1;
                                                } else {
                                                    $mes_anterior = $mes-1;
                                                    $ano_anterior = $ano;
                                                }
                                                if ($mes==12) {
                                                    $mes_posterior = 1;
                                                    $ano_posterior = $ano +1;
                                                } else {
                                                    $mes_posterior = $mes+1;
                                                    $ano_posterior = $ano;
                                                }

                                                $semana_extenso = array(
                                                    'Sun' => 'Domingo', 
                                                    'Mon' => 'Segunda-Feira',
                                                    'Tue' => 'Terca-Feira',
                                                    'Wed' => 'Quarta-Feira',
                                                    'Thu' => 'Quinta-Feira',
                                                    'Fri' => 'Sexta-Feira',
                                                    'Sat' => 'Sábado'
                                                );

                                                $mes_extenso = array(
                                                    '1' => 'Janeiro',
                                                    '2' => 'Fevereiro',
                                                    '3' => 'Marco',
                                                    '4' => 'Abril',
                                                    '5' => 'Maio',
                                                    '6' => 'Junho',
                                                    '7' => 'Julho',
                                                    '8' => 'Agosto',
                                                    '9' => 'Setembro',
                                                    '10' => 'Outubro',
                                                    '11' => 'Novembro',
                                                    '12' => 'Dezembro'
                                                ); 

                                                $html="";
                                                for ($i=1;$i<=32;$i++){
                                                    $diadasemana = date("w",mktime(0,0,0,$mes,$i,$ano));
                                                    $data=date_create("{$ano}-{$mes}-{$i}");
                                                    
                                                    if($i == 1) {
                                                        for ($b=0;$b<$diadasemana;$b++){
                                                            $html.="<div class=\"\">&nbsp;</div>";
                                                        
                                                        }
                                                    }
                                                    
                                                    $class_dia_atual = "";

                                                    if (($ano == date('Y'))and($mes == date('m'))and($i == date('d'))) $class_dia_atual = "calendario-dia-atual";
                                                    
                                                    if (checkdate($mes,$i,$ano)) {
                                                        $html.="<div class=\"{$class_dia_atual}\">";
                                                        $html.="<p class=\"calendario_corpo_span\">".$i."</p>";
                                                        
                                                        $calendario_coleta = $dados_calendariocoleta;
                                                        foreach($calendario_coleta as $value){
                                                            if ($value["data_vencimento"]==date_format($data,"Y-m-d")){
                                                                $html.="<p class=\"calendario_corpo_texto calendario_corpo_texto_coleta\">";
                                                                $html.="<a href=\"".LOCAL."coleta/ver/".$value["cod_coleta"]."\">&nbsp;";
                                                                $aux= $value["texto"]<>""?$value["texto"]:$value["arquivo"];
                                                                $html.=$aux;
                                                                $html.="</a>";
                                                                $html.="</p>";

                                                                $lista[] = array("tipo"=>"Coleta","data"=>date("d/m/Y", strtotime($value["data_vencimento"])),"classe"=>"calendario_lista_item_titulo_coleta","link"=>LOCAL."coleta/ver/".$value["cod_coleta"],"target"=>"","texto"=>$aux);
                                                            }
                                                        }
                                                        
                                                        $calendario_servico = $dados_calendarioservico;
                                                        foreach($calendario_servico as $value){
                                                            if ($value["data_previsao"]==date_format($data,"Y-m-d")){
                                                                $html.="<p class=\"calendario_corpo_texto calendario_corpo_texto_servico\">";
                                                                $html.="<a href=\"".LOCAL."servico/ver/".$value["cod_servico"]."\">&nbsp;";
                                                                $html.=$value["nome"];
                                                                $html.="</a>";
                                                                $html.="</p>";

                                                                $lista[] = array("tipo"=>"Serviço","data"=>date("d/m/Y", strtotime($value["data_previsao"])),"classe"=>"calendario_lista_item_titulo_servico","link"=>LOCAL."servico/ver/".$value["cod_servico"],"target"=>"","texto"=>$value["nome"]);
                                                            }
                                                        }
                                                        
                                                        $calendario_upload = $dados_calendarioupload;
                                                        foreach($calendario_upload as $value){
                                                            if ($value["data_vencimento"]==date_format($data,"Y-m-d")){
                                                                $html.="<p class=\"calendario_corpo_texto calendario_corpo_texto_upload\">";
                                                                $html.="<a href=\"".$value["caminho"]."\" target=\"_blank\">&nbsp;";
                                                                $html.=$value["nome"];
                                                                $html.="</a>";
                                                                $html.="</p>";

                                                                $lista[] = array("tipo"=>"Upload","data"=>date("d/m/Y", strtotime($value["data_vencimento"])),"classe"=>"calendario_lista_item_titulo_upload","link"=>$value["caminho"],"target"=>"_blank","texto"=>$value["nome"]);
                                                            }
                                                        }
                                                        
                                                        $html.="</div>";
                                                    }
                                                }
                                                echo $html;
                                                ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="row calendario_lista">
                                        <div class="col-sm-12">
                                            <br><br><h1>Lista de Obrigações do Mês</h1>
                                        </div>
                                        <div class="col-sm-12">
                                            <hr>
                                        </div>
                                        <div class="col-sm-12">
                                            <?php
                                                if (isset($lista)){
                                                    //var_dump($lista);
                                                    foreach($lista as $value){
                                                        ?>
                                                        <a href="<?=$value["link"]?>" target="<?=$value["target"]?>">
                                                        <div class="calendario_lista_item">
                                                            <div class="calendario_lista_item_titulo <?=$value["classe"]?>">
                                                                &nbsp;
                                                                <?php
                                                                    echo (substr($value["tipo"],0,1));
                                                                ?>
                                                                &nbsp;
                                                            </div>
                                                            <div class="calendario_lista_item_texto">
                                                                <p>
                                                                <span><?=$value["tipo"]?> <?=$value["data"]?> </span>
                                                                <br><?=$value["texto"]?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        </a>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br><br>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
