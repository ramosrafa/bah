<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
?>

<div class="col-sm-12 div-operacao">
    <div class="row">
        <?php
            if ($operacao=="listar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>log" data-acao="listar">Buscar</a>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
                <?php
            }
            if ($operacao=="ver"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>log" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
        ?>
    </div>
</div>

<?php
    if ($operacao=="listar"){
        ?>
        <div class="col-sm-12">
            <div class="div-table">
                <div class="div-table-fixed">
                    <table class="tablebah">
                        <thead>
                            <tr>
                                <?php
                                    echo $this->functions->table_column("1","objeto","Objeto","*","a-acao");
                                    echo $this->functions->table_column("","usuario","Usuário","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","data","Dt Ação","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","acao","Ação","*","a-acao");
                                ?>
                                <th width="3%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($dados as $value){
                                    ?>
                                    <tr class="tr-linha" data-objeto="<?=LOCAL ?>log" data-acao="ver" data-cod="<?=$value["cod_log"] ?>">
                                        <td><?=$value["objeto"]?></td>
                                        <td class="tdoculta"><?=$value["usuario"]?></td>
                                        <td class="tdoculta"><?=$value["data_c_"]?></td>
                                        <td><?=$value["acao"]?></td>
                                        <td><a class="btn btn-ver a-acao" data-objeto="<?=LOCAL ?>log" data-acao="ver" data-cod="<?=$value["cod_log"] ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pull-right col-paginacao">
                <?=$paginacao; ?>
            </div>
        </div>
        <?php
    }
    if (($operacao=="ver")){
        ?>
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <h2>Log <?=@$var_cod_log;?></h2>
                        <p>
                        Objeto: <?=@$var_objeto;?> 
                        <br>Usuário: <?=@$var_usuario;?>.
                        <br>Data: <?=@$var_data_c_;?>
                        <br>Ação: <?=@$var_acao;?>
                        <br>Log: <?=var_dump(unserialize(@$var_log));?>
                    </div>            
                </div>
            </div> 
        </div> 
        <?php
    }
?>


