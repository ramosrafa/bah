<?php 
    if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
?>

<div class="col-sm-12">
    <div class="row responsive-hide">     
        <?php
            $warning=0;
            $success=0;
            $danger=0;
            foreach($dados_servicos as $value){
                if ($value["classe"]=="warning") $warning++;
                if ($value["classe"]=="success") $success++;
                if ($value["classe"]=="danger") $danger++;
            }
            foreach($dados_alvaras as $value){
                if ($value["vencimento_classe"]=="warning" or $value["pagamento_classe"]=="warning") $warning++;
                if ($value["vencimento_classe"]=="success" or $value["pagamento_classe"]=="success") $success++;
                if ($value["vencimento_classe"]=="danger" or $value["pagamento_classe"]=="danger") $danger++;
            }
            foreach($dados_certificados as $value){
                if ($value["classe"]=="warning") $warning++;
                if ($value["classe"]=="success") $success++;
                if ($value["classe"]=="danger") $danger++;
            }
            //Computa vencimento
            foreach($dados_procuracoes as $value){
                if ($value["classe"]=="warning") $warning++;
                if ($value["classe"]=="success") $success++;
                if ($value["classe"]=="danger") $danger++;
            }
            //Computa se não possui
            foreach($dados_procuracoes as $value){
                if ($value["procuracao"]<>"S") $danger++;
                if ($value["procuracao"]=="S") $success++;
            }
            foreach($dados_clientessemusuario as $value){
                $danger++;
            }
            $total = $danger+$warning;
            //$total = ($total>0?$total:1);
            //$danger = ($danger>0?$danger:1);
            //$warning = ($warning>0?$warning:1);
        ?>
        <div class="col-sm-4">
            <div class="div-painel div-painel-warning">
                <div class="row">     
                    <div class="col-sm-3 div-painel-icone">
                        <center><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></center>
                    </div>
                    <div class="col-sm-4 div-painel-texto">
                        Ops, vamos observar
                        <br><span><?php try{echo (intval($warning*100));}catch (ErrorException $e) {echo "0";}?>% Em atenção</span>
                    </div>
                    <div class="col-sm-4 div-painel-contador">
                        <?=$warning?>
                    </div>
                </div>
            </div>     
        </div>
        <div class="col-sm-4">
            <div class="div-painel div-painel-danger">
                <div class="row">     
                    <div class="col-sm-3 div-painel-icone">
                        <center><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></center>
                    </div>
                    <div class="col-sm-5 div-painel-texto">
                        Isso não devia estar assim
                        <br><span><?php try{echo (intval($danger*100));}catch (ErrorException $e) {echo "0";}?>% Em desacordo</span>
                    </div>
                    <div class="col-sm-4 div-painel-contador">
                        <?=$danger?>
                    </div>
                </div>
            </div>     
        </div>
        <div class="col-sm-4">
            <div class="div-painel div-painel-chart">
                <div id="div-painel-chart-container">
                    <center><img src="<?=LOCAL?>web/loading.gif" width="50px"></center>
                </div>
            </div>
        </div>
    </div>     
    <div class="row">     
        <div class="col-sm-12">
            <div class="panel-group" id="accordion">

                <?php
                    $warning=0;
                    $success=0;
                    $danger=0;
                    foreach($dados_servicos as $value){
                        if ($value["classe"]=="warning") $warning++;
                        if ($value["classe"]=="success") $success++;
                        if ($value["classe"]=="danger") $danger++;
                    }
                ?>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse0"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Serviços</a>
                        <ul class="list-group pull-right"><li class="list-group-item list-group-item-small list-group-item-danger"><?=$danger?></li></ul>
                        <ul class="list-group pull-right"><li class="list-group-item list-group-item-small list-group-item-warning"><?=$warning?></li></ul>
                        </h4>
                    </div>
                    <div id="collapse0" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="tablebah" width="100%">
                                <thead>
                                    <tr>
                                        <th width="*">Tarefa</th>
                                        <th width="*">Cliente</th>
                                        <th width="*">Dt Início</th>
                                        <th width="*" class="tdoculta">Dt&nbsp;Previsão</th>
                                        <th width="*" class="tdoculta">Dt&nbsp;Conclusão</th>
                                        <th width="*" class="tdoculta">Situação</th>
                                        <th width="3%">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach($dados_servicos as $value){
                                            ?>
                                            <tr>
                                                <td><?=$value["tarefa"]?></td>
                                                <td><?=$value["cliente"]?></td>
                                                <td><?=$value["data_inicio_"]?></td>
                                                <td class="tdoculta"><?=$value["data_previsao_"]?></td>
                                                <td class="tdoculta"><?=$value["data_conclusao_"]?></td>
                                                <td class="tdoculta">
                                                    <ul class="list-group"><li class="list-group-item list-group-item-<?=$value["classe"]?>"><?=$value["situacao"]?></li></ul>
                                                </td>
                                                <td><a class="btn btn-editar-plus a-acao" data-objeto="<?=LOCAL ?>servico" data-acao="editar" data-cod="<?=$value["cod_servico"] ?>"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></a></td>
                                            </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            
                <?php
                    $warning=0;
                    $success=0;
                    $danger=0;
                    foreach($dados_alvaras as $value){
                        if ($value["vencimento_classe"]=="warning" or $value["pagamento_classe"]=="warning") $warning++;
                        if ($value["vencimento_classe"]=="success" or $value["pagamento_classe"]=="success") $success++;
                        if ($value["vencimento_classe"]=="danger" or $value["pagamento_classe"]=="danger") $danger++;
                    }
                ?>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Alvarás</a>
                        <ul class="list-group pull-right"><li class="list-group-item list-group-item-small list-group-item-danger"><?=$danger?></li></ul>
                        <ul class="list-group pull-right"><li class="list-group-item list-group-item-small list-group-item-warning"><?=$warning?></li></ul>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="tablebah" width="100%">
                                <thead>
                                    <tr>
                                        <th width="*">
                                            Cliente
                                        </th>
                                        <th width="*" class="tdoculta">
                                            Alvará
                                        </th>
                                        <th width="*" class="tdoculta">
                                            Número
                                        </th>
                                        <th width="*" class="tdoculta">
                                            Dt&nbsp;Vencimento
                                        </th>
                                        <th width="*" class="tdoculta">
                                            Dt&nbsp;Pagamento
                                        </th>
                                        <th width="3%">
                                            &nbsp;
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        
                                        foreach($dados_alvaras as $value_){ 
                                            ?>
                                            <tr>
                                                <td>
                                                    <?=$value_["cliente"]?>
                                                </td>
                                                <td class="tdoculta">
                                                    <?=$value_["alvara"]?>
                                                </td>
                                                <td class="tdoculta">
                                                    <?=$value_["numero"]?>
                                                </td>
                                                <td class="tdoculta">
                                                    <?=$value_["data_vencimento_"]?>
                                                </td>
                                                <td class="tdoculta">
                                                    <ul class="list-group"><li class="list-group-item list-group-item-<?=$value_["vencimento_classe"]?>"><?=$value_["vencimento_situacao"]?></li></ul>
                                                </td>
                                                <td class="tdoculta">
                                                    <?=$value_["data_pagamento_"]?>
                                                </td>
                                                <td class="tdoculta">
                                                    <ul class="list-group"><li class="list-group-item list-group-item-<?=$value_["pagamento_classe"]?>"><?=$value_["pagamento_situacao"]?></li></ul>
                                                </td>
                                                <td>
                                                    <a href="<?=LOCAL?>cliente/editar/<?=$value_["cod_cliente"]?>" class="btn btn-editar-plus"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            
                <?php
                    $warning=0;
                    $success=0;
                    $danger=0;
                    foreach($dados_certificados as $value){
                        if ($value["classe"]=="warning") $warning++;
                        if ($value["classe"]=="success") $success++;
                        if ($value["classe"]=="danger") $danger++;
                    }
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Certificados</a>
                        <ul class="list-group pull-right"><li class="list-group-item list-group-item-small list-group-item-danger"><?=$danger?></li></ul>
                        <ul class="list-group pull-right"><li class="list-group-item list-group-item-small list-group-item-warning"><?=$warning?></li></ul>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="tablebah" width="100%">
                                <thead>
                                    <tr>
                                        <th width="*">
                                            Cliente
                                        </th>
                                        <th width="*" class="tdoculta">
                                            Vencimento
                                        </th>
                                        <th width="*" class="tdoculta">
                                            Arquivo
                                        </th>
                                        <th width="*" class="tdoculta">
                                            Situação
                                        </th>
                                        <th width="3%">
                                            &nbsp;
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php

                                        foreach($dados_certificados as $value_){ 
                                            $cod_cliente = $value_["cod_cliente"];
                                            $nome = $value_["nome"];
                                            $certificado_vencimento = $value_["certificado_vencimento_"]; 
                                            $situacao = $value_["situacao"];
                                            $classe = $value_["classe"];
                                            $arquivo = $value_["arquivo"];
                                            ?>
                                            <tr>
                                                <td>
                                                    <?=$nome?>
                                                </td>
                                                <td class="tdoculta">
                                                    <?=$certificado_vencimento?>
                                                </td>
                                                <td class="tdoculta">
                                                    <?php
                                                        if (file_exists($arquivo)){
                                                            $arquivo =str_replace("./",LOCAL,$arquivo);
                                                            ?>
                                                            <a href="<?=$arquivo?>" class="btn btn-sub" target="_blank">Abrir certificado</a>
                                                            <?php
                                                        } else {
                                                            echo "Sem arquivo";
                                                        }
                                                    ?>

                                                </td>
                                                <td class="tdoculta">
                                                    <ul class="list-group"><li class="list-group-item list-group-item-<?=$classe?>"><?=$situacao?></li></ul>
                                                </td>
                                                <td>
                                                    <a href="<?=LOCAL?>cliente/editar/<?=$cod_cliente?>" class="btn btn-editar-plus"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <?php
                    $warning=0;
                    $success=0;
                    $danger=0;
                    //Computa vencimento
                    foreach($dados_procuracoes as $value){
                        if ($value["classe"]=="warning") $warning++;
                        if ($value["classe"]=="success") $success++;
                        if ($value["classe"]=="danger") $danger++;
                    }
                    //Computa se não possui
                    foreach($dados_procuracoes as $value){
                        if ($value["procuracao"]<>"S") $danger++;
                        if ($value["procuracao"]=="S") $success++;
                    }

                ?>
            
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Procurações</a>
                        <ul class="list-group pull-right"><li class="list-group-item list-group-item-small list-group-item-danger"><?=$danger?></li></ul>
                        <ul class="list-group pull-right"><li class="list-group-item list-group-item-small list-group-item-warning"><?=$warning?></li></ul>
                        </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="tablebah" width="100%">
                                <thead>
                                    <tr>
                                        <th width="*">
                                            Cliente
                                        </th>
                                        <th width="*" class="tdoculta">
                                            Possui
                                        </th>
                                        <th width="*" class="tdoculta">
                                            Vencimento
                                        </th>
                                        <th width="*" class="tdoculta">
                                            Situação
                                        </th>
                                        <th width="3%">
                                            &nbsp;
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php

                                        foreach($dados_procuracoes as $value_){ 
                                            $cod_cliente = $value_["cod_cliente"];
                                            $nome = $value_["nome"];
                                            $procuracao = $value_["procuracao"];
                                            $procuracao_classe = $value_["procuracao_classe"];
                                            $procuracao_vencimento = $value_["procuracao_vencimento_"]; 
                                            $situacao = $value_["situacao"];
                                            $classe = $value_["classe"];
                                            ?>
                                            <tr>
                                                <td>
                                                    <?=$nome?>
                                                </td>
                                                <td class="tdoculta">
                                                    <ul class="list-group"><li class="list-group-item list-group-item-<?=$procuracao_classe?>"><?=$procuracao?></li></ul>
                                                </td>
                                                <td class="tdoculta">
                                                    <?=$procuracao_vencimento?>
                                                </td>
                                                <td class="tdoculta">
                                                    <ul class="list-group"><li class="list-group-item list-group-item-<?=$classe?>"><?=$situacao?></li></ul>
                                                </td>
                                                <td>
                                                    <a href="<?=LOCAL?>cliente/editar/<?=$cod_cliente?>" class="btn btn-editar-plus"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <?php
                    $danger=0;
                    foreach($dados_clientessemusuario as $value){
                        if ($value["tipo"]=="0") $danger++;
                    }
                ?>
            
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Clientes sem usuário</a>
                            <ul class="list-group pull-right"><li class="list-group-item list-group-item-small list-group-item-small list-group-item-danger"><?=$danger?></li></ul>
                        </h4>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse">
                        <div class="div-table">
                            <table class="tablebah">
                                <thead>
                                    <tr>
                                        <th width="*">
                                            Cliente
                                        </th>
                                        <th width="3%">
                                            &nbsp;
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach($dados_clientessemusuario as $value_){ 
                                            $tipo = $value_["tipo"];
                                            $cod_cliente = $value_["cod_cliente"];
                                            $nome = $value_["nome"];
                                            if ($tipo=="0"){
                                                ?>
                                                <tr class="tr-linha" data-href="<?=LOCAL?>cliente/editar/<?=$cod_cliente?>">
                                                    <td>
                                                        <?=$nome?>
                                                    </td>
                                                    <td>
                                                        <a href="<?=LOCAL?>cliente/editar/<?=$cod_cliente?>" class="btn btn-editar-plus"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <?php
                    $danger=0;
                    foreach($dados_clientessemusuario as $value){
                        if ($value["tipo"]=="1") $danger++;
                    }
                ?>
            
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Clientes sem usuário de notificação</a>
                            <ul class="list-group pull-right"><li class="list-group-item list-group-item-small list-group-item-small list-group-item-danger"><?=$danger?></li></ul>
                        </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse">
                        <div class="div-table">
                            <table class="tablebah">
                                <thead>
                                    <tr>
                                        <th width="*">
                                            Cliente
                                        </th>
                                        <th width="3%">
                                            &nbsp;
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach($dados_clientessemusuario as $value_){ 
                                            $tipo = $value_["tipo"];
                                            $cod_cliente = $value_["cod_cliente"];
                                            $nome = $value_["nome"];
                                            if ($tipo=="1"){
                                                ?>
                                                <tr class="tr-linha" data-href="<?=LOCAL?>cliente/editar/<?=$cod_cliente?>">
                                                    <td>
                                                        <?=$nome?>
                                                    </td>
                                                    <td>
                                                        <a href="<?=LOCAL?>cliente/editar/<?=$cod_cliente?>" class="btn btn-editar-plus"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
