<?php 
    if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 

    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');

    $var_cod_cliente = $this->input->get_post('cod_cliente');

?>

<div class="col-sm-12 div-form">
    <div class="row">
        <div class="col-sm-12">
            <label class="control-label" for="progresso" id="cliente_progressbar_label">Qualidade da Estratégia</label>  
            <div class="progress">
                <div class="progress-bar" id="cliente_progressbar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;1 Sobre a empresa</a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse">
                    <div class="panel-body">
                        <input type="text" class="clienteestrategia" id="id_estrategia_sobreempresa" value="<?=@$var_estrategia_sobreempresa?>">
                        <hr>

                        <p><strong>Site e redes sociais</strong></p> 
                        <input type="text" class="clienteestrategia" id="id_estrategia_site" value="<?=@$var_estrategia_site?>">
                        <hr>

                        <p><strong>Regime tributário</strong></p> 
                        <input type="text" class="clienteestrategia" id="id_estrategia_regimetributario" value="<?=@$var_estrategia_regimetributario?>">
                        <hr>

                        <p><strong>Regime tributário Vencimento</strong></p> 
                        <input type="text" class="clienteestrategia" id="id_estrategia_regimetributariodata" value="<?=@$var_estrategia_regimetributariodata?>">
                        <hr>

                        <p><strong>Número Matriz e Filial</strong></p> 
                        <input type="text" class="clienteestrategia" id="id_estrategia_numeromatriz" value="<?=@$var_estrategia_numeromatriz?>">
                        <hr>

                        <p><strong>Pertence ao Grupo Empresarial? Se sim, qual?</strong></p> 
                        <input type="text" class="clienteestrategia" id="id_estrategia_grupoempresarial" value="<?=@$var_estrategia_grupoempresarial?>">
                        <hr>

                        <p><strong>Número profissionais</strong></p> 
                        <input type="text" class="clienteestrategia" id="id_estrategia_numeroprofissionais" value="<?=@$var_estrategia_numeroprofissionais?>">
                        <hr>

                        <p><strong>Quantidade sócios</strong></p> 
                        <input type="text" class="clienteestrategia" id="id_estrategia_quantidadesocios" value="<?=@$var_estrategia_quantidadesocios?>">
                        <hr>

                        <p><strong>Sócios Participam em Outra Empresa? Se sim, qual sócio e qual empresa?</strong></p> 
                        <input type="text" class="clienteestrategia" id="id_estrategia_sociosoutraempresa" value="<?=@$var_estrategia_sociosoutraempresa?>">
                        <hr>

                        <p><strong>Atividades (CNAES)</strong></p> 
                        <input type="text" class="clienteestrategia" id="id_estrategia_atividades" value="<?=@$var_estrategia_atividades?>">
                        <hr>

                        <p><strong>Faturamento Últimos 12 Meses</strong></p> 
                        <input type="text" class="clienteestrategia" id="id_estrategia_faturamento" value="<?=@$var_estrategia_faturamento?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;2 Sobre o projeto</a>
                    </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <input type="text" class="clienteestrategia" id="id_estrategia_sobreprojeto" value="<?=@$var_estrategia_sobreprojeto?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;3 Produtos e serviços</a>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse">
                    <div class="panel-body">
                        <input type="text" class="clienteestrategia" id="id_estrategia_produtoservico" value="<?=@$var_estrategia_produtoservico?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;4 Pessoas de contato</a>
                    </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse">
                    <div class="panel-body">
                        <input type="text" class="clienteestrategia" id="id_estrategia_pessoacontato" value="<?=@$var_estrategia_pessoacontato?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;5 Objetivos Principais da estratégia</a>
                    </h4>
                </div>
                <div id="collapse5" class="panel-collapse collapse">
                    <div class="panel-body">
                        <?php
                            $var_estrategia_objetivoestrategia = explode("#",@$var_estrategia_objetivoestrategia);
                            $aux="";
                            if (in_array("1",$var_estrategia_objetivoestrategia)) $aux =  "Qualificação de Gestão";
                            if (in_array("2",$var_estrategia_objetivoestrategia)) $aux =  "Planejamento Societário";
                            if (in_array("3",$var_estrategia_objetivoestrategia)) $aux =  "Planejamento Tributário";
                            if (in_array("4",$var_estrategia_objetivoestrategia)) $aux =  "Revisão Tributária";
                            if (in_array("5",$var_estrategia_objetivoestrategia)) $aux =  "Revisão de Relações Trabalhistas";
                            if (in_array("6",$var_estrategia_objetivoestrategia)) $aux =  "Recuperação de Créditos Tributários";
                            if (in_array("7",$var_estrategia_objetivoestrategia)) $aux =  "Informações para Tomada de Decisão";
                        ?>
                        <input type="text" class="clienteestrategia" id="id_estrategia_objetivoestrategia" value="<?=$aux?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse6"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;Estágio de Qualificação de Gestão</a>
                    </h4>
                </div>
                <div id="collapse6" class="panel-collapse collapse">
                    <div class="panel-body">
                        <?php
                            if ($var_estrategia_estagioqualificacao=="1") $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Básica";
                            if ($var_estrategia_estagioqualificacao=="2") $aux =  "Intermediária";
                            if ($var_estrategia_estagioqualificacao=="3") $aux =  "Qualificada";
                            if ($var_estrategia_estagioqualificacao=="4") $aux =  "Avançada";
                        ?>
                        <input type="text" class="clienteestrategia" id="id_estagioqualificacao" value="<?=@$aux?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse7"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;7 KPIs de acompanhamento Contabilidade</a>
                    </h4>
                </div>
                <div id="collapse7" class="panel-collapse collapse">
                    <div class="panel-body">
                        <?php
                            $var_estrategia_kpicontabilidade = explode("#",@$var_estrategia_kpicontabilidade);
                            $aux="";
                            if (in_array("1",$var_estrategia_kpicontabilidade)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Demonstrações Mês + 5° dia útil";
                            if (in_array("2",$var_estrategia_kpicontabilidade)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Demonstrações Mês + 10° dia útil";
                            if (in_array("3",$var_estrategia_kpicontabilidade)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Demonstrações Mês + 20° dia útil";
                            if (in_array("4",$var_estrategia_kpicontabilidade)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Demonstrações Mês + 50 dias";
                            if (in_array("5",$var_estrategia_kpicontabilidade)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Demonstrações Bimestre + 20 dias";
                            if (in_array("6",$var_estrategia_kpicontabilidade)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Demonstrações Trimestre + 20 dias";
                            if (in_array("7",$var_estrategia_kpicontabilidade)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Demonstrações Semestre + 20 dias";
                            if (in_array("8",$var_estrategia_kpicontabilidade)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Ano + 60 dias";
                            if (in_array("9",$var_estrategia_kpicontabilidade)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;DRE Combinado (Horizontal)";
                            if (in_array("10",$var_estrategia_kpicontabilidade)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Ativo e Passivo Combinado (Horizonta)";
                            if (in_array("11",$var_estrategia_kpicontabilidade)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Fluxo de Caixa";
                            if (in_array("12",$var_estrategia_kpicontabilidade)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Margem de Contribuição";
                            if (in_array("13",$var_estrategia_kpicontabilidade)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;DashBoard Estratégico";
                            if (in_array("14",$var_estrategia_kpicontabilidade)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Demonstrações em Lingua Estrangeira ";
    
                        ?>
                        <input type="text" class="clienteestrategia" id="id_estrategia_kpicontabilidade" value="<?=@$aux?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse8"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;8 KPIs de acompanhamento Tributários</a>
                    </h4>
                </div>
                <div id="collapse8" class="panel-collapse collapse">
                    <div class="panel-body">
                        <?php
                            $var_estrategia_kpitributario = explode("#",@$var_estrategia_kpitributario);
                            $aux="";
                            if(in_array("1",$var_estrategia_kpitributario)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Guarda de Notas Fiscais Eletronicas";
                            if(in_array("2",$var_estrategia_kpitributario)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Gestão Estilo Triple A (GET) ";
                            if(in_array("3",$var_estrategia_kpitributario)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Evolução Faturamento";
                            if(in_array("4",$var_estrategia_kpitributario)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Recuperação de Créditos Administrativos";
                            if(in_array("5",$var_estrategia_kpitributario)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Revisão de Cadastros Fiscais ";     
                        ?>
                        <input type="text" class="clienteestrategia" id="id_estrategia_kpitributario" value="<?=@$aux?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse9"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;9 KPIs de acompanhamento Relações Trabalhistas</a>
                    </h4>
                </div>
                <div id="collapse9" class="panel-collapse collapse">
                    <div class="panel-body">
                        <?php
                            $var_estrategia_kpitrabalhista = explode("#",@$var_estrategia_kpitrabalhista);
                            $aux="";
                            if(in_array("1",$var_estrategia_kpitrabalhista)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Gestão de Beneficios";
                            if(in_array("2",$var_estrategia_kpitrabalhista)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Gestão de Admissões e Demissões";
                            if(in_array("3",$var_estrategia_kpitrabalhista)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Gestão Estimo Triple A";
                            if(in_array("4",$var_estrategia_kpitrabalhista)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Custo Minuto";                             
                            if(in_array("5",$var_estrategia_kpitrabalhista)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Pró-Labore";   
                        ?>
                        <input type="text" class="clienteestrategia" id="id_estrategia_kpitrabalhista" value="<?=@$aux?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse10"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;10 KPIs de acompanhamento Societário</a>
                    </h4>
                </div>
                <div id="collapse10" class="panel-collapse collapse">
                    <div class="panel-body">
                        <?php
                            $var_estrategia_kpisocietario = explode("#",@$var_estrategia_kpisocietario);
                            $aux="";
                            if(in_array("1",$var_estrategia_kpisocietario)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Adequação de Capital EIRELI";
                            if(in_array("2",$var_estrategia_kpisocietario)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Recomposição de Quadro Societário";
                            if(in_array("3",$var_estrategia_kpisocietario)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Deliberação de Lucros Desproporcionais";
                            if(in_array("4",$var_estrategia_kpisocietario)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Deliberações Especiais";
                            if(in_array("5",$var_estrategia_kpisocietario)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Controle de Certidões";
                            if(in_array("6",$var_estrategia_kpisocietario)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Controle de Certificados";
                            if(in_array("7",$var_estrategia_kpisocietario)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Ata de Sociedade Limitadas "; 
                        ?>
                        <input type="text" class="clienteestrategia" id="id_estrategia_kpisocietario" value="<?=@$aux?>">
                        <?php
                            $arquivo = "./data/".md5(@$var_cod_interno)."/estrategia_anexoacompanhamento.pdf";
                            if (file_exists($arquivo)){
                                $arquivo = LOCAL."data/".md5(@$var_cod_interno)."/estrategia_anexoacompanhamento.pdf";
                                ?>
                                <a href="<?=$arquivo?>" target="_blank">Abrir anexo</a><br><br>
                                <?php
                            }

                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse11"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;11 KPIs de acompanhamento Jurídicos</a>
                    </h4>
                </div>
                <div id="collapse11" class="panel-collapse collapse">
                    <div class="panel-body">
                        <?php
                            $var_estrategia_kpijuridico = explode("#",@$var_estrategia_kpijuridico);
                            $aux="";
                            if(in_array("1",$var_estrategia_kpijuridico)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Radar Tributário (AnexarArquivoPDF)";             
                        ?>
                        <input type="text" class="clienteestrategia" id="id_estrategia_kpijuridico" value="<?=@$aux?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse12"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;12 KPIs de acompanhamento Qualificação</a>
                    </h4>
                </div>
                <div id="collapse12" class="panel-collapse collapse">
                    <div class="panel-body">
                        <?php
                            $var_estrategia_kpiqualificacao = explode("#",@$var_estrategia_kpiqualificacao);
                            $aux="";
                            if(in_array("1",$var_estrategia_kpiqualificacao)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Análise Estratégica";
                            if(in_array("2",$var_estrategia_kpiqualificacao)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Preço de Venda";
                            if(in_array("3",$var_estrategia_kpiqualificacao)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Análise de Custos Fixos"; 
                        ?>
                        <input type="text" class="clienteestrategia" id="id_estrategia_kpiqualificacao" value="<?=@$aux?>">
                        <p><strong>Objetivo do estágio atual</strong></p> 
                        <?php
                            $var_estrategia_objetivoestagioatual = explode("#",@$var_estrategia_objetivoestagioatual);
                            $aux="";
                            if(in_array("1",$var_estrategia_objetivoestagioatual)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Gestão Corretiva";
                            if(in_array("2",$var_estrategia_objetivoestagioatual)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Gestão Preventiva";
                            if(in_array("3",$var_estrategia_objetivoestagioatual)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Gestão Evolutiva"; 
                        ?>
                        <input type="text" class="clienteestrategia" id="id_estrategia_objetivoestagioatual" value="<?=@$aux?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse13"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;13 Principais clientes</a>
                    </h4>
                </div>
                <div id="collapse13" class="panel-collapse collapse">
                    <div class="panel-body">
                        <input type="text" class="clienteestrategia" id="id_estrategia_principaisclientes" value="<?=@$var_estrategia_principaisclientes?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse14"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;14 Principais fornecedores</a>
                    </h4>
                </div>
                <div id="collapse14" class="panel-collapse collapse">
                    <div class="panel-body">
                        <input type="text" class="clienteestrategia" id="id_estrategia_principaisfornecedores" value="<?=@$var_estrategia_principaisfornecedores?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse15"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;15 ERP</a>
                    </h4>
                </div>
                <div id="collapse15" class="panel-collapse collapse">
                    <div class="panel-body">
                        <?php
                            $var_estrategia_erp = explode("#",@$var_estrategia_erp);
                            $aux="";
                            if(in_array("0",$var_estrategia_erp)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Não possui";
                            if(in_array("1",$var_estrategia_erp)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Financeiros";
                            if(in_array("2",$var_estrategia_erp)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;NFE";
                            if(in_array("3",$var_estrategia_erp)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;NFSE";
                            if(in_array("4",$var_estrategia_erp)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;CUPOM-E";
                            if(in_array("5",$var_estrategia_erp)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Frente de Caixa";
                            if(in_array("6",$var_estrategia_erp)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Estoque";
                            if(in_array("7",$var_estrategia_erp)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Compras";
                            if(in_array("8",$var_estrategia_erp)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Fluxo de Caixa";
                            if(in_array("9",$var_estrategia_erp)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Orçamento";
                            if(in_array("10",$var_estrategia_erp)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Contabilidade";
                            if(in_array("11",$var_estrategia_erp)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Fiscal";
                            if(in_array("12",$var_estrategia_erp)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;Folha de Pagamento";
                            if(in_array("13",$var_estrategia_erp)) $aux .= "&nbsp;&nbsp;-&nbsp;&nbsp;BI";
                        ?>
                        <input type="text" class="clienteestrategia" id="id_estrategia_erp" value="<?=@$aux?>">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse16"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;16 Considerações gerais para consultores</a>
                    </h4>
                </div>
                <div id="collapse16" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Considerações gerais de estratégia devem ser atualizadas a cada ciclo</p>
                        <hr>

                        <p><strong>Ciclo 1</strong></p> 
                        <input type="text" class="clienteestrategia" id="id_estrategia_consideracaodata1" value="<?=@$var_estrategia_consideracaodata1?>">
                        <input type="text" class="clienteestrategia" id="id_estrategia_consideracaoconsultor1" value="<?=@$var_estrategia_consideracaoconsultor1?>">
                        <hr>

                        <p><strong>Ciclo 2</strong></p> 
                        <input type="text" class="clienteestrategia" id="id_estrategia_consideracaodata2" value="<?=@$var_estrategia_consideracaodata2?>">
                        <input type="text" class="clienteestrategia" id="id_estrategia_consideracaoconsultor2" value="<?=@$var_estrategia_consideracaoconsultor2?>">
                        <hr>

                        <p><strong>Ciclo 3</strong></p> 
                        <input type="text" class="clienteestrategia" id="id_estrategia_consideracaodata3" value="<?=@$var_estrategia_consideracaodata3?>">
                        <input type="text" class="clienteestrategia" id="id_estrategia_consideracaoconsultor3" value="<?=@$var_estrategia_consideracaoconsultor3?>">
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
