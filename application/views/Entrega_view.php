<div class="col-sm-12">
    <div class="div-table">
        <div class="col-sm-1">
            <center><span class="glyphicon glyphicon-cloud-upload" aria-hidden="true" style="font-size:40px"></span></center>
        </div>
        <div class="col-sm-10">
            <input type="file" name="files" id="files">
        </div>
        <div class="col-sm-1">
            <?php if ($operacao=="entrega"){ ?>
                <a class="a-acao-entregaenviar btn" data-objeto="<?=LOCAL ?>entrega" data-acao="enviar" style="width: 100%;">Enviar</a>
            <?php } ?>
            <?php if ($operacao=="manual"){ ?>
                <a class="a-acao btn" data-objeto="<?=LOCAL ?>entrega" data-acao="enviar" style="width: 100%;">Enviar</a>
            <?php } ?>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="div-table">
        <table class="tablebah" id="entrega_resultado">
            <thead>
                <tr>
                    <th width="3%">&nbsp;</th>
                    <th width="*">Arquivo</th>
                    <th width="*">Cliente</th>
                    <th width="*">Pasta</th>
                    <th width="*">Dt Vencimento</th>
                    <th width="*">Retorno</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="col-sm-12">
    <div class="div-table">
        <div class="col-sm-12">
            <p>
                <strong>Nomenclatura do documento</strong>
                <br>
                <br>CÓDIGO DO CLIENTE DO SISTEMA + SIGLA DO DOCUMENTO CADASTRADO NO SISTEMA + MES E ANO DE COMPETÊNCIA. EX: 130-DAS-082017.pdf. 
                <br>Recálculo: Incluir a nova data de vencimento do documento. DIAMESANO (EX: 20082017). EX: 130-DAS-082017-26082017.pdf. 
                <br>Sem vencimento: Informar data 00000 (EX: 130-CGSALTCON-000000.pdf )
            </p>
        </div>
        <div class="col-sm-4">
            <th width="*"><span class="glyphicon glyphicon-cloud-upload span-success" aria-hidden="true"></span>&nbsp;Tudo Ok</th>
        </div>
        <div class="col-sm-4">
            <th width="*"><span class="glyphicon glyphicon-cloud-upload span-warning" aria-hidden="true"></span>&nbsp;Subiu com erro</th>
        </div>
        <div class="col-sm-4">
            <th width="*"><span class="glyphicon glyphicon-cloud-upload span-danger" aria-hidden="true"></span>&nbsp;Não subiu</th>
        </div>
    </div>
</div>