<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');

?>

<script src="https://cdn.tiny.cloud/1/s3be59tfnkydacs66dczy3hndedrhqcymr5r1r4565hfys93/tinymce/5/tinymce.min.js"></script>
<script>
    tinymce.init(
        {
            selector:'textarea',
            height: 300,
            //menubar: false,
            plugins: "code",
            toolbar: "code",
            menubar: "tools",
            toolbar: ' bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent'
        }
    );
    tinymce.activeEditor.execCommand('mceCodeEditor');
</script>

<div class="col-sm-12 div-operacao">
    <div class="row">
        <?php
            if ($operacao=="listar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>template" data-acao="listar">Buscar</a>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
                <?php
            }
            if ($operacao=="novo"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>template" data-acao="inserir" data-validacao="">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>template" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
            if ($operacao=="editar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao-templatetestar" data-objeto="<?=LOCAL ?>template" data-acao="testar" data-validacao="" data-cod="<?=@$var_cod_template;?>">Testar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>template" data-acao="salvar" data-validacao="" data-cod="<?=@$var_cod_template;?>">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>template" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
        ?>
    </div>
</div>

<?php
    if ($operacao=="listar"){
        ?>
        <div class="col-sm-12">
            <div class="div-table">
                <div class="div-table-fixed">
                    <table class="tablebah">
                        <thead>
                            <tr>
                                <?php
                                    echo $this->functions->table_column("1","cod_template","Cód","*","a-acao");
                                    echo $this->functions->table_column("2","nome","Template","*","a-acao");
                                    echo $this->functions->table_column("3","titulo","Título","td-oculta","a-acao");
                                    echo $this->functions->table_column("4","obs","Disparo","td-oculta","a-acao");
                                ?>
                                <th width="3%">&nbsp;</th>
                                <th width="3%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($dados as $value){
                                    ?>
                                    <tr class="tr-linha" data-objeto="<?=LOCAL ?>template" data-acao="editar" data-cod="<?=$value["cod_template"] ?>">
                                        <td><?=$value["cod_template"]?></td>
                                        <td><?=$value["nome"]?></td>
                                        <td class="td-oculta"><?=$value["titulo"]?></td>
                                        <td class="td-oculta"><?=$value["obs"]?></td>
                                        <td><a class="btn btn-editar a-acao" data-objeto="<?=LOCAL ?>template" data-acao="editar" data-cod="<?=$value["cod_template"] ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pull-right col-paginacao">
                <?=$paginacao; ?>
            </div>
        </div>
        <?php
    }
    if (($operacao=="novo")or($operacao=="editar")){
        ?>
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="nome">Nome</label>  
                        <input type="text" name="nome" id="nome" disabled="disabled" maxlength="100" value="<?=@$var_nome;?>" class="form-control">
                    </div>            
                </div>            
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="titulo">Título</label>  
                        <input type="text" name="titulo" id="titulo" maxlength="100" value="<?=@$var_titulo;?>" class="form-control">
                    </div>            
                </div>            
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="titulo">Local Disparo</label>  
                        <input type="text" name="obs" id="obs" maxlength="250" value="<?=@$var_obs;?>" class="form-control">
                    </div>            
                </div>            
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="control-label" for="texto">Texto</label>  
                        <textarea name="texto" id="template_texto"><?=@$var_texto;?></textarea>
                    </div>            
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="pull-right col-auditoria">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Criação:&nbsp;</strong><?=@$var_usuario_c?> em <?=@$var_data_c_?>&nbsp; 
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Alteração:&nbsp;</strong><?=@$var_usuario_a?> em <?=@$var_data_a_?>
            </div>
        </div>

        <?php
    }
?>


