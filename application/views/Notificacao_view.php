<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
?>

<div class="col-sm-12 div-operacao">
    <div class="row">
        <?php
            if ($operacao=="listar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>notificacao" data-acao="novo">+ Adicionar</a>
                </div>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>notificacao" data-acao="listar">Buscar</a>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
                <?php
            }
            if ($operacao=="novo"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>notificacao" data-acao="inserir" data-validacao="">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>notificacao" data-acao="listar">Voltar para lista</a>
                <?php
            }
            if ($operacao=="editar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>notificacao" data-acao="salvar" data-validacao="" data-cod="<?=@$var_cod_notificacao;?>">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>notificacao" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
        ?>
    </div>
</div>

<?php
    if ($operacao=="cliente"){
        ?>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12 div-form">
                    <?php
                    foreach($dados as $value){
                        $cod_notificacao = $value["cod_notificacao"];
                        $titulo = $value["titulo"];
                        $data_a = $value["data_a"];
                        $texto = $value["texto"];
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$cod_notificacao?>"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>&nbsp;<?=$data_a?>&nbsp;.&nbsp;<?=$titulo?></a>
                                </h4>
                            </div>
                            <div id="collapse<?=$cod_notificacao?>" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <?=$texto?>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                ?>
            </div>
        </div>
        <?php
    }
    
    if ($operacao=="listar"){
        ?>
        <div class="col-sm-12">
            <div class="div-table">
                <div class="div-table-fixed">
                    <table class="tablebah">
                        <thead>
                            <tr>
                                <?php
                                    echo $this->functions->table_column("1","titulo","Notificação","*","a-acao");
                                    echo $this->functions->table_column("","cliente","Cliente","*","a-acao");
                                ?>
                                <th width="3%">&nbsp;</th>
                                <th width="3%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($dados as $value){
                                    $cod_notificacao = $value["cod_notificacao"];
                                    $titulo = $value["titulo"];
                                    $cliente = $value["cliente"];
                                    ?>
                                    <tr class="tr-linha" data-objeto="<?=LOCAL ?>notificacao" data-acao="editar" data-cod="<?=$value["cod_notificacao"] ?>">
                                        <td><?=$titulo?></td>
                                        <td class="tdoculta"><?=$cliente?></td>
                                        <td><a class="btn btn-editar a-acao" data-objeto="<?=LOCAL ?>notificacao" data-acao="editar" data-cod="<?=$cod_notificacao ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                        <td><a class="btn btn-excluir a-acao" data-objeto="<?=LOCAL ?>notificacao" data-acao="excluir" data-cod="<?=$cod_notificacao ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pull-right col-paginacao">
                <?=$paginacao; ?>
            </div>
        </div>
        <?php
    }
    if (($operacao=="novo")or($operacao=="editar")){
        ?>
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="form-group">
                    <label class="control-label" for="cod_cliente">Cliente&nbsp;<span  data-toggle="tooltip" data-placement="bottom" title="Selecione um cliente para fazer uma notificação específica ou deixe em branco e envie para todos."><span class="glyphicon glyphicon-question-sign" aria-hidden="true" style="color:#4c637b;"></span></span></label>
                    <select class="form-control" name="cod_cliente" id="cod_cliente">
                    <option value="NULL">Selecione...</option>
                    <?php
                        foreach($dados_cliente as $value){
                            $cod_cliente = $value["cod_cliente"];
                            $nome = $value["nome"];
                            
                            $selected = (@$var_cod_cliente==$cod_cliente)? "selected": "";
                            ?>
                            <option value="<?=$cod_cliente ?>" <?=$selected ?>><?=$nome ?></option>
                            <?php
                        }
                    ?>
                    </select>
                </div>            

                <div class="form-group">
                    <label class="control-label" for="titulo">Nome</label>  
                    <input type="text" name="titulo" id="titulo" maxlength="100" value="<?=@$var_titulo;?>" class="form-control">
                </div>            
                <div class="form-group">
                    <label class="control-label" for="texto">Texto</label>  
                    <textarea class="form-control" rows="5" name="texto" id="texto"><?=@$var_texto;?></textarea>
                </div>            
            </div>
        </div>
        <?php
    }
    if ($operacao=="editar"){
        ?>
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-12">
                    <h4>Leitura&nbsp;</h4>
                </div>       
                <div class="col-sm-12">
                    <hr>
                </div>
                <div class="col-sm-12">
                    <div class="div-table">
                        <table class="tablebah">
                            <thead>
                                <tr>
                                    <th width="*">Usuário</th>
                                    <th width="*">Tipo</th>
                                    <th width="*">Dt&nbsp;Leitura</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($dados_usuario as $value){
                                        $usuario = $value["usuario"];
                                        $nome = $value["nome"];
                                        $tipo = $value["tipo"];
                                        $lida = $value["lida"];
                                        $data_a = $value["data_a"];
                                        
                                        ?>
                                        <tr>
                                            <td><?=$nome?></td>
                                            <td><?=$tipo?></td>
                                            <td><?php if ($lida=="S") echo $data_a ?></td>
                                        </tr>
                                        <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="pull-right col-auditoria">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Criação:&nbsp;</strong><?=@$var_usuario_c?> em <?=@$var_data_c_?>&nbsp; 
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Alteração:&nbsp;</strong><?=@$var_usuario_a?> em <?=@$var_data_a_?>
            </div>
        </div>
        <?php
    }
?>


