<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
?>

<div class="col-sm-12 div-operacao"> 
    <div class="row">
        <?php
            if ($operacao=="listar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>cliente" data-acao="novo">+ Adicionar</a>
                </div>
                <div class="pull-right">
                    <a class="btn a-acao-buscar">Buscar</a>
                </div>
                <div class="pull-right div-painel-busca-check">
                    <input type="checkbox" class="form-check-input" name="busca_inativo" id="busca_inativo" value="S" <?php if (@$this->input->get_post('busca_inativo')=="S") echo "checked"; ?>><label class="form-check-label" for="busca_inativo">&nbsp;Inativos&nbsp;&nbsp;&nbsp;</label>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
                <?php
            }
            if ($operacao=="novo"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>cliente" data-acao="inserir" data-validacao="">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>cliente" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
            if ($operacao=="editar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>cliente" data-acao="salvar" data-validacao="" data-cod="<?=@$var_cod_cliente;?>">Salvar</a>
                    <a href="<?=LOCAL?>cliente/relatorio_clienteficha/<?=@$var_cod_cliente?>" class="btn pull-right" target="_blank">Imprimir</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>cliente" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
        ?>
    </div>
</div>
        
<?php
    if ($operacao=="listar"){
        ?>
        <div class="col-sm-12">
            <div class="div-table">
                <div class="div-table-fixed">
                    <table class="tablebah">
                        <thead>
                            <tr>
                                <?php
                                    echo $this->functions->table_column("1","nome","Cliente","*","a-acao");
                                    echo $this->functions->table_column("","cod_interno","Cód Interno","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","documento","CPF/CNPJ","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","regime","Regime","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","ativo","Ativo","*","a-acao");
                                ?>
                                <th width="3%">&nbsp;</th>
                                <th width="3%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($dados as $value){
                                    $cod_cliente =$value["cod_cliente"];
                                    $cod_interno =$value["cod_interno"];
                                    $documento =$value["documento"];
                                    $ativo =$value["ativo"];
                                    $nome =$value["nome"];
                                    $regime =$value["regime"];
                                    $favorito =$value["favorito"];
                                    if ($favorito<>null){
                                        $class ="glyphicon glyphicon-star";
                                        $acao ="desfavoritar";
                                    }else{
                                        $class ="glyphicon glyphicon-star-empty";
                                        $acao ="favoritar";
                                    }
                                    ?>
                                    <tr class="tr-linha" data-objeto="<?=LOCAL ?>cliente" data-acao="editar" data-cod="<?=$value["cod_cliente"] ?>">
                                        <td>
                                            <?php 
                                                if ($value["filiais"]<>""){
                                                    echo "<a class=\"tr-cliente-filial\" data-id=\"#cliente-tr-{$cod_cliente}\"><span class=\"glyphicon glyphicon-plus-sign\" aria-hidden=\"true\" style=\"color:rgba(93,178,48,1); font-size:8\"></span></a> ";
                                                } 
                                                echo $nome;
                                            ?>
                                        </td>
                                        <td class="tdoculta"><?=$cod_interno?></td>
                                        <td class="tdoculta"><?=$documento?></td>
                                        <td class="tdoculta"><?=$regime?></td>
                                        <td><?=$ativo?></td>
                                        <td><a class="btn btn-favoritar a-acao-favorito" data-objeto="<?=LOCAL ?>cliente" data-acao="<?=$acao ?>" data-cod="<?=$cod_cliente?>"><span id="favorito_<?=$cod_cliente ?>" class="glyphicon <?=$class?>" aria-hidden="true"><span></a></td>
                                        <td><a class="btn btn-editar a-acao" data-objeto="<?=LOCAL ?>cliente" data-acao="editar" data-cod="<?=$cod_cliente?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                    </tr>
                                    <?php
                                        if ($value["filiais"]<>""){
                                            //var_dump($value["filiais"]);
                                            ?>
                                            <tr id="cliente-tr-<?=$cod_cliente?>" style="display:none" data-objeto="<?=LOCAL ?>cliente" data-acao="editar" data-cod="<?=$value["cod_cliente"] ?>">
                                                <td colspan="8">
                                                    <?php
                                                        $aux = explode ("#",$value["filiais"]);
                                                        foreach ($aux as $value_){
                                                            if ($value_<>""){
                                                                $aux_ = explode ("~",$value_);
                                                                ?>
                                                                <a class="a-acao" data-objeto="<?=LOCAL ?>cliente" data-acao="editar" data-cod="<?=$aux_[0]?>"><span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span><?=$aux_[1]?> <?=$aux_[2]?></a><br>
                                                                <?php
                                                            }
                                                        }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    ?>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pull-right col-paginacao">
                <?=$paginacao; ?>
            </div>
        </div>
        <?php
    }
    if (($operacao=="novo")or($operacao=="editar")){
        ?>

        <div class="col-sm-12">
            <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-cadastro">Cadastro</a></li>
            <li><a data-toggle="tab" href="#tab-estrategia">Estratégia</a></li>
            <li><a data-toggle="tab" href="#tab-quadro">Quadro Societário</a></li>
            <li><a data-toggle="tab" href="#tab-alvara">Alvarás</a></li>
            <?php
                if ($operacao=="editar"){
                    ?>
                    <li><a data-toggle="tab" href="#tab-entrega">Entregas</a></li>
                    <li><a data-toggle="tab" href="#tab-filial">Filiais</a></li>
                    <li><a data-toggle="tab" href="#tab-servico">Serviços</a></li>
                    <li><a data-toggle="tab" href="#tab-coleta">Coleta</a></li>
                    <li><a data-toggle="tab" href="#tab-notificacao">Notificação</a></li>
                    <li><a data-toggle="tab" href="#tab-usuario">Usuários</a></li>
                    <li><a data-toggle="tab" href="#tab-funcionario">Funcionários</a></li>
                <?php
                }
            ?>
            </ul>
        </div>

        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="tab-content">
                    <div id="tab-cadastro" class="tab-pane fade in active">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="nome">Cliente (Apelido)</label>  
                                <input type="text" name="nome" id="nome" maxlength="100" value="<?=@$var_nome;?>" class="form-control">
                            </div>            
                        </div>            

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="razao_social">Razão Social</label>  
                                <input type="text" name="razao_social" id="razao_social" maxlength="100" value="<?=@$var_razao_social;?>" class="form-control">
                            </div>            
                        </div>            

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="control-label" for="cod_interno">Código Interno</label>  
                                <input type="text" name="cod_interno" id="cod_interno" maxlength="100" value="<?=@$var_cod_interno;?>" class="form-control">
                            </div>            
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="cod_clientematriz">Matriz&nbsp;</label>
                                <select class="form-control" name="cod_clientematriz" id="cod_clientematriz">
                                <option value="NULL">Selecione...</option>
                                <?php
                                    foreach($dados_clientematriz as $value){
                                        $selected = ($var_cod_clientematriz==$value["cod_cliente"])? "selected": "";
                                        ?>
                                        <option value="<?=$value["cod_cliente"] ?>" <?=$selected ?>><?=$value["nome"] ?></option>
                                        <?php
                                    }
                                ?>
                                </select>
                            </div>            
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="cod_regime">Regime&nbsp;Tributário</label>
                                <select class="form-control" name="cod_regime" id="cod_regime">
                                <option value="NULL">Selecione...</option>
                                <?php
                                    foreach($dados_regime as $value){
                                        $cod_regime =$value["cod_regime"];
                                        $nome =$value["nome"];

                                        $selected =(@$var_cod_regime==$cod_regime)? "selected": "";
                                        ?>
                                        <option value="<?=$cod_regime ?>" <?=$selected ?>><?=$nome ?></option>
                                        <?php
                                    }
                                ?>
                                </select>
                            </div>            
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="">Atividade Econômica</label> 
                                <br><br>
                                <input type="checkbox" class="form-check-input" name="atividade_servico" id="atividade_servico"  value="S" <?php if (@$var_atividade_servico=="S") echo "checked"; ?>><label class="form-check-label" for="atividade_servico">&nbsp;Serviço&nbsp;&nbsp;&nbsp;</label>
                                <input type="checkbox" class="form-check-input" name="atividade_comercio" id="atividade_comercio"  value="S" <?php if (@$var_atividade_comercio=="S") echo "checked"; ?>><label class="form-check-label" for="atividade_comercio">&nbsp;Comércio&nbsp;&nbsp;&nbsp;</label>
                                <input type="checkbox" class="form-check-input" name="atividade_industria" id="atividade_industria"  value="S" <?php if (@$var_atividade_industria=="S") echo "checked"; ?>><label class="form-check-label" for="atividade_industria">&nbsp;Indústria&nbsp;&nbsp;&nbsp;</label>
                                <input type="checkbox" class="form-check-input" name="atividade_aluguel" id="atividade_aluguel"  value="S" <?php if (@$var_atividade_aluguel=="S") echo "checked"; ?>><label class="form-check-label" for="atividade_aluguel">&nbsp;Aluguel&nbsp;&nbsp;&nbsp;</label>
                                <input type="checkbox" class="form-check-input" name="atividade_holding" id="atividade_holding"  value="S" <?php if (@$var_atividade_holding=="S") echo "checked"; ?>><label class="form-check-label" for="atividade_holding">&nbsp;Holding&nbsp;&nbsp;&nbsp;</label>
                            </div>            
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="control-label" for="">Situação</label>
                                <?php
                                    if($operacao=="novo"){
                                        $var_ativo="S";
                                    }
                                ?>
                                <br><br><input type="checkbox" class="form-check-input" name="ativo" id="ativo" value="S" <?php if(@$var_ativo=="S") echo "checked" ?>><label class="control-label" for="ativo">&nbsp;Ativo</label>
                            </div>            
                        </div>
                        <div class="col-sm-12">
                            <hr>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="foto">Subir Termo de uso</label>  
                                <input type="file" name="termodeuso" id="termodeuso" class="form-control">
                            </div>            
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="termodeuso_vencimento">Termo de uso</label>  
                                <?php
                                    $arquivo ="./data/".md5(@$var_cod_interno)."/termodeuso.pdf";
                                    if (file_exists($arquivo)){
                                        $arquivo =LOCAL."data/".md5(@$var_cod_interno)."/termodeuso.pdf";
                                        ?>
                                        <br><a href="<?=$arquivo?>" class="btn btn-sub" target="_blank">Abrir termo de uso</a>
                                        <?php
                                    } else {
                                        ?>
                                        <br>Não existe arquivo
                                        <?php
                                    }
                                ?>
                            </div>            
                        </div>      
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="procuracao">Procuração</label>  
                                <select class="form-control" name="procuracao" id="procuracao">
                                <option value="NULL">Selecione...</option>
                                <option value="S" <?php if (@$var_procuracao=="S") echo "selected" ?>>Possui</option>
                                <option value="N" <?php if (@$var_procuracao=="N") echo "selected" ?>>Não possui</option>
                                </select>
                            </div>            
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="procuracao_vencimento">Procuração Vencimento</label>  
                                <input type="text" name="procuracao_vencimento" id="procuracao_vencimento" maxlength="10" value="<?=@$var_procuracao_vencimento_;?>" class="form-control input-date">
                            </div>            
                        </div>            
                        <div class="col-sm-12">
                            <hr>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label" for="documento">CPF/CNPJ</label>  
                                <input type="text" name="documento" id="documento" maxlength="14" value="<?=@$var_documento;?>" class="form-control">
                            </div>            
                        </div>            
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label" for="ie">IE</label>  
                                <input type="text" name="ie" id="ie" maxlength="14" value="<?=@$var_ie;?>" class="form-control">
                            </div>            
                        </div>            
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label" for="im">IM</label>  
                                <input type="text" name="im" id="im" maxlength="10" value="<?=@$var_im;?>" class="form-control">
                            </div>            
                        </div>            
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="cliente_evento">Eventos</label>  
                                <br>
                                <span class="control-label multiselect-native-select">
                                    <select name="cliente_evento[]" id="cliente_evento" multiple="multiple">
                                    <?php
                                        $cod_pasta_="";
                                        foreach($dados_clienteevento as $value){
                                            $cod_pasta =$value["cod_pasta"];
                                            $cod_evento =$value["cod_evento"];
                                            $cod_cliente =$value["cod_cliente"];
                                            $evento =$value["evento"];
                                            $sigla =$value["sigla"];
                                            $cod_pasta =$value["cod_pasta"];

                                            if (($cod_pasta_=="") or ($cod_pasta_<>$cod_pasta)){
                                                $cod_pasta_ =$cod_pasta;
                                                $opt_ini ="<optgroup label=\"{$cod_pasta}\">";
                                                $opt_fim ="</optgroup>";
                                            } else{
                                                $opt_ini ="";
                                                $opt_fim ="";
                                            }

                                            $selected =($cod_cliente<>NULL)?"selected":"";
                                            echo $opt_ini;
                                            echo "<option value=\"{$cod_evento}\" {$selected}>{$evento} ({$sigla})</option>";
                                            echo $opt_fim;
                                            ?>
                                            <?php
                                        }
                                    ?>                    
                                    </select>

                                </span>

                            </div>            
                        </div>
                        <div class="col-sm-12">
                            <hr>
                        </div>
                        
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="endereco">Endereço</label>  
                                <input type="text" name="endereco" id="endereco" maxlength="100" value="<?=@$var_endereco;?>" class="form-control">
                            </div>            
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label" for="cidade">Cidade</label>  
                                <input type="text" name="cidade" id="cidade" maxlength="100" value="<?=@$var_cidade;?>" class="form-control">
                            </div>            
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="control-label" for="uf">UF</label>  
                                <input type="text" name="uf" id="uf" maxlength="2" value="<?=@$var_uf;?>" class="form-control"> 
                            </div>            
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="control-label" for="cep">CEP</label>  
                                <input type="text" name="cep" id="cep" maxlength="100" value="<?=@$var_cep;?>" class="form-control">
                            </div>            
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label" for="email">E-mail</label>  
                                <input type="text" name="email" id="email" maxlength="100" value="<?=@$var_email;?>" class="form-control">
                            </div>            
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label" for="telefone">Telefone</label>  
                                <input type="text" name="telefone" id="telefone" maxlength="100" value="<?=@$var_telefone;?>" class="form-control">
                            </div>            
                        </div>
                        
                        <div class="col-sm-12">
                            <hr>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="control-label" for="certificado">Certificado</label>  
                                <select class="form-control" name="certificado" id="certificado">
                                <option value="NULL">Selecione...</option>
                                <option value="S" <?php if (@$var_certificado=="S") echo "selected" ?>>Possui</option>
                                <option value="N" <?php if (@$var_certificado=="N") echo "selected" ?>>Não possui</option>
                                </select>
                            </div>            
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="control-label" for="cod_certificadotipo">Tipo Certificado&nbsp;</label>
                                <select class="form-control" name="cod_certificadotipo" id="cod_certificadotipo">
                                <option value="NULL">Selecione...</option>
                                <?php
                                    foreach($dados_certificadotipo as $value){
                                        $cod_certificadotipo =$value["cod_certificadotipo"];
                                        $nome =$value["nome"];

                                        $selected =(@$var_cod_certificadotipo==$cod_certificadotipo)? "selected": "";
                                        ?>
                                        <option value="<?=$cod_certificadotipo ?>" <?=$selected ?>><?=$nome ?></option>
                                        <?php
                                    }
                                ?>
                                </select>
                            </div>            
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label" for="certificado_posse">Certificado Posse</label>  
                                <select class="form-control" name="certificado_posse" id="certificado_posse">
                                <option value="NULL">Selecione...</option>
                                <option value="T" <?php if (@$var_certificado_posse=="T") echo "selected" ?>>TripleA</option>
                                <option value="C" <?php if (@$var_certificado_posse=="C") echo "selected" ?>>Cliente</option>
                                </select>
                            </div>            
                        </div>            
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label" for="certificado_vencimento">Certificado Vencimento</label>  
                                <input type="text" name="certificado_vencimento" id="certificado_vencimento" maxlength="10" value="<?=@$var_certificado_vencimento_;?>" class="form-control">
                            </div>            
                        </div>            
                        <div class="col-sm-2">
                            <div class="form-group">
                                <?php if ($this->session->userdata('senha_certificado')=="S") {?>
                                <label class="control-label" for="certificado_vencimento">Certificado Senha</label>  
                                <input type="text" name="certificado_senha" id="certificado_senha" maxlength="20" value="<?=@$var_certificado_senha;?>" class="form-control">
                                <?php }?>
                            </div>            
                        </div>            
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label" for="foto">Subir Certificado</label>  
                                <input type="file" name="certificado" id="certificado" class="form-control">
                            </div>            
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label" for="certificado_vencimento">Certificado Arquivo</label>  
                                <?php
                                    $arquivo ="./data/".md5(@$var_cod_interno)."/certificado.pdf";
                                    if (file_exists($arquivo)){
                                        $arquivo =LOCAL."data/".md5(@$var_cod_interno)."/certificado.pdf";
                                        ?>
                                        <br><a href="<?=$arquivo?>" class="btn btn-sub" id="certificadopdf" target="_blank">Abrir certificado pdf</a>
                                        <a href="#" data-cliente="<?=$var_cod_interno?>" data-ext="pdf" data-obj="certificadopdf" class="cliente_excluircertificado"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                        <?php
                                    } 
                                    $arquivo1 ="./data/".md5(@$var_cod_interno)."/certificado.p12";
                                    if (file_exists($arquivo1)){
                                        $arquivo =LOCAL."data/".md5(@$var_cod_interno)."/certificado.p12";
                                        ?>
                                        <br><a href="<?=$arquivo?>" class="btn btn-sub" id="certificadop12" target="_blank">Abrir certificado p12</a>
                                        <a href="#" data-cliente="<?=$var_cod_interno?>" data-ext="p12" data-obj="certificadop12" class="cliente_excluircertificado"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                        <?php
                                    } 
                                    $arquivo2 ="./data/".md5(@$var_cod_interno)."/certificado.pfx";
                                    if (file_exists($arquivo2)){
                                        $arquivo =LOCAL."data/".md5(@$var_cod_interno)."/certificado.pfx";
                                        ?>
                                        <br><a href="<?=$arquivo?>" class="btn btn-sub" id="certificadopfx" target="_blank">Abrir certificado pfx</a>
                                        <a href="#" data-cliente="<?=$var_cod_interno?>" data-ext="pfx" data-obj="certificadopfx" class="cliente_excluircertificado"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                        <?php
                                    } 

                                ?>

                            </div>            
                        </div>
                        
                    </div>
                    <div id="tab-estrategia" class="tab-pane fade">

                        <div class="col-sm-12">
                            <h4>Estratégia</h4>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label" for="progresso" id="cliente_progressbar_label">Qualidade da Estratégia</label>  
                            <div class="progress">
                                <div class="progress-bar" id="cliente_progressbar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <br>
                            <a href="<?=LOCAL?>cliente/relatorio_estrategia/pdf/<?=@$var_cod_cliente?>" class="btn btn-sub pull-right" target="_blank">Ver relatório</a>
                            <a class="btn btn-sub pull-right a-acao-estrategiacomunicar" data-objeto="<?=LOCAL ?>cliente" data-acao="comunicar_estrategia" data-cod="<?=@$var_cod_cliente;?>">Comunicar alteração</a>
                        </div>
                        <div class="col-sm-12">
                            <hr>
                        </div>
                        <div class="col-sm-12">

                            <div class="form-group">
                                <h4>1 Sobre a empresa</h4>
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_sobreempresa">Descreva brevemente a empresa ou o negócio</label>  
                                <textarea class="form-control clienteestrategia" rows="5" name="estrategia_sobreempresa" id="estrategia_sobreempresa"><?=@$var_estrategia_sobreempresa;?></textarea>
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_site">Site e redes sociais</label>  
                                <input type="text" class="form-control clienteestrategia" name="estrategia_site" id="estrategia_site" maxlength="100" value="<?=@$var_estrategia_site;?>">
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_regimetributario">Regime tributário</label>  

                                <select class="form-control clienteestrategia" name="cod_estrategia_regimetributario" id="cod_estrategia_regimetributario">
                                <option value="">Selecione...</option>
                                <?php
                                    foreach($dados_regime as $value){
                                        $cod_regime =$value["cod_regime"];
                                        $nome =$value["nome"];

                                        $selected =(@$var_cod_estrategia_regimetributario==$cod_regime)? "selected": "";
                                        ?>
                                        <option value="<?=$cod_regime ?>" <?=$selected ?>><?=$nome ?></option>
                                        <?php
                                    }
                                ?>
                                </select>
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_regimetributariodata">Vencimento Regime tributário</label>  
                                <input type="text" class="form-control clienteestrategia" name="estrategia_regimetributariodata" id="estrategia_regimetributariodata" maxlength="10" value="<?=@$var_estrategia_regimetributariodata;?>">
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_numeromatriz">Número Matriz e Filial</label>  
                                <input type="text" class="form-control clienteestrategia" name="estrategia_numeromatriz" id="estrategia_numeromatriz" maxlength="10" value="<?=@$var_estrategia_numeromatriz;?>">
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_grupoempresarial">Pertence ao Grupo Empresarial? Se sim, qual?</label>  
                                <input type="text" class="form-control clienteestrategia" name="estrategia_grupoempresarial" id="estrategia_grupoempresarial" maxlength="10" value="<?=@$var_estrategia_grupoempresarial;?>">
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_numeroprofissionais">Número profissionais</label>  
                                <input type="text" class="form-control clienteestrategia" name="estrategia_numeroprofissionais" id="estrategia_numeroprofissionais" maxlength="10" value="<?=@$var_estrategia_numeroprofissionais;?>">
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_quantidadesocios">Quantidade sócios</label>  
                                <input type="text" class="form-control clienteestrategia" name="estrategia_quantidadesocios" id="estrategia_quantidadesocios" maxlength="10" value="<?=@$var_estrategia_quantidadesocios;?>">
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_sociosoutraempresa">Sócios Participam em Outra Empresa? Se sim, qual sócio e qual empresa?</label>  
                                <textarea class="form-control clienteestrategia" rows="5" name="estrategia_sociosoutraempresa" id="estrategia_sociosoutraempresa"><?=@$var_estrategia_sociosoutraempresa;?></textarea>
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_atividades">Atividades (CNAES)</label>  
                                <textarea class="form-control clienteestrategia" rows="5" name="estrategia_atividades" id="estrategia_atividades"><?=@$var_estrategia_atividades;?></textarea>
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_faturamento">Faturamento Últimos 12 Meses</label>  
                                <br><label class="control-label-sub" for="estrategia_faturamento">Faturamento Últimos 12 Meses</label>  
                                <input type="text" class="form-control clienteestrategia" name="estrategia_faturamento" id="estrategia_faturamento" maxlength="10" value="<?=@$var_estrategia_faturamento;?>">
                            </div>            
                            <div class="form-group">
                                <h4>2 Sobre o projeto</h4>
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_sobreprojeto">Descreva o projeto, caso ele seja um produto/serviço específico dentro da empresa</label>  
                                <textarea class="form-control clienteestrategia" rows="5" name="estrategia_sobreprojeto" id="estrategia_sobreprojeto"><?=@$var_estrategia_sobreprojeto;?></textarea>
                            </div>            
                            <div class="form-group">
                                <h4>3 Produtos e serviços</h4>
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_produtoservico">Descreva os produtos e ou serviços para que possamos compreender melhor seus negócio contextualizando-o as realidades tributarias existentes</label>  
                                <textarea class="form-control clienteestrategia" rows="5" name="estrategia_produtoservico" id="estrategia_produtoservico"><?=@$var_estrategia_produtoservico;?></textarea>
                            </div>            
                            <div class="form-group">
                                <h4>4 Pessoas de contato</h4>
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_pessoacontato">Descreva os contatos</label>  
                                <textarea class="form-control clienteestrategia" rows="5" name="estrategia_pessoacontato" id="estrategia_pessoacontato"><?=@$var_estrategia_pessoacontato;?></textarea>
                            </div>            
                            <div class="form-group">
                                <h4>5 Objetivos Principais da estratégia</h4>
                            </div>            
                            <div class="form-group">
                                <?php
                                    $var_estrategia_objetivoestrategia =explode("#",@$var_estrategia_objetivoestrategia);
                                ?>
                                <select class="form-control clienteestrategia" name="estrategia_objetivoestrategia[]" id="estrategia_objetivoestrategia" multiple="multiple">
                                <option value="1" <?php if (in_array("1",$var_estrategia_objetivoestrategia)) echo "selected" ?>>Qualificação de Gestão</option>
                                <option value="2" <?php if (in_array("2",$var_estrategia_objetivoestrategia)) echo "selected" ?>>Planejamento Societário</option>
                                <option value="3" <?php if (in_array("3",$var_estrategia_objetivoestrategia)) echo "selected" ?>>Planejamento Tributário</option>
                                <option value="4" <?php if (in_array("4",$var_estrategia_objetivoestrategia)) echo "selected" ?>>Revisão Tributária</option>
                                <option value="5" <?php if (in_array("5",$var_estrategia_objetivoestrategia)) echo "selected" ?>>Revisão de Relações Trabalhistas</option>
                                <option value="6" <?php if (in_array("6",$var_estrategia_objetivoestrategia)) echo "selected" ?>>Recuperação de Créditos Tributários</option>
                                <option value="7" <?php if (in_array("7",$var_estrategia_objetivoestrategia)) echo "selected" ?>>Informações para Tomada de Decisão</option>                      
                                </select>
                            </div>            
                            <div class="form-group">
                                <h4>6 Estágio de Qualificação de Gestão</h4>
                            </div>            
                            <div class="form-group">
                                <select class="form-control clienteestrategia" name="estrategia_estagioqualificacao" id="estrategia_estagioqualificacao">
                                <option value="1" <?php if (@$var_estrategia_estagioqualificacao=="1") echo "selected" ?>>Básica</option>
                                <option value="2" <?php if (@$var_estrategia_estagioqualificacao=="2") echo "selected" ?>>Intermediária</option>
                                <option value="3" <?php if (@$var_estrategia_estagioqualificacao=="3") echo "selected" ?>>Qualificada</option>
                                <option value="4" <?php if (@$var_estrategia_estagioqualificacao=="4") echo "selected" ?>>Avançada</option>
                                </select>
                            </div>      
                            <div class="form-group">
                                <label class="control-label" for="estrategia_anexoestagioqualificacao">Anexo</label>  
                                <input type="file" name="estrategia_anexoestagioqualificacao" id="estrategia_anexoestagioqualificacao" class="form-control">
                                <?php
                                    $arquivo ="./data/".md5(@$var_cod_interno)."/estrategia_anexoestagioqualificacao.pdf";
                                    if (file_exists($arquivo)){
                                        $arquivo =LOCAL."data/".md5(@$var_cod_interno)."/estrategia_anexoestagioqualificacao.pdf";
                                        ?>
                                        <br><a href="<?=$arquivo?>" class="btn btn-sub" target="_blank">Abrir anexo</a>
                                        <?php
                                    } else {
                                        ?>
                                        <br>Não existe arquivo
                                        <?php
                                    }

                                ?>
                                <br>
                                <br>
                                <br>
                            </div>
                            <div class="form-group">
                                <h4>7 KPIs de acompanhamento Contabilidade</h4>
                            </div>            
                            <div class="form-group">
                                <?php
                                    $var_estrategia_kpicontabilidade =explode("#",@$var_estrategia_kpicontabilidade);
                                ?>
                                <select class="form-control clienteestrategia" name="estrategia_kpicontabilidade[]" id="estrategia_kpicontabilidade" multiple="multiple">
                                <option value="1" <?php if (in_array("1",$var_estrategia_kpicontabilidade)) echo "selected" ?>>Demonstrações Mês + 5° dia útil</option>
                                <option value="2" <?php if (in_array("2",$var_estrategia_kpicontabilidade)) echo "selected" ?>>Demonstrações Mês + 10° dia útil</option>
                                <option value="3" <?php if (in_array("3",$var_estrategia_kpicontabilidade)) echo "selected" ?>>Demonstrações Mês + 20° dia útil</option>
                                <option value="4" <?php if (in_array("4",$var_estrategia_kpicontabilidade)) echo "selected" ?>>Demonstrações Mês + 50 dias</option>
                                <option value="5" <?php if (in_array("5",$var_estrategia_kpicontabilidade)) echo "selected" ?>>Demonstrações Bimestre + 20 dias</option>
                                <option value="6" <?php if (in_array("6",$var_estrategia_kpicontabilidade)) echo "selected" ?>>Demonstrações Trimestre + 20 dias</option>
                                <option value="7" <?php if (in_array("7",$var_estrategia_kpicontabilidade)) echo "selected" ?>>Demonstrações Semestre + 20 dias</option>
                                <option value="8" <?php if (in_array("8",$var_estrategia_kpicontabilidade)) echo "selected" ?>>Ano + 60 dias</option>
                                <option value="9" <?php if (in_array("9",$var_estrategia_kpicontabilidade)) echo "selected" ?>>DRE Combinado (Horizontal)</option>
                                <option value="10" <?php if (in_array("10",$var_estrategia_kpicontabilidade)) echo "selected" ?>>Ativo e Passivo Combinado (Horizonta)</option>
                                <option value="11" <?php if (in_array("11",$var_estrategia_kpicontabilidade)) echo "selected" ?>>Fluxo de Caixa</option>
                                <option value="12" <?php if (in_array("12",$var_estrategia_kpicontabilidade)) echo "selected" ?>>Margem de Contribuição</option>
                                <option value="13" <?php if (in_array("13",$var_estrategia_kpicontabilidade)) echo "selected" ?>>DashBoard Estratégico</option>
                                <option value="14" <?php if (in_array("14",$var_estrategia_kpicontabilidade)) echo "selected" ?>>Demonstrações em Lingua Estrangeira </option>                               
                                </select>
                            </div>            
                            <div class="form-group">
                                <h4>8 KPIs de acompanhamento Tributários</h4>
                            </div>            
                            <div class="form-group">
                                <?php
                                    $var_estrategia_kpitributario =explode("#",@$var_estrategia_kpitributario);
                                ?>
                                <select class="form-control clienteestrategia" name="estrategia_kpitributario[]" id="estrategia_kpitributario" multiple="multiple">
                                <option value="1" <?php if (in_array("1",$var_estrategia_kpitributario)) echo "selected" ?>>Guarda de Notas Fiscais Eletronicas</option>
                                <option value="2" <?php if (in_array("2",$var_estrategia_kpitributario)) echo "selected" ?>>Gestão Estilo Triple A (GET) </option>
                                <option value="3" <?php if (in_array("3",$var_estrategia_kpitributario)) echo "selected" ?>>Evolução Faturamento</option>
                                <option value="4" <?php if (in_array("4",$var_estrategia_kpitributario)) echo "selected" ?>>Recuperação de Créditos Administrativos</option>
                                <option value="5" <?php if (in_array("5",$var_estrategia_kpitributario)) echo "selected" ?>>Revisão de Cadastros Fiscais </option>                               
                                </select>
                            </div>            
                            <div class="form-group">
                                <h4>9 KPIs de acompanhamento Relações Trabalhistas</h4>
                            </div>            
                            <div class="form-group">
                                <?php
                                    $var_estrategia_kpitrabalhista =explode("#",@$var_estrategia_kpitrabalhista);
                                ?>
                                <select class="form-control clienteestrategia" name="estrategia_kpitrabalhista[]" id="estrategia_kpitrabalhista" multiple="multiple">
                                <option value="1" <?php if (in_array("1",$var_estrategia_kpitrabalhista)) echo "selected" ?>>Gestão de Beneficios</option>
                                <option value="2" <?php if (in_array("2",$var_estrategia_kpitrabalhista)) echo "selected" ?>>Gestão de Admissões e Demissões</option>
                                <option value="3" <?php if (in_array("3",$var_estrategia_kpitrabalhista)) echo "selected" ?>>Gestão Estimo Triple A</option>
                                <option value="4" <?php if (in_array("4",$var_estrategia_kpitrabalhista)) echo "selected" ?>>Custo Minuto</option>     
                                <option value="5" <?php if (in_array("5",$var_estrategia_kpitrabalhista)) echo "selected" ?>>Pró-Labore</option>     
                                </select>
                            </div>            
                            <div class="form-group">
                                <h4>10 KPIs de acompanhamento Societário</h4>
                            </div>            
                            <div class="form-group">
                                <?php
                                    $var_estrategia_kpisocietario =explode("#",@$var_estrategia_kpisocietario);
                                ?>
                                <select class="form-control clienteestrategia" name="estrategia_kpisocietario[]" id="estrategia_kpisocietario" multiple="multiple">
                                <option value="1" <?php if (in_array("1",$var_estrategia_kpisocietario)) echo "selected" ?>>Adequação de Capital EIRELI</option>
                                <option value="2" <?php if (in_array("2",$var_estrategia_kpisocietario)) echo "selected" ?>>Recomposição de Quadro Societário</option>
                                <option value="3" <?php if (in_array("3",$var_estrategia_kpisocietario)) echo "selected" ?>>Deliberação de Lucros Desproporcionais</option>
                                <option value="4" <?php if (in_array("4",$var_estrategia_kpisocietario)) echo "selected" ?>>Deliberações Especiais</option>
                                <option value="5" <?php if (in_array("5",$var_estrategia_kpisocietario)) echo "selected" ?>>Controle de Certidões</option>
                                <option value="6" <?php if (in_array("6",$var_estrategia_kpisocietario)) echo "selected" ?>>Controle de Certificados</option>
                                <option value="7" <?php if (in_array("7",$var_estrategia_kpisocietario)) echo "selected" ?>>Ata de Sociedade Limitadas </option>                               
                                </select>
                            </div>            
                            <div class="form-group">
                                <h4>11 KPIs de acompanhamento Jurídicos</h4>
                            </div>            
                            <div class="form-group">
                                <?php
                                    $var_estrategia_kpijuridico =explode("#",@$var_estrategia_kpijuridico);
                                ?>
                                <select class="form-control clienteestrategia" name="estrategia_kpijuridico[]" id="estrategia_kpijuridico" multiple="multiple">
                                <option value="1" <?php if (in_array("1",$var_estrategia_kpijuridico)) echo "selected" ?>>Radar Tributário</option>                            
                                </select>
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_anexokpijuridico">Anexo</label>  
                                <input type="file" name="estrategia_anexokpijuridico" id="estrategia_anexokpijuridico" class="form-control">
                                <?php
                                    $arquivo ="./data/".md5(@$var_cod_interno)."/estrategia_anexokpijuridico.pdf";
                                    if (file_exists($arquivo)){
                                        $arquivo =LOCAL."data/".md5(@$var_cod_interno)."/estrategia_anexokpijuridico.pdf";
                                        ?>
                                        <br><a href="<?=$arquivo?>" class="btn btn-sub" target="_blank">Abrir anexo</a>
                                        <?php
                                    } else {
                                        ?>
                                        <br>Não existe arquivo
                                        <?php
                                    }

                                ?>
                                <br>
                                <br>
                                <br>
                            </div>            
                            <div class="form-group">
                                <h4>12 KPIs de acompanhamento Qualificação</h4>

                            </div>            
                            <div class="form-group">
                                <?php
                                    $var_estrategia_kpiqualificacao =explode("#",@$var_estrategia_kpiqualificacao);
                                ?>
                                <select class="form-control clienteestrategia" name="estrategia_kpiqualificacao[]" id="estrategia_kpiqualificacao" multiple="multiple">
                                <option value="1" <?php if (in_array("1",$var_estrategia_kpiqualificacao)) echo "selected" ?>>Análise Estratégica</option>
                                <option value="2" <?php if (in_array("2",$var_estrategia_kpiqualificacao)) echo "selected" ?>>Preço de Venda</option>
                                <option value="3" <?php if (in_array("3",$var_estrategia_kpiqualificacao)) echo "selected" ?>>Análise de Custos Fixos</option>                            
                                </select>
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_objetivoestagioatual">Objetivo do estágio atual</label>  
                                <?php
                                    $var_estrategia_objetivoestagioatual =explode("#",@$var_estrategia_objetivoestagioatual);
                                ?>
                                <br>
                                <select class="form-control clienteestrategia" name="estrategia_objetivoestagioatual[]" id="estrategia_objetivoestagioatual" multiple="multiple">
                                <option value="1" <?php if (in_array("1",$var_estrategia_objetivoestagioatual)) echo "selected" ?>>Gestão Corretiva</option>
                                <option value="2" <?php if (in_array("2",$var_estrategia_objetivoestagioatual)) echo "selected" ?>>Gestão Preventiva</option>
                                <option value="3" <?php if (in_array("3",$var_estrategia_objetivoestagioatual)) echo "selected" ?>>Gestão Evolutiva</option>                                
                                </select>
                            </div>            
                            <div class="form-group">
                                <h4>13 Principais clientes</h4>
                            </div>            
                            <div class="form-group">
                                <textarea class="form-control clienteestrategia" rows="5" name="estrategia_principaisclientes" id="estrategia_principaisclientes"><?=@$var_estrategia_principaisclientes;?></textarea>
                            </div>            
                            <div class="form-group">
                                <h4>14 Principais fornecedores</h4>
                            </div>            
                            <div class="form-group">
                                <textarea class="form-control clienteestrategia" rows="5" name="estrategia_principaisfornecedores" id="estrategia_principaisfornecedores"><?=@$var_estrategia_principaisfornecedores;?></textarea>
                            </div>            
                            <div class="form-group">
                                <h4>15 ERP</h4>
                            </div>            
                            <div class="form-group">
                                <?php
                                    $var_estrategia_erp =explode("#",@$var_estrategia_erp);
                                ?>
                                <select class="form-control clienteestrategia" name="estrategia_erp[]" id="estrategia_erp" multiple="multiple">
                                <option value="0" <?php if (in_array("0",$var_estrategia_erp)) echo "selected" ?>>Não possui</option>
                                <option value="1" <?php if (in_array("1",$var_estrategia_erp)) echo "selected" ?>>Financeiros</option>
                                <option value="2" <?php if (in_array("2",$var_estrategia_erp)) echo "selected" ?>>NFE</option>
                                <option value="3" <?php if (in_array("3",$var_estrategia_erp)) echo "selected" ?>>NFSE</option>
                                <option value="4" <?php if (in_array("4",$var_estrategia_erp)) echo "selected" ?>>CUPOM-E</option>
                                <option value="5" <?php if (in_array("5",$var_estrategia_erp)) echo "selected" ?>>Frente de Caixa</option>
                                <option value="6" <?php if (in_array("6",$var_estrategia_erp)) echo "selected" ?>>Estoque</option>
                                <option value="7" <?php if (in_array("7",$var_estrategia_erp)) echo "selected" ?>>Compras</option>
                                <option value="8" <?php if (in_array("8",$var_estrategia_erp)) echo "selected" ?>>Fluxo de Caixa</option>
                                <option value="9" <?php if (in_array("9",$var_estrategia_erp)) echo "selected" ?>>Orçamento</option>
                                <option value="10" <?php if (in_array("10",$var_estrategia_erp)) echo "selected" ?>>Contabilidade</option>
                                <option value="11" <?php if (in_array("11",$var_estrategia_erp)) echo "selected" ?>>Fiscal</option>
                                <option value="12" <?php if (in_array("12",$var_estrategia_erp)) echo "selected" ?>>Folha de Pagamento</option>
                                <option value="13" <?php if (in_array("13",$var_estrategia_erp)) echo "selected" ?>>BI</option>
                                </select>
                            </div>            
                            <div class="form-group">
                                <h4>16 Considerações gerais para consultores</h4>
                                <p>Considerações gerais de estratégia devem ser atualizadas a cada ciclo</p>
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_consideracaodata1">Vencimento Ciclo 1</label>  
                                <input type="text" class="form-control clienteestrategia" name="estrategia_consideracaodata1" id="estrategia_consideracaodata1" maxlength="10" value="<?=@$var_estrategia_consideracaodata1;?>">
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_consideracaoconsultor1">Ciclo 1</label>  
                                <textarea class="form-control clienteestrategia" rows="5" name="estrategia_consideracaoconsultor1" id="estrategia_consideracaoconsultor1"><?=@$var_estrategia_consideracaoconsultor1;?></textarea>
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_consideracaodata2">Vencimento Ciclo 2</label>  
                                <input type="text" class="form-control clienteestrategia" name="estrategia_consideracaodata2" id="estrategia_consideracaodata2" maxlength="10" value="<?=@$var_estrategia_consideracaodata2;?>">
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_consideracaoconsultor2">Ciclo 2</label>  
                                <textarea class="form-control clienteestrategia" rows="5" name="estrategia_consideracaoconsultor2" id="estrategia_consideracaoconsultor2"><?=@$var_estrategia_consideracaoconsultor2;?></textarea>
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_consideracaodata3">Vencimento Ciclo 3</label>  
                                <input type="text" class="form-control clienteestrategia" name="estrategia_consideracaodata3" id="estrategia_consideracaodata3" maxlength="10" value="<?=@$var_estrategia_consideracaodata3;?>">
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="estrategia_consideracaoconsultor3">Ciclo 3</label>  
                                <textarea class="form-control clienteestrategia" rows="5" name="estrategia_consideracaoconsultor3" id="estrategia_consideracaoconsultor3"><?=@$var_estrategia_consideracaoconsultor3;?></textarea>
                            </div>            

                        </div>
                    </div>

                    <div id="tab-quadro" class="tab-pane fade">
                        <div class="col-sm-3">
                            <h4>Quadro Societário</h4>
                        </div>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-12">
                                    <a href="<?=LOCAL?>cliente/relatorio_quadro/pdf/<?=@$var_cod_cliente?>" class="btn btn-sub pull-right" target="_blank">Ver relatório</a>
                                    <a href="#" class="btn btn-sub pull-right" id="quadro_campoinserir">Inserir</a>
                                </div>            
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <hr>
                        </div>
                        <div class="col-sm-12">
                            <div class="row" id="quadro">
                                <div class="row">
                                    <div class="col-sm-1">
                                        Ordem
                                    </div>       
                                    <div class="col-sm-2">
                                        Nome
                                    </div>       
                                    <div class="col-sm-2">
                                        CPF/CNPJ
                                    </div>       
                                    <div class="col-sm-1">
                                        Dt&nbsp;Registro
                                    </div>       
                                    <div class="col-sm-1">
                                        Dt&nbsp;Início
                                    </div>       
                                    <div class="col-sm-1">
                                        Dt&nbsp;Saída
                                    </div>       
                                    <div class="col-sm-1">
                                        Paticipação %
                                    </div>       
                                    <div class="col-sm-1">
                                        Quotas R$
                                    </div>       
                                    <div class="col-sm-1">
                                        Resp. Receita
                                    </div>       
                                    <div class="col-sm-1">
                                        &nbsp;
                                    </div>       
                                </div>
                                <?php
                                    if (isset($dados_quadro)){
                                        $count=0;
                                        foreach($dados_quadro as $value){
                                            ?>
                                            <div class="row" id="quadro-<?=$count;?>">
                                                <div class="col-sm-1">
                                                    <input type="text" class="form-control" name="quadro_ordem[]" maxlength="2" value="<?=@$value["ordem"];?>" placeholder="Ordem">
                                                </div>       
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control" name="quadro_nome[]" maxlength="100" value="<?=@$value["nome"];?>" placeholder="Nome">
                                                </div>       
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control" name="quadro_documento[]" maxlength="100" value="<?=@$value["documento"];?>" placeholder="CPF/CNPJ">
                                                </div>       
                                                <div class="col-sm-1">
                                                    <input type="text" class="form-control input-date" name="quadro_data_registro[]" maxlength="10" value="<?=@$value["data_registro_"];?>" placeholder="Dt&nbsp; Registro">
                                                </div>       
                                                <div class="col-sm-1">
                                                    <input type="text" class="form-control input-date" name="quadro_data_inicio[]" maxlength="10" value="<?=@$value["data_inicio_"];?>" placeholder="Dt&nbsp; Início">
                                                </div>       
                                                <div class="col-sm-1">
                                                    <input type="text" class="form-control input-date" name="quadro_data_saida[]" maxlength="10" value="<?=@$value["data_saida_"];?>" placeholder="Dt&nbsp; Saída">
                                                </div>       
                                                <div class="col-sm-1">
                                                    <input type="text" class="form-control" name="quadro_participacao[]" maxlength="100" value="<?=@$value["participacao"];?>" placeholder="Paticipação">
                                                </div>       
                                                <div class="col-sm-1">
                                                    <input type="text" class="form-control" name="quadro_quotas[]" maxlength="100" value="<?=@$value["quotas"];?>" placeholder="Quotas">
                                                </div>       
                                                <div class="col-sm-1">
                                                    <select class="form-control" name="quadro_responsavel[]" id="quadro_responsavel">
                                                    <option value="NULL">Selecione...</option>
                                                    <option value="S" <?php if ($value["responsavel"]=="S") echo "selected"?> >Sim</option>
                                                    <option value="N" <?php if ($value["responsavel"]=="N") echo "selected"?> >Não</option>
                                                    </select>
                                                </div>       
                                                <div class="col-sm-1">
                                                    <a href="#" class="quadro_remover" data-quadro="quadro-<?=$count;?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                                </div>       
                                            </div>
                                            <?php
                                            $count++;    
                                        }
                                    }
                                ?>
                                <input type="hidden" id="quadro-aux" value="<?=@$count?>">

                            </div>

                        
                        </div>                                 
                    </div>

                    <div id="tab-alvara" class="tab-pane fade">
                        <div class="col-sm-3">
                            <h4>Alvarás</h4>
                        </div>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-12">
                                    <a href="<?=LOCAL?>cliente/relatorio_alvara/<?=@$var_cod_cliente?>" class="btn btn-sub pull-right" target="_blank">Ver relatório</a>
                                    <a href="#" class="btn btn-sub pull-right" id="alvara_campoinserir">Inserir</a>
                                </div>            
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <hr>
                        </div>
                        <div class="col-sm-12">
                            <div class="row" id="alvaras">
                                <div class="row">
                                    <div class="col-sm-3">
                                        Alvará
                                    </div>       
                                    <div class="col-sm-2">
                                        Número
                                    </div>       
                                    <div class="col-sm-2">
                                        Vencimento
                                    </div>       
                                    <div class="col-sm-2">
                                        Dt&nbsp; Validade
                                    </div>       
                                    <div class="col-sm-2">
                                        Dt&nbsp; Pagamento Taxa
                                    </div>       
                                    <div class="col-sm-1">
                                        &nbsp;
                                    </div>       
                                </div>
                                <?php
                                    if (isset($dados_alvara)){
                                        $count=0;
                                        foreach($dados_alvara as $value){
                                            ?>
                                            <div class="row" id="alvara-<?=$count;?>">
                                                <div class="col-sm-3">
                                                    <select class="form-control" name="alvara_cod_alvara[]" id="alvara_cod_alvara">
                                                    <option value="NULL">Selecione...</option>
                                                    <?php
                                                        foreach($dados_tipoalvara as $value_){
                                                            $selected =(@$value["cod_alvara"]==$value_["cod_alvara"])? "selected": "";
                                                            ?>
                                                            <option value="<?=$value_["cod_alvara"] ?>" <?=$selected?> ><?=$value_["nome"] ?></option>
                                                            <?php
                                                        }
                                                    ?>
                                                    </select>
                                                </div>       
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control" name="alvara_numero[]" maxlength="20" value="<?=@$value["numero"];?>" placeholder="Número">
                                                </div>       
                                                <div class="col-sm-2">
                                                    <select class="form-control" name="alvara_vencimento[]" id="alvara_vencimento">
                                                        <option value="NULL">Selecione...</option>
                                                        <option value="C" <?php if ($value["vencimento"]=="C") echo "selected"?> >Com vencimento</option>
                                                        <option value="I" <?php if ($value["vencimento"]=="I") echo "selected"?> >Tempo indeterminado</option>
                                                    </select>
                                                </div>       
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control input-date" name="alvara_data_vencimento[]" maxlength="10" value="<?=@$value["data_vencimento_"];?>" placeholder="Dt&nbsp; Vencimento">
                                                </div>       
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control input-mes" name="alvara_data_pagamento[]" maxlength="8" value="<?=@$value["data_pagamento_"];?>" placeholder="Dt&nbsp; Pagamento">
                                                </div>       
                                                <div class="col-sm-1">
                                                    <a href="#" class="alvara_remover" data-alvara="alvara-<?=$count;?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                                </div>       
                                            </div>
                                            <?php
                                            $count++;    
                                        }
                                    }
                                ?>
                                <input type="hidden" id="alvara-aux" value="<?=@$count?>">

                            </div>

                        
                        </div>                                 
                    </div>

                    <?php
                        if ($operacao=="editar"){
                            ?>
                            <div id="tab-entrega" class="tab-pane fade">
                                <div class="col-sm-6">
                                    <h4>Entregas</h4>
                                </div>
                                <div class="col-sm-6">
                                    <a href="<?=LOCAL?>cliente/relatorio_entregas/<?=@$var_cod_cliente?>" class="btn btn-sub pull-right" target="_blank">Ver relatório</a>
                                    <br>
                                </div>
                                <div class="col-sm-12">
                                    <hr>
                                </div>
                                <div class="col-sm-12">
                                    <div class="div-table">
                                        <table class="tablebah">
                                            <thead>
                                                <tr>
                                                    <th width="*">
                                                        Arquivo
                                                    </th>
                                                    <th width="*">
                                                        Pasta
                                                    </th>
                                                    <th width="20%" class="tdoculta">
                                                        Entrega
                                                    </th>
                                                    <th width="20%" class="tdoculta">
                                                        Vencimento
                                                    </th>
                                                    <th width="*">
                                                        &nbsp;
                                                    </th>
                                                    <th width="*">
                                                        &nbsp;
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    foreach($dados_arquivos as $value_){
                                                        ?>
                                                        <tr id="tr_<?=$value_["arquivo"]?>">
                                                            <td>
                                                                <?=$value_["nome_original"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["cod_pasta"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["data_c_"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["data_vencimento_"]?>
                                                            </td>
                                                            <td>
                                                                <a class="a-acao-e" data-objeto="<?=LOCAL."arquivo"?>" data-acao="e" data-a="<?=$value_["arquivo"]?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                                            </td>
                                                            <td>
                                                                <a class="a-acao-d" data-objeto="<?=LOCAL."arquivo"?>" data-acao="a" data-a="<?=$value_["arquivo"]?>"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span></a>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>                                 
                            </div>
                
                            <div id="tab-filial" class="tab-pane fade">
                                <div class="col-sm-12">
                                    <h4>Filiais</h4>
                                    <hr>
                                </div>
                                <div class="col-sm-12">
                                    <div class="div-table">
                                        <table class="tablebah">
                                            <thead>
                                                <tr>
                                                    <th width="*">
                                                        Cliente
                                                    </th>
                                                    <th width="*">
                                                        Cód Interno
                                                    </th>
                                                    <th width="*">
                                                        CPF/CNPJ
                                                    </th>
                                                    <th width="*">
                                                        Ativo
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    foreach($dados_filiais as $value_){
                                                        ?>
                                                        <tr id="tr_<?=$arquivo?>">
                                                            <td>
                                                                <?=$value_["nome"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["cod_interno"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["documento"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["ativo"]?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>                    
                                </div>
                            </div>
                
                            <div id="tab-servico" class="tab-pane fade">
                                <div class="col-sm-12">
                                    <h4>Serviços</h4>
                                    <hr>
                                </div>
                                <div class="col-sm-12">
                                    <div class="div-table">
                                        <table class="tablebah">
                                            <thead>
                                                <tr>
                                                    <th width="*">
                                                        Serviço
                                                    </th>
                                                    <th width="*">
                                                        Dt&nbsp; Início
                                                    </th>
                                                    <th width="*">
                                                        Dt&nbsp; Previsão
                                                    </th>
                                                    <th width="*">
                                                        Dt&nbsp; Conclusão
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    foreach($dados_servicos as $value_){
                                                        ?>
                                                        <tr id="tr_<?=$arquivo?>">
                                                            <td>
                                                                <?=$value_["tarefa"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["data_inicio_"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["data_previsao_"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["data_conclusao_"]?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>                    
                                </div>                    
                            </div>

                            <div id="tab-coleta" class="tab-pane fade">
                                <div class="col-sm-12">
                                    <h4>Coleta</h4>
                                    <hr>
                                </div>
                                <div class="col-sm-12">
                                    <div class="div-table">
                                        <table class="tablebah">
                                            <thead>
                                                <tr>
                                                <th width="*">
                                                        Dt&nbsp;Solicitacão
                                                    </th>
                                                    <th width="*">
                                                        Dt&nbsp;Vencimento
                                                    </th>
                                                    <th width="*">
                                                        Solicitação
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    foreach($dados_coleta as $value_){
                                                    ?>
                                                        <tr id="tr_<?=$arquivo?>">
                                                            <td>
                                                                <?=$value_["data_solicitacao_"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["data_vencimento_"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["texto"]?>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>   
                                </div>   
                            </div>   

                            <div id="tab-notificacao" class="tab-pane fade">
                                <div class="col-sm-12">
                                    <h4>Notificação</h4>
                                    <hr>
                                </div>
                                <div class="col-sm-12">
                                    <div class="div-table">
                                        <table class="tablebah">
                                            <thead>
                                                <tr>
                                                    <th width="*">
                                                        Dt&nbsp;Envio
                                                    </th>
                                                    <th width="*">
                                                        Título
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    foreach($dados_notificacao as $value_){
                                                    ?>
                                                        <tr id="tr_<?=$arquivo?>">
                                                            <td>
                                                                <?=$value_["data_c_"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["titulo"]?>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>                    
                                </div>                    
                            </div>                    

                            <div id="tab-usuario" class="tab-pane fade">
                                <div class="col-sm-12">
                                    <h4>Usuários habilitados</h4>
                                    <hr>
                                </div>
                                <div class="col-sm-12">
                                    <div class="div-table">
                                        <table class="tablebah">
                                            <thead>
                                                <tr>
                                                    <th width="*">
                                                        Nome
                                                    </th>
                                                    <th width="*">
                                                        E-mail
                                                    </th>
                                                    <th width="*">
                                                        Tipo
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    foreach($dados_usuarios as $value_){
                                                        ?>
                                                        <tr id="tr_<?=$arquivo?>">
                                                            <td>
                                                                <?=$value_["nome"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["email"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["tipo"]?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>                    
                                </div>                    
                            </div>

                            <div id="tab-funcionario" class="tab-pane fade">
                                <div class="col-sm-12">
                                    <h4>Funcionários</h4>
                                    <hr>
                                </div>
                                <div class="col-sm-12">
                                    <div class="div-table">
                                        <table class="tablebah">
                                            <thead>
                                                <tr>
                                                    <th width="*">
                                                        Nome
                                                    </th>
                                                    <th width="*">
                                                        Documento
                                                    </th>
                                                    <th width="*">
                                                        Dt Admissão
                                                    </th>
                                                    <th width="*">
                                                        Dt Demissão
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    foreach($dados_funcionarios as $value_){
                                                        ?>
                                                        <tr id="tr_<?=$arquivo?>">
                                                            <td>
                                                                <?=$value_["nome"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["documento"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["data_admissao_"]?>
                                                            </td>
                                                            <td>
                                                                <?=$value_["data_demissao_"]?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>                    
                                </div>                    
                            </div>

                        <?php
                        }                    
                    ?>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="pull-right col-auditoria">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Criação:&nbsp;</strong><?=@$var_usuario_c?> em <?=@$var_data_c_?>&nbsp; 
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Alteração:&nbsp;</strong><?=@$var_usuario_a?> em <?=@$var_data_a_?>
            </div>
        </div>
                    
    <?php
    }
?>

