<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
    //var_dump($dados_campos);exit;
?>

<div class="col-sm-12 div-operacao">
    <div class="row">
        <?php
            if ($operacao=="listar"){
                if ($this->session->userdata('tipo')=="T"){
                    ?>
                    <div class="pull-right">
                        <a class="btn a-acao" data-objeto="<?=LOCAL ?>coleta" data-acao="novo">+ Adicionar</a>
                    </div>
                    <?php
                }
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>coleta" data-acao="listar">Buscar</a>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
                <?php
            }
            if ($operacao=="novo"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>coleta" data-acao="inserir" data-validacao="">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>coleta" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
            if ($operacao=="editar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>coleta" data-acao="salvar" data-validacao="" data-cod="<?=@$var_cod_coleta;?>">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>coleta" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
            if ($operacao=="ver"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>coleta" data-acao="enviar" data-cod="<?=@$var_cod_coleta;?>">Enviar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>coleta" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
        ?>
    </div>
</div>

<?php
    if ($operacao=="listar"){
        ?>
        <div class="col-sm-12">
            <div class="div-table">
                <div class="div-table-fixed">
                    <table class="tablebah">
                        <thead>
                            <tr>
                                <?php
                                    echo $this->functions->table_column("1","cod_pasta","Pasta","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","formulario","Formulário","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","data_solicitacao_","Dt&nbsp;Solicitação","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","data_vencimento_","Dt&nbsp;Vencimento","*","a-acao");
                                    echo $this->functions->table_column("","texto","Solicitação","*","tdoculta a-acao");
                                ?>
                                <?php if ($this->session->userdata('tipo')=="T"){ ?>
                                    <th width="3%">&nbsp;</th>
                                    <th width="3%">&nbsp;</th>
                                    <th width="3%">&nbsp;</th>
                                <?php } ?>
                                <?php if ($this->session->userdata('tipo')=="C"){ ?>
                                    <th width="3%">&nbsp;</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($dados as $value){
                                    ?>
                                    <?php if ($this->session->userdata('tipo')=="T"){ ?>
                                        <tr class="tr-linha" data-objeto="<?=LOCAL ?>coleta" data-acao="editar" data-cod="<?=$value["cod_coleta"] ?>">
                                    <?php } ?>
                                    <?php if ($this->session->userdata('tipo')=="C"){ ?>
                                        <tr class="tr-linha" data-objeto="<?=LOCAL ?>coleta" data-acao="ver" data-cod="<?=$value["cod_coleta"] ?>">
                                    <?php } ?>
                                        <td class="tdoculta"><?=$value["cod_pasta"]?></td>
                                        <td class="tdoculta"><?=$value["formulario"]?></td>
                                        <td class="tdoculta"><?=$value["data_solicitacao_"]?></td>
                                        <td class=""><?=$value["data_vencimento_"]?></td>
                                        <td class="tdoculta"><?=$value["texto"]?></td>
                                        <?php if ($this->session->userdata('tipo')=="T"){ ?>
                                            <td><a class="btn btn-editar a-acao" data-objeto="<?=LOCAL ?>coleta" data-acao="editar" data-cod="<?=$value["cod_coleta"] ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                            <td><a class="btn btn-ver a-acao" data-objeto="<?=LOCAL ?>coleta" data-acao="ver" data-cod="<?=$value["cod_coleta"] ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                                            <td><a class="btn btn-excluir a-acao" data-objeto="<?=LOCAL ?>coleta" data-acao="excluir" data-cod="<?=$value["cod_coleta"] ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></td>
                                        <?php } ?>
                                        <?php if ($this->session->userdata('tipo')=="C"){ ?>
                                            <td><a class="btn btn-ver a-acao" data-objeto="<?=LOCAL ?>coleta" data-acao="ver" data-cod="<?=$value["cod_coleta"] ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                                        <?php } ?>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pull-right col-paginacao">
                <?=$paginacao; ?>
            </div>
        </div>
        <?php
    }
    if (($operacao=="novo")or($operacao=="editar")){
        ?>
        <div class="col-sm-12 div-form">
            <div class="row">
                <input type="hidden" name="cod_coleta" id="cod_coleta" value="<?=@$var_cod_coleta;?>">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="coleta_cliente">Cliente</label>  
                        <br>
                        <span class="control-label multiselect-native-select">
                            <select name="coleta_cliente[]" id="coleta_cliente" multiple="multiple">
                            <?php
                            
                                foreach($dados_coletacliente as $value){
                                    $cod_coleta = $value["cod_coleta"];
                                    $cod_cliente = $value["cod_cliente"];
                                    $cliente = $value["cliente"];

                                    $selected = ($cod_coleta<>NULL)?"selected":"";
                                    echo "<option value=\"{$cod_cliente}\" {$selected}>{$cliente}</option>";
                                    ?>
                                    <?php
                                }
                            ?>                    
                            </select>
                        </span>
                    </div>            
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="cod_pasta">Pasta&nbsp;</label>
                        <select class="form-control" name="cod_pasta" id="cod_pasta">
                        <option value="NULL">Selecione...</option>
                        <?php
                            foreach($dados_cod_pasta as $value){
                                $cod_pasta = $value["cod_pasta"];
                                $nome = $value["nome"];

                                $selected = (@$var_cod_pasta==$cod_pasta)? "selected": "";
                                ?>
                                <option value="<?=$cod_pasta ?>" <?=$selected ?>><?=$nome ?></option>
                                <?php
                            }
                        ?>
                        </select>
                    </div>            
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="cod_formulario">Formulário&nbsp;</label>
                        <select class="form-control" name="cod_formulario" id="cod_formulario">
                        <option value="NULL">Selecione...</option>
                        <?php
                            foreach($dados_formulario as $value){
                                $cod_formulario = $value["cod_formulario"];
                                $nome = $value["nome"];

                                $selected = (@$var_cod_formulario==$cod_formulario)? "selected": "";
                                ?>
                                <option value="<?=$cod_formulario ?>" <?=$selected ?>><?=$nome ?></option>
                                <?php
                            }
                        ?>
                        </select>
                    </div>            
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="data_solicitacao">Dt&nbsp;Solicitação</label>  
                        <input type="text" name="data_solicitacao" id="data_solicitacao" maxlength="10" value="<?=@$var_data_solicitacao_;?>" class="form-control input-date">
                    </div>            
                </div>            
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="data_vencimento">Dt&nbsp;Vencimento</label>  
                        <input type="text" name="data_vencimento" id="data_vencimento" maxlength="10" value="<?=@$var_data_vencimento_;?>" class="form-control input-date">
                    </div>            
                </div>            
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="control-label" for="texto">Texto</label>
                        <textarea class="form-control" rows="5" name="texto" id="texto"><?=@$var_texto;?></textarea>
                    </div>            
                </div>
            </div> 
        </div> 
        <?php
    }
    if ($operacao=="editar"){
        ?>
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-12">
                    <h4>Cliente</h4>
                    <hr>
                </div>
                <div class="col-sm-12">
                    <div class="div-table">
                        <table class="tablebah">
                            <thead>
                                <tr>
                                    <th width="*">Cliente</th>
                                    <th width="*">Arquivo</th>
                                    <th width="*">Formulário</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($dados_listarcliente as $value){
                                        ?>
                                        <tr>
                                            <td><?=$value["cliente"]?></td>
                                            <td>
                                            <?php
                                                //var_dump($dados_arquivos); exit;
                                                foreach( $dados_arquivos as $value_ ){
                                                    if ($value["cod_cliente"]==$value_["cod_cliente"]){
                                                        $arquivo ="./data/".md5($value_["cod_interno"])."/".$value_["arquivo"];
                                                        //echo "Arquivo $arquivo";
                                                        if (file_exists($arquivo) and $value_["arquivo"]<> ""){
                                                            $arquivo =LOCAL."data/".md5($value_["cod_interno"])."/".$value_["arquivo"];
                                                            ?>
                                                            <a href="<?=$arquivo?>" class="btn btn-sub" target="_blank"><?=$value_["nome_original"]?> <?=$value_["data_c_"]?></a>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                           
                                            </td>
                                            <td>
                                                <span class="span-texto">
                                                <?=$value["formulario"]?>
                                                </span>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="pull-right col-auditoria">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Criação:&nbsp;</strong><?=@$var_usuario_c?> em <?=@$var_data_c_?>&nbsp; 
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Alteração:&nbsp;</strong><?=@$var_usuario_a?> em <?=@$var_data_a_?>
            </div>
        </div>
        <?php
    }

    if (($operacao=="ver")){
        ?>
        <div class="col-sm-12 div-form">
            <div class="row">
                <input type="hidden" name="cod_coletacliente" id="cod_coletacliente" value="<?=@$var_cod_coletacliente;?>">
                <input type="hidden" name="cod_coleta" id="cod_coleta" value="<?=@$var_cod_coleta;?>">
                <input type="hidden" name="cod_pasta" id="cod_pasta" value="<?=@$var_cod_pasta;?>">
                <input type="hidden" name="cod_interno" id="cod_interno" value="<?=@$var_cod_interno;?>">
                <div class="col-sm-12">
                    <div class="form-group">
                        <h2><?=@$var_texto;?></h2>
                        <p><?=@$var_cod_pasta;?> Dt&nbsp;Solicitado: <?=@$var_data_solicitacao_;?> Dt&nbsp;Vencimento: <?=@$var_data_vencimento_;?>
                    </div>            
                </div>
            </div> 
        </div> 

        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-12">
                    <h2><?=@$var_formulario_titulo;?></h2>
                </div>
            </div>
            <div class="row">
                <input type="hidden" name="cod_formulario" id="cod_formulario" value="<?=@$var_cod_formulario;?>">
                <div class="col-sm-8">
                    <div class="row">
                        <?php
                        
                        if (@$var_cod_formulario){
                            foreach( $dados_campos as $value ){
                                ?>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label" for="formulario_<?=$value["nome"]?>"><?=$value["label"]?></label> 
                                        <?php
                                        if ($value["ajuda"]){
                                            ?>
                                            <span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?=$value["ajuda"]?>"><span class="glyphicon glyphicon-question-sign" aria-hidden="true" style="color:#4c637b;"></span></span>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                            switch( $value["tipo"] ){
                                                case 'info': 
                                                    ?>
                                                    <hr>
                                                    <?php
                                                break;
                                                case 'select': 
                                                    $opcoes = explode(",",$value["opcoes"]);
                                                    ?>
                                                    <select class="form-control" name="formulario_<?=$value["nome"]?>" id="formulario_<?=$value["nome"]?>">
                                                    <option value="NULL">Selecione...</option>
                                                    <?php foreach( $opcoes as $value_ ){ ?>
                                                        <option value="<?=$value_?>"><?=$value_?></option>
                                                    <?php } ?>
                                                    </select>
                                                    <?php
                                                break;
                                                case 'date': 
                                                    ?>
                                                    <input type="text" name="formulario_<?=$value["nome"]?>" id="formulario_<?=$value["nome"]?>" maxlength="10" value="" class="form-control input-date">
                                                    <?php
                                                break;
                                                default:
                                                    ?>
                                                    <input type="text" name="formulario_<?=$value["nome"]?>" id="formulario_<?=$value["nome"]?>" maxlength="200" value="" class="form-control">
                                                    <?php
                                                break;
                                            }
                                        ?>                                    
                                    </div>            
                                </div>                            
                                <?php
                            }
                        }
                    ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?=@$var_formulario;?>
                            </div>                            
                        </div>                            
                    </div>
                </div>
                
            </div> 
        </div> 

        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Anexo</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="div-table">
                        <div class="col-sm-2">
                            <center><span class="glyphicon glyphicon-cloud-upload" aria-hidden="true" style="font-size:40px"></span></center>
                        </div>
                        <div class="col-sm-10">
                            <input type="file" name="files" id="files">
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="div-table">
                        <table class="tablebah" id="upload_resultado">
                            <thead>
                                <tr>
                                    <th width="3%">&nbsp;</th>
                                    <th width="*">Arquivo</th>
                                    <th width="*">Dt Envio</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    //var_dump($dados_arquivos); exit;
                                    if (@$dados_arquivos){
                                        foreach( $dados_arquivos as $value ){
                                            ?>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td><?=$value["nome_original"]?></td>
                                                <td><?=$value["data_c_"]?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div> 

        

        <?php
    }
    
?>


