<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	//if ($this->session->userdata('root')=="S") $this->output->enable_profiler(TRUE);   
    
	$online = $this->functions->usuario_online(); 
	
    $class_login = "";
    if( $this->session->userdata('logado') !== true ) {
        $class_login = "login";
    }

    //$menu[]=array("tipo"=>"HR","admin"=>"","nome"=>"","href"=>"","icone"=>"");
    $menu[]=array("menu"=>"00","sub"=>"","tipo"=>"T","admin"=>"","nome"=>"Painel","controller"=>"painel","href"=>LOCAL."painel/resumo","icone"=>"glyphicon-th-large");
    $menu[]=array("menu"=>"01","sub"=>"","tipo"=>"T","admin"=>"","nome"=>"Drive","controller"=>"drive","href"=>LOCAL."drive","icone"=>"glyphicon-folder-open");
    $menu[]=array("menu"=>"02","sub"=>"","tipo"=>"T","admin"=>"","nome"=>"Entrega","controller"=>"entrega","href"=>LOCAL."entrega/entrega","icone"=>"glyphicon-cloud-upload");
    $menu[]=array("menu"=>"03","sub"=>"","tipo"=>"T","admin"=>"","nome"=>"Coleta","controller"=>"coleta","href"=>LOCAL."coleta/listar","icone"=>"glyphicon-cloud-download");
    $menu[]=array("menu"=>"04","sub"=>"","tipo"=>"T","admin"=>"","nome"=>"Serviço","controller"=>"servico","href"=>LOCAL."servico/listar","icone"=>"glyphicon-check");
    $menu[]=array("menu"=>"05","sub"=>"","tipo"=>"T","admin"=>"","nome"=>"Notificação","controller"=>"notificacao","href"=>LOCAL."notificacao/listar","icone"=>"glyphicon-bullhorn");
    $menu[]=array("menu"=>"06","sub"=>"","tipo"=>"T","admin"=>"","nome"=>"Cliente","controller"=>"","","icone"=>"glyphicon-king");
    $menu[]=array("menu"=>"07","sub"=>"06","tipo"=>"T","admin"=>"","nome"=>"Cadastro","controller"=>"cliente","href"=>LOCAL."cliente/listar","icone"=>"glyphicon-king");
    $menu[]=array("menu"=>"08","sub"=>"06","tipo"=>"T","admin"=>"","nome"=>"Evento","controller"=>"cliente","href"=>LOCAL."cliente/evento","icone"=>"glyphicon-king");
    $menu[]=array("menu"=>"09","sub"=>"06","tipo"=>"T","admin"=>"","nome"=>"Funcionário","controller"=>"funcionario","href"=>LOCAL."funcionario/listar","icone"=>"glyphicon-pawn");
    $menu[]=array("menu"=>"10","sub"=>"","tipo"=>"T","admin"=>"S","nome"=>"Usuário","controller"=>"usuario","href"=>LOCAL."usuario/listar","icone"=>"glyphicon-lock");
    $menu[]=array("menu"=>"11","sub"=>"","tipo"=>"T","admin"=>"","nome"=>"Relatório","controller"=>"","href"=>"","icone"=>"glyphicon-list-alt");
    $menu[]=array("menu"=>"12","sub"=>"11","tipo"=>"T","admin"=>"","nome"=>"Diverso","controller"=>"relatorio","href"=>LOCAL."relatorio/diverso","icone"=>"glyphicon-list-alt");
    $menu[]=array("menu"=>"13","sub"=>"11","tipo"=>"T","admin"=>"","nome"=>"Cliente Evento","controller"=>"relatorio","href"=>LOCAL."relatorio/cliente_evento","icone"=>"glyphicon-list-alt");
    $menu[]=array("menu"=>"14","sub"=>"","tipo"=>"T","admin"=>"","nome"=>"Auxiliar","controller"=>"","","icone"=>"glyphicon-list-alt");
    $menu[]=array("menu"=>"15","sub"=>"14","tipo"=>"T","admin"=>"","nome"=>"Alvará","controller"=>"alvara","href"=>LOCAL."alvara/listar","icone"=>"glyphicon-th-list");
    $menu[]=array("menu"=>"16","sub"=>"14","tipo"=>"T","admin"=>"","nome"=>"Banco","controller"=>"banco","href"=>LOCAL."banco/listar","icone"=>"glyphicon-th-list");
    $menu[]=array("menu"=>"17","sub"=>"14","tipo"=>"T","admin"=>"","nome"=>"Evento","controller"=>"evento","href"=>LOCAL."evento/listar","icone"=>"glyphicon-th-list");
    $menu[]=array("menu"=>"18","sub"=>"14","tipo"=>"T","admin"=>"","nome"=>"E-mail","controller"=>"email","href"=>LOCAL."email/listar","icone"=>"glyphicon-th-list");
    $menu[]=array("menu"=>"19","sub"=>"14","tipo"=>"T","admin"=>"","nome"=>"Feriado","controller"=>"feriado","href"=>LOCAL."feriado/listar","icone"=>"glyphicon-th-list");
    $menu[]=array("menu"=>"20","sub"=>"14","tipo"=>"T","admin"=>"","nome"=>"Formulário","controller"=>"formulario","href"=>LOCAL."formulario/listar","icone"=>"glyphicon-th-list");
    $menu[]=array("menu"=>"21","sub"=>"14","tipo"=>"T","admin"=>"","nome"=>"Links","controller"=>"link","href"=>LOCAL."link/listar","icone"=>"glyphicon-th-list");
    $menu[]=array("menu"=>"22","sub"=>"14","tipo"=>"T","admin"=>"","nome"=>"Log","controller"=>"log","href"=>LOCAL."log/listar","icone"=>"glyphicon-th-list");
    $menu[]=array("menu"=>"23","sub"=>"14","tipo"=>"T","admin"=>"","nome"=>"Pasta","controller"=>"pasta","href"=>LOCAL."pasta/listar","icone"=>"glyphicon-th-list");
    $menu[]=array("menu"=>"24","sub"=>"14","tipo"=>"T","admin"=>"","nome"=>"Setor","controller"=>"setor","href"=>LOCAL."setor/listar","icone"=>"glyphicon-th-list");
    $menu[]=array("menu"=>"25","sub"=>"14","tipo"=>"T","admin"=>"","nome"=>"Tarefa","controller"=>"tarefa","href"=>LOCAL."tarefa/listar","icone"=>"glyphicon-th-list");
    $menu[]=array("menu"=>"26","sub"=>"14","tipo"=>"T","admin"=>"","nome"=>"Template","controller"=>"template","href"=>LOCAL."template/listar","icone"=>"glyphicon-th-list");

    $menu[]=array("menu"=>"27","sub"=>"","tipo"=>"C","admin"=>"","nome"=>"Painel","controller"=>"painel","href"=>LOCAL."painel/resumo","icone"=>"glyphicon-th-large");
    $menu[]=array("menu"=>"28","sub"=>"","tipo"=>"C","admin"=>"","nome"=>"Drive","controller"=>"drive","href"=>LOCAL."drive/".$this->session->userdata('cliente'),"icone"=>"glyphicon-folder-open");
    $menu[]=array("menu"=>"29","sub"=>"","tipo"=>"C","admin"=>"","nome"=>"Estratégia","controller"=>"cliente","href"=>LOCAL."cliente/estrategia","icone"=>"glyphicon-tower");
    $menu[]=array("menu"=>"30","sub"=>"","tipo"=>"C","admin"=>"","nome"=>"Coleta","controller"=>"coleta","href"=>LOCAL."coleta/listar","icone"=>"glyphicon-cloud-download");
    $menu[]=array("menu"=>"31","sub"=>"","tipo"=>"C","admin"=>"","nome"=>"Serviço","controller"=>"servico","href"=>LOCAL."servico/listar","icone"=>"glyphicon-check");
    $menu[]=array("menu"=>"32","sub"=>"","tipo"=>"C","admin"=>"","nome"=>"Equipe","controller"=>"equipe","href"=>LOCAL."equipe/ver","icone"=>"glyphicon-education");
    $menu[]=array("menu"=>"33","sub"=>"","tipo"=>"C","admin"=>"","nome"=>"Notificação","controller"=>"notificacao","href"=>LOCAL."notificacao/cliente","icone"=>"glyphicon-bullhorn"); 
    $menu[]=array("menu"=>"34","sub"=>"","tipo"=>"C","admin"=>"","nome"=>"Úteis","controller"=>"","href"=>"","icone"=>"glyphicon-tower");
    $menu[]=array("menu"=>"35","sub"=>"34","tipo"=>"C","admin"=>"","nome"=>"Certidão","controller"=>"certidao","href"=>LOCAL."certidao/listar","icone"=>"glyphicon-list-alt");
    $menu[]=array("menu"=>"36","sub"=>"34","tipo"=>"C","admin"=>"","nome"=>"Feriado","controller"=>"feriado","href"=>LOCAL."feriado/cliente","icone"=>"glyphicon-calendar");
    $menu[]=array("menu"=>"37","sub"=>"","tipo"=>"C","admin"=>"S","nome"=>"Usuário","controller"=>"usuario","href"=>LOCAL."usuario/listar","icone"=>"glyphicon-lock");
    //print_r ($menu); exit;

?>
<!DOCTYPE html>

<html lang="pt-br">

    <head>

        <meta charset="utf-8" />
    
        <title>TripleA</title>
        <meta name="author" content="Gane Tecnologia. Ramos Tecnologia da Informação. www.ganeti.com.br" />
        <meta name="keywords" content="TripleA, Contabilidade" />
        <meta name="robots" content="noindex,nofollow,noarchive,noimageindex" />
        <meta name="description" content="TripleA. BAH Businness Anywhere" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <!-- Bootstrap -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" rel="stylesheet">
        <link href="<?=LOCAL ?>web/bootstrap-multiselect.css" type="text/css" rel="stylesheet"> 
        <link href="<?=LOCAL ?>web/jquery.fileuploader.css" media="all" rel="stylesheet"> 
        <link href="<?=LOCAL ?>web/ie10-viewport-bug-workaround.css" rel="stylesheet"> 
        
        <!-- Referências internas -->
        <link href="<?=LOCAL ?>web/favicon.png" rel="shortcut icon" />
        <link href="<?=LOCAL ?>web/s_pagina.css?v=<?=time();?>" rel="stylesheet" />

        <!-- Hotjar Tracking Code for https://bah.tripleaie.com.br/ -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:1356624,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>
    </head>
    
    <body class="<?=$class_login; ?>">
        

        <form name="frmFiltro" id="frmFiltro" method="get"></form>
        
        <form name="frm" id="frm" method="post" autocomplete="off" enctype="multipart/form-data">
        <input type="hidden" id="local" name="local" value="<?=LOCAL; ?>">
        <input type="hidden" id="orderby_column" name="orderby_column" value="<?=$this->input->get_post('orderby_column')?$this->input->get_post('orderby_column'):1?>">
        <input type="hidden" id="orderby_order" name="orderby_order" value="<?=$this->input->get_post('orderby_order')?$this->input->get_post('orderby_order'):"ASC"?>">
        <input type="hidden" id="tipo" name="tipo" value="<?=$this->session->userdata('tipo'); ?>">
        <input type="hidden" id="from" name="from" value="BAH">
        
		<?php 
            if( $this->session->userdata('logado') <> true ) {
				$this->load->view($view);
			} else {
				?>
				<!--Cabeçalho--> 
                <div class="aside">
                    <div class="aside-header">
                        <center><a href="<?=LOCAL?>"><img src="<?=LOCAL.LOGO_SISTEMA ?>" width="150px"></a></center>
                    </div>
                    <?php
                        if ($this->session->userdata('tipo')=="C"){
                            ?>
                            <div class="aside-client">
                                <select class="form-control select-cliente" data-logincontroller="" data-loginevent="">
                                <?php
                                    $dados_clientes=$this->session->userdata('clientes');
                                    foreach($dados_clientes as $value){
                                        if ($value["ativo"]=='S'){
                                            $cod_cliente = $value["cod_cliente"];
                                            $cod_interno = $value["cod_interno"];
                                            $nome = $value["nome"];
                                            $uf = $value["uf"];
                                            $cidade = $value["cidade"];

                                            $selected = ($this->session->userdata('cliente')==$cod_cliente)? "selected": "";
                                            
                                            //if ($this->session->userdata('cliente')==""){$this->session->set_userdata( 'cliente', $cod_cliente); }
                                            ?>
                                            <option value="<?=$cod_cliente ?>~<?=$uf?>~<?=$cidade?>" <?=$selected ?>><?=$nome ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                                </select>
                                </div>
                        <?php
                        }
                    ?>
                    <div class="aside-body">
                        <ul class="ul-menu">
                            <?php
                                foreach($menu as $value){
                                    if ($value["tipo"] == $this->session->userdata('tipo') and $value["sub"] == "" and ($value["admin"] == "" or ($value["admin"] == "S" and $this->session->userdata('admin')=="S"))){
                                        $li_selecionado=$value["controller"]==$this->uri->segment("1")?"background-color: #eceff1;":"";
                                        $a_selecionado=$value["controller"]==$this->uri->segment("1")?"color: #5db230!important;":"";
                                        $href = @$value["href"]<>""?"href=\"".@$value["href"]."\"":"";
                                        echo "<li style=\"$li_selecionado\"><a {$href} class=\"a-menu\" style=\"$a_selecionado\"><span class=\"glyphicon ".$value["icone"]."\" aria-hidden=\"true\"></span>&nbsp;&nbsp;&nbsp;&nbsp;".$value["nome"]."</a>";
                                        echo "<ul>";
                                        foreach($menu as $value_){
                                            $li_selecionado=$value["controller"]==$this->uri->segment("1")?"background-color: #eceff1;":"";
                                            $a_selecionado=$value["controller"]==$this->uri->segment("1")?"color: #5db230!important;":"";
                                            if ($value_["tipo"] == $this->session->userdata('tipo') and $value_["sub"] == $value["menu"] and ($value_["admin"] == "" or ($value_["admin"] == "S" and $this->session->userdata('admin')=="S"))){
                                                //echo "<li><a href=\"".$value["href"]."\" class=\"a-menu\"><i class=\"".$value["icone"]."\"></i>&nbsp;&nbsp;&nbsp;&nbsp;".$value["nome"]."</a></li>";
                                                echo "<li style=\"$li_selecionado\"><a href=\"".$value_["href"]."\" class=\"a-menu\" style=\"$a_selecionado\"><span class=\"glyphicon ".$value_["icone"]."\" aria-hidden=\"true\"></span>&nbsp;&nbsp;&nbsp;&nbsp;".$value_["nome"]."</a></li>";
                                            }
                                        }
                                        echo "</ul>";
                                        echo "</li>";
                                    }
                                }
                                
                                /*Cadastros auxiliares
                                if ($this->session->userdata('tipo')=="T"){
                                    echo "<li><a href=\"#\" class=\"a-menu\"><span class=\"glyphicon glyphicon-th-list\" aria-hidden=\"true\"></span>&nbsp;&nbsp;&nbsp;&nbsp;Auxiliar</a>";
                                    echo "<ul>";
                                    foreach($menu as $value){
                                        $li_selecionado=$value["controller"]==$this->uri->segment("1")?"background-color: #eceff1;":"";
                                        $a_selecionado=$value["controller"]==$this->uri->segment("1")?"color: #5db230!important;":"";
                                        if ($value["tipo"] == $this->session->userdata('tipo') and $value["aux"] == "S" and ($value["admin"] == "" or ($value["admin"] == "S" and $this->session->userdata('admin')=="S"))){
                                            //echo "<li><a href=\"".$value["href"]."\" class=\"a-menu\"><i class=\"".$value["icone"]."\"></i>&nbsp;&nbsp;&nbsp;&nbsp;".$value["nome"]."</a></li>";
                                            echo "<li style=\"$li_selecionado\"><a href=\"".$value["href"]."\" class=\"a-menu\" style=\"$a_selecionado\"><span class=\"glyphicon ".$value["icone"]."\" aria-hidden=\"true\"></span>&nbsp;&nbsp;&nbsp;&nbsp;".$value["nome"]."</a></li>";
                                        }
                                    }
                                    echo "</ul>";
                                    echo "</li>";
                                }
                                */
                            ?>
                        </ul>
                        <ul class="ul-menu responsive-show">
                            <li role="separator" class="divider"></li>
                            <li><a href="<?=LOCAL."senha/nova"?>" class="a-menu">&nbsp;<span class="glyphicon glyphicon-lock" aria-hidden="true"></span></i>&nbsp; Senha</a></li>
                            <li><a href="<?=LOCAL."suporte/dados"?>" class="a-menu">&nbsp;<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>&nbsp; Suporte</a></li>
                            <li><a href="<?=LOCAL."login/sair"?>" class="a-menu">&nbsp;<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>&nbsp; Sair</a></li>
                        </ul>
                    </div>
                    <div class="aside-footer">
                        <center><img src="<?=LOCAL.LOGO_TRIPLEAIE ?>" width="100px"></center>
                    </div>
                </div>
                <div class="wrapper" id="wrapper">
                    <div id="div-loading"><img src="<?=LOCAL."web/loading.gif" ?>"></div>
                    <div class="navbar">
                        <div class="row">
                            <div class="col-sm-4 navbar-title">
                                <div class="navbar-title-bars">
                                    <a href="#" id="menu"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></i></a>
                                </div>
                                <div class="navbar-title-h1">
                                    <h1><?=@$titulo?></h1>
                                </div>
                            </div>
                            <div class="col-sm-8 navbar-info responsive-hide">
                                <div class="navbar-info-sair">
                                    <a class="a-acao a-acao-menu a-acao-menuresponsivo" data-objeto="<?=LOCAL."login"?>" data-acao="sair" title="Sair"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>&nbsp; Sair</a>
                                </div>
                                <div class="navbar-info-usuario">
                                    <a class="btn-usuario" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >&nbsp;<span class="caret" style="color:#FFF;"></span>&nbsp;<?=$this->session->userdata('nome')?></a>
                                    <ul class="dropdown dropdown-menu dropdown-usuario" aria-labelledby="dropdownMenu1">
                                        <li><a class="a-acao" data-objeto="<?=LOCAL."senha"?>" data-acao="nova">&nbsp;<span class="glyphicon glyphicon-lock" aria-hidden="true"></span></i>&nbsp; Senha</a></li>
                                        <li><a class="a-acao" data-objeto="<?=LOCAL."suporte"?>" data-acao="dados">&nbsp;<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>&nbsp; Suporte</a></li>
                                    </ul>
                                </div>
                                <?php
                                    if ($this->session->userdata('tipo')=="C"){
                                        ?>
                                        <div class="navbar-info-cliente">
                                            <select class="form-control select-cliente" data-logincontroller="" data-loginevent="">
                                            <?php
                                            
                                                foreach($dados_clientes as $value){
                                                    if ($value["ativo"]=='S'){
                                                        $cod_cliente = $value["cod_cliente"];
                                                        $cod_interno = $value["cod_interno"];
                                                        $nome = $value["nome"];
                                                        $uf = $value["uf"];
                                                        $cidade = $value["cidade"];

                                                        $selected = ($this->session->userdata('cliente')==$cod_cliente)? "selected": "";
                                                        
                                                        //if ($this->session->userdata('cliente')==""){$this->session->set_userdata( 'cliente', $cod_cliente); }
                                                        ?>
                                                        <option value="<?=$cod_cliente ?>~<?=$uf?>~<?=$cidade?>" <?=$selected ?>><?=$nome?></option>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                            </select>
                                        </div>
                                        <?php
                                    }                                      
                                    if ($this->session->userdata('tipo')=="T"){
                                        ?>
                                        <div class="navbar-info-correio">
                                            <a class="a-acao-menu a-acao-correio"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>&nbsp; Conferir Correio</a>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                        <?php
                                    if ($this->session->userdata('tipo')=="T"){
                                        ?>
                                        <div class="navbar-info-docuso">
                                            <a href="https://docs.google.com/document/d/1UaORo3eK6IsZGvXwri0lTV29x61T6kOApHzRr9qbiHg/edit?usp=sharing" class="a-acao-menu a-acao-docuso" target="_blank"><span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp; Documentação de Uso</a>
                                        </div>
                                        <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </div>    
                    <div class="container-fluid">
                        <?php $this->load->view($view); ?>
                        <?php
                            if ($this->session->userdata('cod_usuario')=="1"){
                                ?>
                                <div class="col-sm-12 div-operacao">
                                    <div class="row">
                                        <div class="pull-right col-auditoria">
                                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>SERVER_NAME:&nbsp;</strong><?=$_SERVER['SERVER_NAME']; ?>&nbsp;
                                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>HTTP_HOST:&nbsp;</strong><?=$_SERVER['HTTP_HOST']; ?>&nbsp;
                                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>ENVIRONMENT:&nbsp;</strong><?=ENVIRONMENT; ?>&nbsp;
                                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>HOSTNAME:&nbsp;</strong><?=HOSTNAME.".".DATABASE; ?>&nbsp;
                                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>CONTROLLER:&nbsp;</strong><?=$this->uri->segment("1"); ?>&nbsp;
                                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>METHOD:&nbsp;</strong><?=$this->uri->segment("2"); ?>&nbsp;
                                            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>On-Line:&nbsp;</strong><?=$this->session->userdata('cliente'); ?>&nbsp;
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        ?>
                    </div>
                </div>
				<?php
			}
		?>

        
        <a id="id-backtop" class="btn id-backtop" role="button" title="Voltar para o topo"><span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a>
        
        </form>
        
        <script src="<?=LOCAL ?>web/jquery-3.2.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="<?=LOCAL ?>web/bootstrap-multiselect.js" type="text/javascript"></script>
        <script src="<?=LOCAL ?>web/jquery.maskedinput.min.js" type="text/javascript"></script>
        <script src="<?=LOCAL ?>web/jquery.fileuploader.js" type="text/javascript"></script>
        <script src="https://www.gstatic.com/charts/loader.js" type="text/javascript"></script>
        <script src="<?=LOCAL ?>web/bootbox.min.js" type="text/javascript"></script>
        <script src="<?=LOCAL ?>web/date_picker.min.js" type="text/javascript"></script>
        <script src="<?=LOCAL ?>web/j_custom.js?v=<?=time();?>" type="text/javascript"></script>
        
        <script>
            
        </script>
        
    </body>

</html>
