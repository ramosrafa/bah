<?php 
    if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 

    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
    //var_dump($dados["evento_cliente"]);exit;
?>
<div class="col-sm-12 div-operacao">
    <div class="row">
        <div class="pull-right">
            <a class="btn a-acao" data-objeto="<?=LOCAL ?>cliente" data-acao="evento">Buscar</a>
        </div>
        <div class="pull-right div-operacao-text">
            <input type="text" class="input-text" name="busca_evento" id="busca_evento" placeholder="Evento" value="<?=$this->input->get_post('busca_evento')?>">
        </div>
        <div class="pull-right div-operacao-text">
            <input type="text" class="input-text" name="busca_cliente" id="busca_cliente" placeholder="Cliente" value="<?=$this->input->get_post('busca_cliente')?>">
        </div>
        <div class="pull-right div-operacao-text">
            <input type="text" class="input-mes" name="busca_data" id="busca_data" placeholder="Mês/Ano" value="<?=@$dados["mes"]."/".$dados["ano"] ?>">
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="div-table">
        <div class="div-table-fixed">
            <table class="tablebah tablebah-fixed">
                <thead>
                    <tr>
                        <th width="*" class="" title="Cliente"></th>&nbsp;</th>
                        <?php
                            if (@$dados["evento"]){
                                foreach($dados["evento"] as $value_evento){
                                    ?>
                                    <th colspan="2" width="200px" class="" title="<?=$value_evento["nome"]?>">
                                        <a class="a-acao" data-objeto="<?=LOCAL ?>evento" data-acao="editar" data-cod="<?=$value_evento["cod_evento"]?>">&nbsp;&nbsp;&nbsp;<?=$value_evento["sigla"]?>&nbsp;&nbsp;&nbsp;</a>
                                        <br>
                                        <?php
                                            echo "<span>&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;";
                                            echo "<span title=\"Calendário\">D</span>";
                                            echo @$value_evento["dia"]."";
                                            if (@$value_evento["calendario"]=="S"){
                                                echo "&nbsp;-&nbsp;<span title=\"Calendário\">C&nbsp;</span>";
                                            }
                                            echo "&nbsp;)</span>";
                                        ?>
                                    </th>
                                    <?php
                                }
                            }
                        ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($dados["cliente"] as $value_cliente){
                            ?>
                            <tr>
                                <td>
                                    <?=$value_cliente["nome"]?> (<?=$value_cliente["cod_interno"]?>)
                                </td>
                                <?php
                                    foreach($dados["evento"] as $value_evento){
                                        ?>
                                        <td width="100px" class="td-tablebah" id="<?=$value_evento["cod_evento"].$value_cliente["cod_cliente"]?>"
                                                                data-objeto="<?=LOCAL ?>cliente" 
                                                                data-acao="associar_evento" 
                                                                data-cod-cliente="<?=$value_cliente["cod_cliente"]?>" 
                                                                data-nome-cliente="<?=$value_cliente["nome"]?>" 
                                                                data-cod-evento="<?=$value_evento["cod_evento"]?>" 
                                                                data-sigla-evento="<?=$value_evento["sigla"]?>" 
                                                                data-cod-clienteevento="<?=@$dados["evento_cliente"][$value_cliente["cod_cliente"]][$value_evento["cod_evento"]]['cod_clienteevento']?>">
                                            <?php
                                                if (@$dados["evento_cliente"][$value_cliente["cod_cliente"]][$value_evento["cod_evento"]]){
                                                    echo "<span>&nbsp;&nbsp;&nbsp;S&nbsp;</span>";
                                                }
                                                if (@$dados["evento_cliente"][$value_cliente["cod_cliente"]][$value_evento["cod_evento"]]["data_entrega_"]){
                                                    //echo "<span>&nbsp;e&nbsp;</span>".$dados["evento_cliente"][$value_cliente["cod_cliente"]][$value_evento["cod_evento"]]["data_entrega_"];
                                                }
                            
                                            ?>
                                        </td>
                                        <td width="100px" id="<?=$value_evento["cod_evento"].$value_cliente["cod_cliente"]?>_arquivo">
                                            &nbsp;
                                            <?php
                                            if (@$dados["evento_cliente"][$value_cliente["cod_cliente"]][$value_evento["cod_evento"]] and @$dados["evento_cliente"][$value_cliente["cod_cliente"]][$value_evento["cod_evento"]]["arquivo"]){
                                                echo "<a class=\"a-acao-d\" data-objeto=\"".LOCAL."arquivo"."\" data-acao=\"a\" data-a=\"".$dados["evento_cliente"][$value_cliente["cod_cliente"]][$value_evento["cod_evento"]]["arquivo"]."\" title=\"".$dados["evento_cliente"][$value_cliente["cod_cliente"]][$value_evento["cod_evento"]]["data_entrega_"]."\">&nbsp;&nbsp;<span class=\"glyphicon glyphicon-cloud-download\" aria-hidden=\"true\"></span>&nbsp;&nbsp;</a>";
                                            }
                                            ?>
                                        </td>
                                        <?php
                                    }
                                ?>
                            </tr>
                            <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>