<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
?>

<div class="col-sm-12 div-operacao">
    <div class="row">
        <?php
            if ($operacao=="listar"){
                if ($this->session->userdata('tipo')=="T"){
                    ?>
                    <div class="pull-right">
                        <a class="btn a-acao" data-objeto="<?=LOCAL ?>servico" data-acao="novo">+ Adicionar</a>
                    </div>
                    <?php
                }
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>servico" data-acao="listar">Buscar</a>
                </div>
                <div class="pull-right div-painel-busca-check">
                    <input type="checkbox" class="form-check-input" name="busca_concluido" id="busca_concluido" value="S" <?php if (@$this->input->get_post('busca_concluido')=="S") echo "checked"; ?>><label class="form-check-label" for="busca_concluido">&nbsp;Concluídos&nbsp;&nbsp;&nbsp;</label>
                </div>
                <div class="pull-right div-painel-busca-check">
                    <input type="checkbox" class="form-check-input" name="busca_suspenso" id="busca_suspenso" value="S" <?php if (@$this->input->get_post('busca_suspenso')=="S") echo "checked"; ?>><label class="form-check-label" for="busca_suspenso">&nbsp;Suspensos&nbsp;&nbsp;&nbsp;</label>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
                <?php
            }
            if ($operacao=="novo"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>servico" data-acao="inserir" data-validacao="">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>servico" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
            if ($operacao=="editar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>servico" data-acao="salvar" data-validacao="" data-cod="<?=@$var_cod_servico;?>">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>servico" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
            if ($operacao=="ver"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>servico" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
        ?>
    </div>
</div>

<?php
    if ($operacao=="listar"){
        ?>
        <div class="col-sm-12">
            <div class="div-table">
                <div class="div-table-fixed">
                    <table class="tablebah">
                        <thead>
                            <tr>
                                <?php
                                    echo $this->functions->table_column("1","cliente","Cliente","*","a-acao");
                                    echo $this->functions->table_column("","cod_pasta","Pasta","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","tarefa","Tarefa","*","a-acao");
                                    echo $this->functions->table_column("","data_inicio_","Dt&nbsp;Início","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","data_previsao_","Dt&nbsp;Previsão","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","data_conclusao_","Dt&nbsp;Conclusão","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","data_suspensao_","Dt&nbsp;Suspensão","*","tdoculta a-acao");
                                ?>
                                <?php if ($this->session->userdata('tipo')=="T"){ ?>
                                    <th width="3%">&nbsp;</th>
                                    <th width="3%">&nbsp;</th>
                                    <th width="3%">&nbsp;</th>
                                <?php } ?>
                                <?php if ($this->session->userdata('tipo')=="C"){ ?>
                                    <th width="3%">&nbsp;</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($dados as $value){
                                    ?>
                                    <?php if ($this->session->userdata('tipo')=="T"){ ?>
                                        <tr class="tr-linha" data-objeto="<?=LOCAL ?>servico" data-acao="editar" data-cod="<?=$value["cod_servico"] ?>">
                                    <?php } ?>
                                    <?php if ($this->session->userdata('tipo')=="C"){ ?>
                                        <tr class="tr-linha" data-objeto="<?=LOCAL ?>servico" data-acao="ver" data-cod="<?=$value["cod_servico"] ?>">
                                    <?php } ?>
                                        <td class=""><?=$value["cliente"]?></td>
                                        <td class="tdoculta "><?=$value["cod_pasta"]?></td>
                                        <td class=""><?=$value["tarefa"]?></td>
                                        <td class="tdoculta "><?=$value["data_inicio_"]?></td>
                                        <td class="tdoculta "><?=$value["data_previsao_"]?></td>
                                        <td class="tdoculta "><?=$value["data_conclusao_"]?></td>
                                        <td class="tdoculta "><?=$value["data_suspensao_"]?></td>
                                        <?php if ($this->session->userdata('tipo')=="T"){ ?>
                                            <td><a class="btn btn-editar a-acao" data-objeto="<?=LOCAL ?>servico" data-acao="editar" data-cod="<?=$value["cod_servico"] ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                            <td><a class="btn btn-ver a-acao" data-objeto="<?=LOCAL ?>servico" data-acao="ver" data-cod="<?=$value["cod_servico"] ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                                            <td><a class="btn btn-excluir a-acao" data-objeto="<?=LOCAL ?>servico" data-acao="excluir" data-cod="<?=$value["cod_servico"] ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></td>
                                        <?php } ?>
                                        <?php if ($this->session->userdata('tipo')=="C"){ ?>
                                            <td><a class="btn btn-ver a-acao" data-objeto="<?=LOCAL ?>servico" data-acao="ver" data-cod="<?=$value["cod_servico"] ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                                        <?php } ?>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>        
                </div>    
            </div>    
            <div class="pull-right col-paginacao">
                <?=$paginacao; ?>
            </div>
        </div>
        <?php
    }
    if (($operacao=="novo")or($operacao=="editar")){
        ?>
        <div class="col-sm-12 div-form">
            <div class="row">
                <input type="hidden" name="cod_servico" id="cod_servico" value="<?=@$var_cod_servico;?>">
                <input type="hidden" name="cod_interno" id="cod_interno" value="<?=@$var_cod_interno;?>">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="cod_cliente">Cliente&nbsp;</label>
                        <select class="form-control servico_cod_cliente" name="cod_cliente" id="cod_cliente" <?php if ($operacao=="editar")echo "disabled" ?>>
                        <option value="NULL">Selecione...</option>
                        <?php
                            foreach($dados_cliente as $value){
                                $cod_cliente = $value["cod_cliente"];
                                $nome = $value["nome"];

                                $selected = (@$var_cod_cliente==$cod_cliente)? "selected": "";
                                ?>
                                <option value="<?=$cod_cliente ?>" <?=$selected ?>><?=$nome ?></option>
                                <?php
                            }
                        ?>
                        </select>
                    </div>            
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="cod_cliente">Pasta&nbsp;</label>
                        <select class="form-control" name="cod_pasta" id="cod_pasta" <?php if ($operacao=="editar")echo "disabled" ?>>
                        <option value="NULL">Selecione...</option>
                        <?php
                            foreach($dados_cod_pasta as $value){
                                $cod_pasta = $value["cod_pasta"];
                                $nome = $value["nome"];

                                $selected = (@$var_cod_pasta==$cod_pasta)? "selected": "";
                                ?>
                                <option value="<?=$cod_pasta ?>" <?=$selected ?>><?=$nome ?></option>
                                <?php
                            }
                        ?>
                        </select>
                    </div>            
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="cod_tarefa">Tarefa&nbsp;</label>
                        <select class="form-control servico-etapas" name="cod_tarefa" id="cod_tarefa" <?php if ($operacao=="editar")echo "disabled" ?>>
                        <option value="NULL">Selecione...</option>
                        <?php
                            foreach($dados_tarefa as $value){
                                $cod_tarefa = $value["cod_tarefa"];
                                $nome = $value["nome"];

                                $selected = (@$var_cod_tarefa==$cod_tarefa)? "selected": "";
                                ?>
                                <option value="<?=$cod_tarefa ?>" <?=$selected ?>><?=$nome ?></option>
                                <?php
                            }
                        ?>
                        </select>
                    </div>            
                </div>
                <div class="col-sm-1">
                    <div class="form-group">
                        <label class="control-label" for="data_inicio">Dt&nbsp;Início</label>  
                        <input type="text" name="data_inicio" id="data_inicio" maxlength="10" value="<?=@$var_data_inicio_;?>" class="form-control input-date">
                    </div>            
                </div>            
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="data_previsao">Dt&nbsp;Previsão&nbsp;<span  data-toggle="tooltip" data-placement="bottom" title="É a maior data de vencimento das etapas gravada ao salvar o serviço."><span class="glyphicon glyphicon-question-sign" aria-hidden="true" style="color:#4c637b;"></span></span></label>  
                        <input type="text" name="data_previsao" id="data_previsao" readonly maxlength="10" value="<?=@$var_data_previsao_;?>" class="form-control input-date">
                    </div>            
                </div>            
                <div class="col-sm-1">
                    <div class="form-group">
                        <label class="control-label" for="data_conclusao">Dt&nbsp;Conclusão</label>  
                        <input type="text" name="data_conclusao" id="data_conclusao" maxlength="10" value="<?=@$var_data_conclusao_;?>" class="form-control input-date">
                    </div>            
                </div>            
                <div class="col-sm-1">
                    <div class="form-group">
                        <label class="control-label" for="data_suspensao">Dt&nbsp;Suspensão</label>  
                        <input type="text" name="data_suspensao" id="data_suspensao" maxlength="10" value="<?=@$var_data_suspensao_;?>" class="form-control input-date">
                    </div>            
                </div>            
            
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="control-label" for="texto">Texto</label>  
                        <textarea class="form-control" rows="5" name="texto" id="texto"><?=@$var_texto;?></textarea>
                    </div>            
                </div>
            </div>
        </div>
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-10">
                    <h4>Etapas&nbsp;<span data-toggle="tooltip" data-placement="bottom" title="Para excluir uma etapa deixe o nome em branco"><span class="glyphicon glyphicon-question-sign" aria-hidden="true" style="color:#4c637b;"></span></span></h4>
                </div>       
                <div class="col-sm-2">
                    <a class="btn pull-right" id="servico_campoinserir">Inserir</a>
                </div>            
                <div class="col-sm-12">
                    <hr>
                </div>
                <div class="col-sm-12" id="etapas">
                    <div class="row">
                        <div class="col-sm-1">
                            <strong>Ordem&nbsp;<span data-toggle="tooltip" data-placement="bottom" title="É possível organizar as etapas pela ordem, podendo utilizar pontuação para sub-etapas (ex 1, 1.1, 1.2, ...)"><span class="glyphicon glyphicon-question-sign" aria-hidden="true" style="color:#4c637b;"></span></span></strong>
                        </div>       
                        <div class="col-sm-1">
                            <strong>Responsável</strong>
                        </div>       
                        <div class="col-sm-3">
                            <strong>Etapa</strong>
                        </div>       
                        <div class="col-sm-1">
                            <strong>Previsão (Dias)</strong>
                        </div>       
                        <div class="col-sm-1">
                            <strong>Dt&nbsp;Início</strong>
                        </div>       
                        <div class="col-sm-1">
                            <strong>Dt&nbsp;Previsão</strong>
                        </div>       
                        <div class="col-sm-1">
                            <strong>Dt&nbsp;Conclusão</strong>
                        </div>       
                        <div class="col-sm-1">
                            <strong>Arquivo</strong>
                        </div>       
                        <div class="col-sm-2">
                            &nbsp;
                        </div>       
                    </div>                
                    <?php
                        if (isset($dados_etapas)){
                            foreach($dados_etapas as $value){
                                ?>
                                <div class="row">
                                    <div class="col-sm-1">
                                        <input type="hidden" class="form-control" name="etapa_cod_servicoetapa[]" value="<?=@$value["cod_servicoetapa"];?>">
                                        <input type="hidden" class="form-control" name="etapa_cod_tarefaetapa[]" value="<?=@$value["cod_tarefaetapa"];?>">
                                        <input type="hidden" class="form-control" name="etapa_arquivoanexado[]" value="<?=@$value["arquivo"];?>">
                                        <input type="hidden" class="form-control" name="etapa_arquivoanexadooriginal[]" value="<?=@$value["arquivo_original"];?>">
                                        <input type="text" class="form-control" name="etapa_ordem[]" maxlength="10" value="<?=@$value["ordem"];?>" placeholder="Ordem">
                                    </div>       
                                    <div class="col-sm-1">
                                        <select class="form-control" name="etapa_tipo[]">
                                        <option value="NULL">Selecione...</option>
                                        <option value="T" <?php if ($value["tipo"]=="T") echo "selected"?> >TripleAie</option>
                                        <option value="C" <?php if ($value["tipo"]=="C") echo "selected"?> >Cliente</option>
                                        </select>
                                    </div>       
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="etapa_nome[]" maxlength="250" value="<?=@$value["nome"];?>" placeholder="Etapa">
                                    </div>       
                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" name="etapa_previsao[]" value="<?=@$value["previsao"];?>" placeholder="Previsão (Dias)">
                                    </div>       
                                    <div class="col-sm-1">
                                        <input type="text" class="form-control servico_etapa_data_inicio input-date" name="etapa_data_inicio[]" maxlength="10" value="<?=@$value["data_inicio_"];?>" placeholder="Dt&nbsp;Início" data-campo="previsao_<?=@$value["cod_tarefaetapa"];?>" data-previsao="<?=@$value["previsao"];?>">
                                    </div>       
                                    <div class="col-sm-1">
                                        <input type="text" class="form-control input-date" id="previsao_<?=@$value["cod_tarefaetapa"];?>" name="etapa_data_previsao[]" maxlength="10" value="<?=@$value["data_previsao_"];?>" placeholder="Dt&nbsp;Previsão">
                                    </div>       
                                    <div class="col-sm-1">
                                        <input type="text" class="form-control input-date" name="etapa_data_conclusao[]" maxlength="10" value="<?=@$value["data_conclusao_"];?>" placeholder="Dt&nbsp;Conclusão">
                                    </div>       
                                    <div class="col-sm-1">
                                        <input type="file" name="etapa_arquivo[]" id="etapa_arquivo[]" class="form-control">
                                    </div>       
                                    <div class="col-sm-2">
                                        
                                        <?php
                                            $arquivo ="./data/".md5(@$var_cod_interno)."/".$value["arquivo"];
                                            //echo $arquivo;
                                            if (file_exists($arquivo) and $value["arquivo"]<>""){
                                                ?>
                                                <a href="<?=LOCAL."data/".md5(@$var_cod_interno)."/".$value["arquivo"]?>" class="btn btn-sub" target="_blank"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span></a>
                                                <a class="btn btn-excluir servico_excluirarquivo" data-pasta="<?=md5(@$var_cod_interno)?>" data-servico="<?=$var_cod_servico?>" data-servicoetapa="<?=$value["cod_servicoetapa"]?>"?><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                                <?php
                                            }
                                        ?>
                                    </div>       
                                </div>
                            <?php
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="pull-right col-auditoria">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Criação:&nbsp;</strong><?=@$var_usuario_c?> em <?=@$var_data_c_?>&nbsp; 
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Alteração:&nbsp;</strong><?=@$var_usuario_a?> em <?=@$var_data_a_?>
            </div>
        </div>

        <?php
    }

    if (($operacao=="ver")){
        ?>
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <h2><?=@$var_texto;?></h2>
                        <p><?=@$var_cod_pasta;?> / <?=@$var_tarefa;?> Dt&nbsp;Início: <?=@$var_data_inicio_;?> Dt&nbsp;Previsão: <?=@$var_data_previsao_;?> Dt&nbsp;Conclusão: <?=@$var_data_conclusao_;?>
                    </div>            
                </div>
            </div>
        </div>

        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-12">
                    <h4>Etapas</h4>
                    <hr>
                </div>
                <div class="col-sm-12">
                    <div class="row" id="etapas">
                        <div class="col-sm-1">
                            Responsável
                        </div>       
                        <div class="col-sm-6">
                            Etapa
                        </div>       
                        <div class="col-sm-1">
                            Dt&nbsp;Início
                        </div>       
                        <div class="col-sm-1">
                            Dt&nbsp;Previsão
                        </div>       
                        <div class="col-sm-1">
                            Dt&nbsp;Conclusão
                        </div>       
                        <div class="col-sm-2">
                            Anexo
                        </div>       
                    </div>       
                    <?php
                        if (isset($dados_etapas)){
                            foreach($dados_etapas as $value){
                                ?>
                                <input type="hidden" class="form-control" name="etapa_cod_tarefaetapa[]" value="<?=@$value["cod_tarefaetapa"];?>">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" readonly name="etapa_tipo[]" value="<?=@$value["tipo_"];?>" placeholder="Etapa">
                                    </div>       
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" readonly name="etapa_nome[]" value="<?=@$value["nome"];?>" placeholder="Etapa">
                                    </div>       
                                    <div class="col-sm-1">
                                        <input type="text" class="form-control servico_etapa_data_inicio input-date" name="etapa_data_inicio[]" readonly value="<?=@$value["data_inicio_"];?>" placeholder="Dt&nbsp;Início" data-campo="previsao_<?=@$value["cod_tarefaetapa"];?>" data-previsao="<?=@$value["previsao"];?>">
                                    </div>       
                                    <div class="col-sm-1">
                                        <input type="text" class="form-control input-date" id="previsao_<?=@$value["cod_tarefaetapa"];?>" name="etapa_data_previsao[]" readonly value="<?=@$value["data_previsao_"];?>" placeholder="Dt&nbsp;Previsão">
                                    </div>       
                                    <div class="col-sm-1">
                                        <input type="text" class="form-control input-date" name="etapa_data_conclusao[]" readonly value="<?=@$value["data_conclusao_"];?>" placeholder="Dt&nbsp;Conclusão">
                                    </div>       
                                    <div class="col-sm-2">
                                        <?php
                                            $arquivo ="./data/".md5(@$var_cod_interno)."/".$value["arquivo"];
                                            //echo $arquivo;
                                            if (file_exists($arquivo) and $value["arquivo"]<>""){
                                                $arquivo =LOCAL."data/".md5(@$var_cod_interno)."/".$value["arquivo"];
                                                ?>
                                                <a href="<?=$arquivo?>" class="btn btn-sub" target="_blank">Anexo</a>
                                                <?php
                                            }
                                        ?>
                                    </div>       
                                </div>
                            <?php
                            }
                        }
                    ?>
                                
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    
?>


