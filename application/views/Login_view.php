<div class="container div-container-login">
    <div class="row">
        <div class="col-sm-12">
            <div class="div-login">
                <div class="div-login-header">
                    <br>
                    <center><img src="<?=LOCAL ?>web/bah.png" width="200px"></center>
                    <br>
                </div>
                <div class="div-login-body">
                    <div id="div-logar">
                        <div class="row">
                            <input type="text" name="email" id="email" maxlength="50" placeholder="E-mail">
                            <br><input type="password" name="senha" id="senha" maxlength="10" placeholder="Senha">
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="margin-bottom:10px;text-align:center">
                                <a id="id-recuperar" class="a-acao-login a-acao-linkrecuperar"><br>Recuperar senha</a>
                            </div>
                            <div class="col-md-6" style="margin-top:10px; margin-bottom:10px;">
                                <a style="width:100%" id="login-entrar" class="a-acao-logar btn" data-objeto="<?=LOCAL."login"?>" data-acao="logar">Entrar</a>                            
                            </div>
                        </div>
                    </div>

                    <div id="div-recuperar">
                        <div class="row">
                            <p class="p-login">Esqueceu sua senha?</p>
                            <br><input type="text" name="recuperar_email" id="recuperar_email" maxlength="50" placeholder="E-mail">
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="margin-bottom:10px;text-align:center">
                                <a   id="id-logar" class="a-acao-login a-acao-linklogar"><br>Logar</a>
                            </div>
                            <div class="col-md-6" style="margin-top:10px; margin-bottom:10px;">
                                <a style="width:100%" class="a-acao-recuperar btn" data-objeto="<?=LOCAL."login"?>" data-acao="recuperar_senha">Enviar</a>
                            </div>
                        </div>
                    </div>                    

                    <div id="div-cliente">
                        <div class="row">
                            <p class="p-login">&nbsp;</p>
                            <select class="form-control select-cliente" data-logincontroller="" data-loginevent="">
                                <option value="">Selecione o cliente</option>
                            </select>
                            <p class="p-login">&nbsp;</p>
                        </div>
                    </div>                    
                </div>
                <div class="div-login-footer">
                </div>
            </div>
            <br><center><img src="<?=LOCAL.LOGO_TRIPLEAIE ?>" width="130px"></center>

        </div>

    </div>

</div>