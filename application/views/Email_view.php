<?php 
    if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 

    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
?>

<div class="col-sm-12 div-operacao">
    <div class="row">
        <?php
            if ($operacao=="listar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>email" data-acao="novo">+ Adicionar</a>
                </div>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>email" data-acao="listar">Buscar</a>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
                <?php
            }
            if ($operacao=="ver"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>email" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
        ?>
    </div>
</div>

<?php
    if ($operacao=="listar"){
        ?>
        <div class="col-sm-12">
            <div class="div-table">
                <div class="div-table-fixed">
                    <table class="tablebah">
                        <thead>
                            <tr>
                                <?php
                                    echo $this->functions->table_column("1","from","De","*","a-acao");
                                    echo $this->functions->table_column("","to","Para","*","a-acao");
                                    echo $this->functions->table_column("","subject","Assunto","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","data_c_","Dt Envio","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","status","Status","*","a-acao");
                                ?>
                                <th width="3%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($dados as $value){
                                    ?>
                                    <tr class="tr-linha" data-objeto="<?=LOCAL ?>email" data-acao="ver" data-cod="<?=$value["cod_email"] ?>">
                                        <td><?=$value["from"]?></td>
                                        <td class="tdoculta"><?=$value["to"]?></td>
                                        <td class="tdoculta"><?=$value["subject"]?></td>
                                        <td class="tdoculta"><?=$value["data_c_"]?></td>
                                        <td class="tdoculta"><?=$value["status"]?></td>
                                        <td><a class="btn btn-ver a-acao" data-objeto="<?=LOCAL ?>email" data-acao="ver" data-cod="<?=$value["cod_email"] ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pull-right col-paginacao">
                <?=$paginacao; ?>
            </div>
        </div>
        <?php
    }

    if (($operacao=="ver")){
        ?>
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <h2>E-mail <?=@$var_cod_email;?></h2>
                        <p>
                        <br><strong>De:</strong> <?=@$var_from;?>
                        <br><strong>Para:</strong> <?=@$var_to;?>
                        <br><strong>Assunto:</strong> <?=@$var_subject;?>
                        <br><strong>Dt Envio:</strong> <?=@$var_data_c_;?>
                        <br><strong>Status:</strong> <?=@$var_status;?>
                        <br>
                        <br><strong>ID Externo:</strong> <?=@$var_id_externo;?>
                    </div>            
                </div>
            </div> 
        </div> 

        <?php
    }
?>


