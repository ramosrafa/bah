<?php 

    if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 

    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
    //print_r($dados);

    if (isset($dados)){
        ?>

            
        <html>
            <head>
                <style>
                   
                    body {
                        font-family: sans-serif;
                        font-size: 9pt;
                        line-height: 10px!important;
                        margin-top: 30mm!important;
                        margin-bottom: 60mm!important;
                    }
                    h2{
                        font-size: 12pt;
                        text-align: left;
                        color: #575759;
                    }
                    p {	
                        font-family: sans-serif;
                        font-size: 9pt;
                        line-height: 20px;
                        margin-bottom: 10px; 
                    }
                    strong{
                        margin-top: 10px!important;
                    }
                    #header{
                        border-bottom: 1px solid #000;
                    }
                    #body{
                        margin-top: 20px;
                    }
                    .form-group{
                        border-bottom: 1px solid #CCC;
                    }
                    #footer table{
                        font-size: 90pt; 
                        text-align: center; 
                        margin-top: 50px;
                        padding-top: 15px;
                        border-top: 1px solid #000000; 
                    }
                    .table-border{
                        width: 100%;
                        border: 1px solid #CCC;
                        line-height: 22px;
                        margin: 10px 0 10px 0;
                    }
                    .table-border-bottom{
                        width: 100%;
                        border-bottom: 1px solid #CCC;
                        line-height: 22px;
                        margin: 10px 0 10px 0;
                    }
                    .td-border-top{
                        border-top: 1px solid #CCC;
                    }
                     @page *{
                        margin-top: 1.54cm;
                        margin-bottom: 10.54cm!important;
                        margin-left: 3.175cm;
                        margin-right: 3.175cm;
                    }
                </style>
            </head>
            <body>
                
                <!--mpdf
                <htmlpageheader name="myheader">
                </htmlpageheader>
                <htmlpagefooter name="myfooter">
                    <div id="footer">
                        <table width="100%">
                            <tr>
                                <td width="50" valign="top">
                                    <img src="<?=LOCAL.LOGO_SISTEMA ?>" height="30px">
                                </td>
                                <td width="50" valign="top">
                                    <p>Relatório gerado do sistema Cálculo Legal www.calculolegal.net.br</p>
                                    <p>Página {PAGENO} de {nb}</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </htmlpagefooter>
                <sethtmlpageheader name="myheader" value="on" show-this-page="1" />
                <sethtmlpagefooter name="myfooter" value="off" />
                mpdf-->
                
                <div id="header">
                    <table width="100%">
                        <tr>
                            <td width="50%"><img src="<?=LOCAL.LOGO_RELATORIO ?>" height="60px"></td>
                            <td width="50%"><h1>Estratégia</h1></td>
                        </tr>
                    </table>    
                </div>
                <div id="body">
                    <table class="table-border" width="100%">
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Nome:</strong> <?=@$var_nome;?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Código Sistema:</strong> <?=@$var_cod_interno;?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Regime&nbsp;Tributário:</strong> <?=$var_regimetributario?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Atividade Econômica:</strong> 
                                <?php if (@$var_atividade_servico=="S") echo "Serviço, "; ?>
                                <?php if (@$var_atividade_comercio=="S") echo "Comércio, "; ?>
                                <?php if (@$var_atividade_industria=="S") echo "Indústria, "; ?>
                                <?php if (@$var_atividade_aluguel=="S") echo "Aluguel, "; ?>
                                <?php if (@$var_atividade_holding=="S") echo "Holding, "; ?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>CPF/CNPJ:</strong> <?=@$var_documento_?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>IE:</strong> <?=@$var_ie?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>IM:</strong> <?=@$var_im;?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Certificado Tipo:</strong> <?=@$var_certificadotipo;?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Certificado Posse:</strong> <?php if (@$var_certificado_posse=="T") echo "TripleA"; ?><?php if (@$var_certificado_posse=="C") echo "Cliente"; ?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Certificado Vencimento:</strong> <?=@$var_certificado_vencimento_;?>
                                </p>
                            </td>
                        </tr>
                    </table>
                    
                    <br>
                    <h2>1 Sobre a empresa</h2>

                    <p><?=@$var_estrategia_sobreempresa;?></p>
                    <hr>

                    <p><strong>Site e redes sociais</strong></p> 
                    <p><?=@$var_estrategia_site;?></p>
                    <hr>

                    <p><strong>Regime tributário</strong></p> 
                    <p><?=@$var_estrategia_regimetributario;?></p>
                    <hr>

                    <p><strong>Regime tributário Vencimento</strong></p> 
                    <p><?=@$var_estrategia_regimetributariodata;?></p>
                    <hr>

                    <p><strong>Número Matriz e Filial</strong></p> 
                    <p><?=@$var_estrategia_numeromatriz;?></p>
                    <hr>

                    <p><strong>Pertence ao Grupo Empresarial? Se sim, qual?</strong></p> 
                    <p><?=@$var_estrategia_grupoempresarial;?></p>
                    <hr>

                    <p><strong>Número profissionais</strong></p> 
                    <p><?=@$var_estrategia_numeroprofissionais;?></p>
                    <hr>

                    <p><strong>Quantidade sócios</strong></p> 
                    <p><?=@$var_estrategia_quantidadesocios;?></p>
                    <hr>

                    <p><strong>Sócios Participam em Outra Empresa? Se sim, qual sócio e qual empresa?</strong></p> 
                    <p><?=@$var_estrategia_sociosoutraempresa;?></p>
                    <hr>

                    <p><strong>Atividades (CNAES)</strong></p> 
                    <p><?=@$var_estrategia_atividades;?></p>
                    <hr>

                    <p><strong>Faturamento Últimos 12 Meses</strong></p> 
                    <p><?=@$var_estrategia_faturamento;?></p>
                    <hr>

                    <h2>2 Sobre o projeto</h2>
                    <p><?=@$var_estrategia_sobreprojeto;?></p>
                    <hr>

                    <h2>3 Produtos e serviços</h2>
                    <p><?=@$var_estrategia_produtoservico;?></p>
                    <hr>

                    <h2>4 Pessoas de contato</h2>
                    <p><?=@$var_estrategia_produtoservico;?></p>
                    <hr>

                    <h2>5 Objetivos Principais da estratégia</h2>
                    <?php
                        $var_estrategia_objetivoestrategia = explode("#",@$var_estrategia_objetivoestrategia);
                    ?>
                    <?php if (in_array("1",$var_estrategia_objetivoestrategia)) echo "<p>Qualificação de Gestão</p>";?>
                    <?php if (in_array("2",$var_estrategia_objetivoestrategia)) echo "<p>Planejamento Societário</p>";?>
                    <?php if (in_array("3",$var_estrategia_objetivoestrategia)) echo "<p>Planejamento Tributário</p>";?>
                    <?php if (in_array("4",$var_estrategia_objetivoestrategia)) echo "<p>Revisão Tributária</p>";?>
                    <?php if (in_array("5",$var_estrategia_objetivoestrategia)) echo "<p>Revisão de Relações Trabalhistas</p>";?>
                    <?php if (in_array("6",$var_estrategia_objetivoestrategia)) echo "<p>Recuperação de Créditos Tributários</p>";?>
                    <?php if (in_array("7",$var_estrategia_objetivoestrategia)) echo "<p>Informações para Tomada de Decisão</p>";  ?>                    
                    
                    <hr>

                    <h2>6 Estágio de Qualificação de Gestão</h2>
                    <?php if ($var_estrategia_estagioqualificacao=="1") echo "<p>Básica</p>";?>
                    <?php if ($var_estrategia_estagioqualificacao=="2") echo "<p>Intermediária</p>";?>
                    <?php if ($var_estrategia_estagioqualificacao=="3") echo "<p>Qualificada</p>";?>
                    <?php if ($var_estrategia_estagioqualificacao=="4") echo "<p>Avançada</p>";?>
                    
                    <hr>

                    <h2>7 KPIs de acompanhamento Contabilidade</h2>
                    <?php
                        $var_estrategia_kpicontabilidade = explode("#",@$var_estrategia_kpicontabilidade);
                    ?>
                    <?php if (in_array("1",$var_estrategia_kpicontabilidade)) echo "<p>Demonstrações Mês + 5° dia útil</p>";?>
                    <?php if (in_array("2",$var_estrategia_kpicontabilidade)) echo "<p>Demonstrações Mês + 10° dia útil</p>";?>
                    <?php if (in_array("3",$var_estrategia_kpicontabilidade)) echo "<p>Demonstrações Mês + 20° dia útil</p>";?>
                    <?php if (in_array("4",$var_estrategia_kpicontabilidade)) echo "<p>Demonstrações Mês + 50 dias</p>";?>
                    <?php if (in_array("5",$var_estrategia_kpicontabilidade)) echo "<p>Demonstrações Bimestre + 20 dias</p>";?>
                    <?php if (in_array("6",$var_estrategia_kpicontabilidade)) echo "<p>Demonstrações Trimestre + 20 dias</p>";?>
                    <?php if (in_array("7",$var_estrategia_kpicontabilidade)) echo "<p>Demonstrações Semestre + 20 dias</p>";?>
                    <?php if (in_array("8",$var_estrategia_kpicontabilidade)) echo "<p>Ano + 60 dias</p>";?>
                    <?php if (in_array("9",$var_estrategia_kpicontabilidade)) echo "<p>DRE Combinado (Horizontal)</p>";?>
                    <?php if (in_array("10",$var_estrategia_kpicontabilidade)) echo "<p>Ativo e Passivo Combinado (Horizonta)</p>";?>
                    <?php if (in_array("11",$var_estrategia_kpicontabilidade)) echo "<p>Fluxo de Caixa</p>";?>
                    <?php if (in_array("12",$var_estrategia_kpicontabilidade)) echo "<p>Margem de Contribuição</p>";?>
                    <?php if (in_array("13",$var_estrategia_kpicontabilidade)) echo "<p>DashBoard Estratégico</p>";?>
                    <?php if (in_array("14",$var_estrategia_kpicontabilidade)) echo "<p>Demonstrações em Lingua Estrangeira </p>";?>                               
                    <hr>

                    <h2>8 KPIs de acompanhamento Tributários</h2>
                    <?php
                        $var_estrategia_kpitributario = explode("#",@$var_estrategia_kpitributario);
                    ?>
                    <?php if (in_array("1",$var_estrategia_kpitributario)) echo "<p>Guarda de Notas Fiscais Eletronicas</p>";?>
                    <?php if (in_array("2",$var_estrategia_kpitributario)) echo "<p>Gestão Estilo Triple A (GET) </p>";?>
                    <?php if (in_array("3",$var_estrategia_kpitributario)) echo "<p>Evolução Faturamento</p>";?>
                    <?php if (in_array("4",$var_estrategia_kpitributario)) echo "<p>Recuperação de Créditos Administrativos</p>";?>
                    <?php if (in_array("5",$var_estrategia_kpitributario)) echo "<p>Revisão de Cadastros Fiscais </p>";?>                               
                    <hr>

                    <h2>9 KPIs de acompanhamento Relações Trabalhistas</h2>
                    <?php
                        $var_estrategia_kpitrabalhista = explode("#",@$var_estrategia_kpitrabalhista);
                    ?>
                    <?php if (in_array("1",$var_estrategia_kpitrabalhista)) echo "<p>Gestão de Beneficios</p>";?>
                    <?php if (in_array("2",$var_estrategia_kpitrabalhista)) echo "<p>Gestão de Admissões e Demissões</p>";?>
                    <?php if (in_array("3",$var_estrategia_kpitrabalhista)) echo "<p>Gestão Estimo Triple A</p>";?>
                    <?php if (in_array("4",$var_estrategia_kpitrabalhista)) echo "<p>Custo Minuto</p>";?>        
                    <?php if (in_array("5",$var_estrategia_kpitrabalhista)) echo "<p>Pró-Labore</p>";?> 
                    <hr>

                    <h2>10 KPIs de acompanhamento Societário</h2>
                    <?php
                        $var_estrategia_kpisocietario = explode("#",@$var_estrategia_kpisocietario);
                    ?>
                    <?php if (in_array("1",$var_estrategia_kpisocietario)) echo "<p>Adequação de Capital EIRELI</p>";?>
                    <?php if (in_array("2",$var_estrategia_kpisocietario)) echo "<p>Recomposição de Quadro Societário</p>";?>
                    <?php if (in_array("3",$var_estrategia_kpisocietario)) echo "<p>Deliberação de Lucros Desproporcionais</p>";?>
                    <?php if (in_array("4",$var_estrategia_kpisocietario)) echo "<p>Deliberações Especiais</p>";?>
                    <?php if (in_array("5",$var_estrategia_kpisocietario)) echo "<p>Controle de Certidões</p>";?>
                    <?php if (in_array("6",$var_estrategia_kpisocietario)) echo "<p>Controle de Certificados</p>";?>
                    <?php if (in_array("7",$var_estrategia_kpisocietario)) echo "<p>Ata de Sociedade Limitadas </p>";?>                               
                    <hr>

                    <h2>11 KPIs de acompanhamento Jurídicos</h2>
                    <?php
                        $var_estrategia_kpijuridico = explode("#",@$var_estrategia_kpijuridico);
                    ?>
                    <?php if (in_array("1",$var_estrategia_kpijuridico)) echo "<p>Radar Tributário (AnexarArquivoPDF)</p>";?>                            
                    <hr>

                    <h2>12 KPIs de acompanhamento Qualificação</h2>
                    <?php
                        $var_estrategia_kpiqualificacao = explode("#",@$var_estrategia_kpiqualificacao);
                    ?>
                    <?php if (in_array("1",$var_estrategia_kpiqualificacao)) echo "<p>Análise Estratégica</p>";?>
                    <?php if (in_array("2",$var_estrategia_kpiqualificacao)) echo "<p>Preço de Venda</p>";?>
                    <?php if (in_array("3",$var_estrategia_kpiqualificacao)) echo "<p>Análise de Custos Fixos</p>";?>                            

                    <p><strong>Objetivo do estágio atual</strong></p> 
                    <?php
                        $var_estrategia_objetivoestagioatual = explode("#",@$var_estrategia_objetivoestagioatual);
                    ?>
                    <?php if (in_array("1",$var_estrategia_objetivoestagioatual)) echo "<p>Gestão Corretiva</p>";?>
                    <?php if (in_array("2",$var_estrategia_objetivoestagioatual)) echo "<p>Gestão Preventiva</p>";?>
                    <?php if (in_array("3",$var_estrategia_objetivoestagioatual)) echo "<p>Gestão Evolutiva</p>";?>                                
                    <hr>

                    <h2>13 Principais clientes</h2>
                    <p><?=@$var_estrategia_principaisclientes;?></p>
                    <hr>

                    <h2>14 Principais fornecedores</h2>
                    <p><?=@$var_estrategia_principaisfornecedores;?></p>
                    <hr>

                    <h2>15 ERP</h2>
                    <?php
                        $var_estrategia_erp = explode("#",@$var_estrategia_erp);
                    ?>
                    <?php if (in_array("0",$var_estrategia_erp)) echo "<p>Não possui</p>";?>
                    <?php if (in_array("1",$var_estrategia_erp)) echo "<p>Financeiros</p>";?>
                    <?php if (in_array("2",$var_estrategia_erp)) echo "<p>NFE</p>";?>
                    <?php if (in_array("3",$var_estrategia_erp)) echo "<p>NFSE</p>";?>
                    <?php if (in_array("4",$var_estrategia_erp)) echo "<p>CUPOM-E</p>";?>
                    <?php if (in_array("5",$var_estrategia_erp)) echo "<p>Frente de Caixa</p>";?>
                    <?php if (in_array("6",$var_estrategia_erp)) echo "<p>Estoque</p>";?>
                    <?php if (in_array("7",$var_estrategia_erp)) echo "<p>Compras</p>";?>
                    <?php if (in_array("8",$var_estrategia_erp)) echo "<p>Fluxo de Caixa</p>";?>
                    <?php if (in_array("9",$var_estrategia_erp)) echo "<p>Orçamento</p>";?>
                    <?php if (in_array("10",$var_estrategia_erp)) echo "<p>Contabilidade</p>";?>
                    <?php if (in_array("11",$var_estrategia_erp)) echo "<p>Fiscal</p>";?>
                    <?php if (in_array("12",$var_estrategia_erp)) echo "<p>Folha de Pagamento</p>";?>
                    <?php if (in_array("13",$var_estrategia_erp)) echo "<p>BI</p>";?>
                    <hr>

                    <h2>16 Considerações gerais para consultores</h2>
                    <p>Considerações gerais de estratégia devem ser atualizadas a cada ciclo</p>
                    <hr>

                    <p><strong>Ciclo 1</strong></p> 
                    <p><?=@$var_estrategia_consideracaodata1;?></p>
                    <p><?=@$var_estrategia_consideracaoconsultor1;?></p>
                    <hr>

                    <p><strong>Ciclo 2</strong></p> 
                    <p><?=@$var_estrategia_consideracaodata2;?></p>
                    <p><?=@$var_estrategia_consideracaoconsultor2;?></p>
                    <hr>

                    <p><strong>Ciclo 3</strong></p> 
                    <p><?=@$var_estrategia_consideracaodata3;?></p>
                    <p><?=@$var_estrategia_consideracaoconsultor3;?></p>
       
                </div>
                    
            </body>
        </html>            

        <?php
    }
?>