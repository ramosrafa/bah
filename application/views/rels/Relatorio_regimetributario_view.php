<?php 

    if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 

    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
    //print_r($dados);

    if (isset($dados)){
        ?>

            
        <html>
            <head>
                <style>
                   
                    body {
                        font-family: sans-serif;
                        font-size: 9pt;
                        line-height: 10px!important;
                        margin-top: 30mm!important;
                        margin-bottom: 60mm!important;
                    }
                    h1{
                        font-size: 16pt!important;
                        text-align: right!important;
                        color: #575759;
                    }
                    h2{
                        font-size: 12pt!important;
                        text-align: right!important;
                        color: #575759;
                    }
                    p {	
                        font-family: sans-serif;
                        font-size: 9pt;
                        text-align: left;
                        line-height: 20px;
                        margin: 0pt; 
                    }
                    strong{
                        margin-top: 10px!important;
                    }
                    #header{
                        border-bottom: 1px solid #000;
                    }
                    #body{
                        margin-top: 20px;
                    }
                    #footer table{
                        font-size: 90pt; 
                        text-align: center; 
                        margin-top: 50px;
                        padding-top: 15px;
                        border-top: 1px solid #000000; 
                    }
                    .table-border{
                        width: 100%;
                        border: 1px solid #CCC;
                        line-height: 22px;
                        margin: 10px 0 10px 0;
                    }
                    .table-border-bottom{
                        width: 100%;
                        border-bottom: 1px solid #CCC;
                        line-height: 22px;
                        margin: 10px 0 10px 0;
                    }
                    .td-border-bottom{
                        border-bottom: 2px solid #CCC;
                    }
                    
                    .td-border-top{
                        border-top: 1px solid #CCC;
                    }
                     @page *{
                        margin-top: 1.54cm;
                        margin-bottom: 10.54cm!important;
                        margin-left: 3.175cm;
                        margin-right: 3.175cm;
                    }
                </style>
            </head>
            <body>
                
                <!--mpdf
                <htmlpageheader name="myheader">
                </htmlpageheader>
                <htmlpagefooter name="myfooter">
                    <div id="footer">
                        <table width="100%">
                            <tr>
                                <td width="50" valign="top">
                                    <img src="<?=LOCAL.LOGO_SISTEMA ?>" height="30px">
                                </td>
                                <td width="50" valign="top">
                                    <p>Relatório gerado do sistema Cálculo Legal www.calculolegal.net.br</p>
                                    <p>Página {PAGENO} de {nb}</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </htmlpagefooter>
                <sethtmlpageheader name="myheader" value="on" show-this-page="1" />
                <sethtmlpagefooter name="myfooter" value="off" />
                mpdf-->
                
                <div id="header">
                    <table width="100%">
                        <tr>
                            <td width="50%"><img src="<?=LOCAL.LOGO_RELATORIO ?>" height="60px"></td>
                            <td width="50%"><h1>Regime Tributário</h1><h2>&nbsp;</h2></td>
                        </tr>
                    </table>    
                </div>
                <div id="body">
                    <table class="" width="100%">
                        <tr class="tr-border-bottom">
                            <td class="td-border-bottom">
                                <p><strong>Cliente</strong></p>
                            </td>
                            <td class="td-border-bottom">
                                <p><strong>Regime</strong></p>
                            </td>
                            <td class="td-border-bottom">
                                <p><strong>Data</strong></p>
                            </td>
                        </tr>
                        <?php
                            foreach($dados as $value){
                                ?>
                                <tr>
                                    <td>
                                        <p><?=$value["nome"]?></p>
                                    </td>
                                    <td>
                                        <p><?=$value["regime"]?></p>
                                    </td>
                                    <td>
                                        <p><?=$value["estrategia_regimetributariodata"]?></p>
                                    </td>
                                </tr>
                                <?php
                            }
                        ?>
                    </table>
                  
                </div>
                <br />
            </body>
        </html>            

        <?php
    }
?>