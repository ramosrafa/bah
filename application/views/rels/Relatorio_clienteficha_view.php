<?php 

    if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 

    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
    print_r($dados);

    if (isset($dados)){
        ?>

            
        <html>
            <head>
                <style>
                   
                    body {
                        font-family: sans-serif;
                        font-size: 9pt;
                        line-height: 10px!important;
                        margin-top: 30mm!important;
                        margin-bottom: 60mm!important;
                    }
                    h2{
                        font-size: 12pt;
                        text-align: left;
                        color: #575759;
                    }
                    p {	
                        font-family: sans-serif;
                        font-size: 9pt;
                        line-height: 20px;
                        margin-bottom: 10px; 
                    }
                    strong{
                        margin-top: 10px!important;
                    }
                    #header{
                        border-bottom: 1px solid #000;
                    }
                    #body{
                        margin-top: 20px;
                    }
                    .form-group{
                        border-bottom: 1px solid #CCC;
                    }
                    #footer table{
                        font-size: 90pt; 
                        text-align: center; 
                        margin-top: 50px;
                        padding-top: 15px;
                        border-top: 1px solid #000000; 
                    }
                    .table-border{
                        width: 100%;
                        border: 1px solid #CCC;
                        line-height: 22px;
                        margin: 10px 0 10px 0;
                    }
                    .table-border-bottom{
                        width: 100%;
                        border-bottom: 1px solid #CCC;
                        line-height: 22px;
                        margin: 10px 0 10px 0;
                    }
                    .td-border-top{
                        border-top: 1px solid #CCC;
                    }
                     @page *{
                        margin-top: 1.54cm;
                        margin-bottom: 10.54cm!important;
                        margin-left: 3.175cm;
                        margin-right: 3.175cm;
                    }
                </style>
            </head>
            <body>
                
                <!--mpdf
                <htmlpageheader name="myheader">
                </htmlpageheader>
                <htmlpagefooter name="myfooter">
                    <div id="footer">
                        <table width="100%">
                            <tr>
                                <td width="50" valign="top">
                                    <img src="<?=LOCAL.LOGO_SISTEMA ?>" height="30px">
                                </td>
                                <td width="50" valign="top">
                                    <p>Relatório gerado do sistema Cálculo Legal www.calculolegal.net.br</p>
                                    <p>Página {PAGENO} de {nb}</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </htmlpagefooter>
                <sethtmlpageheader name="myheader" value="on" show-this-page="1" />
                <sethtmlpagefooter name="myfooter" value="off" />
                mpdf-->
                
                <div id="header">
                    <table width="100%">
                        <tr>
                            <td width="50%"><img src="<?=LOCAL.LOGO_RELATORIO ?>" height="60px"></td>
                            <td width="50%"><h1>Ficha do Cliente</h1></td>
                        </tr>
                    </table>    
                </div>
                <div id="body">
                    <table class="" width="100%">
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Nome:</strong> <?=@$var_nome;?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Código Interno:</strong> <?=@$var_cod_interno;?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Regime&nbsp;Tributário:</strong> <?=$var_regimetributario?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Atividade Econômica:</strong> 
                                <?php if (@$var_atividade_servico=="S") echo "Serviço, "; ?>
                                <?php if (@$var_atividade_comercio=="S") echo "Comércio, "; ?>
                                <?php if (@$var_atividade_industria=="S") echo "Indústria, "; ?>
                                <?php if (@$var_atividade_aluguel=="S") echo "Aluguel, "; ?>
                                <?php if (@$var_atividade_holding=="S") echo "Holding, "; ?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Procuração Vencimento:</strong> <?=@$var_procuracao_vencimento_?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <hr>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>CPF/CNPJ:</strong> <?=@$var_documento?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>IE:</strong> <?=@$var_ie?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>IM:</strong> <?=@$var_im;?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <hr>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Endereço:</strong> <?=@$var_endereco;?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Cidade:</strong> <?=@$var_cidade;?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>UF:</strong> <?=@$var_uf;?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>CEP:</strong> <?=@$var_cep;?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>E-mail:</strong> <?=@$var_email;?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Telefone:</strong> <?=@$var_telefone;?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <hr>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Certificado Tipo:</strong> <?=@$var_certificadotipo;?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Certificado Posse:</strong> <?php if (@$var_certificado_posse=="T") echo "TripleA"; ?><?php if (@$var_certificado_posse=="C") echo "Cliente"; ?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p>
                                <strong>Certificado Vencimento:</strong> <?=@$var_certificado_vencimento_;?>
                                </p>
                            </td>
                        </tr>
                    </table>
                    
                    <br>
                    <h2>Quadro Societário</h2>
                    <br>
                    
                    <table class="" width="100%">
                        <tr class="tr-border-bottom">
                            <td class="td-border-bottom">
                                <p><strong>Cliente</strong></p>
                            </td>
                            <td class="td-border-bottom">
                                <p><strong>Ordem</strong></p>
                            </td>
                            <td class="td-border-bottom">
                                <p><strong>Sócio</strong></p>
                            </td>
                            <td class="td-border-bottom">
                                <p><strong>Documento</strong></p>
                            </td>
                            <td class="td-border-bottom">
                                <p><strong>Paticipação %</strong></p>
                            </td>
                            <td class="td-border-bottom">
                                <p><strong>Quotas R$</strong></p>
                            </td>
                        </tr>
                        <?php
                            foreach($dados_quadro as $value){
                                ?>
                                <tr>
                                    <td>
                                        <?=$value["cliente"]?>
                                    </td>
                                    <td>
                                        <?=$value["ordem"]?>
                                    </td>
                                    <td>
                                        <?=$value["nome"]?>
                                    </td>
                                    <td>
                                        <?=$value["documento"]?>
                                    </td>
                                    <td>
                                        <?=$value["participacao"]?>
                                    </td>
                                    <td>
                                        <?=$value["quotas"]?>
                                    </td>
                                    
                                </tr>
                                <?php
                            }
                        ?>
                    </table>                    
       
                    <br>
                    <h2>Alvarás</h2>
                    <br>
                    
                    <table class="" width="100%">
                        <tr class="tr-border-bottom">
                            <td class="td-border-bottom">
                                <p><strong>Número</strong></p>
                            </td>
                            <td class="td-border-bottom">
                                <p><strong>Dt&nbsp;Vencimento</strong></p>
                            </td>
                            <td class="td-border-bottom">
                                <p><strong>Dt&nbsp;Pagamento</strong></p>
                            </td>
                            <td class="td-border-bottom">
                                <p><strong>Valor</strong></p>
                            </td>
                        </tr>
                        <?php
                            foreach($dados_alvara as $value){
                                ?>
                                <tr>
                                    <td>
                                        <?=$value["numero"]?>
                                    </td>
                                    <td>
                                        <?=$value["data_vencimento_"]?>
                                    </td>
                                    <td>
                                        <?=$value["data_pagamento_"]?>
                                    </td>
                                    <td>
                                        <?=$value["valor"]?>
                                    </td>
                                </tr>
                                <?php
                            }
                        ?>
                    </table>
                </div>
                    
            </body>
        </html>            

        <?php
    }
?>