<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
?>

<div class="col-sm-12 div-operacao">
    <div class="row">
        <?php
            if ($operacao=="listar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>funcionario" data-acao="novo">+ Adicionar</a>
                </div>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>funcionario" data-acao="listar">Buscar</a>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
                <?php
            }
            if ($operacao=="novo"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>funcionario" data-acao="inserir" data-validacao="">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>funcionario" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
            if ($operacao=="editar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>funcionario" data-acao="salvar" data-validacao="" data-cod="<?=@$var_cod_funcionario;?>">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>funcionario" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
        ?>
    </div>
</div>

<?php
    if ($operacao=="listar"){
        ?>
        <div class="col-sm-12">
            <div class="div-table">
                <div class="div-table-fixed">
                    <table class="tablebah">
                        <thead>
                            <tr>
                                <?php
                                    echo $this->functions->table_column("1","nome","Funcionário","*","a-acao");
                                    echo $this->functions->table_column("","cliente","Cliente","*","a-acao");
                                    echo $this->functions->table_column("","documento","CPF","*","a-acao");
                                    echo $this->functions->table_column("","data_admissao","Dt Admissão","*","a-acao");
                                    echo $this->functions->table_column("","data_demissao","Dt Demissão","*","a-acao");
                                ?>
                                <th width="3%">&nbsp;</th>
                                <th width="3%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($dados as $value){
                                    ?>
                                    <tr class="tr-linha" data-objeto="<?=LOCAL ?>funcionario" data-acao="editar" data-cod="<?=$value["cod_funcionario"] ?>">
                                        <td><?=$value["nome"]?></td>
                                        <td><?=$value["cliente"]?></td>
                                        <td><?=$value["documento"]?></td>
                                        <td><?=$value["data_admissao_"]?></td>
                                        <td><?=$value["data_demissao_"]?></td>
                                        <td><a class="btn btn-editar a-acao" data-objeto="<?=LOCAL ?>funcionario" data-acao="editar" data-cod="<?=$value["cod_funcionario"] ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                        <td><a class="btn btn-excluir a-acao" data-objeto="<?=LOCAL ?>funcionario" data-acao="excluir" data-cod="<?=$value["cod_funcionario"] ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pull-right col-paginacao">
                <?=$paginacao; ?>
            </div>
        </div>
        <?php
    }
    if (($operacao=="novo")or($operacao=="editar")){
        ?>

        <div class="col-sm-12">
            <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-cadastro">Cadastro</a></li>
            <li><a data-toggle="tab" href="#tab-profissional">Dados Profissionais</a></li>
            </ul>
        </div>
        
        <div class="tab-content">
            <div id="tab-cadastro" class="tab-pane fade in active">           
        <div class="col-sm-12 div-form">
                <div class="row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label" for="cod_cliente">Cliente&nbsp;<span  data-toggle="tooltip" data-placement="bottom" title="Selecione um cliente para fazer fazer uma notificação específica ou deixe em branco e envie para todos."><span class="glyphicon glyphicon-question-sign" aria-hidden="true" style="color:#4c637b;"></span></span></label>
                            <select class="form-control" name="cod_cliente" id="cod_cliente">
                            <option value="NULL">Selecione...</option>
                            <?php
                                foreach($dados_cliente as $value){
                                    $cod_cliente = $value["cod_cliente"];
                                    $nome = $value["nome"];
                                
                                    $selected = (@$var_cod_cliente==$cod_cliente)? "selected": "";
                                    ?>
                                    <option value="<?=$cod_cliente ?>" <?=$selected ?>><?=$nome ?></option>
                                    <?php
                                }
                            ?>
                            </select>
                        </div>            
                    </div>
                </div>

                <div class="col-sm-12">
                    <h4>Dados de Identificação</h4>
                    <hr>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label" for="nome">Nome Completo</label>  
                        <input type="text" name="nome" id="nome" maxlength="100" value="<?=@$var_nome;?>" class="form-control">
                    </div>            
                </div>            
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="documento">CPF/CNPJ</label>  
                        <input type="text" name="documento" id="documento" maxlength="14" value="<?=@$var_documento;?>" class="form-control">
                    </div>            
                </div>          
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="data_nascimento">Data de Nascimento</label>  
                        <input type="text" name="data_nascimento" id="data_nascimento" maxlength="10" value="<?=@$var_data_nascimento_;?>" class="form-control input-date">
                    </div>            
                </div>  
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="cod_funcionario">Estado Civil&nbsp;</label>
                        <select class="form-control" name="cod_funcionario" id="cod_funcionario">
                        <option value="NULL">Selecione...</option>
                            <option value="<?=$cod_funcionario ?>">Solteiro(a)</option>
                            <option value="<?=$cod_funcionario ?>">Casado(a)</option>
                            <option value="<?=$cod_funcionario ?>">Viúvo(a)</option>
                            <option value="<?=$cod_funcionario ?>">Separado(a)</option>
                            <option value="<?=$cod_funcionario ?>">Divorciado(a)</option>
                            <option value="<?=$cod_funcionario ?>">União Estável</option>
                            <option value="<?=$cod_funcionario ?>">Outro</option>    
                        </select>
                    </div>
                </div>
                <hr>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label" for="municipio">Município de Nascimento</label>  
                        <input type="text" name="municipio" id="municipio" maxlength="200" value="<?=@$var_municipio;?>" class="form-control">
                    </div>            
                </div> 
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="rg">RG</label>  
                        <input type="text" name="rg" id="rg" maxlength="14" value="<?=@$var_rg;?>" class="form-control">
                    </div>            
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="emissor">Órgão Emissor</label>  
                        <input type="text" name="emissor" id="emissor" maxlength="200" value="<?=@$var_emissor;?>" class="form-control">
                    </div>            
                </div>  
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="data_emissao1">Data de Emissão</label>  
                        <input type="text" name="data_emissao1" id="data_emissao1" maxlength="10" value="<?=@$var_data_emissao1_;?>" class="form-control input-date">
                    </div>            
                </div>     
                <div class="col-sm-12">
                    <h4>Dados de Localização</h4>
                    <hr>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="endereco">Endereço</label>  
                        <input type="text" name="endereco" id="endereco" maxlength="200" value="<?=@$var_endereco;?>" class="form-control">
                    </div>            
                </div>
                <div class="col-sm-1">
                    <div class="form-group">
                        <label class="control-label" for="complemento">Complemento</label>  
                        <input type="text" name="complemento" id="complemento" maxlength="100" value="<?=@$var_complemento;?>" class="form-control">
                    </div>            
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="bairro">Bairro</label>  
                        <input type="text" name="bairro" id="bairro" maxlength="200" value="<?=@$var_bairro;?>" class="form-control">
                    </div>            
                </div>
                <hr>
                <div class="col-sm-1">
                    <div class="form-group">
                        <label class="control-label" for="cep">CEP</label>  
                        <input type="text" name="cep" id="cep" maxlength="14" value="<?=@$var_cep;?>" class="form-control">
                    </div>            
                </div>
                <div class="col-sm-1">
                    <div class="form-group">
                        <label class="control-label" for="uf">UF</label>  
                        <input type="text" name="uf" id="uf" maxlength="2" value="<?=@$var_uf;?>" class="form-control">
                    </div>            
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="cidade">Cidade</label>  
                        <input type="text" name="cidade" id="cidade" maxlength="200" value="<?=@$var_cidade;?>" class="form-control">
                    </div>            
                </div>
                <div class="col-sm-12">
                    <h4>Dados Adicionais</h4>
                    <hr>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label" for="email">E-mail</label>  
                        <input type="text" name="email" id="email" maxlength="100" value="<?=@$var_email;?>" class="form-control">
                    </div>            
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label" for="telefone">Telefone</label>  
                        <input type="text" name="telefone" id="telefone" maxlength="100" value="<?=@$var_telefone;?>" class="form-control">
                    </div>            
                </div>
                <div class="col-sm-12">
                    <h4>Título Eleitoral</h4>
                    <hr>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="titulo_eleitor">Número do Título</label>  
                        <input type="text" name="titulo_eleitor" id="titulo_eleitor" maxlength="20" value="<?=@$var_titulo_eleitor;?>" class="form-control">
                    </div>            
                </div> 
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="data_emissao2">Data de Emissão</label>  
                        <input type="text" name="data_emissao2" id="data_emissao2" maxlength="10" value="<?=@$var_data_emissao2_;?>" class="form-control input-date">
                    </div>            
                </div> 
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="zona">Zona</label>  
                        <input type="text" name="zona" id="zona" maxlength="100" value="<?=@$var_zona;?>" class="form-control">
                    </div>            
                </div> 
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="secao">Seção</label>  
                        <input type="text" name="secao" id="secao" maxlength="100" value="<?=@$var_secao;?>" class="form-control">
                    </div>            
                </div> 
            </div>
        </div>
        
        <div id="tab-profissional" class="tab-pane fade">
        <div class="col-sm-12 div-form">
                <div class="col-sm-12">
                    <h4>Dados Profissionais</h4>
                    <hr>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="carteira">Carteira de Trabalho</label>  
                        <input type="text" name="carteira" id="carteira" maxlength="100" value="<?=@$var_carteira;?>" class="form-control">
                    </div>            
                </div> 
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="serie_carteira">Série da Carteira</label>  
                        <input type="text" name="serie_carteira" id="serie_carteira" maxlength="100" value="<?=@$var_serie_carteira;?>" class="form-control">
                    </div>            
                </div> 
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="uf_carteira">UF</label>  
                        <input type="text" name="uf_carteira" id="uf_carteira" maxlength="2" value="<?=@$var_uf_carteira;?>" class="form-control">
                    </div>            
                </div> 
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="data_emissao3">Data de Emissão</label>  
                        <input type="text" name="data_emissao3" id="data_emissao3" maxlength="10" value="<?=@$var_data_emissao3_;?>" class="form-control input-date">
                    </div>            
                </div> 
                <hr>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="escolaridade">Escolaridade</label>  
                        <input type="text" name="escolaridade" id="escolaridade" maxlength="100" value="<?=@$var_escolaridade;?>" class="form-control">
                    </div>            
                </div> 
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="pis">Número PIS</label>  
                        <input type="text" name="pis" id="pis" maxlength="100" value="<?=@$var_pis;?>" class="form-control">
                    </div>            
                </div> 
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="data_pis">Data de Cadastro PIS</label>  
                        <input type="text" name="data_pis" id="data_pis" maxlength="10" value="<?=@$var_data_pis_;?>" class="form-control input-date">
                    </div>            
                </div> 
                <div class="col-sm-12">
                    <h4>Contrato de Trabalho</h4>
                    <hr>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label" for="empresa">Empresa</label>  
                        <input type="text" name="empresa" id="empresa" maxlength="100" value="<?=@$var_empresa;?>" class="form-control">
                    </div>            
                </div>    
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label" for="funcao">Função</label>  
                        <input type="text" name="funcao" id="funcao" maxlength="100" value="<?=@$var_funcao;?>" class="form-control">
                    </div>            
                </div>    
                <hr>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label" for="cod_funcionario">Recebe Vale Refeição?&nbsp;</label>
                        <select class="form-control" name="cod_funcionario" id="cod_funcionario">
                        <option value="NULL">Selecione...</option>
                            <option value="<?=$cod_funcionario ?>">Sim</option> 
                            <option value="<?=$cod_funcionario ?>">Não</option> 
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label" for="cod_funcionario">Recebe Vale Transporte?&nbsp;</label>
                        <select class="form-control" name="cod_funcionario" id="cod_funcionario">
                        <option value="NULL">Selecione...</option>
                            <option value="<?=$cod_funcionario ?>">Sim</option> 
                            <option value="<?=$cod_funcionario ?>">Não</option> 
                        </select>
                    </div>
                </div>
                <hr>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="salario">Salário</label>  
                        <input type="text" name="salario" id="salario" maxlength="100" value="<?=@$var_salario;?>" class="form-control">
                    </div>            
                </div>    
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="periodo">Horário de Trabalho</label>  
                        <input type="text" name="periodo" id="periodo" maxlength="10" value="<?=@$var_periodo;?>" class="form-control">
                    </div>            
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="data_admissao">Dt Admissão</label>  
                        <input type="text" name="data_admissao" id="data_admissao" maxlength="10" value="<?=@$var_data_admissao_;?>" class="form-control input-date">
                    </div>            
                </div>            
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="data_demissao">Dt Demissão</label>  
                        <input type="text" name="data_demissao" id="data_demissao" maxlength="10" value="<?=@$var_data_demissao_;?>" class="form-control input-date">
                    </div>            
                </div>            
            </div>
        </div>
    </div>
        <div class="col-sm-12">
            <div class="pull-right col-auditoria">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Criação:&nbsp;</strong><?=@$var_usuario_c?> em <?=@$var_data_c_?>&nbsp; 
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Alteração:&nbsp;</strong><?=@$var_usuario_a?> em <?=@$var_data_a_?>
            </div>
        </div>

        <?php
    }
?>

