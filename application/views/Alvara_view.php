<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
?>

<div class="col-sm-12 div-operacao">
    <div class="row">
        <?php
            if ($operacao=="listar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>alvara" data-acao="novo">+ Adicionar</a>
                </div>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>alvara" data-acao="listar">Buscar</a>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
                <?php
            }
            if ($operacao=="novo"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>alvara" data-acao="inserir" data-validacao="">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>alvara" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
            if ($operacao=="editar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>alvara" data-acao="salvar" data-validacao="" data-cod="<?=@$var_cod_alvara;?>">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>alvara" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
        ?>
    </div>
</div>

<?php
    if ($operacao=="listar"){
        ?>
        <div class="col-sm-12">
            <div class="div-table">
                <div class="div-table-fixed">
                    <table class="tablebah">
                        <thead>
                            <tr>
                                <?php
                                    echo $this->functions->table_column("1","nome","Alvará","*","a-acao");
                                ?>
                                <th width="3%">&nbsp;</th>
                                <th width="3%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($dados as $value){
                                    $cod_alvara = $value["cod_alvara"];
                                    $nome = $value["nome"];
                                    ?>
                                    <tr class="tr-linha" data-objeto="<?=LOCAL ?>alvara" data-acao="editar" data-cod="<?=$value["cod_alvara"] ?>">
                                        <td><?=$nome?></td>
                                        <td><a class="btn btn-editar a-acao" data-objeto="<?=LOCAL ?>alvara" data-acao="editar" data-cod="<?=$cod_alvara ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                        <td><a class="btn btn-excluir a-acao" data-objeto="<?=LOCAL ?>alvara" data-acao="excluir" data-cod="<?=$cod_alvara ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pull-right col-paginacao">
                <?=$paginacao; ?>
            </div>
        </div>
        <?php
    }
    if (($operacao=="novo")or($operacao=="editar")){
        ?>
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="control-label" for="nome">Nome</label>  
                        <input type="text" name="nome" id="nome" maxlength="100" value="<?=@$var_nome;?>" class="form-control">
                    </div>            
                </div>            
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="control-label" for="texto">Texto</label>  
                        <textarea class="form-control" rows="5" name="texto" id="texto"><?=@$var_texto;?></textarea>
                    </div>            
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="pull-right col-auditoria">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Criação:&nbsp;</strong><?=@$var_usuario_c?> em <?=@$var_data_c_?>&nbsp; 
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Alteração:&nbsp;</strong><?=@$var_usuario_a?> em <?=@$var_data_a_?>
            </div>
        </div>
        <?php
    }
?>


