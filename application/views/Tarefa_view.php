<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
?>

<div class="col-sm-12 div-operacao">
    <div class="row">
        <?php
            if ($operacao=="listar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>tarefa" data-acao="novo">+ Adicionar</a>
                </div>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>tarefa" data-acao="listar">Buscar</a>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
                <?php
            }
            if ($operacao=="novo"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>tarefa" data-acao="inserir" data-validacao="">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>tarefa" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
            if ($operacao=="editar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>tarefa" data-acao="salvar" data-validacao="" data-cod="<?=@$var_cod_tarefa;?>">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>tarefa" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
        ?>
    </div>
</div>

<?php
    if ($operacao=="listar"){
        ?>
        <div class="col-sm-12">
            <div class="div-table">
                <div class="div-table-fixed">
                    <table class="tablebah">
                        <thead>
                            <tr>
                                <?php
                                    echo $this->functions->table_column("1","nome","Tarefa","*","a-acao");
                                    echo $this->functions->table_column("","cod_pasta","Categoria","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","previsao","Previsão (Dias)","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","valor","Valor (R$)","*","tdoculta a-acao");
                                ?>
                                <th width="3%">&nbsp;</th>
                                <th width="3%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($dados as $value){
                                    ?>
                                    <tr class="tr-linha" data-objeto="<?=LOCAL ?>tarefa" data-acao="editar" data-cod="<?=$value["cod_tarefa"] ?>">
                                        <td><?=$value["nome"]?></td>
                                        <td class="tdoculta"><?=$value["cod_pasta"]?></td>
                                        <td class="tdoculta"><?=$value["previsao"]?></td>
                                        <td class="tdoculta"><?=$value["valor"]?></td>
                                        <td><a class="btn btn-editar a-acao" data-objeto="<?=LOCAL ?>tarefa" data-acao="editar" data-cod="<?=$value["cod_tarefa"] ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                        <td><a class="btn btn-excluir a-acao" data-objeto="<?=LOCAL ?>tarefa" data-acao="excluir" data-cod="<?=$value["cod_tarefa"] ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pull-right col-paginacao">
                <?=$paginacao; ?>
            </div>
        </div>
        <?php
    }
    if (($operacao=="novo")or($operacao=="editar")){
        ?>
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="nome">Tarefa</label>  
                        <input type="text" name="nome" id="nome" maxlength="200" value="<?=@$var_nome;?>" class="form-control">
                    </div>            
                </div>       
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="cod_pasta">Categoria&nbsp;</label>
                        <select class="form-control" name="cod_pasta" id="cod_pasta">
                        <option value="NULL">Selecione...</option>
                        <?php
                            foreach($dados_cod_pasta as $value){
                                $cod_pasta = $value["cod_pasta"];
                                $nome = $value["nome"];

                                $selected = (@$var_cod_pasta==$cod_pasta)? "selected": "";
                                ?>
                                <option value="<?=$cod_pasta ?>" <?=$selected ?>><?=$nome ?></option>
                                <?php
                            }
                        ?>
                        </select>
                    </div>            
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="nome">Previsão (Dias)</label>  
                        <input type="text" name="previsao" id="previsao" maxlength="3" value="<?=@$var_previsao;?>" class="form-control">
                    </div>            
                </div>       
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="valor">Valor (R$)</label>  
                        <input type="text" name="valor" id="valor" maxlength="10" value="<?=@$var_valor;?>" class="form-control">
                    </div>            
                </div>       
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="control-label" for="texto">Observações</label>  
                        <textarea class="form-control" rows="5" name="texto" id="texto"><?=@$var_texto;?></textarea>
                    </div>            
                </div>
            </div>
        </div>
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-10">
                    <h4>Etapas&nbsp;<span  data-toggle="tooltip" data-placement="bottom" title="Para remover uma etapa deixe o nome (etapa) em branco"><span class="glyphicon glyphicon-question-sign" aria-hidden="true" style="color:#4c637b;"></span></span></h4>
                </div>
                <div class="col-sm-2">
                    <a href="#" class="btn pull-right" id="tarefa_etapainserir">Inserir</a>
                </div>
                <div class="col-sm-12">
                    <hr>
                </div>
                <div class="col-sm-12" id="etapas">
                    <div class="row">
                        <div class="col-sm-2">
                            Ordem
                        </div>       
                        <div class="col-sm-2">
                            Responsável
                        </div>       
                        <div class="col-sm-2">
                            Etapa
                        </div>       
                        <div class="col-sm-2">
                            Previsão (Dias)
                        </div>       
                        <div class="col-sm-2">
                            Anexo
                        </div>       
                        <div class="col-sm-2">
                            &nbsp;
                        </div>       
                    </div>
                    <?php
                        if (isset($dados_etapas)){
                            foreach($dados_etapas as $value){
                                ?>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <input type="hidden" class="form-control" name="etapa_arquivoanexado[]" value="<?=@$value["arquivo"];?>">
                                        <input type="hidden" class="form-control" name="etapa_arquivoanexadooriginal[]" value="<?=@$value["arquivo_original"];?>">
                                        <input type="text" class="form-control" name="etapa_ordem[]" maxlength="2" value="<?=@$value["ordem"];?>" placeholder="Ordem">
                                    </div>       
                                    <div class="col-sm-2">
                                        <select class="form-control" name="etapa_tipo[]">
                                        <option value="NULL">Selecione...</option>
                                        <option value="T" <?php if ($value["tipo"]=="T") echo "selected"?> >TripleAie</option>
                                        <option value="C" <?php if ($value["tipo"]=="C") echo "selected"?> >Cliente</option>
                                        </select>
                                    </div>       
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" name="etapa_nome[]" maxlength="200" value="<?=@$value["nome"];?>" placeholder="Etapa">
                                    </div>       
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" name="etapa_previsao[]" maxlength="3" value="<?=@$value["previsao"];?>" placeholder="Previsão (Dias)">
                                    </div>       
                                    <div class="col-sm-2">
                                        <input type="file" class="form-control" name="etapa_arquivo[]">
                                    </div>       
                                    <div class="col-sm-2">
                                        <?php
                                            $arquivo ="./data/tarefa/".@$value["arquivo"];
                                            //echo $arquivo;
                                            if (file_exists($arquivo) and $value["arquivo"]<>""){
                                                ?>
                                                <a href="<?=LOCAL."data/tarefa/".@$value["arquivo"]?>" class="btn btn-sub" target="_blank"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span></a>
                                                <a class="btn btn-excluir tarefa_excluirarquivo" data-pasta="tarefa" data-arquivo="<?=@$value["arquivo"]?>"?><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                                <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="pull-right col-auditoria">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Criação:&nbsp;</strong><?=@$var_usuario_c?> em <?=@$var_data_c_?>&nbsp; 
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Alteração:&nbsp;</strong><?=@$var_usuario_a?> em <?=@$var_data_a_?>
            </div>
        </div>

        <?php
    }
    
?>


