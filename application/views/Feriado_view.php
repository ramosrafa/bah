<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)and is_array($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
?>

<div class="col-sm-12 div-operacao">
    <div class="row">
        <?php
            if ($operacao=="listar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>feriado" data-acao="novo">+ Adicionar</a>
                </div>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>feriado" data-acao="listar">Buscar</a>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
                <?php
            }
            if ($operacao=="novo"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>feriado" data-acao="inserir" data-validacao="">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>feriado" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
            if ($operacao=="editar"){ 
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>feriado" data-acao="salvar" data-validacao="" data-cod="<?=@$var_cod_feriado;?>">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>feriado" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
        ?>
    </div>
</div>

<?php
    if ($operacao=="listar"){
        ?>
        <div class="col-sm-12">
            <div class="div-table">
                <div class="div-table-fixed">
                    <table class="tablebah">
                        <thead>
                            <tr>
                                <?php
                                    echo $this->functions->table_column("1","nome","Nome","*","a-acao");
                                    echo $this->functions->table_column("","data","Dt Feriado","*","a-acao");
                                    echo $this->functions->table_column("","feriado","Feriado","*","a-acao");
                                    echo $this->functions->table_column("","uf","Estado","*","a-acao");
                                    echo $this->functions->table_column("","cidade","Cidade","*","a-acao");
                                ?>
                                <th width="3%">&nbsp;</th>
                                <th width="3%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($dados as $value){
                                    $nome = $value["nome"];
                                    $data = $value["data"];
                                    $feriado = $value["feriado"];
                                    $uf = $value["uf"];
                                    $cidade = $value["cidade"];
                                    ?>
                                    <tr class="tr-linha" data-objeto="<?=LOCAL ?>feriado" data-acao="editar" data-cod="<?=$value["cod_feriado"] ?>">
                                        <td><?=$nome?></td>
                                        <td><?=$data?></td>
                                        <td><?=$feriado?></td>
                                        <td><?=$uf?></td>
                                        <td><?=$cidade?></td>
                                        <td><a class="btn btn-editar a-acao" data-objeto="<?=LOCAL ?>feriado" data-acao="editar" data-cod="<?=$value["cod_feriado"] ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                        <td><a class="btn btn-excluir a-acao" data-objeto="<?=LOCAL ?>feriado" data-acao="excluir" data-cod="<?=$value["cod_feriado"] ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pull-right col-paginacao">
                <?=$paginacao; ?>
            </div>
        </div>
        <?php
    }
    if (($operacao=="novo")or($operacao=="editar")){
        ?>
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label" for="nome">Nome</label>  
                        <input type="text" name="nome" id="nome" maxlength="100" value="<?=@$var_nome;?>" class="form-control">
                    </div>            
                </div>      
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="data">Dt Feriado</label>  
                        <input type="text" name="data" id="data" maxlength="10" value="<?=@$var_data_;?>" class="form-control input-date">
                    </div>            
                </div>  
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="feriado">Feriado</label>  
                        <input type="text" name="feriado" id="feriado" maxlength="50" value="<?=@$var_feriado;?>" class="form-control">
                    </div>            
                </div>    
                <div class="col-sm-12">
                    <hr>   
                </div>  
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label" for="uf">Estado</label>
                        <input type="text" name="uf" id="uf" maxlength="2" value="<?=@$var_uf;?>" class="form-control">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label" for="cidade">Cidade</label>
                        <input type="text" name="cidade" id="cidade" maxlength="100" value="<?=@$var_cidade;?>" class="form-control">
                    </div>
                </div>
            </div>
        </div>  
        <div class="col-sm-12">
            <div class="pull-right col-auditoria">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Criação:&nbsp;</strong><?=@$var_usuario_c?> em <?=@$var_data_c_?>&nbsp; 
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Alteração:&nbsp;</strong><?=@$var_usuario_a?> em <?=@$var_data_a_?>
            </div>
        </div>
        <?php
    }
    if ($operacao=="cliente"){
        ?>
        <div class="col-sm-12">
            <div class="div-table">
                <table class="tablebah">
                    <thead>
                        <tr>
                            <th width="20%">Data</th>
                            <th width="*">Nome</th>
                            <th width="*">Tipo</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach($dados as $value){
                                ?>
                                <tr>
                                    <td><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>&nbsp;&nbsp;<?=$value["data"]?></td>
                                    <td><?=$value["nome"]?></td>
                                    <td><?=$value["feriado"]?></td>
                                    <!--<td><a href="<?=$value->link?>" target="_blank" class="btn pull-right">Ver mais</a></td>-->
                                </tr>
                                <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php
    }
?>


