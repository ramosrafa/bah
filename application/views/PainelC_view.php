<?php 

    if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
?>
<div class="col-sm-12">
    <div class="div-box">
        <div class="div-box-30">     
            <table class="tablepainel">
                <thead>
                    <tr>
                        <th width="10px"><span class="glyphicon glyphicon-bell" aria-hidden="true"></span></th>
                        <th width="*">&nbsp;&nbsp;Notificações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($dados_notificacoes as $value){
                            ?>
                            <tr class="tr-notificacao" data-texto="<?=$value["texto"] ?>">
                                <td><span class="glyphicon glyphicon-certificate" aria-hidden="true"></span></td>
                                <td><?=$value["titulo"] ?></td>
                            </tr>
                            <?php
                        }
                    ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td align="right"><a href="<?=LOCAL?>notificacao/cliente">Ver todas</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="div-box-0">&nbsp;</div>
        <div class="div-box-20">     
            <table class="tablepainel">
                <thead>
                    <tr>
                        <th width="10px" class="certificado"><span class="glyphicon glyphicon-bell" aria-hidden="true"></span></th>
                        <th width="*" class="certificado" colspan="2">&nbsp;&nbsp;Certificado</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        //Vencimento do certificado
                        if (isset($dados_situacaocertificado)) {
                            extract ($dados_situacaocertificado,EXTR_PREFIX_ALL, 'var');
                            ?>
                            <tr>
                                <td><span class="glyphicon glyphicon-certificate" aria-hidden="true"></span></td>
                                <td>Certificado: </td>
                                <td align="right"><?=$var_certificadotipo?></td>
                            </tr>
                            <tr>
                                <td><span class="glyphicon glyphicon-certificate" aria-hidden="true"></span></td>
                                <td>Vencimento:</td>
                                <td align="right"><?=$var_certificado_vencimento_?></td>
                            </tr>
                            <tr>
                                <td><span class="glyphicon glyphicon-certificate" aria-hidden="true"></span></td>
                                <td>Situação:</td>
                                <td align="right"><span class="span-<?=$var_classe?>"><?=$var_situacao?></span></td>
                            </tr>
                            <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="div-box-0">&nbsp;</div>
        <div class="div-box-40">     
            <table class="tablepainel">
                <thead>
                    <tr>
                        <th width="10px"><span class="glyphicon glyphicon-check servico" aria-hidden="true"></span></th>
                        <th width="*" colspan="2">&nbsp;&nbsp;Serviços Em Andamento</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($dados_servico as $value){
                            ?>
                            <tr class="tr-linha" data-objeto="<?=LOCAL ?>servico" data-acao="ver" data-cod="<?=$value["cod_servico"] ?>">
                                <td><span class="glyphicon glyphicon-certificate" aria-hidden="true"></span></td>
                                <td><?=$value["tarefa"] ?></td>
                                <td align="right"><?=$value["data_previsao_"] ?></td>
                            </tr>
                            <?php
                        }
                    ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="right"><a href="<?=LOCAL?>servico/listar">Ver todos</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="div-box">
        <div class="div-box-30">     
            <div class="div-calendario">     
                ...
            </div>
        </div>
        <div class="div-box-0">&nbsp;</div>
        <div class="div-box-30">     
            <table class="tablepainel tabela-coleta">
                <thead>
                    <tr>
                        <th width="10px" class="coleta"><span class="glyphicon glyphicon-cloud-download" aria-hidden="true"></span></th>
                        <th width="*" class="coleta">&nbsp;&nbsp;Coletas<br><i>Obrigações Cliente</i></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="div-box-0">&nbsp;</div>
        <div class="div-box-30">     
            <table class="tablepainel tabela-entrega">
                <thead>
                    <tr>
                        <th width="10px" class="upload"><span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span></th>
                        <th width="*" class="upload">&nbsp;&nbsp;Entregas<br><i>Rotinas TripleAie</i></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>
    </div>
</div>
