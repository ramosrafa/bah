<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
?>

<div class="col-sm-12 div-operacao">
    <div class="row">
        <?php
            if ($operacao=="listar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>evento" data-acao="novo">+ Adicionar</a>
                </div>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>evento" data-acao="listar">Buscar</a>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
                <?php
            }
            if ($operacao=="novo"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>evento" data-acao="inserir" data-validacao="validar">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>evento" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
            if ($operacao=="editar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>evento" data-acao="salvar" data-validacao="validar" data-cod="<?=@$var_cod_evento;?>">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>evento" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
        ?>
    </div>
</div>

<?php
    if ($operacao=="listar"){
        ?>
        <div class="col-sm-12">
            <div class="div-table">
                <div class="div-table-fixed">
                    <table class="tablebah">
                        <thead>
                            <tr>
                                <?php
                                    echo $this->functions->table_column("1","nome","Evento","*","a-acao");
                                    echo $this->functions->table_column("","cod_pasta","Pasta","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","sigla","Sigla","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","tipo","Tipo","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","calendario","Calendário","*","tdoculta a-acao");
                                ?>
                                <th width="3%">&nbsp;</th>
                                <th width="3%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($dados as $value){
                                    $cod_evento = $value["cod_evento"];
                                    $nome = $value["nome"];
                                    $cod_pasta = $value["cod_pasta"];
                                    $sigla = $value["sigla"];
                                    $tipo = $value["tipo"];
                                    $calendario = $value["calendario"];
                                    ?>
                                    <tr class="tr-linha" data-objeto="<?=LOCAL ?>evento" data-acao="editar" data-cod="<?=$value["cod_evento"] ?>">
                                        <td><?=$nome?></td>
                                        <td class="tdoculta"><?=$cod_pasta?></td>
                                        <td class="tdoculta"><?=$sigla?></td>
                                        <td class="tdoculta"><?=$tipo?></td>
                                        <td class="tdoculta"><?=$calendario?></td>
                                        <td><a class="btn btn-editar a-acao" data-objeto="<?=LOCAL ?>evento" data-acao="editar" data-cod="<?=$cod_evento ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                        <td><a class="btn btn-excluir a-acao" data-objeto="<?=LOCAL ?>evento" data-acao="excluir" data-cod="<?=$cod_evento ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pull-right col-paginacao">
                <?=$paginacao; ?>
            </div>
        </div>
        <?php
    }
    if (($operacao=="novo")or($operacao=="editar")){
        ?>
        <input type="hidden" name="cod_evento" id="cod_evento" value="<?=@$var_cod_evento ?>">
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="nome">Nome</label>  
                            <input type="text" name="nome" id="nome" maxlength="100" value="<?=@$var_nome;?>" class="form-control">
                        </div>            
                    </div>            
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label" for="sigla">Sigla&nbsp;<span  data-toggle="tooltip" data-placement="bottom" title="Sigla utilizada ao salvar o arquivo."><span class="glyphicon glyphicon-question-sign" aria-hidden="true" style="color:rgba(93,178,48,1);"></span></span></label>  
                            <input type="text" name="sigla" id="sigla" maxlength="10" value="<?=@$var_sigla;?>" class="form-control">
                        </div>            
                    </div>            
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label" for="cod_pasta">Pasta&nbsp;</label>
                            <select class="form-control" name="cod_pasta" id="cod_pasta">
                            <option value="NULL">Selecione...</option>
                            <?php
                                foreach($dados_cod_pasta as $value){
                                    $cod_pasta = $value["cod_pasta"];
                                    $nome = $value["nome"];

                                    $selected = ($var_cod_pasta==$cod_pasta)? "selected": "";
                                    ?>
                                    <option value="<?=$cod_pasta ?>" <?=$selected ?>><?=$nome ?></option>
                                    <?php
                                }
                            ?>
                            </select>
                        </div>            
                    </div>            
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label" for="tipo">Tipo</label>  
                            <div class="radio">
                                <label><input type="radio" name="tipo" class="cliente-selecionatipo" value="T" <?php if(@$var_tipo=="T") echo "checked" ?>>Obrigação TripleA</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="tipo" class="cliente-selecionatipo" value="C" <?php if(@$var_tipo=="C") echo "checked" ?>>Rotina Cliente</label>
                            </div>
                        </div>            
                    </div>            
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label" for="calendario">Calendário</label>  
                            <div class="radio">
                                <label><input type="radio" name="calendario" class="cliente-selecionacalendario" value="S" <?php if(@$var_calendario=="S") echo "checked" ?>>Mostrar</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="calendario" class="cliente-selecionacalendario" value="N" <?php if(@$var_calendario=="N") echo "checked" ?>>Não mostrar</label>
                            </div>
                        </div>            
                    </div>            
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label" for="texto">Texto</label>  
                            <textarea class="form-control" rows="2" name="texto" id="texto"><?=@$var_texto;?></textarea>
                        </div>            
                    </div>
                    <div class="col-sm-12 div-form-sub">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Ocorrência (Meses)</h4>
                                <hr>         
                                <?php
                                    for ($i=1;$i<=12;$i++){
                                        $mes = ($i<10)?"0$i":$i;
                                        
                                        ?>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <input type="checkbox" class="form-check-input" name="mes_<?=$mes?>" id="mes_<?=$mes?>"  value="S" <?php if (@${"var_mes_$mes"}=="S") echo "checked"; ?>>
                                                <label class="form-check-label" for="mes_<?=$mes?>">&nbsp;<?=MES_[$mes]?>&nbsp;&nbsp;&nbsp;</label>
                                            </div>            
                                        </div> 
                                        <?php
                                    }
                                ?>

                            </div>            
                        </div>            
                    </div>    
                    
                    <div class="col-sm-12 div-form-sub">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Cálculo Vencimento</h4>
                                <hr>         
                            </div>            
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="vencimento">Vencimento</label>  
                                    <select class="form-control" name="vencimento">
                                    <option value="NULL">Selecione...</option>
                                    <option value="S" <?php if (@$var_vencimento=="S") echo "selected" ?>>Calcular</option>
                                    <option value="N" <?php if (@$var_vencimento=="N") echo "selected" ?>>Não calcular</option>
                                    </select>
                                </div>            
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="vencimento_dias">Dias P/ Vencimento</label>  
                                    <input type="text" name="vencimento_dias" id="vencimento_dias" maxlength="2" value="<?=@$var_vencimento_dias;?>" class="form-control"> 
                                </div>            
                            </div>            
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="vencimento_diastipo">Contar dias</label>  
                                    <select class="form-control" name="vencimento_diastipo" id="vencimento_diastipo">
                                    <option value="NULL">Selecione...</option>
                                    <option value="C" <?php if (@$var_vencimento_diastipo=="C") echo "selected" ?>>Dias corridos</option>
                                    <option value="U" <?php if (@$var_vencimento_diastipo=="U") echo "selected" ?>>Dias úteis</option>
                                    <option value="T" <?php if (@$var_vencimento_diastipo=="T") echo "selected" ?>>Último dia</option>
                                    </select>
                                </div>            
                            </div>            
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="vencimento_meses">Meses P/ Vencimento</label>  
                                    <input type="text" name="vencimento_meses" id="vencimento_meses" maxlength="2" value="<?=@$var_vencimento_meses;?>" class="form-control"> 
                                </div>            
                            </div>            
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="vencimento_ajuste">Ajuste Vencimento</label>  
                                    <select class="form-control" name="vencimento_ajuste" id="vencimento_ajuste">
                                    <option value="NULL">Selecione...</option>
                                    <option value="M" <?php if (@$var_vencimento_ajuste=="M") echo "selected" ?>>Manter</option>
                                    <option value="A" <?php if (@$var_vencimento_ajuste=="A") echo "selected" ?>>Adiantar</option>
                                    <option value="P" <?php if (@$var_vencimento_ajuste=="P") echo "selected" ?>>Postegar</option>
                                    </select>
                                </div>            
                            </div>                        
                        </div>            
                    </div>            
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="pull-right col-auditoria">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Criação:&nbsp;</strong><?=@$var_usuario_c?> em <?=@$var_data_c_?>&nbsp; 
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Alteração:&nbsp;</strong><?=@$var_usuario_a?> em <?=@$var_data_a_?>
            </div>
        </div>

        <?php
    }
?>


