<?php 

    if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 

?>

<div class="col-sm-12">
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <h1>Documentos</h1>
                </div>
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="panel-group" id="accordion">
                
                <div class="panel panel-custom">
                    <?php
                        $lista_cod_pasta = array();
                        
                        foreach($dados_cod_pasta as $value){ 
                            ?>
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion" href="#<?=$value["cod_pasta"]?>">
                                    <span class="glyphicon glyphicon-chevron-right"></span>&nbsp;<?=$value["nome"]?>
                                </a>
                            </div>
                            <div id="<?=$value["cod_pasta"]?>" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <table id="painel" class="">
                                        <tr>
                                            <th width="*">
                                                Documento
                                            </th>
                                            <th width="*" class="tdoculta">
                                                Pasta
                                            </th>
                                            <th width="20%" class="tdoculta">
                                                Entrega
                                            </th>
                                            <th width="20%" class="tdoculta">
                                                Vencimento
                                            </th>
                                            <th width="*">
                                                &nbsp;
                                            </th>
                                        </tr>
                                        <?php
                                            foreach($dados_uploads as $value_){
                                                $arquivo = $value_["arquivo"];
                                                $documento = $value_["documento"];
                                                $cod_pasta = $value_["cod_pasta"];
                                                $cod_pasta = $value_["cod_pasta"];
                                                $entrega = $value_["data_a"];
                                                $vencimento = $value_["data_vencimento"];
                                                $situacao = ($value_["situacao"] == NULL)?'warning':$value_["situacao"];
                                                if ($cod_pasta == $value["cod_pasta"]){
                                                    ?>
                                                    <tr id="tr_<?=$arquivo?>">
                                                        <td>
                                                            <?=$documento?>
                                                        </td>
                                                        <td class="tdoculta">
                                                            <?=$cod_pasta?>
                                                        </td>
                                                        <td class="tdoculta">
                                                            <?=$entrega?>
                                                        </td>
                                                        <td class="tdoculta">
                                                            <?=$vencimento?>
                                                        </td>
                                                        <td>
                                                            <div class="pull-right div-painel-lista">
                                                                <ul class="list-group pull-right"><li class="list-group-item list-group-item-<?=$situacao?>"></li></ul>
                                                            </div>
                                                            <?php
                                                                if ($value_["arquivo"]<>""){
                                                                    ?>
                                                                    <div class="pull-right div-painel-lista">
                                                                        <a class="a-acao-e" data-objeto="<?=LOCAL."painel"?>" data-acao="e" data-a="<?=$arquivo?>">&nbsp;&nbsp;<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>&nbsp;&nbsp;&nbsp;&nbsp;</a>
                                                                    </div>
                                                                    <div class="pull-right div-painel-lista">
                                                                        <a class="a-acao-d" data-objeto="<?=LOCAL."painel"?>" data-acao="a" data-a="<?=$arquivo?>">&nbsp;&nbsp;<span class="glyphicon glyphicon-cloud-download" aria-hidden="true"></span>&nbsp;&nbsp;</a>
                                                                    </div>
                                                                    <?php
                                                                }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </table>
                                </div>
                            </div>
                            <?php
                        }
                    
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
