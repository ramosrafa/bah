<?php 

    if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 

?>

<div class="col-sm-12">
    <div class="row">
                
        
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <br><br>
                    <h1>Entregas</h1>
                </div>
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="panel panel-custom">
                <div class="panel-heading">
                    <div class="row">

                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label" for="documento">Documento</label>  
                                <br><input type="text" name="documento" id="documento" value="<?=@$documento ?>">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label" for="cod_pasta">Pasta</label>  
                                <br>
                                <select class="form-control" name="cod_pasta" id="cod_pasta">
                                <option value="NULL">Selecione...</option>
                                <?php
                                    foreach($dados_cod_pasta as $value){
                                        $selected = ($value["cod_pasta"]==$cod_pasta)? "selected": "";
                                        ?>
                                        <option value="<?=$value["cod_pasta"] ?>" <?=$selected ?>><?=$value["nome"] ?></option>
                                        <?php
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label" for="status">Indicador</label>  
                                <br>
                                <select class="form-control" name="situacao" id="situacao">
                                <option value="NULL">Selecione...</option>
                                <option value="success" <?php if ($situacao=="success") echo "selected" ?>>No prazo</option>
                                <option value="warning" <?php if ($situacao=="warning") echo "selected" ?>>Aguardando</option>
                                <option value="danger" <?php if ($situacao=="danger") echo "selected" ?>>Atraso</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="status">Referência</label>  
                                <br><input type="text" class="painel-mes" name="mes_referencia" id="mes_referencia" value="<?=$mes_referencia ?>">
                                <input type="text" class="painel-ano" name="ano_referencia" id="ano_referencia" value="<?=$ano_referencia ?>">
                                <a class="btn a-acao-painelver" data-objeto="<?=LOCAL ?>painel" data-acao="entrega">Ver</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="panel-group" id="accordion">
                <?php
                    //Verifica situação geral de cada cliente
                    //$arr_cli = array();
                    $cod_cliente_="";
                    foreach($dados as $value){
                        $cod_cliente = $value["cod_cliente"];

                        if ($cod_cliente_=="" or $cod_cliente_<>$cod_cliente){
                            $cod_cliente_ = $cod_cliente;
                            $arr_cli[$cod_cliente] = $value["situacao"];

                        }else{
                            if ($arr_cli[$cod_cliente]==NULL) $arr_cli[$cod_cliente] = "warning";
                            if ($arr_cli[$cod_cliente]=="success" and $value["situacao"]=="warning") $arr_cli[$cod_cliente] = "warning";
                            if (($arr_cli[$cod_cliente]=="success" or $arr_cli[$cod_cliente]=="waning") and $value["situacao"]=="danger") $arr_cli[$cod_cliente] = "danger";
                        }
                    }
                    //print_r ($arr_cli); exit;

                    $cod_cliente_="";
                    foreach($dados as $value){
                        $cod_cliente = $value["cod_cliente"];
                        $cliente = $value["cliente"];

                        if ($cod_cliente_=="" or $cod_cliente_<>$cod_cliente){
                            $cod_cliente_ = $cod_cliente;

                            ?>
                            <div class="panel panel-custom">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#<?=$cod_cliente?>">
                                        <span class="glyphicon glyphicon-chevron-right"></span>&nbsp;<?=$cliente?>
                                        <ul class="list-group pull-right"><li class="list-group-item list-group-item-<?=$arr_cli[$cod_cliente]?>"></li></ul>
                                    </a>
                                </div>
                                <div id="<?=$cod_cliente?>" class="panel-collapse collapse">
                                    <div class="panel-body">

                                        <table id="painel" class="">
                                            <tr>
                                                <th width="*">
                                                    Documento
                                                </th>
                                                <th width="*" class="tdoculta">
                                                    Pasta
                                                </th>
                                                <th width="20%" class="tdoculta">
                                                    Entrega
                                                </th>
                                                <th width="20%" class="tdoculta">
                                                    Competência
                                                </th>
                                                <th width="20%" class="tdoculta">
                                                    Vencimento
                                                </th>
                                                <th width="*">
                                                    &nbsp;
                                                </th>
                                            </tr>
                                            <?php
                                                foreach($dados as $value_){
                                                    $arquivo = $value_["arquivo"];
                                                    $documento = $value_["documento"];
                                                    $cod_pasta = $value_["cod_pasta"];
                                                    $entrega = $value_["data_a"];
                                                    $competencia = $value_["data_competencia"];
                                                    $vencimento = $value_["data_vencimento"];
                                                    $situacao = ($value_["situacao"] == NULL)?'warning':$value_["situacao"];

                                                    if ($value_["cod_cliente"]==$cod_cliente){
                                                        ?>
                                                        <tr id="tr_<?=$arquivo?>">
                                                            <td>
                                                                <?=$documento?>
                                                            </td>
                                                            <td class="tdoculta">
                                                                <?=$cod_pasta?>
                                                            </td>
                                                            <td class="tdoculta">
                                                                <?=$entrega?>
                                                            </td>
                                                            <td class="tdoculta">
                                                                <?=$competencia?>
                                                            </td>
                                                            <td class="tdoculta">
                                                                <?=$vencimento?>
                                                            </td>
                                                            <td>
                                                                <div class="pull-right div-painel-lista">
                                                                    <ul class="list-group pull-right"><li class="list-group-item list-group-item-<?=$situacao?>"></li></ul>
                                                                </div>
                                                                <?php
                                                                    if ($value_["arquivo"]<>""){
                                                                        ?>
                                                                        <div class="pull-right div-painel-lista">
                                                                            <a class="a-acao-e" data-objeto="<?=LOCAL."painel"?>" data-acao="e" data-a="<?=$arquivo?>">&nbsp;&nbsp;<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>&nbsp;&nbsp;&nbsp;&nbsp;</a>
                                                                        </div>
                                                                        <div class="pull-right div-painel-lista">
                                                                            <a class="a-acao-d" data-objeto="<?=LOCAL."painel"?>" data-acao="a" data-a="<?=$arquivo?>">&nbsp;&nbsp;<span class="glyphicon glyphicon-cloud-download" aria-hidden="true"></span>&nbsp;&nbsp;</a>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </table>

                                    </div>
                                </div>
                            </div>

                            <?php
                        }
                    }
                ?>
            </div>
        </div>                    
         

    </div>
</div>
