<div class="col-sm-12 div-form">
    <div class="row">
        <div class="col-sm-12">
            <h2>Triple A</h2>
            <p class="p-verde">Canoas/RS</p>
            <p>R. Regente Feijó, 71
            <br>Canoas – RS - Brasil
            <br>+ 55 51 3031-7171
            <br>tripleaie@tripleaie.com.br
            </p>
        </div>
    </div>
</div>
<div class="col-sm-12 div-form">
    <div class="row">
        <div class="col-sm-12">
            <h2>Formulário</h2>
            <input type="hidden" name="nome" id="nome" maxlength="100" class="form-control" value="<?=$this->session->userdata('nome') ?>">
            <input type="hidden" name="email" id="email" maxlength="100" class="form-control" value="<?=$this->session->userdata('email') ?>">
            <div class="form-group">
                <label class="control-label" for="cod_pasta">Pasta&nbsp;</label>
                <select class="form-control" name="cod_pasta" id="cod_pasta">
                <option value="NULL">Selecione...</option>
                <?php
                    foreach($dados_cod_pasta as $value){
                        $nome = $value["nome"];
                        ?>
                        <option value="<?=str_replace(" ","_",$nome) ?>"><?=$nome ?></option>
                        <?php
                    }
                ?>
                </select>
            </div>            
            
            <div class="form-group">
                <label class="control-label" for="mensagem">Mensagem</label>  
                <textarea class="form-control" rows="5" name="mensagem" id="mensagem"></textarea>
            </div>            
            <div class="form-group">
                <a class="btn a-acao-suporteenviar" data-objeto="<?=LOCAL ?>suporte" data-acao="enviar">Enviar</a>
            </div>            
        </div>
    </div>
</div>
