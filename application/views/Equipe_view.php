<div class="col-sm-12">
    <div class="row">
        <?php
            foreach($dados_equipe as $value){
                $cod_usuario = $value["cod_usuario"];
                $nome = $value["nome"];
                $texto = $value["texto"];
                $email = $value["email"];
                $setor = $value["setor"];

                $foto = LOCAL."data/usuario/{$cod_usuario}/usuario_{$cod_usuario}.jpg";
                if (!file_exists("data/usuario/{$cod_usuario}/usuario_{$cod_usuario}.jpg"))$foto = LOCAL."web/usuario.png";
                ?> 
                <div class="col-sm-6">
                    <div class="col-equipe" style="height: 320px;">
                        <center>
                        <a href="<?=LOCAL."suporte/{$cod_usuario}"?>"><img class="img-responsive img-circle" style="cursor:pointer; width:105px; height:98px" src="<?=$foto?>"></a>
                        <br>
                        <h5><?=$nome?> / <?=$setor?></h5>
                        <span><span class="glyphicon glyphicon-send" aria-hidden="true"></span>&nbsp;<?=$email?></span>
                        <br>
                        <br>
                        <p>&nbsp;<?=$texto?></p>
                        </center>
                    </div>
                </div>
                <?php
            }
        ?>
    </div>
</div>