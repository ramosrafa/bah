<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
    //var_dump($dados_usuariopasta);exit;
?>

<div class="col-sm-12 div-operacao">
    <div class="row">
        <?php
            if ($operacao=="listar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>usuario" data-acao="novo">+ Adicionar</a>
                </div>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>usuario" data-acao="listar">Buscar</a>
                </div>
                <div class="pull-right div-operacao-check">
                    <input type="radio" class="form-check-input" name="busca_tipo_cliente" value="" <?php if (@$this->input->get_post('busca_tipo_cliente')=="") echo "checked"; ?>><label class="form-check-label" for="atividade_servico">&nbsp;Todos&nbsp;&nbsp;&nbsp;</label>
                    <input type="radio" class="form-check-input" name="busca_tipo_cliente" value="T" <?php if (@$this->input->get_post('busca_tipo_cliente')=="T") echo "checked"; ?>><label class="form-check-label" for="atividade_servico">&nbsp;TripleA&nbsp;&nbsp;&nbsp;</label>
                    <input type="radio" class="form-check-input" name="busca_tipo_cliente" value="C" <?php if (@$this->input->get_post('busca_tipo_cliente')=="C") echo "checked"; ?>><label class="form-check-label" for="atividade_servico">&nbsp;Cliente&nbsp;&nbsp;&nbsp;</label>
                    <input type="radio" class="form-check-input" name="busca_tipo_cliente" value="P" <?php if (@$this->input->get_post('busca_tipo_cliente')=="P") echo "checked"; ?>><label class="form-check-label" for="atividade_servico">&nbsp;Partner&nbsp;&nbsp;&nbsp;</label>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
                <?php
            }
            if ($operacao=="novo"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>usuario" data-acao="inserir" data-validacao="validar">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>usuario" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
            if ($operacao=="editar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>usuario" data-acao="salvar" data-validacao="validar" data-cod="<?=@$var_cod_usuario;?>">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>usuario" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
        
        ?>
    </div>
</div>

<?php
    if ($operacao=="listar"){
        ?>
        <div class="col-sm-12">
            <div class="div-table">
                <div class="div-table-fixed">
                    <table class="tablebah">
                        <thead>
                            <tr>
                                <?php
                                    echo $this->functions->table_column("1","nome","Nome","*","a-acao");
                                    echo $this->functions->table_column("","admin","Admin","*","a-acao");
                                    echo $this->functions->table_column("","email","E-mail","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","tipo","Tipo","*","tdoculta a-acao");
                                    echo $this->functions->table_column("","suspenso","Suspenso","*","a-acao");
                                ?>
                                <th width="3%">&nbsp;</th>
                                <th width="3%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($dados as $value){
                                    $cod_usuario = $value["cod_usuario"];
                                    $nome = $value["nome"];
                                    $admin = $value["admin"];
                                    $email = $value["email"];
                                    $tipo = $value["tipo"];
                                    $suspenso = $value["suspenso"];
                                    ?>
                                    <tr class="tr-linha" data-objeto="<?=LOCAL ?>usuario" data-acao="editar" data-cod="<?=$value["cod_usuario"] ?>">
                                        <td><?=$nome?></td>
                                        <td><?=$admin?></td>
                                        <td class="tdoculta"><?=$email?></td> 
                                        <td class="tdoculta"><?=$tipo?></td>
                                        <td><?=$suspenso?></td>
                                        <td><?php if ($cod_usuario<>1) { ?><a class="btn btn-editar a-acao" data-objeto="<?=LOCAL ?>usuario" data-acao="editar" data-cod="<?=$cod_usuario ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a><?php } ?></td> 
                                        <td><?php if ($cod_usuario<>1) { ?><a class="btn btn-excluir a-acao" data-objeto="<?=LOCAL ?>usuario" data-acao="excluir" data-cod="<?=$cod_usuario ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span><?php } ?></td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pull-right col-paginacao">
                <?=$paginacao; ?>
            </div>
        </div>
        <?php
    }
    if (($operacao=="novo")or($operacao=="editar")){
        ?>

        <input type="hidden" name="cod_cliente" id="cod_cliente" value="<?=($this->session->userdata('cod_cliente'))?>">
        <input type="hidden" name="cod_usuario" id="cod_usuario" value="<?=@$var_cod_usuario ?>">
        <div class="col-sm-12 div-form">
            <div class="row">
                <?php
                    if ($operacao=="novo"){
                        ?>
                        <div class="col-sm-12">
                            <div class="alert alert-warning" role="alert">
                            Atenção. Ao salvar o novo usuário o sistema gera e envia uma senha para o e-mail.
                            </div>
                        </div>
                        <?php
                    }
                ?>
                <div class="tab-content">
                    <div id="tab-cadastro" class="tab-pane fade in active">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="foto">Foto Perfil</label> 
                                <br>
                                <?php
                                    $arquivo ="./data/usuario/".@$var_cod_usuario."/usuario_".@$var_cod_usuario.".jpg";
                                    if (file_exists($arquivo)){
                                        $arquivo =LOCAL."data/usuario/".@$var_cod_usuario."/usuario_".@$var_cod_usuario.".jpg";
                                        ?>
                                        <center> 
                                        <img src="<?=$arquivo?>" alt="Foto" height="50px">
                                        </center> 
                                        <?php
                                    } else {
                                        ?>
                                        <br>
                                        <?php
                                    }
                                ?>
                            </div>            
                            <div class="form-group">
                                <input type="file" name="foto" id="foto" class="form-control">
                            </div>            
                            <div class="form-group">
                                <br>
                                <hr>
                                <br>
                            </div>            
                            <div class="form-group">
                                <input type="checkbox" class="form-check-input" name="suspenso" id="suspenso"  value="S" <?php if (@$var_suspenso=="S") echo "checked"; ?>><label class="control-label" for="">&nbsp;Suspenso&nbsp;&nbsp;&nbsp;</label>
                                <?php if ($this->session->userdata('tipo')=="T") {?>
                                    <br><input type="checkbox" name="admin" id="admin" value="S" <?php if (@$var_admin=="S") echo "checked";?>><label class="control-label" for="tipo">&nbsp;Admin&nbsp;&nbsp;&nbsp;</label> 
                                    <br><input type="checkbox" name="senha_certificado" id="senha_certificado" value="S" <?php if (@$var_senha_certificado=="S") echo "checked";?>><label class="control-label" for="tipo">&nbsp;Ver senha certificado&nbsp;&nbsp;&nbsp;</label>
                                <?php }?>                
                                <br><input type="checkbox" class="form-check-input" name="config_receberemail" id="config_receberemail"  value="S" <?php if (@$var_config_receberemail=="S") echo "checked"; ?>><label class="control-label" for="">&nbsp;Receber notificações por e-mail&nbsp;&nbsp;&nbsp;</label>
                            </div>            
                            <div class="form-group">
                                <?php if ($this->session->userdata('tipo')=="T") {?>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="tipo_cliente">Tipo</label>  
                                            <div class="radio">
                                            <label><input type="radio" name="tipo_cliente" class="cliente-selecionatipo" value="T" <?php if(@$var_tipo=="T") echo "checked" ?>>TripleA</label>
                                            </div>
                                            <div class="radio">
                                            <label><input type="radio" name="tipo_cliente" class="cliente-selecionatipo" value="C" <?php if(@$var_tipo=="C") echo "checked" ?>>Cliente</label>
                                            </div>
                                            <div class="radio">
                                            <label><input type="radio" name="tipo_cliente" class="cliente-selecionatipo" value="P" <?php if(@$var_tipo=="P") echo "checked" ?>>Partner</label>
                                            </div>
                                        </div>            
                                    </div>
                                <?php }?>                
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label class="control-label" for="nome">Nome</label>  
                                <input type="text" name="nome" id="nome" maxlength="100" value="<?=@$var_nome;?>" class="form-control">
                            </div>            
                            <div class="form-group">
                                <label class="control-label" for="email">E-mail</label>  
                                <input type="text" name="email" id="email" maxlength="100" value="<?=@$var_email;?>" class="form-control">
                            </div>        
                            <?php
                                if ($operacao=="editar"){
                                    ?>
                                    <div class="form-group">
                                        <label class="control-label" for="senha">Senha</label>  
                                        <input type="password" name="senha" id="senha" maxlength="100" value="" class="form-control">
                                    </div>            
                                    <?php
                                }
                            ?>    
                            

                            <div class="form-group">
                                <label class="control-label" for="cod_setor">Setor&nbsp;</label>
                                <select class="form-control" name="cod_setor" id="cod_setor">
                                <option value="NULL">Selecione...</option>
                                <?php
                                    foreach($dados_setor as $value){
                                        $cod_setor = $value["cod_setor"];
                                        $nome = $value["nome"];

                                        $selected = (@$var_cod_setor==$cod_setor)? "selected": "";
                                        ?>
                                        <option value="<?=$cod_setor ?>" <?=$selected ?>><?=$nome ?></option>
                                        <?php
                                    }
                                ?>
                                </select>
                            </div>            

                            <div class="form-group">
                                <label class="control-label" for="usuario_pasta">Pasta</label>  
                                <br>
                                <span class="control-label multiselect-native-select">
                                    <select name="usuario_pasta[]" id="usuario_pasta" multiple="multiple">
                                    <?php
                                    
                                        foreach($dados_usuariopasta as $value){
                                            $cod_usuario = $value["cod_usuario"];
                                            $cod_pasta = $value["cod_pasta"];
                                            $pasta = $value["pasta"];

                                            $selected = ($cod_usuario<>NULL)?"selected":"";
                                            echo "<option value=\"{$cod_pasta}\" {$selected}>{$pasta}</option>";
                                            ?>
                                            <?php
                                        }
                                    ?>                    
                                    </select>
                                </span>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="cliente">Cliente</label>  
                                <br>
                                <span class="control-label multiselect-native-select">
                                    <select name="usuario_cliente[]" id="usuario_cliente" multiple="multiple">
                                    <?php
                                        foreach($dados_usuariocliente as $value){
                                            $cod_usuario = $value["cod_usuario"];
                                            $cod_cliente = $value["cod_cliente"];
                                            $cliente = $value["cliente"];

                                            $selected = ($cod_usuario<>NULL)?"selected":"";
                                            echo "<option value=\"{$cod_cliente}\" {$selected}>{$cliente}</option>";
                                            ?>
                                            <?php
                                        }
                                    ?>                    
                                    </select>
                                </span>
                            </div> 
                            <div class="form-group">
                                <label class="control-label" for="texto">Apresentação</label>  
                                <textarea class="form-control" rows="5" name="texto" maxlength="212" id="texto"><?=@$var_texto;?></textarea>
                            </div>            
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="pull-right col-auditoria">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Criação:&nbsp;</strong><?=@$var_usuario_c?> em <?=@$var_data_c_?>&nbsp; 
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Alteração:&nbsp;</strong><?=@$var_usuario_a?> em <?=@$var_data_a_?>
            </div>
        </div>
        <?php
    }
?>
