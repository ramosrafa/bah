<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
?>

<?php

    if ($operacao=="listar_cliente"){
        ?>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="<?=LOCAL ?>drive">Selecionar o cliente</a></li>
                    </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="div-operacao">
                <div class="pull-right">
                    <a class="btn a-acao-buscar">Buscar</a>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="div-table">
                <table class="tablebah">
                    <thead>
                        <tr>
                            <th width="*">Cliente</th>
                            <th width="*" class="tdoculta">Código Interno</th>
                            <th width="3%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach($dados as $value){
                                ?>
                                <tr class="tr-linha" data-href="<?=LOCAL ?>drive/<?=$value["cod_cliente"] ?>">
                                    <td><?=$value["nome"]?></td>
                                    <td class="tdoculta"><?=$value["cod_interno"]?></td>
                                    <td><a class="btn btn-editar" href="<?=LOCAL ?>drive/<?=$value["cod_cliente"] ?>"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span></i></a></td>
                                </tr>
                                <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php
    }
    
    if ($operacao=="listar_pasta"){
        ?>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=LOCAL ?>drive"><?=$cliente["nome"]?> (<?=$cliente["cod_interno"]?>)</a></li>
                        <li class="breadcrumb-item active"><a href="#">Selecionar a pasta</a></li>
                    </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="div-table">
                <table class="tablebah">
                    <thead>
                        <tr>
                            <th width="*">Pasta</th>
                            <th width="3%">Qtd</th>
                            <th width="3%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach($dados as $value){
                                ?>
                                <tr class="tr-linha" data-href="<?=LOCAL ?>drive/<?=$cliente["cod_cliente"]?>/<?=$value["cod_pasta"]?>">
                                    <td><?=$value["nome"]?></td>
                                    <td><?=$value["qtd"]?></td>
                                    <td><a class="btn btn-editar" href="<?=LOCAL ?>drive/<?=$cliente["cod_cliente"]?>/<?=$value["cod_pasta"]?>"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span></i></a></td>
                                </tr>
                                <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php
    }

    if ($operacao=="listar_arquivo"){
        ?>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?=LOCAL ?>drive/<?=$cliente["cod_cliente"]?>"><?=$cliente["nome"]?> (<?=$cliente["cod_interno"]?>)</a></li>
                    <li class="breadcrumb-item"><a href="<?=LOCAL ?>drive/<?=$cliente["cod_cliente"]?>/<?=$cod_pasta["cod_pasta"]?>"><?=$cod_pasta["nome"]?></a></li>
                    </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="div-operacao">
                <div class="pull-right">
                    <a class="btn a-acao-d" data-objeto="<?=LOCAL."arquivo"?>" data-acao="a">&nbsp;&nbsp;<span class="glyphicon glyphicon-cloud-download" aria-hidden="true"></span>&nbsp;&nbsp;Marcados</a>
                </div>
                <div class="pull-right">
                    <a class="btn a-acao-buscar">Buscar</a>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="div-table">
                <table class="tablebah">
                    <thead>
                        <tr>
                            <th width="*"><input type="checkbox" class="form-check-input selecionartodos"></th>
                            <th width="*">Documento</th>
                            <th width="*">Origem</th>
                            <th width="*">Dt Envio</th>
                            <th width="*">Dt Competência</th>
                            <th width="*">Dt Vencimento</th>
                            <th width="3%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach($dados_arquivos as $value){
                                ?>
                                <tr>
                                    <td><input type="checkbox" class="form-check-input" name="arquivo[]" value="<?=$value["arquivo"]?>"></td>
                                    <td><span class="glyphicon <?=$value["icone"]?>" aria-hidden="true" style="color:<?=$value["cor"]?>"></span>&nbsp;<?=$value["nome_original"]?></td>
                                    <td><?=$value["origem_"]?></td>
                                    <td><?=$value["data_c_"]?></td>
                                    <td><?=$value["data_competencia_"]?></td>
                                    <td><?=$value["data_vencimento_"]?></td>
                                    <td>
                                        <a class="a-acao-d" data-objeto="<?=LOCAL."arquivo"?>" data-acao="a" data-a="<?=$value["arquivo"]?>">&nbsp;&nbsp;<span class="glyphicon glyphicon-cloud-download" aria-hidden="true"></span>&nbsp;&nbsp;</a>
                                    </td>
                                </tr>
                                <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php
    }
    ?>

