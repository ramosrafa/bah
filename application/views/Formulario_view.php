<?php if ( ! defined('BASEPATH')) exit('Acesso não permitido'); 
    if (isset($dados)) extract ($dados,EXTR_PREFIX_ALL, 'var');
?>

<div class="col-sm-12 div-operacao">
    <div class="row">
        <?php
            if ($operacao=="listar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>formulario" data-acao="novo">+ Adicionar</a>
                </div>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>formulario" data-acao="listar">Buscar</a>
                </div>
                <div class="pull-right div-operacao-text">
                    <input type="text" name="busca" id="busca" placeholder="busca..." value="<?=@$busca ?>">
                </div>
                <?php
            }
            if ($operacao=="novo"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>formulario" data-acao="inserir" data-validacao="">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>formulario" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
            if ($operacao=="editar"){
                ?>
                <div class="pull-right">
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>formulario" data-acao="salvar" data-validacao="" data-cod="<?=@$var_cod_formulario;?>">Salvar</a>
                    <a class="btn a-acao" data-objeto="<?=LOCAL ?>formulario" data-acao="listar">Voltar para lista</a>
                </div>
                <?php
            }
        ?>
    </div>
</div>

<?php
    if ($operacao=="listar"){
        ?>
        <div class="col-sm-12">
            <div class="div-table">
                <div class="div-table-fixed">
                    <table class="tablebah">
                        <thead>
                            <tr>
                                <?php
                                    echo $this->functions->table_column("1","nome","Nome","*","a-acao");
                                    echo $this->functions->table_column("","cod_pasta","Categoria","*","tdoculta a-acao");
                                ?>
                                <th width="3%">&nbsp;</th>
                                <th width="3%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($dados as $value){
                                    ?>
                                    <tr class="tr-linha" data-objeto="<?=LOCAL ?>formulario" data-acao="editar" data-cod="<?=$value["cod_formulario"] ?>">
                                        <td><?=$value["nome"]?></td>
                                        <td class="tdoculta"><?=$value["cod_pasta"]?></td>
                                        <td><a class="btn btn-editar a-acao" data-objeto="<?=LOCAL ?>formulario" data-acao="editar" data-cod="<?=$value["cod_formulario"] ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                        <td><a class="btn btn-excluir a-acao" data-objeto="<?=LOCAL ?>formulario" data-acao="excluir" data-cod="<?=$value["cod_formulario"] ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pull-right col-paginacao">
                <?=$paginacao; ?>
            </div>
        </div>
        <?php
    }
    if (($operacao=="novo")or($operacao=="editar")){
        ?>
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label" for="nome">Nome</label>  
                        <input type="text" name="nome" id="nome" maxlength="200" value="<?=@$var_nome;?>" class="form-control">
                    </div>            
                </div>       
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label" for="cod_pasta">Categoria&nbsp;</label>
                        <select class="form-control" name="cod_pasta" id="cod_pasta">
                        <option value="NULL">Selecione...</option>
                        <?php
                            foreach($dados_cod_pasta as $value){
                                $cod_pasta = $value["cod_pasta"];
                                $nome = $value["nome"];

                                $selected = (@$var_cod_pasta==$cod_pasta)? "selected": "";
                                ?>
                                <option value="<?=$cod_pasta ?>" <?=$selected ?>><?=$nome ?></option>
                                <?php
                            }
                        ?>
                        </select>
                    </div>            
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="control-label" for="texto">Observações</label>  
                        <textarea class="form-control" rows="5" name="texto" id="texto"><?=@$var_texto;?></textarea>
                    </div>            
                </div>
            </div>
        </div>
        <div class="col-sm-12 div-form">
            <div class="row">
                <div class="col-sm-10">
                    <h4>Campos&nbsp;<span  data-toggle="tooltip" data-placement="bottom" title="Para remover uma campo deixe o nome (campo) em branco"><span class="glyphicon glyphicon-question-sign" aria-hidden="true" style="color:#4c637b;"></span></span></h4>
                </div>
                <div class="col-sm-2">
                    <a href="#" class="btn pull-right" id="formulario_campoinserir">Inserir</a>
                </div>
                <div class="col-sm-12">
                    <hr>
                </div>
                <div class="col-sm-12" id="campos">
                    <div class="row">
                        <div class="col-sm-2">
                            Ordem
                        </div>       
                        <div class="col-sm-2">
                            Tipo
                        </div>       
                        <div class="col-sm-1">
                            Nome
                        </div>       
                        <div class="col-sm-1">
                            Label
                        </div>       
                        <div class="col-sm-2">
                            Opções
                        </div>       
                        <div class="col-sm-2">
                            Requerido
                        </div>       
                        <div class="col-sm-2">
                            &nbsp;
                        </div>       
                    </div>
                    <?php

                        if (isset($dados_campos)){
                            foreach($dados_campos as $value){
                                ?>
                                <div class="row">
                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" name="campo_ordem[]" maxlength="2" value="<?=@$value["ordem"];?>" placeholder="Ordem">
                                    </div>       
                                    <div class="col-sm-2">
                                        <select class="form-control" name="campo_tipo[]">
                                        <option value="NULL">Selecione...</option>
                                        <option value="info" <?php if ($value["tipo"]=="info") echo "selected"?> >Informação</option>
                                        <option value="text" <?php if ($value["tipo"]=="text") echo "selected"?> >Texto</option>
                                        <option value="date" <?php if ($value["tipo"]=="date") echo "selected"?> >Data</option>
                                        <option value="date" <?php if ($value["tipo"]=="select") echo "selected"?> >Seleção</option>
                                        </select>
                                    </div>       
                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" name="campo_nome[]" maxlength="20" value="<?=@$value["nome"];?>" placeholder="Nome">
                                    </div>       
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" name="campo_label[]" value="<?=@$value["label"];?>" placeholder="Label">
                                    </div>       
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" name="campo_opcoes[]" value="<?=@$value["opcoes"];?>" placeholder="Opções">
                                    </div>       
                                    <div class="col-sm-2">
                                        <select class="form-control" name="campo_requerido[]">
                                        <option value="NULL">Selecione...</option>
                                        <option value="S" <?php if ($value["requerido"]=="S") echo "selected"?> >Sim</option>
                                        <option value="N" <?php if ($value["requerido"]=="N") echo "selected"?> >Não</option>
                                        </select>
                                    </div>       
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" name="campo_ajuda[]" maxlength="100" value="<?=@$value["ajuda"];?>" placeholder="Ajuda">
                                    </div>       
                                </div>
                            <?php
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="pull-right col-auditoria">
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Criação:&nbsp;</strong><?=@$var_usuario_c?> em <?=@$var_data_c_?>&nbsp; 
                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;<strong>Alteração:&nbsp;</strong><?=@$var_usuario_a?> em <?=@$var_data_a_?>
            </div>
        </div>
        <?php
    }
    
?>


