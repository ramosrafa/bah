<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['post_controller'] = array(     	// post_controller indicated execution of hooks after controller is finished
    'filepath'  => 'hooks',         	// Name of folder where Hook file is stored
    'filename'  => 'Log_hook.php',    	// Name of the Hook file
    'class'     => 'Log_hook',          // Name of Class
    'function'  => 'Log',     			// Name of function to be executed in from Class
);