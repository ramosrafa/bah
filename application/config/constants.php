<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('EXIBIR_LOG', 'S');
define('AUTOR', 'GANE Tecnologia da Informação; Rafael Ramos');
define('KEYWORDS', 'TripleA. Contabilidade.');
define('TITULO', 'TripleA');
define('TITULO_SISTEMA', 'TripleA');

if (ENVIRONMENT=='development') define('LOGO_SISTEMA', 'web/bah.png');
if (ENVIRONMENT=='testing') define('LOGO_SISTEMA', 'web/bah_teste.png');
if (ENVIRONMENT=='production') define('LOGO_SISTEMA', 'web/bah.png');

define('LOGO_RELATORIO', 'web/bah_relatorio.png');
define('LOGO_TRIPLEAIE', 'web/triplea.png');

define('EMAIL_ADM', 'bah@tripleaie.com.br');
define('EMAIL_SUPORTE', 'bah@tripleaie.com.br '); 
 
if (ENVIRONMENT=='development') define('EMAIL_NR', 'ramosrafa18@gmail.com'); 
if (ENVIRONMENT=='testing') define('EMAIL_NR', 'ramosrafa18@gmail.com'); 
if (ENVIRONMENT=='production') define('EMAIL_NR', 'bah@tripleaie.com.br'); 

define('CHAVE_APP', 'gane');

define("GOOGLE_API_KEY", "AAAAdGKWIj0:APA91bEpF4zQrr4aCD6c3mIgn-mM1YHsPUVHIFNXfNcBUBA_V6_5wZH_WAICyspzT_L3QMAyE8cyJdwZ0OvV55GGjVRUjn4jGb64ZGACNOGKceG-kQ9l5c3ddgR0X2XSSyqojmvH1k-Q"); 

define('LIMIT', 11);
define('IP', $_SERVER['SERVER_ADDR']);
define('NAVEGADOR', $_SERVER['HTTP_USER_AGENT']); 

//MESES POR EXTENSO
define("MES", ['01' => 'Janeiro','02' => 'Fevereiro','03' => 'Marco','04' => 'Abril','05' => 'Maio','06' => 'Junho','07' => 'Julho','08' => 'Agosto','09' => 'Setembro','10' => 'Outubro','11' => 'Novembro','12' => 'Dezembro']);
define("MES_", ['01' => 'Jan','02' => 'Fev','03' => 'Mar','04' => 'Abr','05' => 'Mai','06' => 'Jun','07' => 'Jul','08' => 'Ago','09' => 'Set','10' => 'Out','11' => 'Nov','12' => 'Dez']);

//EMAIL
define('EMAIL_HOST', "{imap.kinghost.net:993/imap/ssl/novalidate-cert}INBOX");
define('EMAIL_USER', "gane@tripleaie.com.br");
define('EMAIL_PASS', "tri2007");

//FTP
define('FTP_HOSTNAME', '107.161.186.242');
define('FTP_USERNAME', 'cscombr');
define('FTP_PASSWORD', 'pwdadm');
if (ENVIRONMENT=='development')	define('FTP_LOCALMIDIA', 'data');
if (ENVIRONMENT=='testing')	define('FTP_LOCALMIDIA', 'data');
if (ENVIRONMENT=='production')	define('FTP_LOCALMIDIA', 'data');

//ASAAS
if (ENVIRONMENT<>'development'){
    define('ASAAS_URL', 'https://sandbox.asaas.com/api/v3/');    
    define('ASAAS_KEY', '128abc706f1043259cc543662febda13117f343869ef5e9473bb2acf00bb6cea');    
} else {
    define('ASAAS_URL', 'https://www.asaas.com/api/v3/');    
    define('ASAAS_KEY', 'e8e4626ba0a31392133e01a124e5ccf57426e1b49ae4655a744115cfc1d79ddc');    
}

//BD
if (ENVIRONMENT=='development'){
    define('HOSTNAME', 'mysql06-farm13.kinghost.net');
    define('USERNAME', 'tripleaie01');
    define('PASSWORD', 'tri2020');
    define('DATABASE', 'tripleaie01');
    define('DBDRIVER', 'mysql');
    define('DBPREFIX', '');
    define('PCONNECT', 'TRUE');
    define('DB_DEBUG', 'TRUE');
    define('CACHE_ON', 'TRUE');
    define('CACHEDIR', 'cache');
    define('CHAR_SET', 'utf8');
    define('DBCOLLAT', 'utf8_general_ci');
    define('SWAP_PRE', '');
    define('AUTOINIT', 'TRUE');
    define('STRICTON', 'FALSE'); 
}

if (ENVIRONMENT=='testing'){ 
    define('HOSTNAME', 'mysql06-farm13.kinghost.net');
    define('USERNAME', 'tripleaie01');
    define('PASSWORD', 'tri2020');
    define('DATABASE', 'tripleaie01');
    define('DBDRIVER', 'mysql');
    define('DBPREFIX', '');
    define('PCONNECT', 'TRUE');
    define('DB_DEBUG', 'TRUE');
    define('CACHE_ON', 'TRUE');
    define('CACHEDIR', 'cache');
    define('CHAR_SET', 'utf8');
    define('DBCOLLAT', 'utf8_general_ci');
    define('SWAP_PRE', '');
    define('AUTOINIT', 'TRUE');
    define('STRICTON', 'FALSE'); 
}

if (ENVIRONMENT=='production'){  
    define('HOSTNAME', 'mysql.tripleaie.com.br');
    define('USERNAME', 'tripleaie03');
    define('PASSWORD', 'tri2020');
    define('DATABASE', 'tripleaie03');
    define('DBDRIVER', 'mysql');
    define('DBPREFIX', '');
    define('PCONNECT', 'TRUE');
    define('DB_DEBUG', 'TRUE');
    define('CACHE_ON', 'TRUE');
    define('CACHEDIR', 'cache');
    define('CHAR_SET', 'utf8');
    define('DBCOLLAT', 'utf8_general_ci');
    define('SWAP_PRE', '');
    define('AUTOINIT', 'TRUE');
    define('STRICTON', 'FALSE');
}

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); //truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); //truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); //no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); //generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); //configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); //file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); //unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); //unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); //invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); //database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); //lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); //highest automatically-assigned error code
