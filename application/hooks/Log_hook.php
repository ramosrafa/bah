<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_hook {
 
    function __construct() {
       // Anything except exit() :P
       $this->CI =& get_instance();
    }
 
    // Name of function same as mentioned in Hooks Config
    function Log() {

        $filepath = APPPATH . 'logs/query-log-' . date('Y-m-d') . '.php'; // Creating Query Log file with today's date in application/logs folder
        $handle = fopen($filepath, "a+");                 // Opening file with pointer at the end of the file

        $times = $this->CI->db->query_times;                   // Get execution time of all the queries executed by controller
        foreach ($this->CI->db->queries as $key => $query) { 
            //$this->CI->Log_model->inserir("tit","inserir",$query);
            $sql = $query . " \n Execution Time:" . $times[$key]; // Generating SQL file alongwith execution time
            fwrite($handle, $sql . "\n\n");              // Writing it in the log file
        }
        fclose($handle);      // Close the file 
    }
 
}