window.onload=function() { 
    $('#div-loading').css('display','none');
    console.log("On Loading");
}

$(document).ready(function(){


    //Geral - Ativar toltip
    $('[data-toggle="tooltip"]').tooltip(); 

    //Máscaras 
    function fnMascaras(){
        $(".input-date").mask("99/99/9999");
        $(".input-numero").mask('999999', {reverse: true});
        //$(".input-valor").mask('999999,99', {reverse: true});
        $(".input-mes").mask("99/9999");
        $(".input-cartao").mask("9999-9999-9999-9999");
        $(".input-cartaomes").mask("99");
        $(".input-cartaoano").mask("99");
        $(".input-cartaocodigo").mask("999");
    }
    fnMascaras();

    $("#menu").click(function(){
        //console.log(this);
        $('#wrapper').toggleClass( 'wrapper-responsive' );
    });
    
    $( ".input-date" ).dblclick(function() {
        
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = (day<10 ? '0' : '') + day + '/'+ (month<10 ? '0' : '') + month + '/' + d.getFullYear();
        
        $(this).val(output);
        
    });
    
    //Selecionar todos checks de uma lista
    $('.selecionartodos').click(function(e){
        var table= $(e.target).closest('table');
        $('td input:checkbox',table).prop('checked',this.checked);
    });

    //Fixar primeira linha e coluna da tabela
    $('tbody').scroll(function(e) { //detect a scroll event on the tbody
        $('thead').css("left", -$("tbody").scrollLeft()); //fix the thead relative to the body scrolling
        $('thead th:nth-child(1)').css("left", $("tbody").scrollLeft()); //fix the first cell of the header
        $('tbody td:nth-child(1)').css("left", $("tbody").scrollLeft()); //fix the first column of tdbody
    });

    if ($('.div-calendario').length){
        
        $(".div-calendario").on('click',".div-calendario-seleciona",function(e){
            var dia = $(this).attr("data-dia");
            var mes = $(this).attr("data-mes");
            var ano = $(this).attr("data-ano");
            
            fnCalendario(dia,mes,ano);
        });

        fnCalendario();

        function fnCalendario (dia=0,mes=0,ano=0){
            var local = $("#local").val();
            var url=local+"painel/painel/"+dia+"/"+mes+"/"+ano;

            //console.log( "fnCalendario url:"+url);

            $.ajax({url: url, dataType: 'JSON', success: function(data){
                //console.log( data);
                $('.div-calendario').empty();
                $('.div-calendario').append(data.calendario);

                $('.tabela-coleta tbody').empty();
                $(data.coleta).appendTo($(".tabela-coleta"));

                $('.tabela-entrega tbody').empty();
                $(data.entrega).appendTo($(".tabela-entrega"));
            }});
        }
    }

        
    if ($('#div-painel-chart-container').length){
        var local = $("#local").val();
		var url=local+"painel/grafico_quantidademes";

        //console.log(".div-painel-chart :"+url);
        jQuery.ajax({
			url: url,
			dataType: 'JSON', 
            cache : false,
            contentType : false,
            processData : false,
			success: function(jsonData) {
                //console.log (jsonData);

                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {

                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Mes');
                    data.addColumn('number', 'Qtd');
                    $.each(jsonData, function(i,jsonData){
                        aux = jsonData.mes_;
                        data.addRows([ [aux, (jsonData.qtd*1)]]);
                    });

                    var options = {
                        title: 'Entregas nos Últimos Meses',
                        colors: ['#5DB230'],
                        height: 170,
                        curveType: 'function',
                        legend: { position: 'bottom' },
                        backgroundColor: '#f9f9f9',
                        'titleTextStyle': { color: '#606060', fontName: 'Roboto', fontSize: '14', bold: false, italic: false, lineheight: 40,padding:'20px' },
                    };

                    var chart = new google.visualization.AreaChart(document.getElementById('div-painel-chart-container'));

                    $('#div-painel-chart-container').empty();
                    chart.draw(data, options);
                }
			}
        });	
    }
    
    if ($('#entrega').length){
        $.mask.definitions['~'] = "[+-]";
        $("#entrega").mask("99/99/9999");
        $("#entrega").dateTimePicker({format: 'dd/MM/yyyy'});
    }
    
    if ($('#certificado_vencimento').length){
        $.mask.definitions['~'] = "[+-]";
        $("#certificado_vencimento").mask("99/99/9999");
        $("#certificado_vencimento").dateTimePicker({format: 'dd/MM/yyyy'});
    }
    
    if ($('#procuracao_vencimento').length){
        $.mask.definitions['~'] = "[+-]";
        $("#procuracao_vencimento").mask("99/99/9999");
        $("#procuracao_vencimento").dateTimePicker({format: 'dd/MM/yyyy'});
    }
    
    if ($('#vencimento').length){
        $.mask.definitions['~'] = "[+-]";
        $("#vencimento").mask("99/99/9999");
        $("#vencimento").dateTimePicker({format: 'dd/MM/yyyy'});
    }
    
    if ($('#estrategia_regimetributariodata').length){
        $.mask.definitions['~'] = "[+-]";
        $("#estrategia_regimetributariodata").mask("99/99/9999");
        $("#estrategia_regimetributariodata").dateTimePicker({format: 'dd/MM/yyyy'});
    }
    
    if ($('#estrategia_consideracaodata1').length){
        $.mask.definitions['~'] = "[+-]";
        $("#estrategia_consideracaodata1").mask("99/99/9999");
        $("#estrategia_consideracaodata1").dateTimePicker({format: 'dd/MM/yyyy'});
    }
    
    if ($('#estrategia_consideracaodata2').length){
        $.mask.definitions['~'] = "[+-]";
        $("#estrategia_consideracaodata2").mask("99/99/9999");
        $("#estrategia_consideracaodata2").dateTimePicker({format: 'dd/MM/yyyy'});
    }
    
    if ($('#estrategia_consideracaodata3').length){
        $.mask.definitions['~'] = "[+-]";
        $("#estrategia_consideracaodata3").mask("99/99/9999");
        $("#estrategia_consideracaodata3").dateTimePicker({format: 'dd/MM/yyyy'});
    }

    if ($('#data_inicio').length){
        $.mask.definitions['~'] = "[+-]";
        $("#data_inicio").mask("99/99/9999");
        $("#data_inicio").dateTimePicker({format: 'dd/MM/yyyy'});
    }
    
    if ($('#data_previsao').length){
        $.mask.definitions['~'] = "[+-]";
        $("#data_previsao").mask("99/99/9999");
        $("#data_previsao").dateTimePicker({format: 'dd/MM/yyyy'});
    }
    
    if ($('#data_conclusao').length){
        $.mask.definitions['~'] = "[+-]";
        $("#data_conclusao").mask("99/99/9999");
        $("#data_conclusao").dateTimePicker({format: 'dd/MM/yyyy'});
    }
    
    if ($('#data_solicitacao').length){
        $.mask.definitions['~'] = "[+-]";
        $("#data_solicitacao").mask("99/99/9999");
        $("#data_solicitacao").dateTimePicker({format: 'dd/MM/yyyy'});
    }
    if ($('#data_vencimento').length){
        $.mask.definitions['~'] = "[+-]";
        $("#data_vencimento").mask("99/99/9999");
        $("#data_vencimento").dateTimePicker({format: 'dd/MM/yyyy'});
    }
    
    //Campos multiselect
    if ($('#cliente_evento').length){
        $('#cliente_evento').multiselect({
            enableClickableOptGroups: true,
            includeSelectAllOption: true
        });
    }

    if ($('#coleta_cliente').length){
        $('#coleta_cliente').multiselect({
            enableClickableOptGroups: true,
            includeSelectAllOption: true
        });
    }

    if ($('#usuario_pasta').length){
        $('#usuario_pasta').multiselect({
            includeSelectAllOption: true
        });
    }

    if ($('#usuario_cliente').length){
        $('#usuario_cliente').multiselect({
            includeSelectAllOption: true
        });
    }
    
    if ($('#estrategia_objetivoestrategia').length){
        $('#estrategia_objetivoestrategia').multiselect({includeSelectAllOption: true});
    }
    
    if ($('#estrategia_kpicontabilidade').length){
        $('#estrategia_kpicontabilidade').multiselect({includeSelectAllOption: true});
    }
    
    if ($('#estrategia_kpitributario').length){
        $('#estrategia_kpitributario').multiselect({includeSelectAllOption: true});
    }
    
    if ($('#estrategia_kpitrabalhista').length){
        $('#estrategia_kpitrabalhista').multiselect({includeSelectAllOption: true});
    }
    
    if ($('#estrategia_kpisocietario').length){
        $('#estrategia_kpisocietario').multiselect({includeSelectAllOption: true});
    }
    
    if ($('#estrategia_kpijuridico').length){
        $('#estrategia_kpijuridico').multiselect({includeSelectAllOption: true});
    }
    
    if ($('#estrategia_kpiqualificacao').length){
        $('#estrategia_kpiqualificacao').multiselect({includeSelectAllOption: true});
    }
    
    if ($('#estrategia_objetivoestagioatual').length){
        $('#estrategia_objetivoestagioatual').multiselect({includeSelectAllOption: true});
    }
    
    if ($('#estrategia_erp').length){
        $('#estrategia_erp').multiselect({    includeSelectAllOption: true});
    }
    
	//Ação AJAX - ENTREGA
    var input = $('input[name="files"]').fileuploader({
		enableApi: true
	});    
    window.api = $.fileuploader.getInstance(input);
    

    //Usuário - mostrar ou ocultar campo cliente
    if ($('.cliente-selecionatipo').length){
        $('.cliente-selecionatipo').change(function() {
            //console.log(this.value);
            if (this.value == 'T') {
                $(".cliente-tipo").toggleClass('cliente-tipoc cliente-tipot');
            }
            else if (this.value == 'C') {
                $(".cliente-tipo").toggleClass('cliente-tipot cliente-tipoc');
            }
        });    
    }

    $(".tr-cliente-filial").click(function(){ 
        var data_id=$.attr(this, 'data-id');

        //console.log(".tr-cliente-filial id:"+data_id)

        $(data_id).toggle();

        return false;
    })        
    
    $("#certidao-copiardocumento").click(function(){ 
        const inputTest = document.querySelector("#documento");
        inputTest.select();
        document.execCommand('copy');
    })        
    
    //Barra de progresso do cliente estratégia
    function cliente_barraprogresso(){
        total=0;
        preenchido=0;
        vazio=0;
        $('.clienteestrategia').each(function(i, obj) {
            
            if ($(obj).val()!="" && $(obj).val()!="00/00/0000") {
                //console.log($(obj));
                //console.log($(obj).val());
                //console.log($(obj).val().length);
                preenchido++;
            }
            total++;

        });
        vazio=total-preenchido;
        total = Math.round((preenchido/total)*100);
        //console.log("percentual:"+total);
        $("#cliente_progressbar").css('width',total+'%');
        $('#cliente_progressbar_label').text("Qualidade da Estratégia: "+total+" %");
    }

    if ($('#cliente_progressbar').length){
        cliente_barraprogresso();
    }
    
    $( ".clienteestrategia" ).change(function() {
        //console.log("percentual obj:"+$(this).val());
        cliente_barraprogresso();
    });
    
    
    //Accordion
    if ($('.collapse')){
        $('.collapse').on('shown.bs.collapse', function(){
            $(this).parent().find(".glyphicon-menu-down").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hidden.bs.collapse', function(){
            $(this).parent().find(".glyphicon-menu-up").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    }

    //Botão voltar para o topo
	$('#id-backtop').fadeOut();
	$(window).scroll(function () {
        //console.log ($(this).scrollTop());
		if ($(this).scrollTop() > 50) {
			$('#id-backtop').fadeIn();
		} else {
			$('#id-backtop').fadeOut();
		}
	});
	
	// scroll body to 0px on click
	$('#id-backtop').click(function () {
		$('#id-backtop').tooltip('hide');
		$('body,html').animate({scrollTop: 0}, 800);
		return false;
    });
    
	
	//$('#id-backtop').tooltip('show');

	//Função para reconhecer Enter
	$.fn.enterKey = function (fnc) {
		return this.each(function () {
			$(this).keypress(function (ev) {
				var keycode = (ev.keyCode ? ev.keyCode : ev.which);
				if (keycode == '13') {
					fnc.call(this, ev);
				}
			})
		})
	}

	//Campos data 
	$("#cep").mask("99999-999");
	$("#fisico_datanascimento_1").mask("99/99/9999");
	$("#fisico_datanascimento_2").mask("99/99/9999");
	
    //Alterar janelas da tela de Login
    $(".a-acao-linklogar").click(function(){ 
		$(".p-alerta").empty();
		$( "#div-recuperar" ).hide("slow");
		$( "#div-logar" ).show("slow");
		//$('html, body').animate({scrollTop: $(".col-md-5").offset().top}, 2000);		
		$("#email").focus();
	});
    $(".a-acao-linkrecuperar").click(function(){ 
		$(".p-alerta").empty();
		$( "#div-logar" ).hide( "slow" );
		$( "#div-recuperar" ).show( "slow" );
		//$('html, body').animate({scrollTop: $(".col-md-5").offset().top}, 2000);		
		$("#recuperar_email").focus();
	});

	//Limpa ação para evitar novos cliques
	function acao_alterar(obj,acao,texto){
		var local = $("#local").val();
		var contents = obj.contents();

		if (texto=="") texto = obj.filter('title').text();

		obj.attr('data-acao', acao);
		
		if (acao=="") {
			obj.html("<img src="+local+"/web/loading.gif height=15px>").append(contents.slice(1));
		} else {
			obj.html(texto).append(contents.slice(1));
		}
		
	}

    //Campos múltiplos da estratégia
    $("#intervalo_campoinserir").click(function(){
        var id = "intervalo_"+$( "input[name='intervalo_data[]']" ).length;
        
        //console.log("intervalo_campoinserir id"+id);
        
        var html = "";
        html += "<div class=col-md-8>";
        html += "<input type=text name=intervalo_horas[] maxlength=3 placeholder=Horas>";
        html += "</div>";
        html += "<div class=col-md-4>";
        html += "<input type=text name=intervalo_data[] id="+id+" value=teste maxlength=10 placeholder=Data>";
        html += "</div>";
        
        $('#intervalo_campos').append(html);
        
        $("#"+id).mask("99/99/9999");
    });
    
	// ----- Ações AJAX ------

    
	//Ação AJAX - LOGIN
    
    function logar(){
        var data_objeto= $("#login-entrar").attr("data-objeto");
        var data_acao=$("#login-entrar").attr("data-acao");
        var email = $("#email").val();
        var senha = $("#senha").val();

		//Log e validação se existe ação
		//console.log(data_objeto+'/'+data_acao);

        ///Valida campos
        if (email==""){
            $("#email").focus();
            return false;
        }
        if (email.length<2){
            bootbox.alert("O usuário não pode menos de 2 caracteres!");
            $("#email").focus();
            return false;
        } 
        if (senha==""){
            $("#senha").focus();
            return false;
        }
        if (senha.length>10){
            bootbox.alert("A senha não pode ter mais de 10 caracteres!");
            $("#senha").focus();
            return false;
        } 
        
		jQuery.ajax({
			type: "POST",
			url: data_objeto+'/'+data_acao,
            crossDomain: true,
            data: new FormData( $("#frm")[0]),
			dataType: 'json', 
            cache : false,
            contentType : false,
            processData : false,
			beforeSend: function() {
				acao_alterar($(".a-acao-logar"),'');
			},
			success: function(data) {
                //console.log(data);
                if (data.op==0){
                    bootbox.alert({ 
                        message: data.erro, 
                        callback: function(){
                            acao_alterar($(".a-acao-logar"),'logar','Entrar');
                        }
                    })
                }
                if (data.op==1){
                    $alteracao=false;
                    login_controller = data.login_controller!=null?data.login_controller:"painel";
                    login_event = data.login_event!=null?data.login_event:"resumo";
                    location.href = $("#local").val()+login_controller+'/'+login_event;                   
                }
                if (data.op==2){
                    $alteracao=false;
                    $( "#div-recuperar" ).hide("slow");
                    $( "#div-logar" ).hide("slow");
                    $( "#div-cliente" ).show("slow");

                    var clientes =JSON.parse(data.clientes);

                    login_controller = data.login_controller!=null?data.login_controller:"painel";
                    login_event = data.login_event!=null?data.login_event:"resumo";

                    $(".select-cliente").data('logincontroller', login_controller);
                    $(".select-cliente").data('loginevent', login_event);
                    
                    $.each( clientes, function( Key,val ) {
                        if (val.ativo=='S') {
                            $(".select-cliente").append('<option value='+val.cod_cliente+'~'+val.uf+'~'+val.cidade+'>'+val.nome+'</option>');
                        }
                    });
                }
			},
			error: function (xhr, ajaxOptions, thrownError) {
				bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
			}
		});	
    }

    $('#senha').keyup(function(e){
        if(e.keyCode == 13){
            logar();
        }
    });

    ''
	//Ação AJAX - LOGIN
	$(".a-acao-logar").click(function(){ 
        logar();
	}); 

	//Ação AJAX - RECUPERAR
	$(".a-acao-recuperar").click(function(){ 
		var data_objeto=$.attr(this, 'data-objeto');
        var data_acao=$.attr(this, 'data-acao');
        var formData = new FormData( $("#frm")[0] );
        var recuperar_email = $("#recuperar_email").val();

		//Log e validação se existe ação
		if ((($.attr(this, 'data-objeto')=="")||($.attr(this, 'data-acao')=="")))return false;
		//console.log(data_objeto+'/'+data_acao);
		//console.log(formData);

        //Valida campos
        if (recuperar_email==""){
            $("#recuperar_email").focus();
            return false;
        }
        
		jQuery.ajax({
			type: "POST",
			url: data_objeto+'/'+data_acao,
            data: formData,
			dataType: 'text',
            cache : false,
            contentType : false,
            processData : false,
			beforeSend: function() {
				acao_alterar($(".a-acao-recuperar"),'');
			},
			success: function(data) {
                bootbox.alert(data);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
			},
			complete: function() {
				acao_alterar($(".a-acao-recuperar"),'recuperar_senha','Recuperar');
			}
		});		 
	}); 
    
    //Ação AJAX - ALTERAR SENHA
	$(".a-acao-senhaalterar").click(function(){ 
		var data_objeto=$.attr(this, 'data-objeto');
        var data_acao=$.attr(this, 'data-acao');
        var formData = new FormData( $("#frm")[0] );
        var senha = $("#senha").val();
        var senha1 = $("#senha1").val();

		//Log e validação se existe ação
		if ((($.attr(this, 'data-objeto')=="")||($.attr(this, 'data-acao')=="")))return false;
		//console.log(data_objeto+'/'+data_acao);

        //Valida campos
        if ($("#senha").val()==""){
            bootbox.alert("Informe uma senha!");
            $("#senha").focus();
            return false;
        }
        if ($("#senha1").val()==""){
            bootbox.alert("Informe a senha novamente!");
            $("#senha1").focus();
            return false;
        }
        if (senha!=senha1){
            $("#senha").focus();
            bootbox.alert("As senhas não conferem!<br>");
            return false;
        }
        
		jQuery.ajax({
			type: "POST",
			url: data_objeto+'/'+data_acao,
            data: formData,
			dataType: 'text',
            cache : false,
            contentType : false,
            processData : false,
			beforeSend: function() {
				acao_alterar($(".a-acao-recuperar"),'');
			},
			success: function(data) {
                bootbox.alert("Sua senha foi alterada com sucesso!", function(){
                    location.href = $("#local").val()+'painel/resumo'; 
                });
			},
			error: function (xhr, ajaxOptions, thrownError) {
				bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
			},
			complete: function() {
				acao_alterar($(".a-acao-recuperar"),'recuperar_senha','Recuperar');
			}
		});		 
	});     
    
	//Ação AJAX - Enviar mensagem suporte
	$(".a-acao-suporteenviar").click(function(){ 
		var data_objeto=$.attr(this, 'data-objeto');
        var data_acao=$.attr(this, 'data-acao');
        var formData = new FormData( $("#frm")[0] );

		//Log e validação se existe ação
		if ((($.attr(this, 'data-objeto')=="")||($.attr(this, 'data-acao')=="")))return false;
		//console.log(data_objeto+'/'+data_acao);
		//console.log(formData);

        //Valida campos
        if ($("#email").val()==""){
            bootbox.alert("Informe um e-mail válido!");
            $("#email").focus();
            return false;
        }
        er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
        if(!er.exec($("#email").val())){
            bootbox.alert("Informe um e-mail válido!");
            $("#email").focus();
            return false;
        }
        
        
		jQuery.ajax({
			type: "POST",
			url: data_objeto+'/'+data_acao,
            data: formData,
			dataType: 'text',
            cache : false,
            contentType : false,
            processData : false,
			beforeSend: function() {
				acao_alterar($(".a-acao-suporteenviar"),'');
			},
			success: function(data) {
                if (data=='1'){
                    bootbox.alert("Mensagem enviada com sucesso");
                }else{
                    bootbox.alert("Não foi possível enviar sua mensagem");
                }
			},
			error: function (xhr, ajaxOptions, thrownError) {
				bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
			},
			complete: function() {
				acao_alterar($(".a-acao-suporteenviar"),'enviar','Enviar');
			}
		});		 
	}); 

	//Template. Enviar mensagem de teste
	$(".a-acao-templatetestar").click(function(){ 
		var data_objeto=$.attr(this, 'data-objeto');
        var data_acao=$.attr(this, 'data-acao');
        var data_cod=$.attr(this, 'data-cod');

		//Log e validação se existe ação
		if ((($.attr(this, 'data-objeto')=="")||($.attr(this, 'data-acao')=="")))return false;
		//console.log(data_objeto+'/'+data_acao+'/'+data_cod);

		jQuery.ajax({
			type: "POST",
			url: data_objeto+'/'+data_acao+'/'+data_cod,
			dataType: 'text',
            cache : false,
            contentType : false,
            processData : false,
			success: function(data) {
                if (data=='1'){
                    bootbox.alert("Mensagem enviada com sucesso");
                }else{
                    bootbox.alert("Não foi possível enviar sua mensagem");
                }
			},
			error: function (xhr, ajaxOptions, thrownError) {
				bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
			},
			complete: function() {
				acao_alterar($(".a-acao-suporteenviar"),'enviar','Enviar');
			}
		});		 
	}); 

	$(".a-acao-entregaenviar").click(function(){ 
		var data_objeto=$.attr(this, 'data-objeto');
        var data_acao=$.attr(this, 'data-acao');
		var formData = new FormData( $("#frm")[0] );
		
		//Log e validação se existe ação
		if ((($.attr(this, 'data-objeto')=="")||($.attr(this, 'data-acao')=="")))return false;
		//console.log(data_objeto+'/'+data_acao);
        
        $("#entrega_resultado > tbody").html("");

		jQuery.ajax({
			type: "POST",
			url: data_objeto+'/'+data_acao,
			data: formData,
            cache : false,
            contentType : false,
            processData : false,
			dataType: 'JSON',
			beforeSend: function() {
				acao_alterar($(".a-acao-entregaenviar"),'');
			},
			success: function(data) {
                
				//console.log(data);
                
                var op =""; 
                $.each( data, function( key, val ) {
                    if (val.op=="0")  {
                        op = "Erro";
                        cl = "span-danger";
                    }
                    if (val.op=="1")  {
                        op = "Sucesso";
                        cl = "span-success";
                    }
                    if (val.op=="2")  {
                        op = "Atenção";
                        cl = "span-warning";
                    }
                    $('#entrega_resultado > tbody:last-child').append(
                                '<tr>'
                                +'<td><span class=\"glyphicon glyphicon-cloud-upload '+cl+'\" aria-hidden=\"true\"></span></td>'
                                +'<td>'+val.nome+'</td>'
                                +'<td>'+val.cliente+'</td>'
                                +'<td>'+val.pasta+'</td>'
                                +'<td>'+val.data_vencimento+'</td>'
                                +'<td>'+val.erro+'</td>'
                                +'</tr>');                    
                });
                
			},
			error: function (xhr, ajaxOptions, thrownError) {
				bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
			},
			complete: function() {
				acao_alterar($(".a-acao-entregaenviar"),'enviar','Enviar');
                api.reset();
			}
		});		 
	});     

    //Ação Ajax - conferir caixa
    $(".a-acao-correio").click(function(){ 
        var local = $("#local").val();
		var url=local+"email/capturar";
		var formData = new FormData( $("#frm")[0] );
		
		//Log e validação se existe ação
		if ((($.attr(this, 'data-objeto')=="")||($.attr(this, 'data-acao')=="")))return false;
		//console.log(url);

		jQuery.ajax({
			type: "POST",
            url: url,
            data: formData,
            dataType: 'json',
            cache : false,
            contentType : false,
            processData : false,
			beforeSend: function() {
				acao_alterar($(".a-acao-correio"),'');
			},
			success: function(data) {
                
				//console.log(data);
                
                var log =""; 
                $.each( data, function( key, val ) {
                    if (val.op=="0")  {
                        log += "<p><i class=\"fa fa-exclamation\"></i>  "+ val.texto +"</p>";
                    }
                    if (val.op=="1")  {
                        log += "<p><i class=\"fa fa-check\"></i>  "+ val.texto +"</p>";
                    }
                    if (val.op=="2")  {
                        log += "<p>&nbsp;&nbsp;&nbsp;&nbsp;<i class=\"fa fa-envelope\"></i>  "+ val.texto +"</p>";
                    }
                    if (val.op=="3")  {
                        log += "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class=\"fa fa-paperclip\"></i>  "+ val.texto +"</p>";
                    }
                });
                
                var dialog = bootbox.dialog({
                    title: 'Conferir Caixa',
                    size: 'large',
                    message: log,
                    buttons: {
                        ok: {
                            label: "Fechar",
                            className: 'btn-info',
                            callback: function(){
                                //console.log('a-acao-correio: Custom OK clicked');
                            }
                        }
                    }
                });
                
                
			},
			error: function (xhr, ajaxOptions, thrownError) {
				bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
			},
			complete: function() {
				acao_alterar($(".a-acao-correio"),'capturar','<i class=\"fa fa-envelope fa-fw\"></i>');
			}
		});			 
	});     

    //Ação Ajax - excluir arquivo
    $(".a-acao-e").click(function(){ 
		var data_objeto=$.attr(this, 'data-objeto');
        var data_acao=$.attr(this, 'data-acao');
        var data_a=$.attr(this, 'data-a');
		
		//Log e validação se existe ação
		if ((($.attr(this, 'data-objeto')=="")||($.attr(this, 'data-acao')=="")||($.attr(this, 'data-a')=="")))return false;
		//console.log(data_objeto+'/'+data_acao+'/'+data_a+'/'+'table tr#'+data_a);
        
        if (!confirm("Confirma a exclusão?")) return false;
        
        $('table tr#tr_'+data_a).remove();

		jQuery.ajax({
			type: "POST",
			url: data_objeto+'/'+data_acao+'/'+data_a,
            cache : false,
            contentType : false,
            processData : false,
			success: function(data) {
				//console.log(data);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
			}
		});		 
	});     

    //Ação Ajax - excluir arquivo
    $(".td-tablebah").click(function(){ 
		var id=$(this).attr('id');
		var data_objeto=$.attr(this, 'data-objeto');
        var data_acao=$.attr(this, 'data-acao');
        var data_cod_cliente=$.attr(this, 'data-cod-cliente');
        var data_nome_cliente=$.attr(this, 'data-nome-cliente');
        var data_cod_evento=$.attr(this, 'data-cod-evento');
        var data_sigla_evento=$.attr(this, 'data-sigla-evento');

        if ($.attr(this, 'data-cod-clienteevento')!=""){
            $message = "<span class=\"glyphicon glyphicon-minus\" aria-hidden=\"true\"></span> Remover o evento "+data_sigla_evento+" do cliente "+data_nome_cliente+"?";
            $url = data_objeto+'/'+data_acao+'/R/'+data_cod_cliente+'/'+data_cod_evento;
        } else {
            $message = "<span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span> Adicionar o evento "+data_sigla_evento+" ao cliente "+data_nome_cliente+"?";
            $url = data_objeto+'/'+data_acao+'/A/'+data_cod_cliente+'/'+data_cod_evento;
        }
        console.log(".td-tablebah data_cod_cliente:"+data_cod_cliente+ " data_nome_cliente:"+data_nome_cliente+ " data_cod_evento:"+data_cod_evento+ " data_sigla_evento:"+data_sigla_evento+ " data-cod-clienteevento:"+$("#"+id).attr('data-cod-clienteevento')+ "ID:"+id);

        bootbox.dialog({
            title: 'Evento',
            size: 'large',
            message: $message,
            buttons: {
                ok: {
                    label: "Sim",
                    className: 'btn-info',
                    callback: function(){
                        console.log('td-tablebah Remover url:'+data_objeto+'/'+data_acao+'/S/'+data_cod_cliente+'/'+data_cod_evento);
                        jQuery.ajax({
                            type: "POST",
                            url: $url,
                            dataType: 'json',
                            cache : false,
                            contentType : false,
                            processData : false,
                            success: function(data) {
                                console.log(data);
                                if ($("#"+id).attr('data-cod-clienteevento')!=""){
                                    console.log(1);
                                    $("#"+id).attr('data-cod-clienteevento', '');
                                    $("#"+id).empty();
                                    $("#"+id+"_arquivo").empty();
                                } else {
                                    console.log(2);
                                    $("#"+id).attr('data-cod-clienteevento', data.cod_clienteevento)
                                    $("#"+id).html("<span>&nbsp;&nbsp;&nbsp;S&nbsp;</span>");
                                }
                            },
                            error: function (xhr, thrownError) {
                                bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
                            }
                        });	
                    }
                },
                cancel: {
                    label: "Não",
                    className: 'btn-info'
                }
            }
        });
	});     

    //Ação Ajax - marcar postagem como lida
    $(".a-acao-painellida").click(function(){ 
		var data_objeto=$.attr(this, 'data-objeto');
        var data_acao=$.attr(this, 'data-acao');
        var data_cod=$.attr(this, 'data-cod');
		
		//Log e validação se existe ação
		if ((($.attr(this, 'data-objeto')=="")||($.attr(this, 'data-acao')=="")||($.attr(this, 'data-cod')=="")))return false;
		//console.log(data_objeto+'/'+data_acao+'/'+data_cod);
        
		jQuery.ajax({
			type: "POST",
			url: data_objeto+'/'+data_acao+'/'+data_cod,
            cache : false,
            contentType : false,
            processData : false,
			success: function(data) {
				location.href = $("#local").val()+'painel/resumo';                   
			},
			error: function (xhr, ajaxOptions, thrownError) {
				bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
			}
		});		 
	});     

    //Ação Ajax - marcar cliente como favorito
    $(".a-acao-favorito").click(function(){ 
		var data_objeto=$.attr(this, 'data-objeto');
        var data_acao=$.attr(this, 'data-acao');
        var data_cod=$.attr(this, 'data-cod');
        var $input = $( this );
		
		//Log e validação se existe ação
		if ((($.attr(this, 'data-objeto')=="")||($.attr(this, 'data-acao')=="")||($.attr(this, 'data-cod')=="")))return false;
		//console.log(data_objeto+'/'+data_acao+'/'+data_cod);
        
		jQuery.ajax({
			type: "POST",
			url: data_objeto+'/'+data_acao+'/'+data_cod,
            cache : false,
            contentType : false,
            processData : false,
			success: function() {
                if (data_acao=="favoritar") {
				    $input.attr("data-acao","desfavoritar");
				    $("#favorito_"+data_cod).toggleClass('glyphicon-star-empty glyphicon-star');               
                } else{
				    $input.attr("data-acao","favoritar");
				    $("#favorito_"+data_cod).toggleClass('glyphicon-star glyphicon-star-empty');               
            
                }
			},
			error: function (xhr, ajaxOptions, thrownError) {
				bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
			}
        });	
        return false;	 
	});     

    //Ação Ajax
    $(".a-acao-estrategiacomunicar").click(function(){ 
		var data_objeto=$.attr(this, 'data-objeto');
        var data_acao=$.attr(this, 'data-acao');
        var data_cod=$.attr(this, 'data-cod');
        var formData = new FormData( $("#frm")[0] ); 

		//Log e validação se existe ação
		if ((($.attr(this, 'data-objeto')=="")||($.attr(this, 'data-acao')=="")||($.attr(this, 'data-cod')=="")))return false;
		//console.log(data_objeto+'/'+data_acao+'/'+data_cod);
        
		jQuery.ajax({
			type: "POST",
			url: data_objeto+'/'+data_acao+'/'+data_cod,
            data: formData,
			dataType: 'json',
            cache : false,
            contentType : false,
            processData : false,
			beforeSend: function() {
				acao_alterar($(".a-acao-estrategiacomunicar"),'');
			},
			success: function(data) {
                bootbox.alert(data.msg);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
			},
			complete: function() {
				acao_alterar($(".a-acao-estrategiacomunicar"),'comunicar_estrategia','Comunicar alteração');
			}
		});				 
	});     
    
    //Ação Ajax - Termo de uso
    $(".a-acao-termodeuso").click(function(){ 
		var data_objeto=$.attr(this, 'data-objeto');
        var data_acao=$.attr(this, 'data-acao');
        var formData = new FormData( $("#frm")[0] );

		//Log e validação se existe ação
		if ((($.attr(this, 'data-objeto')=="")||($.attr(this, 'data-acao')=="")))return false;
		//console.log(data_objeto+'/'+data_acao);
        
		jQuery.ajax({
			type: "POST",
			url: data_objeto+'/'+data_acao,
            data: formData,
			dataType: 'json',
            cache : false,
            contentType : false,
            processData : false,
			beforeSend: function() {
				acao_alterar($(".a-acao-termodeuso"),'');
			},
			success: function(data) {
                bootbox.alert(data.msg);
                $( "#termodeuso" ).remove();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
			},
			complete: function() {
				acao_alterar($(".a-acao-termodeuso"),'aceitar_termodeuso','Aceitar');
			}
		});				 
	});     
    
    //Ação Ajax - Termo de uso
    $(".a-acao-termodeuso").click(function(){ 
		var data_objeto=$.attr(this, 'data-objeto');
        var data_acao=$.attr(this, 'data-acao');
        var formData = new FormData( $("#frm")[0] );
        var recuperar_email = $("#recuperar_email").val();

		//Log e validação se existe ação
		if ((($.attr(this, 'data-objeto')=="")||($.attr(this, 'data-acao')=="")))return false;
		//console.log(data_objeto+'/'+data_acao);
        
		jQuery.ajax({
			type: "POST",
			url: data_objeto+'/'+data_acao,
            data: formData,
			dataType: 'json',
            cache : false,
            contentType : false,
            processData : false,
			beforeSend: function() {
				acao_alterar($(".a-acao-termodeuso"),'');
			},
			success: function(data) {
                bootbox.alert(data.msg);
                $( "#termodeuso" ).remove();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
			},
			complete: function() {
				acao_alterar($(".a-acao-termodeuso"),'aceitar_termodeuso','Aceitar');
			}
		});				 
	});     
    
    //Ação Ajax - Seleção do 
    $(".select-cliente").change(function(){ 
        var local = $("#local").val();
        var cliente = $(this).val();
		var url=local+"cliente/selecionar/"+cliente;
        var formData = new FormData( $("#frm")[0] );

		//Log e validação se existe ação
        //console.log(".a-acao-cliente url:"+url);
        
		jQuery.ajax({
			type: "POST",
			url: url,
            data: formData,
			dataType: 'text',
            cache : false,
            contentType : false,
            processData : false,
			success: function() {
                if ($(".select-cliente").data('logincontroller')!="" && $(".select-cliente").data('loginevent')!="") {
                    location.href = $("#local").val()+$(".select-cliente").data('logincontroller')+'/'+$(".select-cliente").data('loginevent');                   
                } else {
                    location.reload();
                }
			},
			error: function (xhr, ajaxOptions, thrownError) {
				bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
			}
		});				 
	});     
    
    $(".servico_cod_cliente").click(function(){
        var local = $("#local").val();
        var cod_cliente = $(this).val();
        var url=local+"cliente/codigo/"+cod_cliente;

        //console.log( ".servico_cod_cliente url:"+url);
        
        $.ajax({url: url, dataType: 'JSON', success: function(data){
            //console.log(data.cod_interno);
            $("#cod_interno").val(data.cod_interno);
        }});
    });

    $(".cliente_excluircertificado").click(function(event){  
        var local = $("#local").val();
        var cliente = $(this).attr("data-cliente");
        var ext = $(this).attr("data-ext");
        var obj_link = $(this).attr("data-obj");
        var url=local+"cliente/excluir_certificado/"+cliente+"/"+ext;
        var obj = $(this);

        //console.log( ".cliente_excluircertificado url:"+url);
        
        $.ajax({url: url, dataType: 'JSON', success: function(data){
            obj.remove();
            $('#'+obj_link).remove();
        }});
    });
    

    $("#tarefa_etapainserir").click(function(){
        var count = $( "input[name='etapa_nome[]']" ).length;
        
        //console.log("tarefa_etapainserir count:"+count);
        
        var html = "";


        html += "<div class=row>";
            html += "<div class=col-md-2>";
            html += "<input type=text class=\"form-control\" name=etapa_ordem[] maxlength=2 placeholder=Ordem>";
            html += "</div>  ";     
            html += "<div class=\"col-md-2\">";
            html += "<select class=\"form-control\" name=\"etapa_tipo[]\">";
            html += "<option value=\"NULL\">Selecione...</option>";
            html += "<option value=\"T\">TripleAie</option>";
            html += "<option value=\"C\">Cliente</option>";
            html += "</select>";
            html += "</div>  ";     
            html += "<div class=col-md-2>";
            html += "<input type=text class=\"form-control\" name=etapa_nome[] maxlength=200 placeholder=Etapa>";
            html += "</div>  ";     
            html += "<div class=col-md-2>";
            html += "<input type=text class=\"form-control\" name=etapa_previsao[] maxlength=100 placeholder=Previsão>";
            html += "</div>  ";     
            html += "<div class=col-md-2>";
            html += "<input type=\"file\" class=\"form-control\" name=\"etapa_arquivo[]\">";
            html += "</div>  ";     
            html += "<div class=col-md-2>";
            html += "&nbsp;";
            html += "</div>  ";     
        html += "</div>";
        
        $('#etapas').append(html);
        
        fnMascaras();
        
    });
    
    $("#formulario_campoinserir").click(function(){
        var count = $( "input[name='campo_ordem[]']" ).length;
        
        //console.log("formulario_campoinserir count:"+count);
        
        var html = "";

        html += "<div class=\"row\">";
            html += "<div class=\"col-sm-1\">";
            html += "<input type=\"text\" class=\"form-control\" name=\"campo_ordem[]\" maxlength=\"2\" value=\"\" placeholder=\"Ordem\">";
            html += "</div>"; 
            html += "<div class=\"col-sm-2\">";
            html += "<select class=\"form-control\" name=\"campo_tipo[]\">";
            html += "<option value=\"NULL\">Selecione...</option>";
            html += "<option value=\"info\" >Informação</option>";
            html += "<option value=\"text\" >Texto</option>";
            html += "<option value=\"date\" >Data</option>";
            html += "<option value=\"select\" >Seleção</option>";
            html += "</select>";
            html += "</div>       ";
            html += "<div class=\"col-sm-1\">";
            html += "<input type=\"text\" class=\"form-control\" name=\"campo_nome[]\" maxlength=\"20\" value=\"\" placeholder=\"Nome\">";
            html += "</div>       ";
            html += "<div class=\"col-sm-2\">";
            html += "<input type=\"text\" class=\"form-control\" name=\"campo_label[]\" value=\"\" placeholder=\"Label\">";
            html += "</div>       ";
            html += "<div class=\"col-sm-2\">";
            html += "<input type=\"text\" class=\"form-control\" name=\"campo_opcoes[]\" value=\"\" placeholder=\"Opções (separados por vírgula)\">";
            html += "</div>       ";
            html += "<div class=\"col-sm-2\">";
            html += "<select class=\"form-control\" name=\"campo_requerido[]\">";
            html += "<option value=\"NULL\">Selecione...</option>";
            html += "<option value=\"S\" >Sim</option>";
            html += "<option value=\"N\" >Não</option>";
            html += "</select>";
            html += "</div>";       
            html += "<div class=\"col-sm-2\">";
            html += "<input type=\"text\" class=\"form-control\" name=\"campo_ajuda[]\" maxlength=\"100\" value=\"\" placeholder=\"Ajuda\">";
            html += "</div>";       
        html += "</div>";
        
        $('#campos').append(html);
        
        fnMascaras();
        
    });

    $("#servico_campoinserir").click(function(){
        var count = $( "input[name='etapa_nome[]']" ).length;
        
        //console.log("servico_campoinserir count:"+count);
        
        var html = "";

        html += "<div class=row>";
        html += "<div class=col-md-1>";
        html += "<input type=hidden class=\"form-control\" name=etapa_cod_tarefaetapa[]>";
        html += "<input type=text class=\"form-control\" name=etapa_ordem[] maxlength=10 placeholder=Ordem>";
        html += "</div>  ";     
        html += "<div class=\"col-md-1\">";
        html += "<select class=\"form-control\" name=\"etapa_tipo[]\">";
        html += "<option value=\"NULL\">Selecione...</option>";
        html += "<option value=\"T\">TripleAie</option>";
        html += "<option value=\"C\">Cliente</option>";
        html += "</select>";
        html += "</div>  ";     
        html += "<div class=col-md-3>";
        html += "<input type=text class=\"form-control\" name=etapa_nome[] maxlength=200 placeholder=Etapa>";
        html += "</div>  ";     
        html += "<div class=col-md-1>";
        html += "<input type=text class=\"form-control\" readonly name=etapa_previsao[] placeholder=Previsão>";
        html += "</div>  ";     
        html += "<div class=col-md-1>";
        html += "<input type=text class=\"form-control input-date\" name=etapa_data_inicio[] maxlength=10 placeholder=\"Data Início\">";
        html += "</div>  ";     
        html += "<div class=col-md-1>";
        html += "<input type=text class=\"form-control input-date\" name=etapa_data_previsao[] maxlength=10 placeholder=\"Data Previsão\">";
        html += "</div>  ";     
        html += "<div class=col-md-1>";
        html += "<input type=text class=\"form-control input-date\" name=etapa_data_conclusao[] maxlength=10 placeholder=\"Data Conclusão\">";
        html += "</div>  ";     
        html += "<div class=\"col-md-1\" >"; 
        html += "<input type=\"file\" name=\"etapa_arquivo[]\" id=\"etapa_arquivo[]\" class=\"form-control\">";
        html += "</div>";
        html += "<div class=col-md-2>";
        html += "</div>  ";     
        html += "</div>";
        
        $('#etapas').append(html);
        
        fnMascaras();
        
    });

    
    $(".servico-etapas").click(function(){ 
        var local = $("#local").val();
        var cod_tarefa = $(this).val();
		var url=local+"tarefa/listar_etapas/"+cod_tarefa;
        var formData = new FormData( $("#frm")[0] );

        if (cod_tarefa!=""){
            //console.log(".servico-etapas url:"+url);
        
            jQuery.ajax({
                type: "POST",
                url: url,
                data: formData,
                dataType: 'json',
                cache : false,
                contentType : false,
                processData : false,
                success: function(data) {
                    //console.log (data);
                    $('#etapas').empty();
                    $.each( data, function( key, val ) {

                        $('#texto').val(val.texto);

                        var html = "";
                        html += "<div class=row>";
                        html += "<div class=col-md-1>";
                        html += "<input type=hidden class=\"form-control\" name=etapa_cod_tarefaetapa[] value=\""+val.cod_tarefaetapa+"\">";
                        html += "<input type=hidden class=\"form-control\" name=etapa_arquivotarefa[] value=\""+val.arquivo+"\">";
                        html += "<input type=text class=\"form-control\" name=etapa_ordem[] placeholder=Ordem value=\""+val.ordem+"\">";
                        html += "</div>  ";     
                        html += "<div class=\"col-md-1\">";
                        html += "<select class=\"form-control\" name=\"etapa_tipo[]\">";
                        html += "<option value=\"NULL\">Selecione...</option>";
                        selectedT="";
                        if (val.tipo=='T') selectedT="selected";
                        selectedC="";
                        if (val.tipo=='C') selectedC="selected";
                        html += "<option value=\"T\" "+selectedT+ ">TripleAie</option>";
                        html += "<option value=\"C\" "+selectedC+ ">Cliente</option>";
                        html += "</select>";
                        html += "</div>  ";     
                        html += "<div class=col-md-3>";
                        html += "<input type=text class=\"form-control\" readonly name=etapa_nome[] maxlength=200 placeholder=Etapa value=\""+val.nome+"\">";
                        html += "</div>  ";     
                        html += "<div class=col-md-1>";
                        html += "<input type=text class=\"form-control\" readonly name=etapa_previsao[] placeholder=Previsão value=\""+val.previsao+"\">";
                        html += "</div>  ";     
                        html += "<div class=col-md-1>";
                        html += "<input type=text class=\"form-control servico_etapa_data_inicio input-date\" name=etapa_data_inicio[] maxlength=10 placeholder=\"Data Início\" data-campo=previsao_"+val.cod_tarefaetapa+" data-previsao="+val.previsao+">";
                        html += "</div>  ";     
                        html += "<div class=col-md-1>";
                        html += "<input type=text class=\"form-control input-date\" id=previsao_"+val.cod_tarefaetapa+" name=etapa_data_previsao[] maxlength=10 placeholder=\"Data Previsão\">";
                        html += "</div>  ";     
                        html += "<div class=col-md-1>";
                        html += "<input type=text class=\"form-control input-date\" name=etapa_data_conclusao[] maxlength=10 placeholder=\"Data Conclusão\">";
                        html += "</div>  ";     
                        html += "<div class=\"col-md-2\" >"; 
                        html += "<input type=\"file\" name=\"etapa_arquivo[]\" id=\"etapa_arquivo[]\" class=\"form-control\">";
                        html += "</div>";
                        html += "<div class=col-md-1>";
                        if (val.arquivo!='' && val.arquivo!=null){
                            html += "<a href=\""+local+"data/tarefa/"+val.arquivo+"\" class=\"btn btn-sub\" target=\"_blank\"><i class=\"fa fa-paperclip fa-fw\"></i></a>";
                        }
                        html += "</div>  ";     
                        html += "</div>";

                        $('#etapas').append(html);

                    });

                    fnMascaras();
                    $(".servico_etapa_data_inicio").change(function(event){  
                        var local = $("#local").val();
                        var cod_servico = $("#cod_servico").val();
                        var obj_previsao = $("#"+$.attr(this, 'data-campo'));
                        var data = $(this).val();
                        data = data.substring(6, 10)+'-'+data.substring(3, 5)+'-'+data.substring(0, 2);
                        var dias = $.attr(this, 'data-previsao');
                        var url=local+"servico/calcular_previsao/"+data+"/"+dias;

                        //console.log( ".servico_etapa_data_inicio url:"+url);

                        $.ajax({url: url, dataType: 'JSON', success: function(data){
                            //console.log(data)
                            obj_previsao.val(data.data_etapa);
                            //$("#data_previsao").val(data.data_previsao);
                        }});
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
                }
            });	
        }
	}); 
    
    $(".tarefa_excluirarquivo").click(function(event){  
        var local = $("#local").val();
        var pasta = $.attr(this, 'data-pasta');
        var arquivo = $.attr(this, 'data-arquivo');
        var url=local+"tarefa/excluir_arquivo/"+pasta+"/"+arquivo;
        var obj = $(this);

        //console.log( ".tarefa_excluirarquivo url:"+url);
        
        $.ajax({url: url, dataType: 'JSON', success: function(data){
            obj.remove();
        }});
    });
    
    $(".servico_excluirarquivo").click(function(event){  
        var local = $("#local").val();
        var pasta = $.attr(this, 'data-pasta');
        var cod_servico = $.attr(this, 'data-servico');
        var cod_servicoetapa = $.attr(this, 'data-servicoetapa');
        var url=local+"servico/excluir_arquivo/"+pasta+"/"+cod_servico+"/"+cod_servicoetapa;
        var obj = $(this);

        //console.log( ".servico_excluirarquivo url:"+url);
        
        $.ajax({url: url, dataType: 'JSON', success: function(data){
            obj.remove();
        }});
    });
    
    $(".servico_etapa_data_inicio").change(function(event){  
        var local = $("#local").val();
        var cod_servico = $("#cod_servico").val();
        var obj_previsao = $("#"+$.attr(this, 'data-campo'));
        var data = $(this).val();
        data = data.substring(6, 10)+'-'+data.substring(3, 5)+'-'+data.substring(0, 2);
        var dias = $.attr(this, 'data-previsao');
        var url=local+"servico/calcular_previsao/"+data+"/"+dias;

        //console.log( ".servico_etapa_data_inicio url:"+url);

        $.ajax({url: url, dataType: 'JSON', success: function(data){
            //console.log(data)
            obj_previsao.val(data.data_etapa);
            //$("#data_previsao").val(data.data_previsao);
        }});
    });
    
    
    function fnRemoverQuadro (data_quadro){
        
        //console.log("quadro_remover data_quadro:"+data_quadro);
        
        $("#"+data_quadro).remove();
    }
    
    $("#quadro_campoinserir").click(function(){
        var local = $("#local").val();
        var aux = $( "input[id='quadro-aux']" ).val();
        aux++;
        $( "input[id='quadro-aux']" ).val(aux);
        
        //console.log("quadro_campoinserir aux:"+aux);
        
        var html = "";


        html += "<div class=\"row\" id=\"quadro-"+aux+"\">";
        html += "<div class=\"col-md-1\">";
        html += "<input type=\"text\" class=\"form-control\" name=\"quadro_ordem[]\" maxlength=\"2\" value=\"\" placeholder=\"Ordem\">";
        html += "</div>    ";   
        html += "<div class=\"col-md-2\">";
        html += "<input type=\"text\" class=\"form-control\" name=\"quadro_nome[]\" maxlength=\"100\" value=\"\" placeholder=\"Nome\">";
        html += "</div>    ";   
        html += "<div class=\"col-md-2\">";
        html += "<input type=\"text\" class=\"form-control\" name=\"quadro_documento[]\" maxlength=\"100\" value=\"\" placeholder=\"CPF/CNPJ\">";
        html += "</div>    ";   
        html += "<div class=\"col-md-1\">";
        html += "<input type=\"text\" class=\"form-control input-date\" name=\"quadro_data_registro[]\" maxlength=\"10\" value=\"\" placeholder=\"Data&nbsp;Registro\">";
        html += "</div>  ";     
        html += "<div class=\"col-md-1\">";
        html += "<input type=\"text\" class=\"form-control input-date\" name=\"quadro_data_inicio[]\" maxlength=\"10\" value=\"\" placeholder=\"Data&nbsp;Início\">";
        html += "</div>  ";     
        html += "<div class=\"col-md-1\">";
        html += "<input type=\"text\" class=\"form-control input-date\" name=\"quadro_data_saida[]\" maxlength=\"10\" value=\"\" placeholder=\"Data&nbsp;Saída\">";
        html += "</div>  ";     
        html += "<div class=\"col-md-1\">";
        html += "<input type=\"text\" class=\"form-control\" name=\"quadro_participacao[]\" maxlength=\"100\" value=\"\" placeholder=\"Paticipação\">";
        html += "</div>  ";     
        html += "<div class=\"col-md-1\">";
        html += "<input type=\"text\" class=\"form-control\" name=\"quadro_quotas[]\" maxlength=\"100\" value=\"\" placeholder=\"Quotas\">";
        html += "</div>   ";    
        html += "<div class=\"col-md-1\">";
        html += "<select class=\"form-control\" name=\"quadro_responsavel[]\" id=\"quadro_responsavel\">";
        html += "<option value=\"NULL\">Selecione...</option>";
        html += "<option value=\"S\">Sim</option>";
        html += "<option value=\"N\">Não</option>";
        html += "</select>";
        html += "</div>   ";    
        html += "<div class=\"col-md-1\">";
        html += "<a href=\"#\" class=\"quadro_remover\" data-quadro=\"quadro-"+aux+"\"><i class=\"fa fa-trash fa-fw\"></i></a>";
        html += "</div>";
        html += "</div>";
        
        $('#quadro').append(html);
        
        fnMascaras();
        $(".quadro_remover").click(function(){
            var data_quadro=$.attr(this, 'data-quadro');
            fnRemoverQuadro(data_quadro);
        });
        
    });
    
    $(".quadro_remover").click(function(){
        var data_quadro=$.attr(this, 'data-quadro');
        fnRemoverQuadro(data_quadro);
    });
    

    function fnRemoverAlvara (data_alvara){
        
        //console.log("alvara_remover data_alvara:"+data_alvara);
        
        $("#"+data_alvara).remove();
    }
    
    $("#alvara_campoinserir").click(function(){
        var local = $("#local").val();
        var aux = $( "input[id='alvara-aux']" ).val();
        var url=local+"alvara/json_alvaralistar";
        aux++;
        $( "input[id='alvara-aux']" ).val(aux);
        
        //console.log("alvara_campoinserir url:"+url);
        
        var html = "";


        html += "<div class=\"row\" id=\"alvara-"+aux+"\">";
        html += "<div class=\"col-md-3\">";
        
        html += "<select class=\"form-control alvara_cod_alvara\" name=\"alvara_cod_alvara[]\" id=\"alvara_cod_alvara\">";
        html += "<option value=\"NULL\">Selecione...</option>";
        html += "</select>";
        
        html += "</div>";       
        html += "<div class=\"col-md-2\">";
        html += "<input type=\"text\" class=\"form-control\" name=\"alvara_numero[]\" maxlength=\"20\" placeholder=\"Número\">";
        html += "</div>";       
        html += "<div class=\"col-md-2\">";
        html += "<select class=\"form-control\" name=\"alvara_vencimento[]\">";
        html += "<option value=\"NULL\">Selecione...</option>";
        html += "<option value=\"C\">Com vencimento</option>";
        html += "<option value=\"I\">Tempo indeterminado</option>";
        html += "</select>";
        html += "</div>";       
        html += "<div class=\"col-md-2\">";
        html += "<input type=\"text\" class=\"form-control input-date\" name=\"alvara_data_vencimento[]\" maxlength=\"10\" placeholder=\"Data Vencimento\">";
        html += "</div>";       
        html += "<div class=\"col-md-2\">";
        html += "<input type=\"text\" class=\"form-control input-mes\" name=\"alvara_data_pagamento[]\" maxlength=\"8\" placeholder=\"Data Pagamento\">";
        html += "</div> ";      
        html += "<div class=\"col-md-1\">";
        html += "<a href=\"#\" class=\"alvara_remover\" data-alvara=\"alvara-"+aux+"\"><i class=\"fa fa-trash fa-fw\"></i></a>";
        html += "</div>";
        html += "</div>";
        
        $('#alvaras').append(html);
        
        jQuery.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            cache : false,
            contentType : false,
            processData : false,
            success: function(data) {
                //console.log (data);
                $.each( data, function( key, val ) {
                    $('.alvara_cod_alvara').append($('<option/>', { 
                        value: val["cod_alvara"],
                        text : val["nome"]
                    }));
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
            }
        });	
        
        
        fnMascaras();
        $(".alvara_remover").click(function(){
            var data_alvara=$.attr(this, 'data-alvara');
            fnRemoverAlvara(data_alvara);
        });
        
    });
    
    $(".alvara_remover").click(function(){
        var data_alvara=$.attr(this, 'data-alvara');
        fnRemoverAlvara(data_alvara);
    });    
    
	//------>>>> Ações padrões // Com reload de página
    
    $(".estrategia_cod_cliente").change(function(event){  
        var frm=document.frm;
		frm.submit();
		
    });
    
    $(".a-acao-painelver").click(function(event){  
        var frm=document.frm;
		var data_objeto=$.attr(this, 'data-objeto');
        var data_acao=$.attr(this, 'data-acao');
        var data_mes=$("#mes_referencia").val();
        var data_ano=$("#ano_referencia").val();
        var cliente=$("#cliente").val();
        
		if ((($.attr(this, 'data-objeto')=="")||($.attr(this, 'data-acao')=="")))return false;
		//console.log(data_objeto+'/'+data_acao+'/'+data_mes+'/'+data_ano+'/'+cliente);

		frm.action = data_objeto+'/'+data_acao+'/'+data_mes+'/'+data_ano+'/'+cliente;
		frm.submit();
		
    });
    
    $("body").on('click','.a-acao-d',function(e){
        e.preventDefault();
        var frm=document.frm;
		var data_objeto=$.attr(this, 'data-objeto');
        var data_acao=$.attr(this, 'data-acao');
        var data_a=$.attr(this, 'data-a');
		
		//Log e validação se existe ação
		if ((($.attr(this, 'data-objeto')=="")||($.attr(this, 'data-acao')=="")||($.attr(this, 'data-a')=="")))return false;
		//console.log(data_objeto+'/'+data_acao+'/'+data_a);

		frm.action = data_objeto+'/'+data_acao+'/'+data_a;
		frm.submit();
		
    });
    
    $("body").on('click','.tr-linha',function(e){
        e.preventDefault();
        var frm=document.frm;
        var data_objeto=$(this).attr('data-objeto');
        var data_acao=$(this).attr('data-acao');
        var data_cod=$(this).attr('data-cod');
        var data_href=$(this).attr('data-href');
        var data_aux=$(this).attr('data-aux');

        if (($(this).attr('data-thiseto')=="")||($(this).attr('data-acao')=="")||($(this).attr('data-cod')=="")||($(this).attr('data-href')==""))return false;

        if (typeof data_href !== undefined && data_href !== false && $(this).attr('data-href')) {
            //console.log('tr-linha url data_href:'+data_href);
            frm.action = data_href;
        } else {
            //console.log('tr-linha url url:'+data_objeto+'/'+data_acao+'/'+data_cod+'/'+data_aux);
            frm.action = data_objeto+'/'+data_acao+'/'+data_cod+'/'+data_aux;
        }
        frm.submit();

    });

    $(".tr-notificacao").click(function() {
        var data_texto=$.attr(this, 'data-texto');

        //console.log('tr-notificacao:'+data_texto);

        bootbox.alert(data_texto);
    });

    //Variável para conferir se houve alterações nos dados antes de sair do form
    $alteracao=false;
    $('form').on('change paste', 'input, select, textarea', function(){
        $alteracao=true;
    });
    /*
    $(window).bind("beforeunload",function() {
        //console.log($alteracao);
        return false;
        if($alteracao) return "You have unsaved changes";
    });
    */

    //Acionar a busca com Enter
	$("#busca").enterKey(function () {
        var frm=document.frm;
        $alteracao=false;
        //console.log(1);
		frm.submit();
	})
    
    $(".a-acao-buscar").click(function(){  
        var frm=document.frm;
        $alteracao=false;
        //console.log(2);
        frm.submit();
    });

    //Ação padrão de submit
    $(".a-acao").click(function(){  
        var frm=document.frm;
		var data_objeto=$.attr(this, 'data-objeto');
        var data_acao=$.attr(this, 'data-acao');
        var data_cod="";
        var data_orderbycolumn=$.attr(this, 'data-orderbycolumn');
        var data_info=$.attr(this, 'data-info');
        var orderby_order=$('#orderby_order').val();
        var data_validacao=$.attr(this, 'data-validacao');

		if($.attr(this, 'data-cod')) data_cod = $.attr(this, 'data-cod');
		if($.attr(this, 'target')) frm.target = $.attr(this, 'target');

		if ((($.attr(this, 'data-objeto')=="")||($.attr(this, 'data-acao')=="")))return false;
		//console.log('url:'+data_objeto+'/'+data_acao+'/'+data_cod);
        //console.log('validacao:'+data_objeto+'/'+data_validacao);
        

        //Ação - Salvar ou Inserir com validação
		if (data_acao=="inserir" || data_acao=="salvar"){
            if (data_validacao!="" && data_validacao!="undefined" && (data_validacao)){
                jQuery.ajax({
                    type: "POST",
                    url: data_objeto+'/'+data_validacao,
                    crossDomain: true,
                    data: new FormData( $("#frm")[0]),
                    dataType: 'json',
                    cache : false,
                    contentType : false,
                    processData : false,
                    success: function(data) {
                        //console.log(data)
                        if (data.op=="0"){
                            bootbox.alert(data.msg);
                            validacao=false;
                        }
                        if (data.op=="1"){
                            frm.action = data_objeto+'/'+data_acao+'/'+data_cod;
                            frm.submit();
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        bootbox.alert("Houve um erro na sua operação, por favor contate o suporte!<br>"+xhr.status+" " +thrownError);
                    }
                });
            } else {
                $alteracao=false;
                frm.action = data_objeto+'/'+data_acao+'/'+data_cod;
                frm.submit();
            }
        }
        
		//Ação - Caixa de diálogo
		if (data_acao=="info"){
			bootbox.alert(data_info);
            return false;
		}

		//Ação - Listar
		if (data_acao=="listar"){
			//$alteracao=false;
		}

		//Ação - Excluir
		if (data_acao=="excluir"){
			if (!confirm("Confirma a exclusão?")) return false;
            frm.action = data_objeto+'/'+data_acao+'/'+data_cod;
            frm.submit();
            return false;
		}

		if (data_acao!="inserir" && data_acao!="salvar" && data_acao!="excluir" ){
            
            //Ordenação pela coluna
            if (orderby_order=="ASC") $('#orderby_order').val("DESC");
            if (orderby_order=="DESC") $('#orderby_order').val("ASC");
            if ($('#orderby_column').val()!=data_orderbycolumn) $('#orderby_order').val("ASC");
            $('#orderby_column').val(data_orderbycolumn);

            //Envia form
            frm.action = data_objeto+'/'+data_acao+'/'+data_cod;
            frm.submit();
            return false;
        }
		
    });

    //END Funções

});
